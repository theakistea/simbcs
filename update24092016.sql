
alter table trs_choir add column has_surat_keterangan_usia_date VARCHAR (40);
alter table trs_choir add column has_bukti_registrasi_date VARCHAR (40);
alter table trs_choir add column has_audio_record_date VARCHAR (40);
alter table trs_choir add column has_biograpy_date VARCHAR (40);

CREATE TABLE `trs_dtl_choir_songs` (
	`id_trs_dtl_choir_song` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_trs_choir` Int( 11 ) NOT NULL,
	`id_song` Int( 11 ) NOT NULL,
	PRIMARY KEY ( `id_trs_dtl_choir_song` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 23;

