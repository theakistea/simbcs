<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class PackingToPost extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Mcountry');
		$this->load->model('Mdetail');
    }
  
    public function index() {
        $this->load->view('backend/packingtopost/view');
    }

	public function confirm () {
			$data['template']= $this->db->query("SELECT * FROM mst_letter where type in ('adconfirmation' , 'finalconfirmation') ")->result();
			$this->load->view('backend/packingtopost/confirmation', $data);
	}
  
    public function data_angularnya(){
        $dt=$this->db->query("SELECT * FROM packing_to_post INNER JOIN trs_choir_event
							  ON packing_to_post.id_trs_choir_event = trs_choir_event.id_trs_choir_event where status = 1
							  ")->result();
		
        $arr_data=array();
        $i=0;
        
        foreach($dt as $r){
            $arr_data[$i]['id_trs_choir_event']=$r->id_trs_choir_event;
            $arr_data[$i]['title']=$r->title;
            $arr_data[$i]['status_kirim']=$r->status_kirim;

			$dn=$this->db->query("SELECT COUNT(trs_nested_dtl_choir_event.id_member) num FROM trs_nested_dtl_choir_event INNER JOIN trs_choir_event ON trs_nested_dtl_choir_event.id_trs_choir_event = trs_choir_event.id_trs_choir_event WHERE trs_nested_dtl_choir_event.id_trs_choir_event='$r->id_trs_choir_event'")->row();

					$arr_data[$i]['num']=$dn->num;
				
            $i++;
        }
		
        echo json_encode($arr_data);
    }

	public function get_trs_choir_from_trs_choir_event($id , $page = 1){
		$this->db->select("choir.*");
		$this->db->from("trs_choir as choir");
		$this->db->join('(SELECT id_trs_choir , id_trs_choir_event FROM trs_dtl_choir_event_choir group by id_trs_choir , id_trs_choir_event) as nest_choid_event' , "nest_choid_event.id_trs_choir =  choir.id_trs_choir");
        $this->db->where(array('nest_choid_event.id_trs_choir_event' => $id));
		$this->db->where('
						((select count(*) from trs_dtl_choir_con where trs_dtl_choir_con.id_trs_choir = choir.id_trs_choir  ) > 0 
						or 
						(select count(*) from trs_dtl_choir_man where trs_dtl_choir_man.id_trs_choir = choir.id_trs_choir  ) > 0 )' );
		$this->db->where("(SELECT COUNT(*) from trs_dtl_choir_cat where trs_dtl_choir_cat.id_trs_choir = choir.id_trs_choir AND  trs_dtl_choir_cat.id_choir_category 
						IN (SELECT id_choir_category  from mst_choir_category where code_category != 'NOTYET') ) > 0 ");

						
		$this->db->group_by('choir.id_trs_choir');
		if($page >  1){
			$page  =  ($page * 10);
		}
    	$this->db->limit(10,$page -1 );
		
		$choirs = $this->db->get()->result_array();
		foreach ($choirs as $key => $value) {
			$choirs[$key]['recheiver']= [];
			$this->db->select('conductor.id_conductor as id , conductor.name as name , ("1") as type , "'.$value['id_trs_choir'].'" as choir_id');
			$this->db->from('mst_conductor as conductor');
			$this->db->join('trs_dtl_choir_con as choir_con' , "conductor.id_conductor =  choir_con.id_conductor");
			$this->db->join('(SELECT id_trs_choir , id_trs_choir_event FROM trs_dtl_choir_event_choir group by id_trs_choir , id_trs_choir_event ) as nest_choid_event' , "nest_choid_event.id_trs_choir =  choir_con.id_trs_choir");
			$this->db->where(array('nest_choid_event.id_trs_choir' => $value['id_trs_choir']));
			$this->db->group_by('conductor.id_conductor');
			$choirs[$key]['recheiver']= array_merge($choirs[$key]['recheiver'] , $this->db->get()->result_array());

			$this->db->select('manager.id_manager as id , manager.name as name , ("2") as type , "'.$value['id_trs_choir'].'" as choir_id' );
			$this->db->from('mst_manager as manager');
			$this->db->join('trs_dtl_choir_man as choir_man' , "manager.id_manager =  choir_man.id_manager");
			$this->db->join('(SELECT id_trs_choir , id_trs_choir_event FROM trs_dtl_choir_event_choir group by id_trs_choir , id_trs_choir_event ) as nest_choid_event' , "nest_choid_event.id_trs_choir =  choir_man.id_trs_choir");
			$this->db->where(array('nest_choid_event.id_trs_choir' =>  $value['id_trs_choir']));
			$this->db->group_by('manager.id_manager');
			$choirs[$key]['recheiver'] = array_merge($choirs[$key]['recheiver'] , $this->db->get()->result_array());
		}
		echo json_encode($choirs);
	}
	public function count_trs_choir_from_trs_choir_event($id) {
		$this->db->select("count(choir.id_trs_choir) as count");
		$this->db->from("trs_choir as choir");
		$this->db->join('(SELECT id_trs_choir , id_trs_choir_event FROM trs_dtl_choir_event_choir group by id_trs_choir , id_trs_choir_event) as nest_choid_event' , "nest_choid_event.id_trs_choir =  choir.id_trs_choir");
        $this->db->where(array('nest_choid_event.id_trs_choir_event' => $id));
		$this->db->where('
						((select count(*) from trs_dtl_choir_con where trs_dtl_choir_con.id_trs_choir = choir.id_trs_choir  ) > 0 
						or 
						(select count(*) from trs_dtl_choir_man where trs_dtl_choir_man.id_trs_choir = choir.id_trs_choir  ) > 0 )' );
		$this->db->where("(SELECT COUNT(*) from trs_dtl_choir_cat where trs_dtl_choir_cat.id_trs_choir = choir.id_trs_choir AND  trs_dtl_choir_cat.id_choir_category 
						IN (SELECT id_choir_category  from mst_choir_category where code_category != 'NOTYET') ) > 0 ");

		echo ($this->db->get()->result_array()[0]['count']);
	}

	public function getLetterParticipant($id){
        $this->load->model('LetterRecheiverModel');

        echo json_encode($this->LetterRecheiverModel->getParticipant($id  , '*') );

    }
	public function getLetterParticipantChoir($id){
        $this->load->model('LetterRecheiverModel');

        echo json_encode($this->LetterRecheiverModel->getParticipantByChoir($id  , '*') );

    }
    public function data_single($id_trs_choir_event){
       
        $data_dtl_choir_event = $this->db->query("SELECT * FROM trs_choir_event where trs_choir_event.id_trs_choir_event =$id_trs_choir_event")->result();
        
		echo json_encode($data_dtl_choir_event);
    }
	
	/*public function change_status(){
		$data = (array)json_decode(file_get_contents('php://input'));
		$id_dtl_choir = $data['id_dtl_choir'];

		foreach($data['status'] as $change){
			$this->db->where('id_dtl_choir', $change);
			$this->db->set('status',1);
			$this->db->update('dtl_choir'); 
		}
	}
*/
    public function add(){
//		$data['rows'] = $this->db->query("SELECT id_dtl_choir FROM dtl_choir WHERE SUBSTRING(id_dtl_choir,4,2)= SUBSTRING(date_added,6,2) ORDER BY id_dtl_choir DESC LIMIT 1")->row();
		$data['get_country'] = $this->Mcountry->get_country();
		$data['get_state'] = $this->Mcountry->get_state();
		$data['get_member'] = $this->Mdetail->get_member();
		$data['get_institution'] = $this->Mdetail->get_institution();
		$this->load->view('backend/packingtopost/add', $data);
    }
  
    public function insert_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		//Simpan data ke mysql
		$config=array(
			//$nmfile = date('dmyhis'),
			'upload_path' => './assets/uploads/img/', //lokasi gambar akan di simpan
			'allowed_types' => 'jpg|jpeg|png|gif', //ekstensi gambar yang boleh di unggah
			'max_size' => '2000', //batas maksimal ukuran gambar
			'max_width' => '6000', //batas maksimal lebar gambar
			'max_height' => '6000', //batas maksimal tinggi gambar
			'file_name' => url_title($data['photo']) //nama gambar
		   );
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config); //meng set config yang sudah di atur
			
			$datenow = date('m');
			$queryid = $this->db->query("SELECT id_dtl_choir FROM dtl_choir WHERE SUBSTRING(id_dtl_choir,4,2)= $datenow ORDER BY id_dtl_choir DESC LIMIT 1");
		
		
//		if( !$this->upload->do_upload($data['photo']))
// 		{
//			echo" <script>
//           			   alert('Failed to Upload!');
//       		          </script>";   
	//			redirect('Candj/add');
		//}
		//else{
				$val=array(
					'photo' => $this->upload->file_name,
					//'id_dtl_choir' => 'CJ-'.date('my').$queryid+1,
					'choir_name' => $data['choir_name'],
					'street_address' => $data['street_address'],
					'institution' => $data['institution'],
					'country' => $data['country'],
					'state' => $data['state'],
					'city' => $data['city'],
					'notes' => $data['notes'],
					'email' => $data['email'],
					'status' => 1
				);
		
			$this->db->insert('trs_choir', $val);
		//}
}
	public function insert_data_cache(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		//Simpan data ke mysql
				$val=array(
					'name' => $this->upload->file_name,
					//'id_dtl_choir' => 'CJ-'.date('my').$queryid+1,
					'name' => $data['category_name']
//					'id_choir_category' => $data['id_choir_category'],
			);
			$this->db->insert('cache_category', $val);
}
	public function edit($id_trs_choir_event)
	{
                $data['template']= $this->db->query("SELECT * FROM mst_letter where type in ('satu' , 'dua' , 'templatebali')")->result();
				$this->load->view('backend/packingtopost/edit', $data);
	}
	
	public function getAllMemOn($id_trs_choir_event=null){
		$result = $this->db->join('mst_member','mst_member.id_member=trs_nested_dtl_choir_event.id_member');
		$result = $this->db->get_where("trs_nested_dtl_choir_event",array('trs_nested_dtl_choir_event.id_trs_choir_event'=>$id_trs_choir_event));
        $result = $result->result_array();
		
        /*$result = $this->db->join('trs_nested_dtl_choir_event','trs_nested_dtl_choir_event.id_trs_choir_event=trs_choir_event.id_trs_choir_event');
        $result = $this->db->join('mst_member','mst_member.id_member=trs_dtl_choir_mem.id_member');
        $result = $this->db->get_where("id_trs_nested_dtl_choir_event",array('trs_nested_dtl_choir_event.id_trs_choir_event'=>$id_trs_choir_event));
        $result = $result->result_array();*/

        echo json_encode($result);
    }
	public function getAllMemChoirSelected($id_trs_choir_event){
		$selected =  $this->db->query("SELECT DISTINCT * FROM trs_nested_dtl_choir_event INNER JOIN mst_member ON mst_member.id_member = trs_nested_dtl_choir_event.id_member WHERE trs_nested_dtl_choir_event.id_trs_choir_event = '$id_trs_choir_event' GROUP BY trs_nested_dtl_choir_event.id_member")->result();
		echo json_encode($selected);
    }
	
	public function getdetailMem($id_trs_choir_event=0){

		$data = $this->db->query("SELECT DISTINCT * FROM trs_nested_dtl_choir_event INNER JOIN mst_member ON mst_member.id_member = trs_nested_dtl_choir_event.id_member INNER JOIN trs_choir ON trs_choir.id_trs_choir = trs_nested_dtl_choir_event.id_trs_choir INNER JOIN trs_dtl_choir_mem ON trs_dtl_choir_mem.id_member=mst_member.id_member WHERE trs_nested_dtl_choir_event.id_trs_choir_event = '$id_trs_choir_event' GROUP BY trs_nested_dtl_choir_event.id_member ORDER BY mst_member.name")->result_array();
		echo json_encode($data);
	}
	public function getdetailMem2($id_member=0){
		$data = $this->db->query("SELECT * FROM mst_member INNER JOIN trs_dtl_choir_mem ON trs_dtl_choir_mem.id_member=mst_member.id_member INNER JOIN trs_choir ON trs_choir.id_trs_choir=trs_dtl_choir_mem.id_trs_choir WHERE mst_member.id_member = '$id_member'")->result_array();
		echo json_encode($data);
	}
	
	public function download($id=null,$id_letter=null){


		$this->db->query("UPDATE packing_to_post SET status_kirim = 'SENT' WHERE id_trs_choir_event = '$id'  ");
		if(!empty($id)){
		    $participantType = $this->input->get('download');
            $participantType = explode('-' , $participantType);
            $choir_id = end($participantType);

			unset($participantType[count($participantType) -1 ]);


			$this->load->library('Pdf');
			
			$id_surat = $id_letter;
			$surat = $this->db->query("SELECT * FROM mst_letter WHERE id_letter='$id_surat'")->row();
            $event = $this->db->query("SELECT * ,  DATE_FORMAT(STR_TO_DATE(date_start, '%d-%m-%Y'),'%y') as date_start_formated FROM trs_choir_event WHERE id_trs_choir_event ='$id'")->row();



            $this->db->select("choir.* , detailCat.code_registrasi as code_registrasi , cat.name as categoryname , cat.type_prefix as type_prefix , cat.code_category as code_category");
            $this->db->join('trs_dtl_choir_cat as detailCat' , "detailCat.id_trs_choir = choir.id_trs_choir" , 'LEFT');
            $this->db->join('mst_choir_category cat' , "detailCat.id_choir_category = cat.id_choir_category" , 'LEFT');
            $this->db->join('(SELECT id_trs_choir , id_trs_choir_event FROM trs_dtl_choir_event_choir group by id_trs_choir , id_trs_choir_event ) as nest_choid_event' , "nest_choid_event.id_trs_choir =  choir.id_trs_choir");
        
            $this->db->from("trs_choir as choir");
            $this->db->group_by('choir.id_trs_choir');
            $this->db->where(array('nest_choid_event.id_trs_choir_event ' => $id , 'choir.id_trs_choir' => $choir_id));
            $choirs= $this->db->get()->result_array();

            $this->load->model('LetterRecheiverModel');

            foreach ($choirs as  $key => $choir){
                $choirs[$key]['participants'] = [];
                if(in_array( '2' , $participantType))
                    $choirs[$key]['participants'] =  array_merge($choirs[$key]['participants'] , $this->LetterRecheiverModel->getManager($choir['id_trs_choir'] ));

                if(in_array( '1' , $participantType))
                    $choirs[$key]['participants'] =  array_merge($choirs[$key]['participants'] , $this->LetterRecheiverModel->getConductor($choir['id_trs_choir'] ));

                if(in_array( '3' , $participantType))
                    $choirs[$key]['participants'] =  array_merge($choirs[$key]['participants'] ,$this->LetterRecheiverModel->getMember($choir['id_trs_choir'] ));

            }



            //foreach($query as $query){
			//	echo $query['name']."<br>";
			//}
			//exit;
			
			if(!empty($event)){
				$data['letter'] = $surat;
				$data['company'] = $this->db->get('mst_company_data')->result_array();
				$data['event'] = $event;
				$data['choirs'] = $choirs;
				$data['jumlah_hal'] = count($choirs);

//				$this->load->view('backend/letter/template/'.$surat->type , $data);
//				//$this->pdf->load_view('backend/letter/template/'.$query[0]['type'],$data);
//
//                $this->pdf->load_view('backend/letter/template/dua',$data)
				$this->pdf->load_view('backend/letter/template/'.$surat->type,$data);
				$this->pdf->set_paper('a4', 'potrait');
				$this->pdf->render();
				$this->pdf->stream($event->title.".pdf");
			}
		}
    }

	public function download_confirmationad($id=null,$id_letter=null){		
		$this->db->query("UPDATE packing_to_post SET status_kirim = 'SENT' WHERE id_trs_choir_event = '$id'  ");
		
		if(!empty($id)){
			$this->load->library('Pdf');
			
			$id_surat = $id_letter;
			$surat = $this->db->query("SELECT * FROM mst_letter WHERE id_letter='$id_surat'")->row();
			if($surat->type  == 'finalconfirmation'){
				$this->download_finalconfirmation($id , $id_letter);
				return ;
			}
			
			// $query = $this->db->query("SELECT * FROM trs_nested_dtl_choir_event
			// 						   INNER JOIN trs_choir_event ON trs_choir_event.id_trs_choir_event = trs_nested_dtl_choir_event.id_trs_choir_event
			// 						   INNER JOIN trs_choir ON trs_choir.id_trs_choir = trs_nested_dtl_choir_event.id_trs_choir
			// 						   INNER JOIN mst_member ON mst_member.id_member = trs_nested_dtl_choir_event.id_member
			// 						   WHERE trs_nested_dtl_choir_event.id_trs_choir_event='$id'  AND mst_member.address_status=1
			// ")->result_array();
			$query2 = $this->db->query("SELECT * ,  DATE_FORMAT(STR_TO_DATE(date_start, '%d-%m-%Y'),'%d%y') as date_start_formated FROM trs_choir_event WHERE id_trs_choir_event ='$id'")->row();
			// Get all conductors 
			$rechivers = [array('type' => '1' , 'id' => 7 , 'choir_id' => 1)];
			
			$rechivers = [];
			$request = $this->input->get('data');
			foreach(explode(',' , $request) as $key => $val){
				if($val){
					
					$separate = explode('-' , $val);
					array_push($rechivers , array('id'=> $separate[0] , 'type'=> $separate[1] , 'choir_id'=> $separate[2] ));

				}
			}
			foreach ($rechivers as  $rechiverKey => $rechiver) {
				
				if($rechiver['type'] == '1'){
					$this->db->select("conductor.*");
					$this->db->from("mst_conductor as conductor");
					$this->db->where("conductor.id_conductor" , $rechiver['id']);
					$rechivers[$rechiverKey]['data'] = $this->db->get()->row();
				}else{
					$this->db->select("manager.*");
					$this->db->from("mst_manager as manager");
					$this->db->where("manager.id_manager" , $rechiver['id']);
					$rechivers[$rechiverKey]['data']   = $this->db->get()->row();
				} 

				$choir = array();
				$this->db->select("choir.* , detailCat.code_registrasi as code_registrasi , cat.name as categoryname , cat.type_prefix as type_prefix , cat.code_category as code_category");
				$this->db->join('trs_dtl_choir_cat as detailCat' , "detailCat.id_trs_choir = choir.id_trs_choir");
				$this->db->join('mst_choir_category cat' , "detailCat.id_choir_category = cat.id_choir_category");
				$this->db->from("trs_choir as choir");
				$this->db->where(array('choir.id_trs_choir' =>  $rechiver['choir_id']));
				$choir['data'] = $this->db->get()->result();

				$this->db->select('conductor.*');
				$this->db->from('mst_conductor as conductor');
				$this->db->join('trs_dtl_choir_con as choir_con' , "conductor.id_conductor =  choir_con.id_conductor");
				$this->db->join('(SELECT id_trs_choir , id_trs_choir_event FROM trs_dtl_choir_event_choir group by id_trs_choir , id_trs_choir_event ) as nest_choid_event' , "nest_choid_event.id_trs_choir =  choir_con.id_trs_choir");
				$this->db->where(array('nest_choid_event.id_trs_choir' => $rechiver['choir_id']));
				$this->db->group_by('conductor.id_conductor');
				$choir['conductors']= $this->db->get()->result();

				$this->db->select('manager.*');
				$this->db->from('mst_manager as manager');
				$this->db->join('trs_dtl_choir_man as choir_man' , "manager.id_manager =  choir_man.id_manager");
				$this->db->join('(SELECT id_trs_choir , id_trs_choir_event FROM trs_dtl_choir_event_choir group by id_trs_choir , id_trs_choir_event ) as nest_choid_event' , "nest_choid_event.id_trs_choir =  choir_man.id_trs_choir");
				$this->db->where(array('nest_choid_event.id_trs_choir' =>  $rechiver['choir_id']));
				$this->db->group_by('manager.id_manager');
				$choir['managers'] = $this->db->get()->result();

				$this->db->select('songs.* , choir_songs.jumlah_partitur_diterima , choir_songs.surati_izin_komposer , choir_songs.durasi ,  choir_songs.persetujuan_artistic_commite ');
				$this->db->from('mst_songs as songs');
				$this->db->join('trs_dtl_choir_songs as choir_songs' , "songs.id_songs =  choir_songs.id_song");
				$this->db->join('(SELECT id_trs_choir , id_trs_choir_event FROM trs_dtl_choir_event_choir group by id_trs_choir , id_trs_choir_event ) as nest_choid_event' , "nest_choid_event.id_trs_choir =  choir_songs.id_trs_choir");
				$this->db->where(array('nest_choid_event.id_trs_choir' =>  $rechiver['choir_id']));
				$this->db->group_by('songs.id_songs');
				$choir['songs'] =$this->db->get()->result();

				$this->db->select('eventPackage.*');
				$this->db->from('pd_event_package_a as eventPackage');
				$this->db->where(array('eventPackage.id_trs_choir' =>  $rechiver['choir_id'] , 'eventPackage.id_trs_choir_event' => $id));
				$this->db->group_by('eventPackage.id_pd_event_package_a');
				$dataEvent = $this->db->get()->row();
				if(!$dataEvent) {
					$this->db->select('eventPackage.*');
					$this->db->from('pd_event_package_b as eventPackage');
					$this->db->where(array('eventPackage.id_trs_choir' =>  $rechiver['choir_id'] , 'eventPackage.id_trs_choir_event' => $id));
					$this->db->group_by('eventPackage.id_pd_event_package_b');
					$dataEvent = $this->db->get()->row();
				}
				$choir['eventPackage'] = $dataEvent;


				//TODO : Songs Dari maana

				


				$rechivers[$rechiverKey]['choir']  = $choir;

			}
			

			//foreach($query as $query){
			//	echo $query['name']."<br>";
			//}
			//exit;
			
			if(!empty($rechivers)){
				$data['letter'] = $surat;
				$data['company'] = $this->db->get('mst_company_data')->result_array();
				$data['event'] = $query2;
				// $data['event2'] = $query;
				$data['choirs'] = $rechivers;
				$data['isKonduktor'] = true;
				$data['jumlah_hal'] = count($rechivers);

				// $this->load->view('backend/letter/template/adconfirmation',$data);
				//$this->pdf->load_view('backend/letter/template/'.$query[0]['type'],$data);
				$this->pdf->load_view('backend/letter/template/'.$surat->type,$data);
				$this->pdf->set_paper('a4', 'potrait');
				$this->pdf->render();
				$this->pdf->stream($surat->template_name."-".$query2->title.".pdf");
			}
		}
    }


	public function download_finalconfirmation($id=null,$id_letter=null){		
		$this->db->query("UPDATE packing_to_post SET status_kirim = 'SENT' WHERE id_trs_choir_event = '$id'  ");
		if(!empty($id)){
			$this->load->library('Pdf');
			
			$id_surat = $id_letter;
			$surat = $this->db->query("SELECT * FROM mst_letter WHERE id_letter='$id_surat'")->row();
			
			// $query = $this->db->query("SELECT * FROM trs_nested_dtl_choir_event
			// 						   INNER JOIN trs_choir_event ON trs_choir_event.id_trs_choir_event = trs_nested_dtl_choir_event.id_trs_choir_event
			// 						   INNER JOIN trs_choir ON trs_choir.id_trs_choir = trs_nested_dtl_choir_event.id_trs_choir
			// 						   INNER JOIN mst_member ON mst_member.id_member = trs_nested_dtl_choir_event.id_member
			// 						   WHERE trs_nested_dtl_choir_event.id_trs_choir_event='$id'  AND mst_member.address_status=1
			// ")->result_array();
			$query2 = $this->db->query("SELECT * ,  DATE_FORMAT(STR_TO_DATE(date_start, '%d-%m-%Y'),'%d%y') as date_start_formated FROM trs_choir_event WHERE id_trs_choir_event ='$id'")->row();
			
			$this->db->select('vt.* ');
			$this->db->from('trs_dtl_choir_event_vt as vt');
			$this->db->where(['vt.id_trs_choir_event' => $id]);
			$venue = $this->db->get()->result_array();
			// Get all conductors 
			$rechivers = [array('type' => '1' , 'id' => 7 , 'choir_id' => 1)];
			
			$rechivers = [];
			$request = $this->input->get('data');
			foreach(explode(',' , $request) as $key => $val){
				if($val){
					
					$separate = explode('-' , $val);
					array_push($rechivers , array('id'=> $separate[0] , 'type'=> $separate[1] , 'choir_id'=> $separate[2] ));

				}
			}
			foreach ($rechivers as  $rechiverKey => $rechiver) {
				
				if($rechiver['type'] == '1'){
					$this->db->select("conductor.*");
					$this->db->from("mst_conductor as conductor");
					$this->db->where("conductor.id_conductor" , $rechiver['id']);
					$rechivers[$rechiverKey]['data'] = $this->db->get()->row();
				}else{
					$this->db->select("manager.*");
					$this->db->from("mst_manager as manager");
					$this->db->where("manager.id_manager" , $rechiver['id']);
					$rechivers[$rechiverKey]['data']   = $this->db->get()->row();
				} 

				$choir = array();
				$this->db->select("choir.* , detailCat.code_registrasi as code_registrasi , cat.name as categoryname , cat.type_prefix as type_prefix , cat.code_category as code_category");
				$this->db->join('trs_dtl_choir_cat as detailCat' , "detailCat.id_trs_choir = choir.id_trs_choir");
				$this->db->join('mst_choir_category cat' , "detailCat.id_choir_category = cat.id_choir_category");
				$this->db->from("trs_choir as choir");
				$this->db->where(array('choir.id_trs_choir' =>  $rechiver['choir_id']));
				$choir['data'] = $this->db->get()->result();

				$this->db->select('conductor.*');
				$this->db->from('mst_conductor as conductor');
				$this->db->join('trs_dtl_choir_con as choir_con' , "conductor.id_conductor =  choir_con.id_conductor");
				$this->db->join('(SELECT id_trs_choir , id_trs_choir_event FROM trs_dtl_choir_event_choir group by id_trs_choir , id_trs_choir_event ) as nest_choid_event' , "nest_choid_event.id_trs_choir =  choir_con.id_trs_choir");
				$this->db->where(array('nest_choid_event.id_trs_choir' => $rechiver['choir_id']));
				$this->db->group_by('conductor.id_conductor');
				$choir['conductors']= $this->db->get()->result();

				$this->db->select('manager.*');
				$this->db->from('mst_manager as manager');
				$this->db->join('trs_dtl_choir_man as choir_man' , "manager.id_manager =  choir_man.id_manager");
				$this->db->join('(SELECT id_trs_choir , id_trs_choir_event FROM trs_dtl_choir_event_choir group by id_trs_choir , id_trs_choir_event ) as nest_choid_event' , "nest_choid_event.id_trs_choir =  choir_man.id_trs_choir");
				$this->db->where(array('nest_choid_event.id_trs_choir' =>  $rechiver['choir_id']));
				$this->db->group_by('manager.id_manager');
				$choir['managers'] = $this->db->get()->result();

				$this->db->select('songs.* , choir_songs.jumlah_partitur_diterima , choir_songs.surati_izin_komposer , choir_songs.durasi ,  choir_songs.persetujuan_artistic_commite ');
				$this->db->from('mst_songs as songs');
				$this->db->join('trs_dtl_choir_songs as choir_songs' , "songs.id_songs =  choir_songs.id_song");
				$this->db->join('(SELECT id_trs_choir , id_trs_choir_event FROM trs_dtl_choir_event_choir group by id_trs_choir , id_trs_choir_event ) as nest_choid_event' , "nest_choid_event.id_trs_choir =  choir_songs.id_trs_choir");
				$this->db->where(array('nest_choid_event.id_trs_choir' =>  $rechiver['choir_id']));
				$this->db->group_by('songs.id_songs');
				$choir['songs'] =$this->db->get()->result();


				$this->db->select('eventPackage.*');
				$this->db->from('pd_event_package_a as eventPackage');
				$this->db->where(array('eventPackage.id_trs_choir' =>  $rechiver['choir_id'] , 'eventPackage.id_trs_choir_event' => $id));
				$this->db->group_by('eventPackage.id_pd_event_package_a');
				$dataEvent = $this->db->get()->row();
				if(!$dataEvent) {
					$this->db->select('eventPackage.*');
					$this->db->from('pd_event_package_b as eventPackage');
					$this->db->where(array('eventPackage.id_trs_choir' =>  $rechiver['choir_id'] , 'eventPackage.id_trs_choir_event' => $id));
					$this->db->group_by('eventPackage.id_pd_event_package_b');
					$dataEvent = $this->db->get()->row();
				}
				$choir['eventPackage'] = $dataEvent;

				$this->db->select('eventActivity1.*');
				$this->db->from('pd_event_activity_1 as eventActivity1');
				$this->db->where(array('eventActivity1.id_trs_choir' =>  $rechiver['choir_id'] , 'eventActivity1.id_trs_choir_event' => $id));
				$this->db->group_by('eventActivity1.id_pd_event_activity_1');
				$eventActivity= $this->db->get()->row();
				
				$choir['eventActivity'] = $eventActivity;


				//TODO : Songs Dari maana

				


				$rechivers[$rechiverKey]['choir']  = $choir;

			}
			

			//foreach($query as $query){
			//	echo $query['name']."<br>";
			//}
			//exit;
			
			if(!empty($rechivers)){
				$data['letter'] = $surat;
				$data['venue'] = $venue;
				$data['company'] = $this->db->get('mst_company_data')->result_array();
				$data['event'] = $query2;
				// $data['event2'] = $query;
				$data['choirs'] = $rechivers;
				$data['isKonduktor'] = true;
				$data['jumlah_hal'] = count($rechivers);

				// $this->load->view('backend/letter/template/finalconfirmation',$data);
				// return;
				// $this->pdf->load_view('backend/letter/template/finalconfirmation',$data);
				$this->pdf->load_view('backend/letter/template/'.$surat->type,$data);
				$this->pdf->set_paper('a4', 'potrait');
				$this->pdf->render();
				$this->pdf->stream($surat->template_name."-".$query2->title.".pdf");
			}
		}
    }
    public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));

        $id_dtl_choir = $data['id_dtl_choir'];

        //Simpan data ke mysql
        $val=array(
            'choir_name' => $data['choir_name'],
            //'gender' => $data['gender'],
            //'appellation' => $data['appellation'],
            //'first_title' => $data['first_title'],
            //'last_title' => $data['last_title'],
            'street_address' => $data['street_address'],
            'city' => $data['city'],
            'state' => $data['state'],
            'country' => $data['country'],
            'address_status' => $data['address_status'],
            //'website' => $data['website'],
            //'place_of_birth' => $data['place_of_birth'],
            'date_of_birth' => $data['date_of_birth'],
            //'photo' => $data['photo'],
            'notes' => $data['notes'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            //'social_media' => $data['social_media'],
            'status' => $data['status'],
            //'date_modify' => $data['date_modify']
        );

        $this->db->where('id_dtl_choir', $id_dtl_choir);
        $this->db->update('trs_choir', $val);

    }
	

}
?>