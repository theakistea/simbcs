<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class SongsCategory extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
    }
  
    public function index() {
        $this->load->view('backend/songscategory/view');
    }
  
    public function data_angularnya(){
        $dt=$this->db->get('mst_songs_category')->result();
        $arr_data=array();
        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['id_songs_category']=$r->id_songs_category;
            $arr_data[$i]['title_category']=$r->title_category;
            $arr_data[$i]['notes']=$r->notes;
            $arr_data[$i]['status']=$r->status;
            $i++;
        }
        echo json_encode($arr_data);
    }

    public function data_single($id_songs_category){
        $data = $this->db->query("SELECT * FROM mst_songs_category WHERE id_songs_category='$id_songs_category'")->result();
        echo json_encode($data);
    }

    public function add(){
		//$data['rows'] = $this->db->query("SELECT id_songs_category FROM mst_songs_category WHERE SUBSTRING(id_songs_category,4,2)= SUBSTRING(date_added,6,2) ORDER BY id_songs_category DESC LIMIT 1")->row();
		$this->load->view('backend/songscategory/add');
    }
	public function aktif(){
        $data = (array)json_decode(file_get_contents('php://input'));
        
        /*Convert Object to array*/
        foreach($data as $index => $value){
            // cek data
            $dataNa = $this->db->get_where('mst_songs_category', array('id_songs_category'=>$value))->result_array();
            // kalo ada
            if(!empty($dataNa)){
                $resData[$index] = $dataNa[0]['status']; 
                // Kalo statusnya aktif, ubah ke deaktif
                if($dataNa[0]['status']==1){
                    $query = $this->db->update('mst_songs_category', array('status'=>0), array('id_songs_category'=>$dataNa[0]['id_songs_category']));
                }else{
                    $query = $this->db->update('mst_songs_category', array('status'=>1), array('id_songs_category'=>$dataNa[0]['id_songs_category']));
                }
            }
        };

        if(!empty($resData)){
            print_r($resData);
        }
    }
  
    public function insert_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		//Simpan data ke mysql
		
        $val=array(
            //'id_songs_category' => 2,
            'title_category' => $data['title_category'],
            'notes' => $data['notes'],
            'status' => 1
        );
        $this->db->insert('mst_songs_category', $val);

    }
	
	public function edit($id_songs_category)
	{
		$data['rows'] = $this->db->query("SELECT * FROM mst_songs_category WHERE id_songs_category='$id_songs_category'")->row();
		$this->load->view('backend/songscategory/edit', $data);
	}

    public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));

         $id_songs_category = $data['id_songs_category'];

        //Simpan data ke mysql
        $val=array(
            'title_category' => $data['title_category'],
            'notes' => $data['notes'],
            'status' => $data['status']
        );

        $this->db->where('id_songs_category', $id_songs_category);
        $this->db->update('mst_songs_category', $val);

    }

}
?>