<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class User extends CI_Controller {
    function __construct() {
        parent::__construct();

		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Mcountry');
		$this->load->model('Mupload');
		$this->load->model('Muser');
    }
  
    public function index() {
		$data['rows'] = $this->db->query("SELECT * FROM user");
        $this->load->view('backend/user/view',$data);
    }
  
    public function data_angularnya(){
        $dt=$this->db->get('user')->result();
        $arr_data=array();
        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['idUser']=$r->idUser;
            $arr_data[$i]['username']=$r->username;
            $arr_data[$i]['firstName']=$r->firstName;
            $arr_data[$i]['lastName']=$r->lastName;
            $arr_data[$i]['email']=$r->email;
            $arr_data[$i]['active']=$r->active;
            $arr_data[$i]['created']=$r->created;
            $i++;
        }
        echo json_encode($arr_data);
    }
	function deleteUser($idUser){
		$this->db->where('idUser', $idUser);
		$this->db->delete('user');
		redirect('User');
	}

    public function data_single($idUser){
        $data = $this->db->query("SELECT * FROM user WHERE idUser='$idUser'")->result();
        echo json_encode($data);
    }
	
	/*public function change_status(){
		$data = (array)json_decode(file_get_contents('php://input'));
		$idUser = $data['idUser'];

		foreach($data['status'] as $change){
			$this->db->where('idUser', $change);
			$this->db->set('status',1);
			$this->db->update('user'); 
		}
	}
*/
    public function add(){
        $data['get_user_group'] = $this->Muser->getUserGroup();

		$this->load->view('backend/user/add', $data);
    }

    public function deletePhoto(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
        $file = './assets/uploads/img/user/'.$data['filename'];

        if (!unlink($file)){
            echo 0;
        }else{
            echo 1;
        }
    }

    public function pdf(){
        $this->load->library('pdf');
        $this->pdf->load_view('pdf/undangan');
        $this->pdf->render();
        $this->pdf->stream("adaw.pdf");
    }
    
    public function aktif(){
        $data = (array)json_decode(file_get_contents('php://input'));
        
        /*Convert Object to array*/
        foreach($data as $index => $value){
            // cek data
            $dataNa = $this->db->get_where('user', array('idUser'=>$value))->result_array();
            // kalo ada
            if(!empty($dataNa)){
                $resData[$index] = $dataNa[0]['active']; 
                // Kalo statusnya aktif, ubah ke deaktif
                if($dataNa[0]['active']==1){
                    $query = $this->db->update('user', array('active'=>0), array('idUser'=>$dataNa[0]['idUser']));
                }else{
                    $query = $this->db->update('user', array('active'=>1), array('idUser'=>$dataNa[0]['idUser']));
                }
            }
        };

        if(!empty($resData)){
            print_r($resData);
        }
    }

    public function insert_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		//Simpan data ke mysql
		
        $val=array(
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => $data['password'],
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'idUserGroup' => $data['idUserGroup'],
            'active' => 1
        );
    
        $this->db->insert('user', $val);			
    }
	
	public function edit($idUser)
	{  
		$data['get_user_group'] = $this->Muser->getUserGroup();
		$data['rows'] = $this->db->query("SELECT * FROM user INNER JOIN user_group ON user.idUserGroup=user_group.idUserGroup WHERE idUser='$idUser'")->row();
		$this->load->view('backend/user/edit', $data);
	}

    public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));

        $idUser = $data['idUser'];

        //Simpan data ke mysql
        $val=array(
            'username' => $data['username'],
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'password' => $data['password'],
            'idUserGroup' => $data['idUserGroup'],
            'email' => $data['email']
        );

        $this->db->where('idUser', $idUser);
        $this->db->update('user', $val);

    }

    public function getCountry(){
        $get_country = $this->Mcountry->getCountry();

        echo json_encode($get_country);
    }

    public function getState(){
        $get_state = $this->Mcountry->getState();

        echo json_encode($get_state);
    }        

}
?>