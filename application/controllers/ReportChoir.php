<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class ReportChoir extends CI_Controller {
    function __construct() {
        parent::__construct();

		$this->load->helper('url');
		$this->load->database();
    }
  
    public function index() {
        $this->load->view('backend/report/viewchoir');
    }
  
    public function data_angularnya(){
        $dt=$this->db->query("SELECT t.status,t.choir_name,t.institution,t.city,t.email,t.id_trs_choir, manager.name AS nama_manager, conductor.name AS nama_conductor FROM trs_choir AS t INNER JOIN trs_dtl_choir_cat AS detailCat ON detailCat.id_trs_choir = t.id_trs_choir INNER JOIN trs_dtl_choir_man AS detailMan ON detailMan.id_trs_choir = t.id_trs_choir INNER JOIN mst_manager AS manager ON manager.id_manager = detailMan.id_manager INNER JOIN trs_dtl_choir_con AS detailCon ON detailCon.id_trs_choir = t.id_trs_choir INNER JOIN mst_conductor AS conductor ON conductor.id_conductor = detailCon.id_conductor GROUP BY t.choir_name order by t.id_trs_choir
							  ")->result();
        $arr_data=array();
        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['id_trs_choir']=$r->id_trs_choir;
            $arr_data[$i]['id_trs_dtl_choir']=$r->id_trs_choir;
            $arr_data[$i]['choir_name']=$r->choir_name;
            $arr_data[$i]['city']=$r->city;
            $arr_data[$i]['nama_manager']=$r->nama_manager;
            $arr_data[$i]['nama_conductor']=$r->nama_conductor;
            $arr_data[$i]['institution']=$r->institution;
            $arr_data[$i]['email']=substr($r->email,0,20);
            $i++;
        }
        echo json_encode($arr_data);
    }
        

}
?>