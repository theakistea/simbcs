<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class ChoirEventDetail extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Mcountry');
		$this->load->model('Mdetail');
		$this->load->model('Mcategory');
    }
  
    public function index() {
        $this->load->view('backend/choireventdetail/view');
    }
    
    public function data_dropdown () {
        $this->db->select("choir_event.id_trs_choir_event , choir_event.title , choir_event.code_event ,  DATE_FORMAT(STR_TO_DATE(choir_event.date_start, '%d-%m-%Y'),'%y') as date_start");
        $this->db->from('trs_choir_event as choir_event');
        $this->db->where('choir_event.code_event IS NOT NULL and choir_event.status = 1' ) ;
        echo json_encode ($this->db->get()->result());
    }
  
  
    public function data_angularnya(){
        
        $dt=$this->db->query("SELECT *
                            FROM trs_choir_event
							  ")->result();
        $arr_data=array();
        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['id_trs_choir_event']=$r->id_trs_choir_event;
            $arr_data[$i]['title']=$r->title;
            $arr_data[$i]['date_start']=$r->date_start;
            $arr_data[$i]['date_finish']=$r->date_finish;
            $arr_data[$i]['city']=$r->city;
            $arr_data[$i]['host']=$r->host;
            $arr_data[$i]['PIC']=$r->PIC;
            $arr_data[$i]['status']=$r->status;
            $i++;
        }
        echo json_encode($arr_data);
    }

    public function data_single($id_trs_choir_event){
        $data = $this->db->query("SELECT * FROM trs_choir_event WHERE id_trs_choir_event='$id_trs_choir_event'")->result();
        echo json_encode($data);
    }
	
    public function add(){
//		$data['rows'] = $this->db->query("SELECT id_dtl_choir FROM dtl_choir WHERE SUBSTRING(id_dtl_choir,4,2)= SUBSTRING(date_added,6,2) ORDER BY id_dtl_choir DESC LIMIT 1")->row();
		$data['get_country'] = $this->Mcountry->get_country();
		$data['get_state'] = $this->Mcountry->get_state();
		$data['get_member'] = $this->Mdetail->get_member();
		$data['get_grade'] = $this->Mdetail->get_grade();
		$data['get_kategori'] = $this->Mcategory->get_choircategoryAktif();
		$this->load->view('backend/choireventdetail/add', $data);
    }
	public function deletePhoto(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
        $file = './assets/uploads/img/choireventdetail/'.$data['filename'];

        if (!unlink($file)){
            echo 0;
        }else{
            echo 1;
        }
    }


    public function upload(){
        $this->load->library('upload');

        // var_dump($_FILES['file']);
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/uploads/img/choireventdetail/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        // $config['max_width']  = '1288'; //lebar maksimum 1288 px
        // $config['max_height']  = '768'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
        $config['overwrite']  = 'true';

        $this->upload->initialize($config);

        // var_dump($this->upload->do_upload('file')); // do_upload pake nama parameter file di attribut name form

        // move_uploaded_file($_FILES['file']['tmp_name'],$config['upload_path'].$_FILES['file']['name']);

        if (!$this->upload->do_upload('file')){
            $error = array('error' => $this->upload->display_errors());
            // print_r($error);
            return false;
        }else{   
            $data = $this->upload->data();
            echo $data['file_name'];
        }
    }
  
    public function insert_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		
        $val=array(
            'photo' => $data['photo'],
            'title' => $data['title'],
            'grade' => $data['grade'],
            'code_event' => $data['code_event'],
            'host' => $data['host'],
            'country' => $data['country'],
            'state' => $data['state'],
            'city' => $data['city'],
            'host' => $data['host'],
            'PIC' => $data['PIC'],
            'date_start' => $data['date_start'],
            'date_finish' => $data['date_finish'],
            'notes' => $data['notes'],
            'status' => 1
        );
        $this->db->insert('trs_choir_event', $val);
        $last_id = $this->db->insert_id();

        // ini contoh inputna
        // isi table trs_choir_dtl_cat ada record id_trs_choir_dtl_cat, id_trs_choir & id_choir_category
        // looping insert banyak kategori
		for($i=0;$i<count($data['dataChoir']);$i++){

		 $eventmember = $data['dataChoir'][$i]->id2;
		 $this->db->query("INSERT INTO trs_nested_dtl_choir_event (id_member,id_trs_choir,id_trs_choir_event) SELECT id_member,id_trs_choir,'$last_id' as last_id FROM trs_dtl_choir_mem  WHERE id_trs_choir='$eventmember' ");
		}
		for($i=0;$i<count($data['dataVenue']);$i++){
         $datanya = array(
                 'id_trs_choir_event' => $last_id,
                 'kategori' => $data['dataVenue'][$i]->type,
                 'venue' => $data['dataVenue'][$i]->venue,
                 'purpose' => $data['dataVenue'][$i]->purpose
             );  
             $this->db->insert('trs_dtl_choir_event_vt',$datanya);
         }
		 for($i=0;$i<count($data['dataCandj']);$i++){
         $datanya = array(
                 'id_trs_choir_event' => $last_id,
                 'id_candj' => $data['dataCandj'][$i]->id
             );  
             $this->db->insert('trs_dtl_choir_event_candj',$datanya);
         }
		 for($i=0;$i<count($data['dataChoir']);$i++){
         $datanya = array(
                 'id_trs_choir_event' => $last_id,
                 'id_trs_choir' => $data['dataChoir'][$i]->id2
             );  
			 $this->db->insert('trs_dtl_choir_event_choir',$datanya);
         }
		 for($i=0;$i<count($data['dataCom']);$i++){
         $datanya = array(
                 'id_trs_choir_event' => $last_id,
                 'id_committee' => $data['dataCom'][$i]->id
             );  
			 $this->db->insert('trs_dtl_choir_event_committee',$datanya);
         }
		 for($i=0;$i<count($data['dataSponsor']);$i++){
         $datanya = array(
                 'id_trs_choir_event' => $last_id,
                 'sponsor' => $data['dataSponsor'][$i]->sponsor,
                 'sebagai' => $data['dataSponsor'][$i]->sponsor_as
             );  
			 $this->db->insert('trs_dtl_choir_event_sponsor',$datanya);
         }
		 $datapost = array(
				'id_trs_choir_event' => $last_id,
				'status_kirim' => 'NOT SENT'
		 );
		 $this->db->insert('packing_to_post',$datapost);
		 $datasms = array(
				'id_trs_choir_event' => $last_id,
				'status' => 'NOT SENT'
		 );
		 $this->db->insert('packing_to_sms',$datasms);
		 $dataemail = array(
				'id_trs_choir_event' => $last_id,
				'status' => 'NOT SENT'
		 );
		 $this->db->insert('packing_to_email',$dataemail);
         echo($this->db->insert_id());
    }
	public function getChoir($id_trs_choir_event=0){
		$data = $this->db->query("SELECT DISTINCT * FROM trs_nested_dtl_choir_event
                                                    INNER JOIN trs_choir_event ON
                                                    trs_choir_event.id_trs_choir_event = trs_nested_dtl_choir_event.id_trs_choir_event
													INNER JOIN trs_choir ON
													trs_choir.id_trs_choir = trs_nested_dtl_choir_event.id_trs_choir
                                                    WHERE trs_choir_event.id_trs_choir_event='$id_trs_choir_event'
													GROUP BY trs_nested_dtl_choir_event.id_trs_choir
                                                    ")->result_array();
		echo json_encode($data);
	}
	public function getCandj($id_trs_choir_event=0){
		$data = $this->db->query("SELECT * FROM trs_dtl_choir_event_candj
													INNER JOIN mst_candj ON
													mst_candj.id_candj = trs_dtl_choir_event_candj.id_candj
													INNER JOIN trs_choir_event ON
                                                    trs_choir_event.id_trs_choir_event = trs_dtl_choir_event_candj.id_trs_choir_event
                                                    WHERE trs_choir_event.id_trs_choir_event='$id_trs_choir_event'
                                                    ")->result_array();
		echo json_encode($data);
	}
	public function getComm($id_trs_choir_event=0){
		$data = $this->db->query("SELECT * FROM trs_dtl_choir_event_committee
													INNER JOIN mst_committee ON
													mst_committee.id_committee = trs_dtl_choir_event_committee.id_committee
													INNER JOIN trs_choir_event ON
                                                    trs_choir_event.id_trs_choir_event = trs_dtl_choir_event_committee.id_trs_choir_event
                                                    WHERE trs_choir_event.id_trs_choir_event='$id_trs_choir_event'
                                                    ")->result_array();
		echo json_encode($data);
	}
	
	public function getVenue($id_trs_choir_event=0){
		$data = $this->db->query("SELECT * FROM trs_dtl_choir_event_vt
                                                    INNER JOIN trs_choir_event ON
                                                    trs_choir_event.id_trs_choir_event = trs_dtl_choir_event_vt.id_trs_choir_event
													WHERE trs_choir_event.id_trs_choir_event='$id_trs_choir_event'
                                                    ")->result_array();
		echo json_encode($data);
	}
	public function getSponsor($id_trs_choir_event=0){
		$data = $this->db->query("SELECT * FROM trs_dtl_choir_event_sponsor
                                                    INNER JOIN trs_choir_event ON
                                                    trs_choir_event.id_trs_choir_event = trs_dtl_choir_event_sponsor.id_trs_choir_event
													WHERE trs_choir_event.id_trs_choir_event='$id_trs_choir_event'
                                                    ")->result_array();
		echo json_encode($data);
	}
	
	public function edit($id_trs_choir_event)
	{
		$data['get_grade'] = $this->Mdetail->get_grade();
		$data['get_kategori'] = $this->Mcategory->get_choircategoryAktif();
		$data['get_country'] = $this->Mcountry->get_country();
        $data['get_state'] = $this->Mcountry->get_state();
		$data['rows'] = $this->db->query("SELECT * FROM trs_choir_event WHERE id_trs_choir_event='$id_trs_choir_event'")->row();
		$this->load->view('backend/choireventdetail/edit', $data);
	}
	public function getAllCandjOn($id_trs_choir_event=null){
        $data = $this->db->query("SELECT * FROM  mst_candj
								INNER JOIN trs_dtl_choir_event_candj ON trs_dtl_choir_event_candj.id_candj = mst_candj.id_candj
								WHERE trs_dtl_choir_event_candj.id_trs_choir_event='$id_trs_choir_event'")->result();
        echo json_encode($data);
    }

    public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));

        $id_trs_choir_event = $data['id_trs_choir_event'];

        //Simpan data ke mysql
        $val=array(
            'title' => $data['title'],
            'grade' => $data['grade'],
            'code_event' => $data['code_event'],
            'date_start' => $data['date_start'],
            'date_finish' => $data['date_finish'],
            'country' => $data['country'],
            'city' => $data['city'],
            'host' => $data['host'],
            'PIC' => $data['PIC'],
            'state' => $data['state'],
            'photo' => $data['photo'],
            'notes' => $data['notes'],
        );

        $this->db->where('id_trs_choir_event', $id_trs_choir_event);
        $this->db->update('trs_choir_event', $val);
		//CHOIR
		// delete dulu, trrus insert
		$this->db->where('id_trs_choir_event', $id_trs_choir_event);
		$this->db->delete('trs_dtl_choir_event_choir'); 
		$this->db->where('id_trs_choir_event', $id_trs_choir_event);
		$this->db->delete('trs_nested_dtl_choir_event'); 
        // ini contoh inputna
        // looping insert
         for($i=0;$i<count($data['dataChoir']);$i++){
		 $eventmember = $data['dataChoir'][$i]->id;
		 $eventmember2 = $data['dataChoir'][$i]->id2;
		 $this->db->query("INSERT INTO trs_nested_dtl_choir_event (id_member,id_trs_choir,id_trs_choir_event) SELECT id_member,id_trs_choir,'$id_trs_choir_event' FROM trs_dtl_choir_mem  WHERE id_trs_choir='$eventmember2' ");
                 $this->db->query("INSERT INTO trs_dtl_choir_event_choir(id_trs_choir_event,id_trs_choir) VALUES('$id_trs_choir_event','$eventmember2')");
		}
		//VENUE
		$this->db->where('id_trs_choir_event', $id_trs_choir_event);
		$this->db->delete('trs_dtl_choir_event_vt'); 
		for($i=0;$i<count($data['dataVenue']);$i++){
         $datanya = array(
                 'id_trs_choir_event' => $id_trs_choir_event,
                 'kategori' => $data['dataVenue'][$i]->type,
                 'venue' => $data['dataVenue'][$i]->venue,
                 'purpose' => $data['dataVenue'][$i]->purpose
             );  
             $this->db->insert('trs_dtl_choir_event_vt',$datanya);
         }
		 //CANDJ
		 $this->db->where('id_trs_choir_event', $id_trs_choir_event);
		$this->db->delete('trs_dtl_choir_event_candj'); 
		 for($i=0;$i<count($data['dataCandj']);$i++){
         $datanya = array(
                 'id_trs_choir_event' => $id_trs_choir_event,
                 'id_candj' => $data['dataCandj'][$i]->id
             );  
             $this->db->insert('trs_dtl_choir_event_candj',$datanya);
         }
		 //COMMITTEE
		 $this->db->where('id_trs_choir_event', $id_trs_choir_event);
		$this->db->delete('trs_dtl_choir_event_committee');
		 for($i=0;$i<count($data['dataCom']);$i++){
         $datanya = array(
                 'id_trs_choir_event' => $id_trs_choir_event,
                 'id_committee' => $data['dataCom'][$i]->id
             );  
			 $this->db->insert('trs_dtl_choir_event_committee',$datanya);
         }
		 //SPONSOR
		 $this->db->where('id_trs_choir_event', $id_trs_choir_event);
		$this->db->delete('trs_dtl_choir_event_sponsor'); 
		 for($i=0;$i<count($data['dataSponsor']);$i++){
         $datanya = array(
                 'id_trs_choir_event' => $id_trs_choir_event,
                 'sponsor' => $data['dataSponsor'][$i]->sponsor,
                 'sebagai' => $data['dataSponsor'][$i]->sponsor_as
             );  
			 $this->db->insert('trs_dtl_choir_event_sponsor',$datanya);
         }
    }
	public function getOneCandj($id=0){
        $res = $this->db->get_where('mst_candj',array('id_candj'=> $id))->result_array();
        echo json_encode($res);
    }
	public function getOneCom($id=0){
        $res = $this->db->get_where('mst_committee',array('id_committee'=> $id))->result_array();
        echo json_encode($res);
    }
	public function getOneChoir($id=0){
        $res = $this->db->query("SELECT * FROM trs_choir WHERE id_trs_choir='$id'")->result_array();
        echo json_encode($res);
    }
	public function getAllCandjChoirEvent(){
       echo json_encode($this->Mdetail->get_candjAktif());
    }
	public function getAllChoirChoirEvent(){
       echo json_encode($this->Mdetail->get_choirAktif());
    }
	public function getAllComChoirEvent(){
       echo json_encode($this->Mdetail->get_comAktif());
    }

    public function aktif(){
        $data = (array)json_decode(file_get_contents('php://input'));
        
        /*Convert Object to array*/
        foreach($data as $index => $value){
            // cek data
            $dataNa = $this->db->get_where('trs_choir_event', array('id_trs_choir_event'=>$value))->result_array();
            // kalo ada
            if(!empty($dataNa)){
                $resData[$index] = $dataNa[0]['status']; 
                // Kalo statusnya aktif, ubah ke deaktif
                if($dataNa[0]['status']==1){
                    $query = $this->db->update('trs_choir_event', array('status'=>0), array('id_trs_choir_event'=>$dataNa[0]['id_trs_choir_event']));
                }else{
                    $query = $this->db->update('trs_choir_event', array('status'=>1), array('id_trs_choir_event'=>$dataNa[0]['id_trs_choir_event']));
                }

                $this->db->where("id_trs_choir in (SELECT id_trs_choir from trs_dtl_choir_event_choir where id_trs_choir_event = '" . $dataNa[0]['id_trs_choir_event'] . "' )");
                $this->db->update('trs_choir', array('status' => $dataNa[0]['status'] == 1 ? 0 : 1 ));
            }
        };

        if(!empty($resData)){
            print_r($resData);
        }
    }


}
?>