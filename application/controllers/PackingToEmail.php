<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class PackingToEmail extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Mcountry');
		$this->load->model('Mdetail');
    }
  
    public function index() {
        $this->load->view('backend/packingtoemail/view');
    }
    public function edit($id_trs_choir_event)
	{
        $data['template']= $this->db->query("SELECT * FROM mst_letter")->result();
		
		$this->load->view('backend/packingtoemail/edit', $data);
	}
	
	public function getAllMemOn($id_trs_choir_event=null){
		$result = $this->db->join('mst_member','mst_member.id_member=trs_nested_dtl_choir_event.id_member');
		$result = $this->db->get_where("trs_nested_dtl_choir_event",array('trs_nested_dtl_choir_event.id_trs_choir_event'=>$id_trs_choir_event));
        $result = $result->result_array();
		
        /*$result = $this->db->join('trs_nested_dtl_choir_event','trs_nested_dtl_choir_event.id_trs_choir_event=trs_choir_event.id_trs_choir_event');
        $result = $this->db->join('mst_member','mst_member.id_member=trs_dtl_choir_mem.id_member');
        $result = $this->db->get_where("id_trs_nested_dtl_choir_event",array('trs_nested_dtl_choir_event.id_trs_choir_event'=>$id_trs_choir_event));
        $result = $result->result_array();*/

        echo json_encode($result);
    }

    public function data_single($id_trs_choir_event){
       
        $data_dtl_choir_event = $this->db->query("SELECT * FROM trs_choir_event where trs_choir_event.id_trs_choir_event =$id_trs_choir_event")->result();
        
		echo json_encode($data_dtl_choir_event);
    }



    public function send_to_email ($id_trs_choir_event , $kategori){
		$this->load->config('email');

		$this->load->library('email');

		$this->load->library('Pdf');
		
		$id_surat = $kategori;
		$surat = $this->db->query("SELECT * FROM mst_letter WHERE id_letter='$id_surat'")->row();
		
		$query = $this->db->query("SELECT * FROM trs_nested_dtl_choir_event
									INNER JOIN trs_choir_event ON trs_choir_event.id_trs_choir_event = trs_nested_dtl_choir_event.id_trs_choir_event
									INNER JOIN trs_choir ON trs_choir.id_trs_choir = trs_nested_dtl_choir_event.id_trs_choir
									INNER JOIN mst_member ON mst_member.id_member = trs_nested_dtl_choir_event.id_member
									WHERE trs_nested_dtl_choir_event.id_trs_choir_event='$id_trs_choir_event'  AND mst_member.address_status=1
		")->result_array();
		$query2 = $this->db->query("SELECT * FROM trs_nested_dtl_choir_event
									INNER JOIN trs_choir_event ON trs_choir_event.id_trs_choir_event = trs_nested_dtl_choir_event.id_trs_choir_event
									INNER JOIN trs_choir ON trs_choir.id_trs_choir = trs_nested_dtl_choir_event.id_trs_choir
									INNER JOIN mst_member ON mst_member.id_member = trs_nested_dtl_choir_event.id_member
									WHERE trs_nested_dtl_choir_event.id_trs_choir_event='$id_trs_choir_event' AND mst_member.address_status=1
		")->row();
		//foreach($query as $query){
		//	echo $query['name']."<br>";
		//}
		//exit;
		
		$data['letter'] = $surat;
		$data['company'] = $this->db->get('mst_company_data')->result_array();
		$data['event'] = $query2;
		$data['jumlah_hal'] = count($query);

		foreach ($query as $key => $value) {
			if($value['email'] && $value['email'] != ''){
				$data['event2'] = $value;
				$data['key'] = $key + 1;
			
				$view = $this->load->view('backend/letter/template/dua_single',$data , true);

				$subject = 'Surat  ' . $value['choir_name'] . date('/m/Y/') . substr($query2->title,0,3) .'/'.  $key;
				$message = $view;
				
				// Get full html:
				$body =
					'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
						<meta http-equiv="Content-Type" content="text/html; charset='.strtolower(config_item('charset')).'" />
						<title>'.html_escape($subject).'</title>
						<style type="text/css">
							body {
								font-family: Arial, Verdana, Helvetica, sans-serif;
								font-size: 16px;
							}
						</style>
					</head>
					<body>
					'.$message.'
					</body>
					</html>';
				// Also, for getting full html you may use the following internal method:
				//$body = $this->email->full_html($subject, $message);

				$result = $this->email
					->from($this->config->item('smtp_user'))
					->to($value['email'])
					->subject($subject)
					->message($body)
					->send();

			}
			echo(json_encode(array('status' => 'success')));
			
		}
		
    }
}
?>