<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class ReportMember extends CI_Controller {
    function __construct() {
        parent::__construct();

		$this->load->helper('url');
		$this->load->database();
    }
  
    public function index() {
        $this->load->view('backend/report/viewmember');
    }
  
    public function data_angularnya(){
        $dt=$this->db->query("SELECT * FROM mst_member
							  ")->result();
        $arr_data=array();
        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['id_member']=$r->id_member;
            $arr_data[$i]['name']=$r->name;
            $arr_data[$i]['phone']=$r->phone;
            $arr_data[$i]['city']=$r->city;
            $arr_data[$i]['email']=substr($r->email,0,20);
            $i++;
        }
        echo json_encode($arr_data);
    }
        

}
?>