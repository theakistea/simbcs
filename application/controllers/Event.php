<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class Event extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Mcountry');
    }
  
    public function index() {
        $this->load->view('backend/event/view');
    }
  
    public function data_angularnya(){
        $dt=$this->db->get('mst_event_grade')->result();
        $arr_data=array();
        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['id_event_grade']=$r->id_event_grade;
            $arr_data[$i]['name']=$r->name;
            $arr_data[$i]['notes']=$r->notes;
            $arr_data[$i]['status']=$r->status;
            $i++;
        }
        echo json_encode($arr_data);
    }

    public function data_single($id_event_grade){
        $data = $this->db->query("SELECT * FROM mst_event_grade WHERE id_event_grade='$id_event_grade'")->result();
        echo json_encode($data);
    }

	public function aktif(){
        $data = (array)json_decode(file_get_contents('php://input'));
        
        /*Convert Object to array*/
        foreach($data as $index => $value){
            // cek data
            $dataNa = $this->db->get_where('mst_event_grade', array('id_event_grade'=>$value))->result_array();
            // kalo ada
            if(!empty($dataNa)){
                $resData[$index] = $dataNa[0]['status']; 
                // Kalo statusnya aktif, ubah ke deaktif
                if($dataNa[0]['status']==1){
                    $query = $this->db->update('mst_event_grade', array('status'=>0), array('id_event_grade'=>$dataNa[0]['id_event_grade']));
                }else{
                    $query = $this->db->update('mst_event_grade', array('status'=>1), array('id_event_grade'=>$dataNa[0]['id_event_grade']));
                }
            }
        };

        if(!empty($resData)){
            print_r($resData);
        }
    }
    public function add(){
		//$data['rows'] = $this->db->query("SELECT id_event_grade FROM mst_event_grade WHERE SUBSTRING(id_event_grade,4,2)= SUBSTRING(date_added,6,2) ORDER BY id_event_grade DESC LIMIT 1")->row();
		$this->load->view('backend/event/add');
    }
  
    public function insert_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		//Simpan data ke mysql
		
        $val=array(
            //'id_event_grade' => 2,
            'name' => $data['name'],
            'notes' => $data['notes'],
            'status' => 1
        );
        $this->db->insert('mst_event_grade', $val);

    }
	
	public function edit($id_event_grade)
	{
		$data['rows'] = $this->db->query("SELECT * FROM mst_event_grade WHERE id_event_grade='$id_event_grade'")->row();
		$this->load->view('backend/event/edit', $data);
	}

    public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));

         $id_event_grade = $data['id_event_grade'];

        //Simpan data ke mysql
        $val=array(
            'name' => $data['name'],
            'notes' => $data['notes'],
            'status' => $data['status']
        );

        $this->db->where('id_event_grade', $id_event_grade);
        $this->db->update('mst_event_grade', $val);

    }

}
?>