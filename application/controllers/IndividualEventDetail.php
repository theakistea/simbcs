<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class IndividualEventDetail extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Mcountry');
		$this->load->model('Mdetail');
    }
  
    public function index() {
        $this->load->view('backend/individualeventdetail/view');
    }
  
    public function data_angularnya(){
        $dt=$this->db->query("SELECT *
                            FROM trs_individual_event
							  ")->result();
        $arr_data=array();
        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['id_trs_individual_event']=$r->id_trs_individual_event;
            $arr_data[$i]['title']=$r->title;
            $arr_data[$i]['date_start']=$r->date_start;
            $arr_data[$i]['date_finish']=$r->date_finish;
            $arr_data[$i]['city']=$r->city;
            $arr_data[$i]['host']=$r->host;
            $arr_data[$i]['PIC']=$r->PIC;
            $i++;
        }
        echo json_encode($arr_data);
    }

    public function data_single($id_dtl_choir){
        $data = $this->db->query("SELECT * FROM dtl_choir WHERE id_dtl_choir='$id_dtl_choir'")->result();
        echo json_encode($data);
    }
	
	/*public function change_status(){
		$data = (array)json_decode(file_get_contents('php://input'));
		$id_dtl_choir = $data['id_dtl_choir'];

		foreach($data['status'] as $change){
			$this->db->where('id_dtl_choir', $change);
			$this->db->set('status',1);
			$this->db->update('dtl_choir'); 
		}
	}
*/
    public function add(){
//		$data['rows'] = $this->db->query("SELECT id_dtl_choir FROM dtl_choir WHERE SUBSTRING(id_dtl_choir,4,2)= SUBSTRING(date_added,6,2) ORDER BY id_dtl_choir DESC LIMIT 1")->row();
		$data['get_country'] = $this->Mcountry->get_country();
		$data['get_state'] = $this->Mcountry->get_state();
		$data['get_member'] = $this->Mdetail->get_member();
		$data['get_institution'] = $this->Mdetail->get_institution();
		$this->load->view('backend/individualeventdetail/add', $data);
    }
  
    public function insert_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		//Simpan data ke mysql
		$config=array(
			//$nmfile = date('dmyhis'),
			'upload_path' => './assets/uploads/img/', //lokasi gambar akan di simpan
			'allowed_types' => 'jpg|jpeg|png|gif', //ekstensi gambar yang boleh di unggah
			'max_size' => '2000', //batas maksimal ukuran gambar
			'max_width' => '6000', //batas maksimal lebar gambar
			'max_height' => '6000', //batas maksimal tinggi gambar
			'file_name' => url_title($data['photo']) //nama gambar
		   );
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config); //meng set config yang sudah di atur
			
			$datenow = date('m');
			$queryid = $this->db->query("SELECT id_dtl_choir FROM dtl_choir WHERE SUBSTRING(id_dtl_choir,4,2)= $datenow ORDER BY id_dtl_choir DESC LIMIT 1");
		
		
//		if( !$this->upload->do_upload($data['photo']))
// 		{
//			echo" <script>
//           			   alert('Failed to Upload!');
//       		          </script>";   
	//			redirect('Candj/add');
		//}
		//else{
				$val=array(
					'photo' => $this->upload->file_name,
					//'id_dtl_choir' => 'CJ-'.date('my').$queryid+1,
					'choir_name' => $data['choir_name'],
					'street_address' => $data['street_address'],
					'institution' => $data['institution'],
					'country' => $data['country'],
					'state' => $data['state'],
					'city' => $data['city'],
					'notes' => $data['notes'],
					'email' => $data['email'],
					'status' => 1
				);
		
			$this->db->insert('trs_choir', $val);
		//}
}
	
	public function edit($id_trs_choir)
	{
		$data['rows'] = $this->db->query("SELECT * FROM trs_choir WHERE id_trs_choir='$id_trs_choir'")->row();
		$this->load->view('backend/individualeventdetail/edit', $data);
	}

    public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));

        $id_dtl_choir = $data['id_dtl_choir'];

        //Simpan data ke mysql
        $val=array(
            'choir_name' => $data['choir_name'],
            //'gender' => $data['gender'],
            //'appellation' => $data['appellation'],
            //'first_title' => $data['first_title'],
            //'last_title' => $data['last_title'],
            'street_address' => $data['street_address'],
            'city' => $data['city'],
            'state' => $data['state'],
            'country' => $data['country'],
            'address_status' => $data['address_status'],
            //'website' => $data['website'],
            //'place_of_birth' => $data['place_of_birth'],
            'date_of_birth' => $data['date_of_birth'],
            //'photo' => $data['photo'],
            'notes' => $data['notes'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            //'social_media' => $data['social_media'],
            'status' => $data['status'],
            //'date_modify' => $data['date_modify']
        );

        $this->db->where('id_dtl_choir', $id_dtl_choir);
        $this->db->update('trs_choir', $val);

    }
	
	public function getOneCandj($id=0){
        $res = $this->db->get_where('mst_candj',array('id_candj'=> $id))->result_array();
        echo json_encode($res);
    }
	public function getOneCom($id=0){
        $res = $this->db->get_where('mst_committee',array('id_committee'=> $id))->result_array();
        echo json_encode($res);
    }
	public function getOneChoir($id=0){
        $res = $this->db->get_where('trs_choir',array('id_trs_choir'=> $id))->result_array();
        echo json_encode($res);
    }
	public function getAllCandjChoirEvent(){
       echo json_encode($this->Mdetail->get_candjAktif());
    }
	public function getAllChoirChoirEvent(){
       echo json_encode($this->Mdetail->get_choirAktif());
    }
	public function getAllComChoirEvent(){
       echo json_encode($this->Mdetail->get_comAktif());
    }

}
?>