<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//session_start();
class Admin extends CI_Controller {
function __construct(){
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('Auth');
		}
		$this->load->helper('text');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('layout');
	}
	
	public function logout(){
		$array_items = array('username' => '', 'firstName' => '', 'lastName' => '', 'email' => '');
		$this->session->unset_userdata($array_items);
		$this->session->sess_destroy();
		redirect('Auth');
	}

	public function app()
	{
		$this->load->view('app');
	}

	public function dashboard()
	{
		$rm = $this->db->query("SELECT count(status) jml_member FROM mst_member WHERE status=1")->row();
        $rc = $this->db->query("SELECT count(status) jml_choir FROM trs_choir WHERE status=1")->row();
        $re = $this->db->query("SELECT count(status) jml_event FROM trs_choir_event WHERE status=1")->row();
                
        $data['jml_member'] = $rm->jml_member;
        $data['jml_choir'] = $rc->jml_choir;
        $data['jml_event'] = $re->jml_event;

		$this->load->view('tpl/dashboard', $data);
	}

	public function header()
	{
		$this->load->view('tpl/header');
	}

	public function sidebar()
	{
		$this->load->view('tpl/sidebar');
	}

	public function todo_widget()
	{
		$this->load->view('tpl/todo_widget');
	}

	public function menu_item()
	{
		$this->load->view('tpl/menu_item');
	}

	public function menu_group()
	{
		$this->load->view('tpl/menu_group');
	}

	//page

	//end page

	
}
