<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class Letter extends CI_Controller {
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-04-03 21:29:53
	**/

	function __construct() {
		parent::__construct();
		$this->load->helper('url'); //You should autoload this one ;)
				
	}
	

	public function index(){
		$this->load->view('backend/letter/view');
	}

	public function data_angularnya(){
		$query = $this->db->get('mst_letter')->result_array();
		echo json_encode($query);
    }
	

	public function view($id=null){
		if(!empty($id)){

			$query = $this->db->join('trs_choir', 'trs_choir.id_trs_choir = mst_letter.id_trs_choir');
			$query = $this->db->get_where('mst_letter',array('id_letter'=>$id))->result_array();

			if(!empty($query)){
				$data['letter'] = $query[0];
				$data['company'] = $this->db->get('mst_company_data')->result_array();
				$data['jumlah_hal'] = 2;

				$this->load->view('backend/letter/template/'.$query[0]['type'],$data);
			}
		}
	}    

	public function add(){
	$this->load->helper('url'); 
    $this->load->helper('ckeditor');
    $data['ckeditor'] = array(
                    //ID of the textarea that will be replaced
                   'id'     =>     'konten',
                   'path'    =>    'assets/js/ckeditor',
                           //Optionnal values
                   'config' => array(
                               'toolbar'     =>     "Full",     //Using the Full toolbar
                               'width'     =>     "700px",    //Setting a custom width
                               'height'     =>     '300px',    //Setting a custom height
                                      ),
                           //Replacing styles from the "Styles tool"
                   'styles' => array(
                            //Creating a new style named "style 1"
                       'style 1' => array (
                          'name'         =>     'Blue Title',
                          'element'     =>     'h2',
                          'styles' => array(
                               'color'             =>     'Blue',
                               'font-weight'         =>     'bold'
                           )
                       ),
                            //Creating a new style named "style 2"
     
      
                      'style 2' => array (
                          'name'         =>     'Red Title',
                          'element'     =>     'h2',
                          'styles' => array(
                              'color'             =>     'Red',
                              'font-weight'         =>     'bold',
                              'text-decoration'    =>     'underline'
                          )
                       )                
                                  )
              );
		$this->load->view('backend/letter/add', $data);
	}

	public function insert_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		//Simpan data ke mysql
		
        $val=array(
            'template_name' => $data['template_name'],
            'konten' => $data['konten'],
            'type'	=> $data['type']
        );
        $this->db->insert('mst_letter', $val);
    }

    public function data_single($id_letter){
        $data = $this->db->query("SELECT * FROM mst_letter WHERE id_letter='$id_letter'")->result();
        echo json_encode($data);
    }

    public function edit($id_letter){
	$this->load->helper('url'); 
    $this->load->helper('ckeditor');
    $data['ckeditor'] = array(
                    //ID of the textarea that will be replaced
                   'id'     =>     'konten',
                   'path'    =>    'assets/js/ckeditor',
                           //Optionnal values
                   'config' => array(
                               'toolbar'     =>     "Full",     //Using the Full toolbar
                               'width'     =>     "700px",    //Setting a custom width
                               'height'     =>     '300px',    //Setting a custom height
                                      ),
                           //Replacing styles from the "Styles tool"
                   'styles' => array(
                            //Creating a new style named "style 1"
                       'style 1' => array (
                          'name'         =>     'Blue Title',
                          'element'     =>     'h2',
                          'styles' => array(
                               'color'             =>     'Blue',
                               'font-weight'         =>     'bold'
                           )
                       ),
                            //Creating a new style named "style 2"
     
      
                      'style 2' => array (
                          'name'         =>     'Red Title',
                          'element'     =>     'h2',
                          'styles' => array(
                              'color'             =>     'Red',
                              'font-weight'         =>     'bold',
                              'text-decoration'    =>     'underline'
                          )
                       )                
                                  )
              );
		$data['rows'] = $this->db->query("SELECT * FROM mst_letter WHERE id_letter='$id_letter'")->result_array();
		$this->load->view('backend/letter/edit',$data);
	}

	public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));

        $id_letter = $data['id_letter'];

        //Simpan data ke mysql
        $val=array(
            'template_name' => $data['template_name'],
            'konten' => $data['konten'],
            'type'	=> $data['type']
        );

        $this->db->where('id_letter', $id_letter);
        $this->db->update('mst_letter', $val);

    }

}