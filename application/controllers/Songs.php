<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class Songs extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Mcategory');
    }
  
    public function index() {
        $this->load->view('backend/songs/view');
    }
  
    public function data_angularnya(){
        //$this->db->JOIN('mst_songs_category','mst_songs.id_songs_category = mst_songs_category.id_songs_category');
		$dt=$this->db->query("SELECT id_songs,title,composer,type,title_category,publisher,mst_songs.status, mst_songs.notes
							FROM mst_songs LEFT JOIN mst_songs_category ON 
							mst_songs.id_songs_category = mst_songs_category.id_songs_category")->result();
        $arr_data=array();
        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['id_songs']=$r->id_songs;
            $arr_data[$i]['title']=$r->title;
            $arr_data[$i]['composer']=$r->composer;
            $arr_data[$i]['type']=$r->type;
            $arr_data[$i]['title_category']=$r->title_category;
            $arr_data[$i]['publisher']=$r->publisher;
            $arr_data[$i]['status']=$r->status;
            $arr_data[$i]['notes']=$r->notes;
            $i++;
        }
        echo json_encode($arr_data);
    }
    public function aktif(){
        $data = (array)json_decode(file_get_contents('php://input'));
        
        /*Convert Object to array*/
        foreach($data as $index => $value){
            // cek data
            $dataNa = $this->db->get_where('mst_songs', array('id_songs'=>$value))->result_array();
            // kalo ada
            if(!empty($dataNa)){
                $resData[$index] = $dataNa[0]['status']; 
                // Kalo statusnya aktif, ubah ke deaktif
                if($dataNa[0]['status']==1){
                    $query = $this->db->update('mst_songs', array('status'=>0), array('id_songs'=>$dataNa[0]['id_songs']));
                }else{
                    $query = $this->db->update('mst_songs', array('status'=>1), array('id_songs'=>$dataNa[0]['id_songs']));
                }
            }
        };

        if(!empty($resData)){
            print_r($resData);
        }
    }

    public function data_single($id_songs){
        $data = $this->db->query("SELECT * FROM mst_songs WHERE id_songs='$id_songs'")->result();
        echo json_encode($data);
    }

    public function add(){
		//$data['rows'] = $this->db->query("SELECT id_songs FROM mst_songs WHERE SUBSTRING(id_songs,4,2)= SUBSTRING(date_added,6,2) ORDER BY id_songs DESC LIMIT 1")->row();
		$data['get_category'] = $this->Mcategory->get_category();
		$this->load->view('backend/songs/add',$data);
    }

    public function uploadBalok(){
        $this->upload('fileBalok','balok');
    }

    public function uploadAngka(){
        $this->upload('fileAngka','angka');
    }

    private function upload($attribut=null,$folder=null){
        $this->load->library('upload');

        // var_dump($_FILES['file']);
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/uploads/file/songs/'.$folder.'/'; //path folder
        $config['allowed_types'] = 'pdf'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        // $config['max_width']  = '1288'; //lebar maksimum 1288 px
        // $config['max_height']  = '768'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
        $config['overwrite']  = 'true';

        $this->upload->initialize($config);

        // var_dump($this->upload->do_upload('file')); // do_upload pake nama parameter file di attribut name form

        // move_uploaded_file($_FILES['file']['tmp_name'],$config['upload_path'].$_FILES['file']['name']);

        if (!$this->upload->do_upload($attribut)){
            $error = array('error' => $this->upload->display_errors());
            // print_r($error);
            return false;
        }else{   
            $data = $this->upload->data();
            echo $data['file_name'];
        }
    }
  
    public function insert_data(){

        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		//Simpan data ke mysql
		
        $val=array(
            //'id_songs' => 2,
            'title' => $data['title'],
            'type' => $data['type'],
            'year_start' => isset($data['year_start']) ? $data['year_start'] : "",
            'year_end' => isset($data['year_end']) ? $data['year_end'] : "",
            'composer' => $data['composer'],
            'publisher'=> $data['publisher'],
            'accompanied_status'=> $data['accompanied_type'],
            'accompanied_value'=> $data['accompanied_value'],
            'file_not_balok'=> $data['balok'],
            'file_not_angka'=> $data['angka'],
            'notes'=> $data['notes'],
            'status' => 1,
            'date_added' => date('Y-m-d h:i:s')
        );
        $this->db->insert('mst_songs', $val);
        $id = $this->db->insert_id();
        foreach ($data['data_category'] as $key => $value) {
            $catData = [
                'id_songs' => $id,
                'id_songs_category' => $value->id_songs_category,
            ];
            $this->db->insert('dtl_songs_category', $catData);
        }

    }

    public function getAllCategory()
    {
        echo json_encode($this->db->query('select * from mst_songs_category')->result());

    }
	

    public function getCategory($songs_id)
    {
        echo json_encode($this->db->query("select cat.* from dtl_songs_category join mst_songs_category cat on cat.id_songs_category = dtl_songs_category.id_songs_category where dtl_songs_category.id_songs = '$songs_id' group by dtl_songs_category.id_songs_category " )->result());

    }
	
    
    public function getOneCategory($cat_id)
    {
        echo json_encode($this->db->query("select * from mst_songs_category where id_songs_category = '$cat_id'")->result());

    }
	
	public function edit($id_songs)
	{
		$data['rows'] = $this->db->query("SELECT * FROM mst_songs WHERE id_songs='$id_songs'")->row();
		$this->load->view('backend/songs/edit', $data);
	}

    public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));

         $id_songs = $data['id_songs'];

        //Simpan data ke mysql
        
        $val=array(
            'title' => $data['title'],
            'year_start' => isset($data['year_start']) ? $data['year_start'] :'' ,
            'year_end' => isset($data['year_end']) ? $data['year_end'] :'' ,
            'composer' => $data['composer'],
            'publisher' => $data['publisher'],
            'accompanied_status' => isset($data['accompanied_status']) ? $data['accompanied_status'] : '',
            'accompanied_value' => $data['accompanied_value'],
            'file_not_balok' => $data['file_not_balok'],
            'file_not_angka' => $data ? $data['file_not_angka'] : '',
            'status' => $data['status'],
            'notes' => $data['notes'],
        );

        $this->db->where('id_songs', $id_songs);
        $this->db->update('mst_songs', $val);

        $this->db->query("delete from dtl_songs_category where id_songs = '$id_songs'");

         foreach ($data['data_category'] as $key => $value) {
            $catData = [
                'id_songs' => $id_songs,
                'id_songs_category' => $value->id_songs_category,
            ];
            $this->db->insert('dtl_songs_category', $catData);
        }

    }

    public function getCat(){
        $getCat = $this->Mcategory->get_category();

        echo json_encode($getCat);
    }

}
?>