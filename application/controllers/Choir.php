<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class Choir extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Mcountry');
        $this->load->model('Mcategory');
    }
  
    public function index() {
        $this->load->view('backend/choir/view');
    }
  
    public function data_angularnya(){
        $this->db->order_by("name", "asc");
        $dt=$this->db->get('mst_choir_category')->result();
        $arr_data=array();
        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['id_choir_category']=$r->id_choir_category;
            $arr_data[$i]['name']=$r->name;
            $arr_data[$i]['age']=$r->age;
            $arr_data[$i]['type']=$r->type;
            $arr_data[$i]['min_singer']=$r->min_singer;
            $arr_data[$i]['max_perform_time']=$r->max_perform_time;
            $arr_data[$i]['num_of_song']=$r->num_of_song;
            $arr_data[$i]['status']=$r->status;
            $i++;
        }
        echo json_encode($arr_data);
    }

    public function data_single($id_choir_category){
        $data = $this->db->query("SELECT * FROM mst_choir_category WHERE id_choir_category='$id_choir_category'")->result();
        echo json_encode($data);
    }
	public function aktif(){
        $data = (array)json_decode(file_get_contents('php://input'));
        
        /*Convert Object to array*/
        foreach($data as $index => $value){
            // cek data
            $dataNa = $this->db->get_where('mst_choir_category', array('id_choir_category'=>$value))->result_array();
            // kalo ada
            if(!empty($dataNa)){
                $resData[$index] = $dataNa[0]['status']; 
                // Kalo statusnya aktif, ubah ke deaktif
                if($dataNa[0]['status']==1){
                    $query = $this->db->update('mst_choir_category', array('status'=>0), array('id_choir_category'=>$dataNa[0]['id_choir_category']));
                }else{
                    $query = $this->db->update('mst_choir_category', array('status'=>1), array('id_choir_category'=>$dataNa[0]['id_choir_category']));
                }
            }
        };

        if(!empty($resData)){
            print_r($resData);
        }
    }

    public function add(){
		$data['rows'] = $this->db->query("SELECT id_choir_category FROM mst_choir_category WHERE SUBSTRING(id_choir_category,4,2)= SUBSTRING(date_added,6,2) ORDER BY id_choir_category DESC LIMIT 1")->row();
		$data['get_country'] = $this->Mcountry->get_country();
		$data['get_state'] = $this->Mcountry->get_state();
		$this->load->view('backend/choir/add', $data);
    }
  
    public function insert_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		//Simpan data ke mysql
		$type_prefix = "";
        switch($data['type']){
            case 'Competition':
                $type_prefix = "CO";
                break;
            case('Championship'):
                $type_prefix = "CO";
                break;
            case('Evaluation Performance'):
                $type_prefix = "EP";
                break;
            case('Choir Clinic'):
                $type_prefix = "CL";
                break;
            case('Friendship Concert'):
                $type_prefix = "FC";
                break;
            
        }
        $val=array(
            //'id_choir_category' => 2,
            'name' => $data['name'],
            'type' => $data['type'],
            'age' => $data['age'],
            'min_singer' => $data['min_singer'],
            'max_perform_time' => $data['max_perform_time'],
            'num_of_song' => $data['num_of_song'],
            'num_of_acapela_song' => $data['num_of_acapela_song'],
            'with_amplification' => $data['with_amplification'],
            'notes' => isset($data['notes']) ? $data['notes'] : "",  
            'code_category' => isset($data['code_category']) ? $data['code_category'] : "",  
            'type_prefix' => $type_prefix,
            'status' => 1
        );
        $this->db->insert('mst_choir_category', $val);

        echo $this->db->insert_id();
    }
	
	public function edit($id_choir_category)
	{
		$data['rows'] = $this->db->query("SELECT * FROM mst_choir_category WHERE id_choir_category='$id_choir_category'")->row();
		$this->load->view('backend/choir/edit', $data);
	}

    public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));

         $id_choir_category = $data['id_choir_category'];
        $type_prefix = "";
        switch($data['type']){
            case 'Competition':
                $type_prefix = "CO";
                break;
            case('Championship'):
                $type_prefix = "CO";
                break;
            case('Evaluation Performance'):
                $type_prefix = "EP";
                break;
            case('Choir Clinic'):
                $type_prefix = "CL";
                break;
            case('Friendship Concert'):
                $type_prefix = "FC";
                break;
            
        }
        //Simpan data ke mysql
        $val=array(
            'name' => $data['name'],
            'type' => $data['type'],
            'age' => $data['age'],
            'min_singer' => $data['min_singer'],
            'max_perform_time' => $data['max_perform_time'],
            'num_of_song' => $data['num_of_song'],
            'num_of_acapela_song' => $data['num_of_acapela_song'],
            'with_amplification' => $data['with_amplification'],
            'notes' => $data['notes'],
            'status' => $data['status'],
            'code_category' => isset($data['code_category']) ? $data['code_category'] : "",  
            'type_prefix' => $type_prefix,
//            'date_modify' => $data['date_modify']
        );

        $this->db->where('id_choir_category', $id_choir_category);
        $this->db->update('mst_choir_category', $val);

    }

    public function getOneCatChoir($id=0){
        $res = $this->Mcategory->getOneCatChoir(array('id_choir_category'=> $id));

        echo json_encode($res);
    }

}
?>