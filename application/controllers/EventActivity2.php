<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class EventActivity2 extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Mcountry');
		$this->load->model('Mdetail');
    }
  
    public function index() {
        $this->load->view('backend/EventActivity2/view');
    }
  
    public function data_angularnya(){
        $query = $this->db->join("trs_choir_event","trs_choir_event.id_trs_choir_event=pd_event_activity_2.id_trs_choir_event");
        $query = $this->db->join("trs_choir","trs_choir.id_trs_choir=pd_event_activity_2.id_trs_choir");

        $query = $this->db->get("pd_event_activity_2")->result_array();
        
        echo json_encode($query);
    }

    public function data_single($id_pd_event_activity_2){
        $data = $this->db->query(" SELECT pd_event_activity_2.* , trs_choir.choir_name , trs_choir_event.title  FROM pd_event_activity_2 INNER JOIN trs_choir ON pd_event_activity_2.id_trs_choir = trs_choir.id_trs_choir INNER JOIN trs_choir_event ON pd_event_activity_2.id_trs_choir_event = trs_choir_event.id_trs_choir_event WHERE pd_event_activity_2.id_pd_event_activity_2='$id_pd_event_activity_2'")->result();
        echo json_encode($data);
    }
	
	/*public function change_status(){
		$data = (array)json_decode(file_get_contents('php://input'));
		$id_dtl_choir = $data['id_dtl_choir'];

		foreach($data['status'] as $change){
			$this->db->where('id_dtl_choir', $change);
			$this->db->set('status',1);
			$this->db->update('dtl_choir'); 
		}
	}
*/
    public function add(){
		//$data['rows'] = $this->db->query("SELECT * FROM trs_nested_dtl_choir_event INNER JOIN trs_choir ON trs_choir.id_trs_choir = trs_nested_dtl_choir_event.id_trs_choir")->row();
		$data['get_country'] = $this->Mcountry->get_country();
		$data['get_state'] = $this->Mcountry->get_state();
		$data['get_member'] = $this->Mdetail->get_member();
		$data['get_institution'] = $this->Mdetail->get_institution();
		$this->load->view('backend/EventActivity2/add', $data);
    }
  
    public function insert_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		//Simpan data ke mysql
				$val=array(
					'id_trs_choir_event' => $data['id_trs_choir_event'],
					'id_trs_choir' => $data['id_trs_choir'],
					'num_of_singer' => $data['num_of_singer'],
					'num_of_musician' => $data['num_of_musician'],
					'num_of_conductor' => $data['num_of_conductor'],
					'num_of_official' => $data['num_of_official'],
					'stage_rehearseal_date' => $data['stage_rehearseal_date'],
					'reg_time' => $data['reg_time'],
					  'reg_time_start' => $data['reg_time_start'],
					  'reg_time_finish' => $data['reg_time_finish'],
					  'performance_schedule' => $data['performance_schedule'],
					  'performance_schedule_time' => $data['performance_schedule_time'],
					  're_reg_time' => $data['re_reg_time'],
					  're_reg_time_start' => $data['re_reg_time_start'],
					  're_reg_time_finish' => $data['re_reg_time_finish'],
					  're_reg_time_preparation' => $data['re_reg_time_preparation'],
					  're_reg_time_backstage' => $data['re_reg_time_backstage'],
					'notes' => $data['notes'],
					'status' => 1
				);
			$this->db->insert('pd_event_activity_2', $val);
			$last_id = $this->db->insert_id();
			//for($i=0;$i<count($data['member']);$i++){
			//	$val2=array(
			//		'id_pd_event_activity_2' => $last_id,
			//		'member' =>  $data['member'][$i]->name,
			//		'ukuran' =>  $data['ukuran'][$i]					
			//	);
			//	$this->db->insert('pd_dtl_event_activity_1', $val2);
			//}
}
	public function edit($id_pd_event_activity_2)
	{
		$data['rows'] = $this->db->query("SELECT * FROM pd_event_activity_2 WHERE id_pd_event_activity_2='$id_pd_event_activity_2'")->row();
		$this->load->view('backend/EventActivity2/edit', $data);
	}

    public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));

        $id_pd_event_activity_2 = $data['id_pd_event_activity_2'];

        //Simpan data ke mysql
        $val=array(
            'num_of_singer' => $data['num_of_singer'],
					'num_of_musician' => $data['num_of_musician'],
					'num_of_conductor' => $data['num_of_conductor'],
					'num_of_official' => $data['num_of_official'],
					'stage_rehearseal_date' => $data['stage_rehearseal_date'],
					'reg_time' => $data['reg_time'],
					  'reg_time_start' => $data['reg_time_start'],
					  'reg_time_finish' => $data['reg_time_finish'],
					  'performance_schedule' => $data['performance_schedule'],
					  'performance_schedule_time' => $data['performance_schedule_time'],
					  're_reg_time' => $data['re_reg_time'],
					  're_reg_time_start' => $data['re_reg_time_start'],
					  're_reg_time_finish' => $data['re_reg_time_finish'],
					  're_reg_time_preparation' => $data['re_reg_time_preparation'],
					  're_reg_time_backstage' => $data['re_reg_time_backstage'],
					'notes' => $data['notes']
        );

        $this->db->where('id_pd_event_activity_2', $id_pd_event_activity_2);
        $this->db->update('pd_event_activity_2', $val);
		$last_id = $this->db->insert_id();
			//delete heula
			//$this->db->where('id_pd_event_activity_2', $id_pd_event_activity_2);
			//$this->db->delete('pd_dtl_event_activity_1');
			//ulang ulang biar pusing
			//for($i=0;$i<count($data['member']);$i++){
			//	$val2=array(
			//		'id_pd_event_activity_2' => $id_pd_event_activity_2,
			//		'member' =>  $data['member'][$i]->member,
			//		'ukuran' =>  $data['ukuran'][$i]					
			//	);
				//asupkeun
			//	$this->db->insert('pd_dtl_event_activity_1', $val2);
			//}

    }
	

    public function getEvent(){

        $query = $this->db->join("trs_dtl_choir_event_choir","trs_dtl_choir_event_choir.id_trs_choir_event=trs_choir_event.id_trs_choir_event");
		$query = $this->db->join("trs_choir", "trs_choir.id_trs_choir=trs_dtl_choir_event_choir.id_trs_choir");
		$query = $this->db->order_by('trs_choir_event.id_trs_choir_event', 'DESC');
        $query = $this->db->get('trs_choir_event');
        $query = $query->result_array();

        // foreach ($query as $key) {
        //     echo "Event : ".$key['title']." - Choir : ".$key['choir_name']."<br>";
        // }

        echo json_encode($query); 
    }

    public function getOneChoir($id=0){
        $query = $this->db->get_where('trs_choir',array('id_trs_choir'=>$id));

        $query = $query->result_array();

        echo json_encode($query);
    }

    public function getOneChoirEvent($id_trs_choir_event=0,$id_trs_choir=0){

        
        
        $query = $this->db->query("SELECT * from trs_choir 
        JOIN trs_dtl_choir_event_choir ON trs_choir.id_trs_choir = trs_dtl_choir_event_choir.id_trs_choir 
        join trs_choir_event on trs_choir_event.id_trs_choir_event = trs_dtl_choir_event_choir.id_trs_choir_event  
        WHERE trs_choir.id_trs_choir='". $id_trs_choir ."' AND  trs_dtl_choir_event_choir.id_trs_choir_event = '" . $id_trs_choir_event . "' limit 0,1");
		
        $query = $query->result_array();

        if(!empty($query)){
            $choir = $this->db->get_where('trs_choir',array('id_trs_choir'=>$id_trs_choir))->result_array();
            $memberIkut = $this->db->join("mst_member","mst_member.id_member=trs_nested_dtl_choir_event.id_member");
            $memberIkut = $this->db->get_where('trs_nested_dtl_choir_event',array('id_trs_choir_event'=>$id_trs_choir_event,'id_trs_choir'=>$id_trs_choir))->result_array();

            $data = array(
                'id_trs_choir_event'         => $query[0]['id_trs_choir_event'],
                'id_trs_choir'         => $query[0]['id_trs_choir'],
                'title'         => $query[0]['title'],
                'choir_name'    => $choir[0]['choir_name'],
                'number_of_singer' => count($memberIkut),
                'member'        => $memberIkut
            );
        }

        echo json_encode($data);
    }

    public function getMemberSelected($idEvent=0,$idChoir=0){
        $data = $this->db->join("mst_member","mst_member.id_member=trs_nested_dtl_choir_event.id_member");
        $data = $this->db->get_where('trs_nested_dtl_choir_event',array('id_trs_choir_event'=>$idEvent,'id_trs_choir'=>$idChoir))->result_array();

        echo json_encode($data);
    }

}
?>