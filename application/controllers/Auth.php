<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
	}
	public function index() {
		$this->load->view('login');
	}

	public function cek_login() {
		$data = array('username' => $this->input->post('username', TRUE),
						'password' => $this->input->post('password', TRUE)
			);
		$this->load->model('model_user'); // load model_user
		$hasil = $this->model_user->cek_user($data);
		if ($hasil->num_rows() == 1) {

			foreach ($hasil->result() as $sess) {

				$sess_data['logged_in'] = 'Logged in';
				$sess_data['idUser'] = $sess->idUser;
				$sess_data['firstName'] = $sess->firstName;
				$sess_data['lastName'] = $sess->lastName;
				$sess_data['active'] = $sess->active;
				$sess_data['email'] = $sess->email;
				$sess_data['username'] = $sess->username;

				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata('active')==1) {
				redirect('Admin');
			}else{
				redirect('Auth');
			}
		}
		else {
			echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
		}
	}

}

?>