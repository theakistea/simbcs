<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class PackingToSms extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Mcountry');
		$this->load->model('Mdetail');
    }
  
    public function index() {
        $this->load->view('backend/packingtosms/view');
    }
  
    public function data_angularnya(){
        $dt=$this->db->query("SELECT t.id_trs_choir,t.choir_name,t.institution,t.city,t.email,t.id_trs_dtl_choir, manager.name AS nama_manager, conductor.name AS nama_conductor
                            FROM trs_choir AS t
                            INNER JOIN trs_dtl_choir AS detail
                            ON detail.id_trs_dtl_choir = t.id_trs_dtl_choir
                            INNER JOIN mst_manager AS manager
                            ON manager.id_manager = detail.id_manager
                            INNER JOIN mst_conductor AS conductor
                            ON conductor.id_conductor = detail.id_conductor
							  ")->result();
        $arr_data=array();
        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['id_trs_choir']=$r->id_trs_choir;
            $arr_data[$i]['id_trs_dtl_choir']=$r->id_trs_choir;
            $arr_data[$i]['choir_name']=$r->choir_name;
            $arr_data[$i]['city']=$r->city;
            $arr_data[$i]['nama_manager']=$r->nama_manager;
            $arr_data[$i]['nama_conductor']=$r->nama_conductor;
            $arr_data[$i]['institution']=$r->institution;
            $arr_data[$i]['email']=substr($r->email,0,20);
            $i++;
        }
        echo json_encode($arr_data);
    }

    public function data_single($id_dtl_choir){
        $data = $this->db->query("SELECT * FROM dtl_choir WHERE id_dtl_choir='$id_dtl_choir'")->result();
        echo json_encode($data);
    }
	
	/*public function change_status(){
		$data = (array)json_decode(file_get_contents('php://input'));
		$id_dtl_choir = $data['id_dtl_choir'];

		foreach($data['status'] as $change){
			$this->db->where('id_dtl_choir', $change);
			$this->db->set('status',1);
			$this->db->update('dtl_choir'); 
		}
	}
*/
    public function add(){
//		$data['rows'] = $this->db->query("SELECT id_dtl_choir FROM dtl_choir WHERE SUBSTRING(id_dtl_choir,4,2)= SUBSTRING(date_added,6,2) ORDER BY id_dtl_choir DESC LIMIT 1")->row();
		$data['get_country'] = $this->Mcountry->get_country();
		$data['get_state'] = $this->Mcountry->get_state();
		$data['get_member'] = $this->Mdetail->get_member();
		$data['get_institution'] = $this->Mdetail->get_institution();
		$this->load->view('backend/packingtosms/add', $data);
    }
  
    public function insert_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		//Simpan data ke mysql
		$config=array(
			//$nmfile = date('dmyhis'),
			'upload_path' => './assets/uploads/img/', //lokasi gambar akan di simpan
			'allowed_types' => 'jpg|jpeg|png|gif', //ekstensi gambar yang boleh di unggah
			'max_size' => '2000', //batas maksimal ukuran gambar
			'max_width' => '6000', //batas maksimal lebar gambar
			'max_height' => '6000', //batas maksimal tinggi gambar
			'file_name' => url_title($data['photo']) //nama gambar
		   );
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config); //meng set config yang sudah di atur
			
			$datenow = date('m');
			$queryid = $this->db->query("SELECT id_dtl_choir FROM dtl_choir WHERE SUBSTRING(id_dtl_choir,4,2)= $datenow ORDER BY id_dtl_choir DESC LIMIT 1");
		
		
//		if( !$this->upload->do_upload($data['photo']))
// 		{
//			echo" <script>
//           			   alert('Failed to Upload!');
//       		          </script>";   
	//			redirect('Candj/add');
		//}
		//else{
				$val=array(
					'photo' => $this->upload->file_name,
					//'id_dtl_choir' => 'CJ-'.date('my').$queryid+1,
					'choir_name' => $data['choir_name'],
					'street_address' => $data['street_address'],
					'institution' => $data['institution'],
					'country' => $data['country'],
					'state' => $data['state'],
					'city' => $data['city'],
					'notes' => $data['notes'],
					'email' => $data['email'],
					'status' => 1
				);
		
			$this->db->insert('trs_choir', $val);
		//}
}
	public function insert_data_cache(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		//Simpan data ke mysql
				$val=array(
					'name' => $this->upload->file_name,
					//'id_dtl_choir' => 'CJ-'.date('my').$queryid+1,
					'name' => $data['category_name']
//					'id_choir_category' => $data['id_choir_category'],
			);
			$this->db->insert('cache_category', $val);
}
	public function edit($id_trs_choir)
	{
		$data['rows'] = $this->db->query("SELECT * FROM trs_choir WHERE id_trs_choir='$id_trs_choir'")->row();
		$this->load->view('backend/packingtosms/edit', $data);
	}

    public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));

        $id_dtl_choir = $data['id_dtl_choir'];

        //Simpan data ke mysql
        $val=array(
            'choir_name' => $data['choir_name'],
            //'gender' => $data['gender'],
            //'appellation' => $data['appellation'],
            //'first_title' => $data['first_title'],
            //'last_title' => $data['last_title'],
            'street_address' => $data['street_address'],
            'city' => $data['city'],
            'state' => $data['state'],
            'country' => $data['country'],
            'address_status' => $data['address_status'],
            //'website' => $data['website'],
            //'place_of_birth' => $data['place_of_birth'],
            'date_of_birth' => $data['date_of_birth'],
            //'photo' => $data['photo'],
            'notes' => $data['notes'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            //'social_media' => $data['social_media'],
            'status' => $data['status'],
            //'date_modify' => $data['date_modify']
        );

        $this->db->where('id_dtl_choir', $id_dtl_choir);
        $this->db->update('trs_choir', $val);

    }
	
	public function cache_categories(){
		$dt=$this->db->query("SELECT * FROM cache_category 
							  ")->result();
        $arr_data=array();
        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['id_choir_category']=$r->id_choir_category;
            $arr_data[$i]['name']=$r->name;
            $i++;
        }
        echo json_encode($arr_data);
	}
	
	public function cache_conductor(){
		$dt=$this->db->query("SELECT * FROM cache_conductor 
							  ")->result();
        $arr_data=array();
        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['id_conductor']=$r->id_conductor;
            $arr_data[$i]['name']=$r->name;
            $i++;
        }
        echo json_encode($arr_data);
	}
	public function cache_manager(){
		$dt=$this->db->query("SELECT * FROM cache_manager 
							  ")->result();
        $arr_data=array();
        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['id_manager']=$r->id_manager;
            $arr_data[$i]['name']=$r->name;
            $i++;
        }
        echo json_encode($arr_data);
	}

}
?>