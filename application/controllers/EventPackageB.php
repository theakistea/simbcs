<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class EventPackageB extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Mcountry');
		$this->load->model('Mdetail');
    }
  
    public function index() {
        $this->load->view('backend/eventpackageb/view');
    }
  
    public function data_angularnya(){
        $query = $this->db->join("trs_choir_event","trs_choir_event.id_trs_choir_event=pd_event_package_b.id_trs_choir_event");
        $query = $this->db->join("trs_choir","trs_choir.id_trs_choir=pd_event_package_b.id_trs_choir");

        $query = $this->db->get("pd_event_package_b")->result_array();
        
        echo json_encode($query);
    }

    public function data_single($id_pd_event_package_b){
        $data = $this->db->query("SELECT pd_event_package_b.*  , pd_dtl_event_package_b.member , pd_dtl_event_package_b.ukuran  ,trs_choir_event.title , trs_choir.choir_name FROM pd_event_package_b  LEFT JOIN trs_choir ON pd_event_package_b.id_trs_choir = trs_choir.id_trs_choir LEFT JOIN trs_choir_event ON pd_event_package_b.id_trs_choir_event = trs_choir_event.id_trs_choir_event LEFT JOIN pd_dtl_event_package_b ON pd_dtl_event_package_b.id_pd_event_package_b = pd_event_package_b.id_pd_event_package_b WHERE pd_event_package_b.id_pd_event_package_b='$id_pd_event_package_b'")->result();
        echo json_encode($data);
    }
	
	/*public function change_status(){
		$data = (array)json_decode(file_get_contents('php://input'));
		$id_dtl_choir = $data['id_dtl_choir'];

		foreach($data['status'] as $change){
			$this->db->where('id_dtl_choir', $change);
			$this->db->set('status',1);
			$this->db->update('dtl_choir'); 
		}
	}
*/
    public function add(){
		//$data['rows'] = $this->db->query("SELECT * FROM trs_nested_dtl_choir_event INNER JOIN trs_choir ON trs_choir.id_trs_choir = trs_nested_dtl_choir_event.id_trs_choir")->row();
		$data['get_country'] = $this->Mcountry->get_country();
		$data['get_state'] = $this->Mcountry->get_state();
		$data['get_member'] = $this->Mdetail->get_member();
		$data['get_institution'] = $this->Mdetail->get_institution();
		$this->load->view('backend/eventpackageb/add', $data);
    }
  
    public function insert_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		//Simpan data ke mysql
				$val=array(
					'id_trs_choir_event' => $data['id_trs_choir_event'],
					'id_trs_choir' => $data['id_trs_choir'],
					'flight_detail' => $data['flight_detail'],
					'bus_detail' => $data['bus_detail'],
					'check_in' => $data['check_in'],
					'check_out' => $data['check_out'],
					'time_in' => $data['time_in'],
					'time_out' => $data['time_out'],
					'single_room' => $data['single_room'],
					'single_room_person' => $data['single_room_person'],
					'two_bedded_room' => $data['two_bedded_room'],
					'two_bedded_room_person' => $data['two_bedded_room_person'],
					'three_bedded_room' => $data['three_bedded_room'],
					'three_bedded_room_person' => $data['three_bedded_room_person'],
					'four_bedded_room' => $data['four_bedded_room'],
					'four_bedded_room_person' => $data['four_bedded_room_person'],
					'num_of_singer' => $data['num_of_singer'],
					'num_of_musician' => $data['num_of_musician'],
					'num_of_conductor' => $data['num_of_conductor'],
					'num_of_official' => $data['num_of_official'],
					'hallal_food' => $data['hallal_food'],
					'vegetarian_food' => $data['vegetarian_food'],
					'los_information' => $data['los_information'],
					'price_per_person' => $data['price_per_person'],
					'sub_total' => $data['sub_total'],
					'contact_person' => $data['contact_person'],
					'phone' => $data['phone'],
					'email' => $data['email'],
					'staying_address' => $data['staying_address'],
					'address' => $data['address'],
					'notes' => $data['notes'],
					'status' => 1
				);
			$this->db->insert('pd_event_package_b', $val);
			$last_id = $this->db->insert_id();
			for($i=0;$i<count($data['member']);$i++){
				$val2=array(
					'id_pd_event_package_b' => $last_id,
					'member' =>  $data['member'][$i]->name,
					'ukuran' =>  $data['ukuran'][$i]					
				);
				$this->db->insert('pd_dtl_event_package_b', $val2);
			}
}
	public function edit($id_pd_event_package_b)
	{
		$data['rows'] = $this->db->query("SELECT * FROM pd_event_package_b WHERE id_pd_event_package_b='$id_pd_event_package_b'")->row();
		$this->load->view('backend/eventpackageb/edit', $data);
	}

    public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));

        $id_pd_event_package_b = $data['id_pd_event_package_b'];

        //Simpan data ke mysql
        $val=array(
            'check_in' => $data['check_in'],
            'check_out' => $data['check_out'],
            'time_in' => $data['time_in'],
            'time_out' => $data['time_out'],
            'single_room' => $data['single_room'],
            'single_room_person' => $data['single_room_person'],
            'two_bedded_room' => $data['two_bedded_room'],
            'two_bedded_room_person' => $data['two_bedded_room_person'],
			'three_bedded_room' => $data['three_bedded_room'],
            'three_bedded_room_person' => $data['three_bedded_room_person'],
			'four_bedded_room' => $data['four_bedded_room'],
            'four_bedded_room_person' => $data['four_bedded_room_person'],
            'num_of_conductor' => $data['num_of_conductor'],
            'num_of_musician' => $data['num_of_musician'],
            'num_of_official' => $data['num_of_official'],
            'flight_detail' => $data['flight_detail'],
            'bus_detail' => $data['bus_detail'],
            'price_per_person' => $data['price_per_person'],
            'sub_total' => $data['sub_total'],
            'due_date' => $data['due_date'],
            'los_information' => $data['los_information'],
            'phone' => $data['phone'],
            'contact_person' => $data['contact_person'],
            'staying_address' => $data['staying_address'],
            'address' => $data['address'],
            'notes' => $data['notes'],
            'phone' => $data['phone'],
            'email' => $data['email']
        );

        $this->db->where('id_pd_event_package_b', $id_pd_event_package_b);
        $this->db->update('pd_event_package_b', $val);
		$last_id = $this->db->insert_id();
			//delete heula
			$this->db->where('id_pd_event_package_b', $id_pd_event_package_b);
			$this->db->delete('pd_dtl_event_package_b');
			//ulang ulang biar pusing
			for($i=0;$i<count($data['member']);$i++){
				$val2=array(
					'id_pd_event_package_b' => $id_pd_event_package_b,
					'member' =>  $data['member'][$i]->member,
					'ukuran' =>  $data['member'][$i]->ukuran					
				);
				//asupkeun
				$this->db->insert('pd_dtl_event_package_b', $val2);
			}

    }
	

    public function getEvent(){

        $query = $this->db->join("trs_dtl_choir_event_choir","trs_dtl_choir_event_choir.id_trs_choir_event=trs_choir_event.id_trs_choir_event");
        $query = $this->db->join("trs_choir", "trs_choir.id_trs_choir=trs_dtl_choir_event_choir.id_trs_choir");
		$query = $this->db->order_by('trs_choir_event.id_trs_choir_event', 'DESC');
        $query = $this->db->get('trs_choir_event');
        $query = $query->result_array();

        // foreach ($query as $key) {
        //     echo "Event : ".$key['title']." - Choir : ".$key['choir_name']."<br>";
        // }

        echo json_encode($query); 
    }

    public function getOneChoir($id=0){
        $query = $this->db->get_where('trs_choir',array('id_trs_choir'=>$id));

        $query = $query->result_array();

        echo json_encode($query);
    }

    public function getOneChoirEvent($idEvent=0,$idChoir=0){
        $query = $this->db->query("SELECT * from trs_choir 
        JOIN trs_dtl_choir_event_choir ON trs_choir.id_trs_choir = trs_dtl_choir_event_choir.id_trs_choir 
        join trs_choir_event on trs_choir_event.id_trs_choir_event = trs_dtl_choir_event_choir.id_trs_choir_event  
        WHERE trs_choir.id_trs_choir='". $idChoir ."' AND  trs_dtl_choir_event_choir.id_trs_choir_event = '" . $idEvent . "' limit 0,1");



        $query = $query->result_array();
        $data = [
            'id_trs_choir_event'=> '',
            'id_trs_choir'      => '', 
            'title'             => '',
            'choir_name'        => '',
            'number_of_singer'  => 0,
            'member'            => []
        ];

        if(!empty($query)){
            $choir = $this->db->get_where('trs_choir',array('id_trs_choir'=>$idChoir))->result_array();
            $memberIkut = $this->db->join("mst_member","mst_member.id_member=trs_nested_dtl_choir_event.id_member");
            $memberIkut = $this->db->get_where('trs_nested_dtl_choir_event',array('id_trs_choir_event'=>$idEvent,'id_trs_choir'=>$idChoir))->result_array();

            $data = array(
                'id_trs_choir_event'         => $query[0]['id_trs_choir_event'],
                'id_trs_choir'         => $query[0]['id_trs_choir'],
                'title'         => $query[0]['title'],
                'choir_name'    => $choir[0]['choir_name'],
                'number_of_singer' => count($memberIkut),
                'member'        => $memberIkut
            );
        }

        echo json_encode($data);
    }

    public function getMemberSelected($idEvent=0,$idChoir=0){
        $data = $this->db->join("mst_member","mst_member.id_member=trs_nested_dtl_choir_event.id_member");
        $data = $this->db->get_where('trs_nested_dtl_choir_event',array('id_trs_choir_event'=>$idEvent,'id_trs_choir'=>$idChoir))->result_array();

        echo json_encode($data);
    }

}
?>