<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class ChoirDetail extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Mcountry');
		$this->load->model('Mdetail');
        $this->load->model('Mcategory');
    }
  
    public function index() {
        $this->load->view('backend/choirdetail/view');
    }

  
    public function data_angularnya(){
        $dt=$this->db->query("SELECT t.status,t.choir_name,t.institution,t.city,t.email,t.id_trs_choir, manager.name AS nama_manager, 
                                choir_event.code_event  ,  DATE_FORMAT(STR_TO_DATE(choir_event.date_start, '%d-%m-%Y'),'%y') as date_start,
                                conductor.name AS nama_conductor , cat.name AS nama_kategory  , cat.type_prefix  as cat_type_prefix, cat.code_category,
                                detailCat.code_registrasi
                                FROM trs_choir AS t 
                                LEFT JOIN trs_dtl_choir_event_choir as dtl on dtl.id_trs_choir = t.id_trs_choir 
                                LEFT JOIN trs_choir_event as choir_event on choir_event.id_trs_choir_event = dtl.id_trs_choir_event
                                LEFT JOIN trs_dtl_choir_cat AS detailCat ON detailCat.id_trs_choir = t.id_trs_choir 
                                LEFT JOIN mst_choir_category AS cat ON detailCat.id_choir_category= cat.id_choir_category 
                                LEFT JOIN trs_dtl_choir_man AS detailMan ON detailMan.id_trs_choir = t.id_trs_choir 
                                LEFT JOIN mst_manager AS manager ON manager.id_manager = detailMan.id_manager 
                                LEFT JOIN trs_dtl_choir_con AS detailCon ON detailCon.id_trs_choir = t.id_trs_choir 
                                LEFT JOIN mst_conductor AS conductor ON conductor.id_conductor = detailCon.id_conductor 
                                GROUP BY t.id_trs_choir , cat.id_choir_category order by t.id_trs_choir asc")->result();
        $arr_data=array();

        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['id_trs_choir']=$r->id_trs_choir;
            $arr_data[$i]['id_trs_dtl_choir']=$r->id_trs_choir;
            $arr_data[$i]['choir_name']=$r->choir_name;
            $arr_data[$i]['city']=$r->city;
            $arr_data[$i]['nama_manager']=$r->nama_manager;
            $arr_data[$i]['nama_conductor']=$r->nama_conductor;
            $arr_data[$i]['institution']=$r->institution;
            $arr_data[$i]['status']=$r->status;
            $arr_data[$i]['email']=substr($r->email,0,20);
            $arr_data[$i]['nama_kategory']=$r->nama_kategory;
            $arr_data[$i]['date_start']=$r->date_start;
            $arr_data[$i]['code_event']=$r->code_event;
            $arr_data[$i]['cat_type_prefix']=$r->cat_type_prefix;
            $arr_data[$i]['code_registrasi']=$r->code_registrasi;
            $arr_data[$i]['code_category']=$r->code_category;
            $i++;
        }
        echo json_encode($arr_data);
    }

    public function data_single($id_trs_choir){
        $data = $this->db->query("SELECT trs_choir.* ,event.id_trs_choir_event as choir_event , event.code_event  ,event.title  ,DATE_FORMAT(STR_TO_DATE(event.date_start, '%d-%m-%Y'),'%y') as start_date from trs_choir 
                                LEFT JOIN trs_dtl_choir_event_choir as dtl on dtl.id_trs_choir = trs_choir.id_trs_choir 
                                LEFT JOIN trs_choir_event as event on event.id_trs_choir_event = dtl.id_trs_choir_event WHERE trs_choir.id_trs_choir='$id_trs_choir'
                                GROUP BY trs_choir.id_trs_choir")->result();
        echo json_encode($data);
    }   

    public function getAllCatOn($id_trs_choir=null){
        $data = $this->db->query("select * from trs_choir tc INNER JOIN trs_dtl_choir_cat tdcc on tdcc.id_trs_choir=tc.id_trs_choir
        inner join mst_choir_category mcc on tdcc.id_choir_category=mcc.id_choir_category where tc.id_trs_choir='$id_trs_choir'")->result();
        echo json_encode($data);
    }

    public function getAllManOn($id_trs_choir=null){
        $this->db->select('mst_manager.*');
        $result = $this->db->join('trs_dtl_choir_man','trs_dtl_choir_man.id_trs_choir=trs_choir.id_trs_choir');
        $result = $this->db->join('mst_manager','mst_manager.id_manager=trs_dtl_choir_man.id_manager');
        $result = $this->db->get_where("trs_choir",array('trs_choir.id_trs_choir'=>$id_trs_choir));
        $result = $result->result_array();

        echo json_encode($result);
    }

    public function getAllConOn($id_trs_choir=null){
        $this->db->select('mst_conductor.*');
        $result = $this->db->join('trs_dtl_choir_con','trs_dtl_choir_con.id_trs_choir=trs_choir.id_trs_choir');
        $result = $this->db->join('mst_conductor','mst_conductor.id_conductor=trs_dtl_choir_con.id_conductor');
        $result = $this->db->get_where("trs_choir",array('trs_choir.id_trs_choir'=>$id_trs_choir));
        $result = $result->result_array();

        echo json_encode($result);
    }

    public function getAllMemOn($id_trs_choir=null){
        $this->db->select('mst_member.* ');
        $result = $this->db->join('trs_dtl_choir_mem','trs_dtl_choir_mem.id_trs_choir=trs_choir.id_trs_choir');
        $result = $this->db->join('mst_member','mst_member.id_member=trs_dtl_choir_mem.id_member');
        $result = $this->db->get_where("trs_choir",array('trs_choir.id_trs_choir'=>$id_trs_choir));
        $result = $result->result_array();

        echo json_encode($result);
    }
    public function getAllSongsOn($id_trs_choir=null){
        $this->db->select('mst_songs.* , trs_dtl_choir_songs.jumlah_partitur_diterima , trs_dtl_choir_songs.surati_izin_komposer , trs_dtl_choir_songs.durasi , trs_dtl_choir_songs.persetujuan_artistic_commite ');
        $result = $this->db->join('trs_dtl_choir_songs','trs_dtl_choir_songs.id_trs_choir=trs_choir.id_trs_choir');
        $result = $this->db->join('mst_songs','mst_songs.id_songs=trs_dtl_choir_songs.id_song');
        $result = $this->db->get_where("trs_choir",array('trs_choir.id_trs_choir'=>$id_trs_choir));
        $result = $result->result_array();

        echo json_encode($result);
    }
    
    public function getAllCatOn2($id_trs_choir=null){
        $data = $this->db->query("select * from trs_choir tc INNER JOIN trs_dtl_choir_cat tdcc on tdcc.id_trs_choir=tc.id_trs_choir
        inner join mst_choir_category mcc on tdcc.id_choir_category=mcc.id_choir_category where tc.id_trs_choir='$id_trs_choir'")->result();
        return $data;
    }

    public function getAllManOn2($id_trs_choir=null){
        $this->db->select('mst_manager.*');
        $result = $this->db->join('trs_dtl_choir_man','trs_dtl_choir_man.id_trs_choir=trs_choir.id_trs_choir');
        $result = $this->db->join('mst_manager','mst_manager.id_manager=trs_dtl_choir_man.id_manager');
        $result = $this->db->get_where("trs_choir",array('trs_choir.id_trs_choir'=>$id_trs_choir));
        $result = $result->result_array();

        return $result;
    }

    public function getAllConOn2($id_trs_choir=null){
        $this->db->select('mst_conductor.*');
        $result = $this->db->join('trs_dtl_choir_con','trs_dtl_choir_con.id_trs_choir=trs_choir.id_trs_choir');
        $result = $this->db->join('mst_conductor','mst_conductor.id_conductor=trs_dtl_choir_con.id_conductor');
        $result = $this->db->get_where("trs_choir",array('trs_choir.id_trs_choir'=>$id_trs_choir));
        $result = $result->result_array();

        return $result;
    }

    public function getAllMemOn2($id_trs_choir=null){
        $this->db->select('mst_member.* ');
        $result = $this->db->join('trs_dtl_choir_mem','trs_dtl_choir_mem.id_trs_choir=trs_choir.id_trs_choir');
        $result = $this->db->join('mst_member','mst_member.id_member=trs_dtl_choir_mem.id_member');
        $result = $this->db->get_where("trs_choir",array('trs_choir.id_trs_choir'=>$id_trs_choir));
        $result = $result->result_array();

        return $result;
    }
    public function getAllSongsOn2($id_trs_choir=null){
        $this->db->select('mst_songs.* , trs_dtl_choir_songs.jumlah_partitur_diterima , trs_dtl_choir_songs.surati_izin_komposer , trs_dtl_choir_songs.durasi , trs_dtl_choir_songs.persetujuan_artistic_commite ');
        $result = $this->db->join('trs_dtl_choir_songs','trs_dtl_choir_songs.id_trs_choir=trs_choir.id_trs_choir');
        $result = $this->db->join('mst_songs','mst_songs.id_songs=trs_dtl_choir_songs.id_song');
        $result = $this->db->get_where("trs_choir",array('trs_choir.id_trs_choir'=>$id_trs_choir));
        $result = $result->result_array();
        return $result;
    }
	

    public function getChoirOnEvent($eventid) {
        $data = $this->db->query("SELECT trs_choir.*  from trs_choir 
                                LEFT JOIN trs_dtl_choir_event_choir as dtl on dtl.id_trs_choir = trs_choir.id_trs_choir 
                                LEFT JOIN trs_choir_event as event on event.id_trs_choir_event = dtl.id_trs_choir_event 
                                WHERE event.id_trs_choir_event='$eventid'
                                GROUP BY trs_choir.id_trs_choir")->result();
        echo json_encode($data);
    }
	public function change_status(){
		$data = (array)json_decode(file_get_contents('php://input'));
		$id_dtl_choir = $data['id_dtl_choir_event'];

		foreach($data['status'] as $change){
			$this->db->where('id_trs_choir_event', $id_dtl_choir);
			$this->db->set('status',$change);
			$this->db->update('trs_choir_event');
            //update choir status
            $this->db->where("id_trs_choir in (SELECT id_trs_choir FROM  trs_dtl_choir_event_choir where id_trs_choir_event = '" .$id_dtl_choir. "'");
			$this->db->set('status',$change);
			$this->db->update('trs_choir');
            
             
		}
	}

    public function deletePhoto(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
        $file = './assets/uploads/img/choirdetail/'.$data['filename'];

        if (!unlink($file)){
            echo 0;
        }else{
            echo 1;
        }
    }


    public function upload(){
        $this->load->library('upload');

        // var_dump($_FILES['file']);
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/uploads/img/choirdetail/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        // $config['max_width']  = '1288'; //lebar maksimum 1288 px
        // $config['max_height']  = '768'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
        $config['overwrite']  = 'true';

        $this->upload->initialize($config);

        // var_dump($this->upload->do_upload('file')); // do_upload pake nama parameter file di attribut name form

        // move_uploaded_file($_FILES['file']['tmp_name'],$config['upload_path'].$_FILES['file']['name']);

        if (!$this->upload->do_upload('file')){
            $error = array('error' => $this->upload->display_errors());
            // print_r($error);
            return false;
        }else{   
            $data = $this->upload->data();
            echo $data['file_name'];
        }
    }

    public function add(){
//		$data['rows'] = $this->db->query("SELECT id_dtl_choir FROM dtl_choir WHERE SUBSTRING(id_dtl_choir,4,2)= SUBSTRING(date_added,6,2) ORDER BY id_dtl_choir DESC LIMIT 1")->row();
		$data['get_country'] = $this->Mcountry->get_country();
		$data['get_state'] = $this->Mcountry->get_state();

        $data['get_cat'] = $this->Mcategory->get_choircategoryAktif();
		$data['get_memberAktif'] = $this->Mdetail->get_memberAktif();
		$data['get_conductorAktif'] = $this->Mdetail->get_conductorAktif();
		$data['get_managerAktif'] = $this->Mdetail->get_managerAktif();
		$data['get_institution'] = $this->Mdetail->get_institution();


        $data['modals'] = [
            'lattest_choir' => $this->load->view('backend/choirdetail/modals/lattest_choir' , true),
            'member' => $this->load->view('backend/choirdetail/modals/member_modal' , true),
            'manager' => $this->load->view('backend/choirdetail/modals/manager_modal' , true),
            'conductor' => $this->load->view('backend/choirdetail/modals/conductor_modal' , true),
            'song' => $this->load->view('backend/choirdetail/modals/song_modal' , true)
        ];
		$this->load->view('backend/choirdetail/add', $data);
    }
    public function aktif(){
        $data = (array)json_decode(file_get_contents('php://input'));
        
        /*Convert Object to array*/
        foreach($data as $index => $value){
            // cek data
            $dataNa = $this->db->get_where('trs_choir', array('id_trs_choir'=>$value))->result_array();
            // kalo ada
            if(!empty($dataNa)){
                $resData[$index] = $dataNa[0]['status']; 
                // Kalo statusnya aktif, ubah ke deaktif
                if($dataNa[0]['status']==1){
                    $query = $this->db->update('trs_choir', array('status'=>0), array('id_trs_choir'=>$dataNa[0]['id_trs_choir']));
                }else{
                    $query = $this->db->update('trs_choir', array('status'=>1), array('id_trs_choir'=>$dataNa[0]['id_trs_choir']));
                }
            }
        };

        if(!empty($resData)){
            print_r($resData);
        }
    }

    public function cat(){
        $data = (array)json_decode(file_get_contents('php://input'));
        
        echo $data['katChoir'][0]->name." - ";
        echo "Jum Kat : ".count($data['katChoir']);
        echo " Jum Mem : ".count($data['dataMem']);
    }
  
    public function insert_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
        if(!isset($data['id_trs_choir_event']) ){
            $this->output->set_status_header(400);
            return ;
        }
        $val=array(
            'photo' => 'default.jpg',
            'choir_name' => $data['choir_name'],
            'institution' => $data['institution'],
            'street_address' => $data['street_address'],
            'country' => isset($data['country']) ? $data['country'] :null,
            'state' => isset($data['state'])  ? $data['state'] : null,
            'city' => isset($data['city']) ? $data['city'] : null,
            'email' => isset($data['email'])  ?  $data['email'] : null,

            'has_surat_keterangan_usia' => isset($data['has_surat_keterangan_usia'])  ? $data['has_surat_keterangan_usia'] : 0,
            'has_bukti_registrasi' =>isset($data['has_bukti_registrasi'])  ?  $data['has_bukti_registrasi']: 0,

            'has_audio_record' => isset($data['has_audio_record'])  ? $data['has_audio_record']: 0,
            'has_biograpy' =>isset($data['has_biograpy'])  ?  $data['has_biograpy']: 0,
            'has_surat_keterangan_usia_date' => isset($data['has_surat_keterangan_usia_date'])  ? $data['has_surat_keterangan_usia_date'] : '',
            'has_bukti_registrasi_date' =>isset($data['has_bukti_registrasi_date'])  ?  $data['has_bukti_registrasi_date']: '',

            'has_audio_record_date' => isset($data['has_audio_record_date'])  ? $data['has_audio_record_date']: '',
            'has_biograpy_date' =>isset($data['has_biograpy_date'])  ?  $data['has_biograpy_date']: '',
            'status' => 1
        );
        $this->db->insert('trs_choir', $val);
        $last_id = $this->db->insert_id();

        $this->db->insert('trs_dtl_choir_event_choir' , array('id_trs_choir_event' => $data['id_trs_choir_event'] , 'id_trs_choir' => $last_id));

        // delete dulu, trrus insert

        // ini contoh inputna
        // isi table trs_choir_dtl_cat ada record id_trs_choir_dtl_cat, id_trs_choir & id_choir_category
        // looping insert banyak kategori
        //  for($i=0;$i<count($data['katChoir']);$i++){
        //  $datanya = array(
        //          'id_trs_choir' => $last_id,
        //          'id_choir_category' => $data['katChoir'][$i]->id,  
        //          'code_registrasi' => isset($data['katChoir'][$i]->code_registrasi)  ? $data['katChoir'][$i]->code_registrasi  : ''
        //      );  
        //      $this->db->insert('trs_dtl_choir_cat',$datanya);
        //  }
        if(isset($data['katChoir']) && count($data['katChoir']) > 0){
            for($i=0;$i<count($data['katChoir']);$i++){
            $datanya = array(
                    'id_trs_choir' => $last_id,
                    'id_choir_category' => property_exists($data['katChoir'][$i] , 'id') ? $data['katChoir'][$i]->id : $data['katChoir'][$i]->id_choir_category       ,
                    'code_registrasi' => $data['katChoir'][$i]->code_registrasi  ? $data['katChoir'][$i]->code_registrasi  : ''
                );  
                $this->db->insert('trs_dtl_choir_cat',$datanya);
            }
        }else{
            $this->db->query("INSERT into trs_dtl_choir_cat (id_trs_choir , id_choir_category ) VALUES ('". $last_id ."' , (SELECT id_choir_category FROM mst_choir_category WHERE code_category = 'NOTYET')) ");
        }
		 for($i=0;$i<count($data['dataMem']);$i++){
         $datanya = array(
                 'id_trs_choir' => $last_id,
                 'id_member' => $data['dataMem'][$i]->id
             );  
             $this->db->insert('trs_dtl_choir_mem',$datanya);
         }

        for($i=0;$i<count($data['dataMan']);$i++){
         $datanya = array(
                 'id_trs_choir' => $last_id,
                 'id_manager' => $data['dataMan'][$i]->id
             );  
             $this->db->insert('trs_dtl_choir_man',$datanya);
         }
        for($i=0;$i<count($data['dataKon']);$i++){
            $datanya = array(
                'id_trs_choir' => $last_id,
                'id_conductor' => $data['dataKon'][$i]->id
            );
            $this->db->insert('trs_dtl_choir_con',$datanya);
        }
        for($i=0;$i<count($data['dataSongs']);$i++){
            $datanya = array(
                'id_trs_choir' => $last_id,
                'id_song' => $data['dataSongs'][$i]->id_songs,
                'jumlah_partitur_diterima' => isset($data['dataSongs'][$i]->jumlah_partitur_diterima) ? $data['dataSongs'][$i]->jumlah_partitur_diterima : 0,
                'surati_izin_komposer' => isset($data['dataSongs'][$i]->surati_izin_komposer) ? $data['dataSongs'][$i]->surati_izin_komposer : 0,
                'durasi' => isset($data['dataSongs'][$i]->durasi) ? $data['dataSongs'][$i]->durasi : '',
                'persetujuan_artistic_commite' => isset($data['dataSongs'][$i]->persetujuan_artistic_commite) ? $data['dataSongs'][$i]->persetujuan_artistic_commite : 0
            );
            $this->db->insert('trs_dtl_choir_songs',$datanya);
        }
         echo($last_id);
		 
}
	
	public function edit($id_trs_choir)
	{
        // Model member
        $member = $this->db->join('trs_dtl_choir_mem','trs_dtl_choir_mem.id_trs_choir=trs_choir.id_trs_choir');
        $member = $this->db->join('mst_member','mst_member.id_member=trs_dtl_choir_mem.id_member');
        $member = $this->db->get_where("trs_choir",array('trs_choir.id_trs_choir'=>$id_trs_choir));
        // inject ke view $getdetailMem
        $data['getdetailMem'] = $member;

        $manager = $this->db->join('trs_dtl_choir_man','trs_dtl_choir_man.id_trs_choir=trs_choir.id_trs_choir');
        $manager = $this->db->join('mst_manager','mst_manager.id_manager=trs_dtl_choir_man.id_manager');
        $manager = $this->db->get_where("trs_choir",array('trs_choir.id_trs_choir'=>$id_trs_choir));
        // inject ke view $getdetailMem
        $data['getdetailMan'] = $manager;

        $conductor = $this->db->join('trs_dtl_choir_con','trs_dtl_choir_con.id_trs_choir=trs_choir.id_trs_choir');
        $conductor = $this->db->join('mst_conductor','mst_conductor.id_conductor=trs_dtl_choir_con.id_conductor');
        $conductor = $this->db->get_where("trs_choir",array('trs_choir.id_trs_choir'=>$id_trs_choir));
        // inject ke view $getdetailMem
        $data['getdetailCon'] = $conductor;

        //$data['get_institution'] = $this->Mdetail->get_institution();
        $data['getdetailCat'] = $this->db->query("SELECT trs_dtl_choir_cat.* FROM trs_dtl_choir_cat 
                                                    INNER JOIN trs_choir ON
                                                    trs_choir.id_trs_choir = trs_dtl_choir_cat.id_trs_choir
                                                    INNER JOIN mst_choir_category ON
                                                    mst_choir_category.id_choir_category = trs_dtl_choir_cat.id_choir_category 
                                                    WHERE trs_choir.id_trs_choir='$id_trs_choir'
                                                    ");

		$data['rows'] = $this->db->query("SELECT * FROM trs_choir WHERE id_trs_choir='$id_trs_choir'")->row();

        // var_dump($data['rows']);exit()

        $data['get_country'] = $this->Mcountry->get_country();
        $data['get_state'] = $this->Mcountry->get_state();

        $data['get_cat'] = $this->Mcategory->get_choircategoryAktif();
        $data['get_memberAktif'] = $this->Mdetail->get_memberAktif();
        $data['get_conductorAktif'] = $this->Mdetail->get_conductorAktif();
        $data['get_managerAktif'] = $this->Mdetail->get_managerAktif();
        $data['get_institution'] = $this->Mdetail->get_institution();

        
        $data['modals'] = [
            'member' => $this->load->view('backend/choirdetail/modals/member_modal' , true),
            'manager' => $this->load->view('backend/choirdetail/modals/manager_modal' , true),
            'conductor' => $this->load->view('backend/choirdetail/modals/conductor_modal' , true),
            'song' => $this->load->view('backend/choirdetail/modals/song_modal' , true)
        ];
		$this->load->view('backend/choirdetail/edit', $data);
	}

    public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
        if(!isset($data['id_trs_choir_event']) ){
            $this->output->set_status_header(400);
            return ;
        }
        $id_trs_choir = $data['id_trs_choir'];

        //Simpan data ke mysql
        $val=array(
            'choir_name' => $data['choir_name'],
            'institution' => $data['institution'],
            'street_address' => $data['street_address'],
            'city' => $data['city'],
            'state' => $data['state'],
            'country' => $data['country'],
            'notes' => $data['notes'],
            'email' => $data['email'],
            'photo' => $data['photo'],
            
            'has_surat_keterangan_usia' => isset($data['has_surat_keterangan_usia'])  ? $data['has_surat_keterangan_usia'] : 0,
            'has_bukti_registrasi' =>isset($data['has_bukti_registrasi'])  ?  $data['has_bukti_registrasi']: 0,
            
            'has_audio_record' => isset($data['has_audio_record'])  ? $data['has_audio_record']: 0,
            'has_biograpy' =>isset($data['has_biograpy'])  ?  $data['has_biograpy']: 0,
            'date_modify' => date("Y/m/d H:i:s")
        );

        $this->db->where('id_trs_choir', $id_trs_choir);
        $this->db->update('trs_choir', $val);
		// Hapus Current
        
        $this->db->where('id_trs_choir', $id_trs_choir);
        $this->db->delete('trs_dtl_choir_event_choir'  );


        $this->db->insert('trs_dtl_choir_event_choir' , array('id_trs_choir_event' => $data['id_trs_choir_event'] , 'id_trs_choir' => $id_trs_choir));



        $this->db->where('id_trs_choir', $id_trs_choir);
        $this->db->delete('trs_dtl_choir_cat'); 
        
        $this->db->where('id_trs_choir', $id_trs_choir);
		$this->db->delete('trs_dtl_choir_man'); 

        $this->db->where('id_trs_choir', $id_trs_choir);
		$this->db->delete('trs_dtl_choir_con'); 

        $this->db->where('id_trs_choir', $id_trs_choir);
		$this->db->delete('trs_dtl_choir_mem'); 

        $this->db->where('id_trs_choir', $id_trs_choir);
		$this->db->delete('trs_dtl_choir_songs'); 

        // Tambah Data 
        if(isset($data['katChoir']) && count($data['katChoir']) > 0){
            for($i=0;$i<count($data['katChoir']);$i++){
            $datanya = array(
                    'id_trs_choir' => $id_trs_choir,
                    'id_choir_category' => property_exists($data['katChoir'][$i] , 'id') ? $data['katChoir'][$i]->id : $data['katChoir'][$i]->id_choir_category       ,
                    'code_registrasi' => $data['katChoir'][$i]->code_registrasi  ? $data['katChoir'][$i]->code_registrasi  : ''
                );  
                $this->db->insert('trs_dtl_choir_cat',$datanya);
            }
        }else{
            $this->db->query("INSERT into trs_dtl_choir_cat (id_trs_choir , id_choir_category ) VALUES ('". $id_trs_choir ."' , (SELECT if_choir_category FROM mst_choir_category WHERE code_category ='NOTYET')) ");
        }
		 for($i=0;$i<count($data['dataMem']);$i++){
         $datanya = array(
                 'id_trs_choir' => $id_trs_choir,
                 'id_member' =>  property_exists($data['dataMem'][$i] ,  'id') ? $data['dataMem'][$i]->id : $data['dataMem'][$i]->id_member 
             );  
             $this->db->insert('trs_dtl_choir_mem',$datanya);
         }

        $this->db->query("INSERT INTO trs_nested_dtl_choir_event (id_member,id_trs_choir,id_trs_choir_event) SELECT id_member,id_trs_choir,'". $data['id_trs_choir_event'] ."' FROM trs_dtl_choir_mem  WHERE id_trs_choir='$id_trs_choir' ");

        for($i=0;$i<count($data['dataMan']);$i++){
         $datanya = array(
                 'id_trs_choir' => $id_trs_choir,
                 'id_manager' => property_exists($data['dataMan'][$i] ,  'id')  ? $data['dataMan'][$i]->id : $data['dataMan'][$i]->id_manager
             );  
             $this->db->insert('trs_dtl_choir_man',$datanya);
         }
		 for($i=0;$i<count($data['dataKon']);$i++){
         $datanya = array(
                 'id_trs_choir' => $id_trs_choir,
                 'id_conductor' => property_exists($data['dataKon'][$i] ,  'id') ? $data['dataKon'][$i]->id : $data['dataKon'][$i]->id_conductor
             );  
             $this->db->insert('trs_dtl_choir_con',$datanya);
         }
         
        for($i=0;$i<count($data['dataSongs']);$i++){
            $datanya = array(
                'id_trs_choir' => $id_trs_choir,
                'id_song' => $data['dataSongs'][$i]->id_songs,
                'jumlah_partitur_diterima' => isset($data['dataSongs'][$i]->jumlah_partitur_diterima) ? $data['dataSongs'][$i]->jumlah_partitur_diterima : 0,
                'surati_izin_komposer' => isset($data['dataSongs'][$i]->surati_izin_komposer) ? $data['dataSongs'][$i]->surati_izin_komposer : 0,
                'durasi' => isset($data['dataSongs'][$i]->durasi) ? $data['dataSongs'][$i]->durasi : '',
                'persetujuan_artistic_commite' => isset($data['dataSongs'][$i]->persetujuan_artistic_commite) ? $data['dataSongs'][$i]->persetujuan_artistic_commite : 0
            );
            $this->db->insert('trs_dtl_choir_songs',$datanya);
        }
         echo($last_id);
		 

		/*$this->db->where('id_trs_choir', $id_trs_choir);
		$this->db->delete('trs_dtl_choir_cat'); 
		$this->db->delete('trs_dtl_choir_man'); 
		$this->db->delete('trs_dtl_choir_con'); 
		$this->db->delete('trs_dtl_choir_mem'); 
        // ini contoh inputna
        // isi table trs_choir_dtl_cat ada record id_trs_choir_dtl_cat, id_trs_choir & id_choir_category
        /* looping insert banyak kategori
         for($i=0;$i<count($data['songsComposer']);$i++){
         $datanya = array(
                 'id_composer_and_arranger' => $id_composer_and_arranger,
                 'id_songs' => $data['songsComposer'][$i]->id
             );  
			 $this->db->insert('mst_dtl_composer_and_arranger',$datanya);
         }*/
    }

    public function getAllCatChoir(){
       echo json_encode($this->Mcategory->get_choircategoryAktif());
    }
    public function getOneCatChoir($id=0){
        $res = $this->Mcategory->getOneCatChoir(array('id_choir_category'=> $id));

        echo json_encode($res);
    }

    public function getAllMemChoir(){
       echo json_encode($this->Mdetail->get_memberAktif());
    }
    public function getOneMember($id=0){
        $res = $this->Mdetail->getOneMember(array('id_member'=> $id));

        echo json_encode($res);
    }
	public function getOneMember2($id=0){
        $res = $this->Mdetail->getOneMember2(array('id_member'=> $id));

        echo json_encode($res);
    }

    public function getAllKonChoir(){
       echo json_encode($this->Mdetail->get_conductorAktif());
    }
    public function getOneKonduktor($id=0){
        $res = $this->db->get_where('mst_conductor',array('id_conductor'=> $id))->result_array();

        echo json_encode($res);
    }

    public function getAllSongs(){
       echo json_encode($this->Mdetail->get_songsAktif());
    }

    public function getOneSong($id=0){
        $res = $this->Mdetail->getOneSongs(array('id_songs'=> $id));

        echo json_encode($res);
    }
    public function getAllManChoir(){
       echo json_encode($this->Mdetail->get_managerAktif());
    }
    public function getOneManager($id=0){
        $res = $this->db->get_where('mst_manager',array('id_manager'=> $id))->result_array();

        echo json_encode($res);
    }
    public function getCountry(){
        $get_country = $this->Mcountry->getCountry();

        echo json_encode($get_country);
    }

    public function getState(){
        $get_state = $this->Mcountry->getState();

        echo json_encode($get_state);
    }  

    public function getOneChoir($id_trs_choir=0){

        
        $query = $this->db->query("SELECT * from trs_choir 
        WHERE id_trs_choir='". $id_trs_choir ."' limit 0,1");

        $query = $query->result_array();

        if(!empty($query)){
            
            $data = array(
                'trs_choir'     => $query[0],
                'category'      => ($this->getAllCatOn2($id_trs_choir)),
                'manager'       => ($this->getAllManOn2($id_trs_choir)),
                'conductor'     => ($this->getAllConOn2($id_trs_choir)),
                'member'         => ($this->getAllMemOn2($id_trs_choir)),
            );
        }

        echo json_encode($data);
    }

    public function getAllLattestChoir() {
        
        $query = $this->db->query("SELECT * FROM trs_choir where  id_trs_choir in (select max(id_trs_choir) from trs_choir  GROUP BY choir_name order by id_trs_choir ASC)");
        $query = $query->result_array();
        echo json_encode($query);
    }
}
?>