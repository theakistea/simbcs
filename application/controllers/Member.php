<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class Member extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Mcountry');
    }
  
    public function index() {
        $this->load->view('backend/member/view');
    }
  
    public function data_angularnya(){
        $dt=$this->db->get('mst_member')->result();
        $arr_data=array();
        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['id_member']=$r->id_member;
            $arr_data[$i]['name']=$r->name;
            $arr_data[$i]['city']=$r->city;
            $arr_data[$i]['phone']=substr($r->phone,0,20);
            $arr_data[$i]['email']=$r->email;
            $arr_data[$i]['status']=$r->status;
            $i++;
        }
        echo json_encode($arr_data);
    }

    public function data_single($id_member){
        $data = $this->db->query("SELECT * FROM mst_member WHERE id_member='$id_member'")->result();
        echo json_encode($data);
    }

    public function add(){
		// $data['rows'] = $this->db->query("SELECT id_member FROM mst_member WHERE SUBSTRING(id_member,4,2)= SUBSTRING(date_added,6,2) ORDER BY id_member DESC LIMIT 1")->row();
		// $data['get_country'] = $this->Mcountry->get_country();
		// $data['get_state'] = $this->Mcountry->get_state();
        $data['get_country'] = $this->Mcountry->getCountry();
        $data['get_state'] = $this->Mcountry->getState();
		$this->load->view('backend/member/add', $data);
    }
	 public function deletePhoto(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
        $file = './assets/uploads/img/member/'.$data['filename'];

        if (!unlink($file)){
            echo 0;
        }else{
            echo 1;
        }
    }
	public function aktif(){
        $data = (array)json_decode(file_get_contents('php://input'));
        
        /*Convert Object to array*/
        foreach($data as $index => $value){
            // cek data
            $dataNa = $this->db->get_where('mst_member', array('id_member'=>$value))->result_array();
            // kalo ada
            if(!empty($dataNa)){
                $resData[$index] = $dataNa[0]['status']; 
                // Kalo statusnya aktif, ubah ke deaktif
                if($dataNa[0]['status']==1){
                    $query = $this->db->update('mst_member', array('status'=>0), array('id_member'=>$dataNa[0]['id_member']));
                }else{
                    $query = $this->db->update('mst_member', array('status'=>1), array('id_member'=>$dataNa[0]['id_member']));
                }
            }
        };

        if(!empty($resData)){
            print_r($resData);
        }
    }

    public function upload(){
        $this->load->library('upload');

        // var_dump($_FILES['file']);
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/uploads/img/member/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        // $config['max_width']  = '1288'; //lebar maksimum 1288 px
        // $config['max_height']  = '768'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
        $config['overwrite']  = 'true';

        $this->upload->initialize($config);

        // var_dump($this->upload->do_upload('file')); // do_upload pake nama parameter file di attribut name form

        // move_uploaded_file($_FILES['file']['tmp_name'],$config['upload_path'].$_FILES['file']['name']);

        if (!$this->upload->do_upload('file')){
            $error = array('error' => $this->upload->display_errors());
            // print_r($error);
            return false;
        }else{   
            $data = $this->upload->data();
            echo $data['file_name'];
        }
    }
  
    public function insert_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		//Simpan data ke mysql
		
        $val=array(
			'photo' => $data['photo'],
            //'id_member' => 2,
            'name' => $data['name'],
            'gender' => $data['gender'],
            'appellation' => $data['appellation'],
            'first_title' => $data['first_title'],
            'last_title' => $data['last_title'],
            'institution' => $data['institution'],
            'street_address' => $data['street_address'],
            'city' => $data['city'],
            'state' => $data['state'],
            'country' => $data['country'],
            'address_status' => 1,
            'website' => $data['website'],
            'place_of_birth' => $data['place_of_birth'],
            'date_of_birth' => $data['date_of_birth'],
            'notes' => $data['notes'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'social_media' => $data['social_media'],
            'status' => 1
        );
        $this->db->insert('mst_member', $val);
        $result  =  $this->db->insert_id();
        echo($result);

    }
	
	public function edit($id_member)
	{
		$data['rows'] = $this->db->query("SELECT * FROM mst_member WHERE id_member='$id_member'")->row();
		$this->load->view('backend/member/edit', $data);
	}

    public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));

         $id_member = $data['id_member'];

        //Simpan data ke mysql
        $val=array(
            'name' => $data['name'],
            'gender' => $data['gender'],
            'appellation' => $data['appellation'],
            'first_title' => $data['first_title'],
            'last_title' => $data['last_title'],
            'institution' => $data['institution'],
            'street_address' => $data['street_address'],
            'city' => $data['city'],
            'state' => $data['state'],
            'country' => $data['country'],
            'address_status' => $data['address_status'],
            'website' => $data['website'],
            'place_of_birth' => $data['place_of_birth'],
            'date_of_birth' => $data['date_of_birth'],
            'photo' => $data['photo'],
            'notes' => $data['notes'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'social_media' => $data['social_media'],
            'status' => $data['status'],
            'date_modify' => $data['date_modify']
        );

        $this->db->where('id_member', $id_member);
        $this->db->update('mst_member', $val);

    }

    public function getCountry(){
        $get_country = $this->Mcountry->getCountry();

        echo json_encode($get_country);
    }

    public function getState(){
        $get_state = $this->Mcountry->getState();

        echo json_encode($get_state);
    } 

}
?>