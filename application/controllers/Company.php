<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class Company extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
    }
  
    public function index() {
		$data['rows'] = $this->db->query("SELECT * FROM mst_company_data")->row();
        $this->load->view('backend/company/view',$data);
    }

    public function getCompanyData() {
        echo ( json_encode( $data['rows'] = $this->db->query("SELECT * FROM mst_company_data")->row()));
    
    }
	
    public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));

        //Simpan data ke mysql
        $val=array(
            'phone_number' => $data['phone_number'],
            'fax_number' => $data['fax_number'],
            'mailing_address' => $data['mailing_address'],
            'street_address' => $data['street_address'],
            'email_company' => $data['email_company'],
            'contact_person' => $data['contact_person'],
            'phone_cp' => $data['phone_cp'],
            'email_cp' => $data['email_cp'],
            'notes' => $data['notes']
//            'date_modify' => date('d-m-y')
        );

        $this->db->update('mst_company_data', $val);

    }

}
?>