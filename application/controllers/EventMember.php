<?php 
if(!defined('BASEPATH'))exit('No direct script access allowed');

class EventMember extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Mcountry');
		$this->load->model('Mdetail');
		$this->load->model('Mcategory');
    }
  
    public function index() {
        $this->load->view('backend/eventmember/view');
    }
  
    public function data_angularnya(){
        $dt=$this->db->query("SELECT * from trs_choir 
        JOIN trs_dtl_choir_event_choir ON trs_choir.id_trs_choir = trs_dtl_choir_event_choir.id_trs_choir 
        join trs_choir_event on trs_choir_event.id_trs_choir_event = trs_dtl_choir_event_choir.id_trs_choir_event 
							  ")->result();
        $arr_data=array();
        $i=0;
        foreach($dt as $r){
            $arr_data[$i]['id_trs_choir_event']=$r->id_trs_choir_event;
            $arr_data[$i]['title']=$r->title;
            $arr_data[$i]['choir_name']=$r->choir_name;
            $arr_data[$i]['id_trs_choir']=$r->id_trs_choir;
            $arr_data[$i]['city']=$r->city;
            $arr_data[$i]['host']=$r->host;
            $arr_data[$i]['PIC']=$r->PIC;
            $i++;
        }
        echo json_encode($arr_data);
    }

    public function data_single($id_trs_choir,$id_trs_choir_event){
        $data = $this->db->query("SELECT * FROM trs_nested_dtl_choir_event INNER JOIN trs_choir ON trs_choir.id_trs_choir=trs_nested_dtl_choir_event.id_trs_choir
		INNER JOIN trs_choir_event ON trs_choir_event.id_trs_choir_event = trs_nested_dtl_choir_event.id_trs_choir_event INNER JOIN trs_dtl_choir_mem ON trs_dtl_choir_mem.id_member = trs_nested_dtl_choir_event.id_member WHERE trs_nested_dtl_choir_event.id_trs_choir='$id_trs_choir' AND trs_nested_dtl_choir_event.id_trs_choir_event='$id_trs_choir_event'");
        $data = $data->result();
        echo json_encode($data);
    }
	
    public function add(){
//		$data['rows'] = $this->db->query("SELECT id_dtl_choir FROM dtl_choir WHERE SUBSTRING(id_dtl_choir,4,2)= SUBSTRING(date_added,6,2) ORDER BY id_dtl_choir DESC LIMIT 1")->row();
		$data['get_country'] = $this->Mcountry->get_country();
		$data['get_state'] = $this->Mcountry->get_state();
		$data['get_member'] = $this->Mdetail->get_member();
		$data['get_grade'] = $this->Mdetail->get_grade();
		$data['get_kategori'] = $this->Mcategory->get_choircategoryAktif();
		$this->load->view('backend/choireventdetail/add', $data);
    }
  
    public function insert_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));
		
        $val=array(
            'photo' => 'default.jpg',
            'title' => $data['title'],
            'grade' => $data['grade'],
            'host' => $data['host'],
            'country' => $data['country'],
            'state' => $data['state'],
            'city' => $data['city'],
            'host' => $data['host'],
            'PIC' => $data['PIC'],
            'date_start' => $data['date_start'],
            'date_finish' => $data['date_finish'],
            'notes' => $data['notes'],
            'status' => 1
        );
        $this->db->insert('trs_choir_event', $val);
        $last_id = $this->db->insert_id();

        // delete dulu, trrus insert

        // ini contoh inputna
        // isi table trs_choir_dtl_cat ada record id_trs_choir_dtl_cat, id_trs_choir & id_choir_category
        // looping insert banyak kategori

}
	public function getdetailMem($id_trs_choir,$id_trs_choir_event){
		$result = $this->db->query("SELECT * FROM trs_nested_dtl_choir_event INNER JOIN mst_member ON trs_nested_dtl_choir_event.id_member = mst_member.id_member WHERE trs_nested_dtl_choir_event.id_trs_choir = '$id_trs_choir' AND trs_nested_dtl_choir_event.id_trs_choir_event='$id_trs_choir_event'")->result_array();
		echo json_encode($result);
	}
	public function edit($id_trs_choir,$id_trs_choir_event)
	{
        
        $data['rows'] = $this->db->query("SELECT * from trs_choir 
            JOIN trs_dtl_choir_event_choir ON trs_choir.id_trs_choir = trs_dtl_choir_event_choir.id_trs_choir 
            join trs_choir_event on trs_choir_event.id_trs_choir_event = trs_dtl_choir_event_choir.id_trs_choir_event  
            WHERE trs_choir.id_trs_choir='". $id_trs_choir ."' AND  trs_dtl_choir_event_choir.id_trs_choir_event = '" . $id_trs_choir_event . "' limit 0,1")->row();
		$this->load->view('backend/eventmember/edit', $data);
	}
	
	public function getAllMemOn($id_trs_choir,$id_trs_choir_event){
        $result = $this->db->query("SELECT * FROM trs_nested_dtl_choir_event INNER JOIN mst_member ON trs_nested_dtl_choir_event.id_member = mst_member.id_member WHERE trs_nested_dtl_choir_event.id_trs_choir = '$id_trs_choir' AND trs_nested_dtl_choir_event.id_trs_choir_event='$id_trs_choir_event'")->result_array();

        echo json_encode($result);
    }
	public function getAllMemChoirSelected($id_trs_choir){
		$selected =  $this->db->query("SELECT DISTINCT * FROM trs_nested_dtl_choir_event INNER JOIN mst_member ON mst_member.id_member = trs_nested_dtl_choir_event.id_member WHERE trs_nested_dtl_choir_event.id_trs_choir = '$id_trs_choir' GROUP BY trs_nested_dtl_choir_event.id_member")->result();
		echo json_encode($selected);
    }
	

    public function update_data(){
        //Ambil data dari method POST angular
        $data = (array)json_decode(file_get_contents('php://input'));

        $id_trs_choir = $data['id_trs_choir'];
        $id_trs_choir_event = $data['id_trs_choir_event'];

        //Simpan data ke mysql
		$this->db->query("DELETE FROM trs_nested_dtl_choir_event WHERE id_trs_choir='$id_trs_choir' AND id_trs_choir_event='$id_trs_choir_event'");
		for($i=0;$i<count($data['dataMember']);$i++){
         $datanya = array(
                 'id_trs_choir_event' => $id_trs_choir_event,
                 'id_trs_choir' => $id_trs_choir,
                 'id_member' => $data['dataMember'][$i]->id
             );  
             $this->db->insert('trs_nested_dtl_choir_event',$datanya);
         }
    }
	public function getOneCandj($id=0){
        $res = $this->db->get_where('mst_candj',array('id_candj'=> $id))->result_array();
        echo json_encode($res);
    }
	public function getOneCom($id=0){
        $res = $this->db->get_where('mst_committee',array('id_committee'=> $id))->result_array();
        echo json_encode($res);
    }
	public function getOneChoir($id=0){
        $res = $this->db->get_where('trs_choir',array('id_trs_choir'=> $id))->result_array();
        echo json_encode($res);
    }
	public function getAllCandjChoirEvent(){
       echo json_encode($this->Mdetail->get_candjAktif());
    }
	public function getAllChoirChoirEvent(){
       echo json_encode($this->Mdetail->get_choirAktif());
    }
	public function getAllComChoirEvent(){
       echo json_encode($this->Mdetail->get_comAktif());
    }

}
?>