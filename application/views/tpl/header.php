<header class="demo-header mdl-layout__header mdl-color--white mdl-color--grey-100 mdl-color-text--grey-600">
  <div class="mdl-layout__header-row">
    <span class="mdl-layout-title"><a href="#/dashboard">Bandung Choral Society</a> {{pageTitle ? ' / ' + pageTitle : ''}}</span>
    <div class="mdl-layout-spacer"></div>
    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" id="hdrbtn">
      <i class="material-icons">more_vert</i>
    </button>
    <ul class="mdl-menu mdl-js-menu mdl-js-ripple-effect mdl-menu--bottom-right" for="hdrbtn">
      <a href="#/Active"><li class="mdl-menu__item">Settings</li>
      <a href="Admin/logout"><li class="mdl-menu__item">Logout</li></a>
    </ul>
  </div>  
</header>
