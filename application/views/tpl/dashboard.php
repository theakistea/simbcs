<section ng-controller="DashboardController">

  <div class="demo-header-color relative clear " style="min-height: 220px;">
    
    <div class="p-20">
      <h4 class="mdl-color-text--white m-t-20 m-b-5">Dashboard</h4>
      <h5 class="mdl-color-text--white m-b-25 no-m-t w100">Selamat datang di Sistem Informasi Management Event Bandung Choral Society</h5>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing mdl-grid-p-15 cards-top" style="padding-bottom: 0px;">
    <div class="mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet p-r-10-tablet">
      <div class="mdl-card mdl-shadow--0dp  mdl-color--cyan">
        <div class="mdl-card__title block">
          <h4 class="mdl-card__title-text mdl-color-text--white f15 w600">Member</h4>
          <div class="f11 mdl-color-text--white">Jumlah Member aktif di Bandung Choral Society</div>
        </div>
        <div class="p-15">
          <center><h1 style="color:#fff;font-size:100px"><?php echo $jml_member; ?></h1></center>
        </div>
      </div>
    </div>
    <div class="mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet p-l-10-tablet">
      <div class="mdl-card mdl-shadow--0dp  mdl-color--pink-300">
        <div class="mdl-card__title block">
          <h4 class="mdl-card__title-text mdl-color-text--white f15 w600">Choir</h4>
          <div class="f11 mdl-color-text--white">Jumlah Choir aktif di Bandung Choral Society</div>
        </div>
        <div class="p-15">
          <center><h1 style="color:#fff;font-size:100px"><?php echo $jml_choir; ?></h1></center>
        </div>
      </div>
    </div>
    <div class="mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet">
      <div class="mdl-card mdl-shadow--0dp  mdl-color--amber">
        <div class="mdl-card__title block">
          <h4 class="mdl-card__title-text mdl-color-text--white f15 w600">Event</h4>
          <div class="f11 mdl-color-text--white">Jumlah Event aktif di Bandung Choral Society</div>
        </div>
        <div class="p-15">
          <center><h1 style="color:#fff;font-size:100px"><?php echo $jml_event; ?></h1></center>
        </div>
      </div>
    </div>
  </div>
</section>
