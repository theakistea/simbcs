<div class="demo-drawer mdl-layout__drawer mdl-color-text--blue-grey-50 mdl-color--grey-100">
  <header class="mdl-color--pink mdl-color-text--blue-grey-900">

    <div class="brand-logo">
      <div class="logo">
        <img src="<?php echo base_url() ?>assets/img/logobcs.png" style="align:center; position:relative; padding-bottom:60px; padding-top:10px; margin-left:-50px;">
      </div>
    </div>

    <div class="clear">
      <div class="f-left m-l-30 m-r-10">
        <h5 class="mdl-color-text--white m-t-5 no-m-b"><?php echo $this->session->userdata('firstName'); ?> <?php echo $this->session->userdata('lastName'); ?></h4>
        <h6 class="mdl-color-text--white m-t-5 no-m-b"><?php echo $this->session->userdata('email'); ?></h6>
      </div>
    </div>
    <div class="demo-avatar-dropdown">
      <div class="mdl-layout-spacer"></div>
    </div>
  </header>

  <ul ml-menu close-others="false" class="demo-navigation mdl-navigation">
    <ml-menu-item>
      <a class="mdl-navigation__link" href="#/"><i class="mdl-color-text--blue-grey-400 material-icons">dashboard</i>Dashboard</a>
    </ml-menu-item>
    <ml-menu-group path="ui-elements">
      <ml-menu-group-heading>
        <i class="mdl-color-text--blue-grey-400 material-icons">book</i>Master Data
      </ml-menu-group-heading>
      <ml-menu-item><a class="mdl-navigation__link" href="#/Choir">Choir Category</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/Candj">Clinnicians and Juries</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/Committee">Committee</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/Composer">Composer and Arranger</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/Company">Company</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/Conductor">Conductor</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/Event">Event Grade</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/Manager">Manager</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/Member">Member</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/Musician">Musician</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/Officials">Officials</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/Songs">Songs</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/SongsCategory">Songs Category</a></ml-menu-item>
    </ml-menu-group>

    <ml-menu-group path="forms">
      <ml-menu-group-heading>
        <i class="mdl-color-text--blue-grey-400 material-icons">work</i>Management
      </ml-menu-group-heading>
      <ml-menu-item><a class="mdl-navigation__link" href="#/ChoirEventDetail">Choir Event</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/ChoirDetail">Choir Registration</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/EventMember">Event Member</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/IndividualEventDetail">Individual Event</a></ml-menu-item>
    </ml-menu-group>

    <ml-menu-group path="tables">
      <ml-menu-group-heading>
        <i class="mdl-color-text--blue-grey-400 material-icons">format_align_left</i>Participans Detail
      </ml-menu-group-heading>
      <ml-menu-item><a class="mdl-navigation__link" href="#/EventPackageA">Event Package A</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/EventPackageB">Event Package B</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/EventActivity1">Event Activity 1</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/EventActivity2">Event Activity 2</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/EventActivity3">Event Activity 3</a></ml-menu-item>
    </ml-menu-group>

    <ml-menu-group path="maps">
      <ml-menu-group-heading>
        <i class="mdl-color-text--blue-grey-400 material-icons">email</i>Packing Letters
      </ml-menu-group-heading>
      <ml-menu-item><a class="mdl-navigation__link" href="#/PackingToEmail">Packing to Email</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/PackingToPost">Packing to POST (Expedition)</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/PackingToSms">Packing to SMS</a></ml-menu-item>
    </ml-menu-group>

    <ml-menu-group path="layouts">
      <ml-menu-group-heading>
        <i class="mdl-color-text--blue-grey-400 material-icons">chrome_reader_mode</i>History Report
      </ml-menu-group-heading>
      <ml-menu-item><a class="mdl-navigation__link" href="#/ReportChoir">Choir</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/ReportConductor">Conductor</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/ReportMember">Member</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/ReportMusician">Musician</a></ml-menu-item>
    </ml-menu-group>

    <ml-menu-group path="extras">
      <ml-menu-group-heading>
        <i class="mdl-color-text--blue-grey-400 material-icons">settings</i>Settings
      </ml-menu-group-heading>
      <ml-menu-item><a class="mdl-navigation__link" href="#/Active" >Activate/Deactivate</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="" >Transfer</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/Letter" >Type of Letter</a></ml-menu-item>
    </ml-menu-group>
	
	<ml-menu-group path="tables">
      <ml-menu-group-heading>
        <i class="mdl-color-text--blue-grey-400 material-icons">build</i>Maintenance
      </ml-menu-group-heading>
      <ml-menu-item><a class="mdl-navigation__link" href="">Backup/Restore</a></ml-menu-item>
      <ml-menu-item><a class="mdl-navigation__link" href="#/User">User Management</a></ml-menu-item>
    </ml-menu-group>
  </ul>
</div>