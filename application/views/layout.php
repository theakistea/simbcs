<!DOCTYPE html>
<html lang="en" ng-app="material-lite">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Material Lite Angular Admin Theme">
  <meta name="author" content="Theme Guys - The Netherlands">

  <title>SIM Event BCS</title>

  <link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/icon.png">

  <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- build:css css/vendors.min.css -->
  <link href="<?php echo base_url() ?>assets/bower_components/angular-ui-select/dist/select.css" rel="stylesheet" />
  <link href="<?php echo base_url() ?>assets/css/select2.css" rel="stylesheet"><!-- Required by angular-ui-select -->
	
  <link href="<?php echo base_url() ?>assets/bower_components/ng-wig/dist/css/ng-wig.css" rel="stylesheet" /><!-- Text editor -->
  <link href="<?php echo base_url() ?>assets/bower_components/ng-table/dist/ng-table.css" rel="stylesheet" />
  <link href="<?php echo base_url() ?>assets/bower_components/angular-bootstrap/ui-bootstrap-csp.css" rel="stylesheet" />
  <link href="<?php echo base_url() ?>assets/bower_components/pikaday/css/pikaday.css" rel="stylesheet" />
  <link href="<?php echo base_url() ?>assets/bower_components/c3/c3.css" rel="stylesheet" />
  <link href="<?php echo base_url() ?>assets/bower_components/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="<?php echo base_url() ?>assets/bower_components/animate.css/animate.css" rel="stylesheet" />
  <!-- endbuild -->
  <!-- build:css css/demo.min.css -->
  <link href="<?php echo base_url() ?>assets/css/material-lite-demo.css" rel="stylesheet">
  <!-- <link href="<?php echo base_url() ?>assets/css/modal.css" rel="stylesheet"> -->
  <link href="<?php echo base_url() ?>assets/css/helpers.css" rel="stylesheet">
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/modal-style.css"> -->
  <!-- endbuild -->

  <style>
  
  
			.dn-timepicker-popup {
	            max-height: 300px;

	            overflow-y: scroll;
	        }
          </style>

  <!-- Jquery -->
  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/jquery/dist/jquery.min.js"></script> 

  <!-- Bootstrap -->
  <link href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

  <!-- Modal bootstrap -->
  <!-- <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/bootstrap/js/modal.js"></script> -->

  <!-- IE Compatibility shims -->
  <!--[if lt IE 9]>
  <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js""></script>
  <![endif]-->
  <!--[if IE]>
  <script src="//cdnjs.cloudflare.com/ajax/libs/es5-shim/4.1.7/es5-shim.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/classlist/2014.01.31/classList.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/flexie/1.0.3/flexie.min.js"></script>
  <![endif]-->
  <!-- end shims -->

</head>

<body ng-controller="MainController">
  <div id="app" class="app" ng-include="'Admin/app'"></div>

  <!--@grep release:s-->
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-62479268-2', 'auto');
    ga('send', 'pageview');
  </script>
  <!--@grep release:e-->

  <!-- build:js js/vendors.min.js -->
  
  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/angular/angular.js"></script>

  <script src="<?php echo base_url() ?>assets/ng-tags-input.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/ng-tags-input.min.css">
  <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dialog-polyfill/0.4.2/dialog-polyfill.min.css"> -->
  <!-- <link rel="stylesheet" type="text/css" href="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.indigo-pink.min.css"> -->

  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/FileSaver.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/material-design-lite/material.js"></script>

  <!-- <script charset="utf-8" src="https://cdnjs.cloudflare.com/ajax/libs/dialog-polyfill/0.4.2/dialog-polyfill.min.js"></script> -->

  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/angular-route/angular-route.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/angular-animate/angular-animate.js"></script>

  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/angular-ui-select/dist/select.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/angular-sanitize/angular-sanitize.js"></script><!-- Required by angular-ui-select -->

  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/angular-local-storage/dist/angular-local-storage.js"></script><!-- Required by todo module -->

  <!-- <script charset="utf-8" src="bower_components/lodash/lodash.min.js"></script> Required by angular google maps -->
  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/angular-simple-logger/dist/angular-simple-logger.js"></script><!-- Required by angular google maps -->
  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/angular-google-maps/dist/angular-google-maps.js"></script>

  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/ng-file-upload/ng-file-upload.js"></script>

  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/ng-table/dist/ng-table.js"></script>

  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/ng-wig/dist/ng-wig.js"></script><!-- Text editor -->

  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/moment/moment.js"></script><!-- Required by pikaday -->
  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/pikaday/pikaday.js"></script><!-- Required by pikaday-angular -->
  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/pikaday-angular/pikaday-angular.js"></script><!-- Datepicker -->

  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/d3/d3.js"></script><!-- Charts -->
  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/c3/c3.js"></script><!-- Charts -->
  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/c3-angular/c3-angular.min.js"></script><!-- C3 Chart directives -->

  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/angulargrid/angulargrid.js"></script>

  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/vendors/angular-placeholders.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/vendors/angular-mdl.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/bower_components/checklist-model/checklist-model.js"></script>
  
  <!-- Time Picker -->
  <script src="<?php echo base_url() ?>assets/bower_components/angular-bootstrap/ui-bootstrap.js"></script>
  <script src="<?php echo base_url() ?>assets/bower_components/angular-timepicker/dist/angular-timepicker.js"></script>
  <script src="<?php echo base_url() ?>assets/bower_components/angular-dateParser/dist/angular-dateParser.js"></script>

  
  <!-- CKEditor -->
  <script src="<?php echo base_url() ?>assets/bower_components/ckeditor/ckeditor.js"></script>
  <script src="<?php echo base_url() ?>assets/bower_components/ng-ckeditor/ng-ckeditor.js"></script>
  
  
  <!-- endbuild -->
  <!-- build:js js/demo.min.js -->
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/app.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/app.route.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/app.config.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/app.constants.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/controllers/main.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/controllers/dashboard.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/controllers/todo.js"></script>

  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/controllers/ui-elements/loading.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/controllers/gallery.js"></script>

  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/controllers/forms/advanced-elements/select.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/controllers/forms/advanced-elements/upload.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/controllers/forms/advanced-elements/text-editor.js"></script>

  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/controllers/maps/clickable-map.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/controllers/maps/searchable-map.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/controllers/maps/zoomable-map.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/controllers/maps/styled-map.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/controllers/maps/full-map.js"></script>

  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/controllers/charts.js"></script>

  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/directives/dynamic-color.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/directives/header.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/demo/directives/sidebar.js"></script>

  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/modules/chat.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/modules/menu.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/modules/svg-map.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/modules/todo.js"></script>

  <script charset="utf-8" src="<?php echo base_url() ?>assets/js/directives/sticky.js"></script>
  <!-- endbuild -->
  <!--<script src="//localhost:35729/livereload.js"></script>--><!--@grep demo--><!--@grep release-->
	
  <!-- Get Data -->
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-choir.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-composer.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-event.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-songs.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-songscategory.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-candj.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-committee.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-conductor.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-manager.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-member.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-officials.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-musician.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-event-package-a.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-event-package-b.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-event-activity-1.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-event-activity-2.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-event-activity-3.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-packing-to-email.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-packing-to-post.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-packing-to-sms.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-user.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-letter.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-active.js"></script>
  <!-- Detail -->
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-choireventdetail.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-eventmember.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-individualeventdetail.js"></script>
  <!-- Report -->
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-reportchoir.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-reportconductor.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-reportmember.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/data/tabel-reportmusician.js"></script>
  <!-- End Get Data -->

  <!-- Insert Data -->
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-choir.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-composer.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-event.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-songs.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-songscategory.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-candj.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-committee.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-conductor.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-manager.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-member.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-officials.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-musician.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-event-package-a.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-event-package-b.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-event-activity-1.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-event-activity-2.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-event-activity-3.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-user.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-letter.js"></script>
  <!-- Detail -->
  <script charset="utf-8" src="<?php echo base_url() ?>assets/add/add-choireventdetail.js"></script>
  <!-- End Insert Data -->

  <!-- Update Data -->
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-choir.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-composer.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-company.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-event.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-songs.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-songscategory.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-candj.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-committee.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-conductor.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-manager.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-member.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-officials.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-musician.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-event-package-a.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-event-package-b.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-event-activity-1.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-event-activity-2.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-event-activity-3.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-packing-to-email.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-packing-to-post.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-user.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-letter.js"></script>
  
  <!-- Detail -->
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-choireventdetail.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/edit/edit-eventmember.js"></script>
  <!-- End Insert Data -->



  <script charset="utf-8" src="<?php echo base_url() ?>assets/choir_detail/add-choirdetail.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/choir_detail/edit-choirdetail.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/choir_detail/tabel-choirdetail.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/choir_detail/modals/conductor_modal.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/choir_detail/modals/lattest_choir_modal.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/choir_detail/modals/manager_modal.js"></script>
  <script charset="utf-8" src="<?php echo base_url() ?>assets/choir_detail/modals/song_modal.js"></script>

  
  <script charset="utf-8" src="<?php echo base_url() ?>assets/songs/category_modal.js"></script>

</body>
</html>
