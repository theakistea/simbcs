<section ng-controller="UpdateDataPackingToPostController" class="text-fields">
  <div class="mdl-color--amber ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Event Member</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Edit Data</h3>
          <p>An overview of basic form styles and elements.</p>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/PackingToPost">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form>
			  <!--input type="hidden" value="<?php// echo $rows->id_trs_choir_event ?>" name="id_trs_choir_event">
			  <input type="hidden" value="<?php //echo $rows->id_trs_choir ?>" name="id_trs_choir"-->
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <label style="color:blue;">Event Name</label><br>
			{{title}}
              </div>
			  <div class="p-20--small" ng-controller="SelectController">
                <p style="color:blue;"><b>Members</b></p>

                <div>
                <table ng-table="tableParams" template-pagination="custom/pager" id="memberTable" class="table mdl-data-table">
                  <tr ng-repeat="item in data">
                    <td data-title="'NO'">{{$index + 1}}</td>
                    <td data-title="'MEMBER'">{{item.name}}</td>
                    <td data-title="'EMAIL'">{{item.email}}</td>
                    <td data-title="'CHOIR NAME'">{{item.choir_name}}</td>
                    <!-- <td data-title="'Action'" sortable="'action'">{{item.action}}</td> -->
                  </tr>
                </table>
        </div>
      </div>
		<br>
<div class="mdl-textfield mdl-js-textfield">
                <p>Template</p>
                <select 
                  class="mdl-textfield__input" 
                  id="template" name="template"
                  ng-model="formData.template">
<option value="">--Select Template--</option>
<?php foreach($template as $rows){ ?>
<option value="<?php echo $rows->id_letter; ?>"><?php echo $rows->template_name; ?></option>
<?php } ?>
                </select>
              </div>
				<a  class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" ng-click="send_to_email()">
					Send
				</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>