<section ng-controller="InsertDataChoirDetailController" class="text-fields">
    <div class="mdl-color--blue ml-header relative clear">
        <div class="p-20">
            <h3 class="mdl-color-text--white m-t-20 m-b-5">Choir's Registration Detail</h3>
        </div>
    </div>

    <div class="mdl-grid mdl-grid--no-spacing">

        <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
            <div class="p-40 p-r-20 p-20--small">
                <div class=" mdl-color-text--blue-grey-400">
                    <h3><i class="material-icons f-left m-r-5">format_align_left</i> Add Data</h3>
                    <div class="m-t-30">
                        <ul class="list-bordered">
                            <li>
                                <a href="#/ChoirDetail">
                                    <i class="material-icons m-r-5 f11">arrow_back</i>
                                    Back to Data
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
            <div class="p-20 ml-card-holder ml-card-holder-first">
                <div class="mdl-card mdl-shadow--1dp">
                    <div class="p-30">
                        <form name="myform">


                            <div class="mdl-textfield mdl-js-textfield">
                                <p>Choir Event</p>
                                <select class="mdl-textfield__input" id="choir_event" ng-model="choir_event"
                                        ng-change="getEventCode()">
                                    <option value="">- Select Choir -</option>
                                    <option ng-repeat="event in events track by event.id_trs_choir_event"
                                            value="{{event.id_trs_choir_event}}">{{event.title}}
                                    </option>
                                </select>
                            </div>
                           <span
                                        data-toggle="modal" data-target="#modalInsChoir"
                                        data-upgraded=",MaterialButton,MaterialRipple"
                                        class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                                <i class="material-icons">search</i>
                                <span class="mdl-button__ripple-container">
                                <span
                                    style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);"
                                    class="mdl-ripple is-animating">
                                </span>
                                </span>
                            </span>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" name="choir_name" id="choir_name"
                                       ng-model="choir_name" required/>
                                <label class="mdl-textfield__label" for="choir_name">Choir Name</label>
                            </div>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" name="institution" id="institution"
                                       ng-model="institution"/>
                                <label class="mdl-textfield__label" for="institution">Institution</label>
                            </div>

                            <div class="p-20--small">

                                <p>Categories
                                    <span
                                        data-toggle="modal" data-target="#modalKategori"
                                        data-upgraded=",MaterialButton,MaterialRipple"
                                        class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                    <i class="material-icons">add</i>
                    <span class="mdl-button__ripple-container">
                      <span
                          style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);"
                          class="mdl-ripple is-animating">
                      </span>
                    </span>
                </span>

                                    <span
                                        data-toggle="modal" data-target="#modalInsKategori"
                                        data-upgraded=",MaterialButton,MaterialRipple"
                                        class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                    <i class="material-icons">search</i>
                    <span class="mdl-button__ripple-container">
                      <span
                          style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);"
                          class="mdl-ripple is-animating">
                      </span>
                    </span>
                </span>
                                </p>
                                <div class="">
                                    <table ng-table="tableParams" template-pagination="custom/pager" id="kategoriTable"
                                           class="table mdl-data-table">
                                        <tr ng-repeat="item in $data">
                                            <td data-title="'NO'">No</td>
                                            <td data-title="'Nama Kategori'">{{item.nama_kategori}}</td>
                                            <td data-title="'Tipe Kategori '">{{item.type}}</td>
                                            <td data-title="'Kode Registrasi '">{{item.code_registrasi}}</td>
                                            <!-- <td data-title="'Action'" sortable="'action'">{{item.action}}</td> -->
                                        </tr>
                                        <tr ng-repeat="kat in kategoriChoir" class="eusi">
                                            <td>{{$index + 1 }}</td>
                                            <td>{{kat.name}}</td>
                                            <td>{{kat.type}}</td>
                                            <td>{{code_event}}/{{kat.type_prefix}} - <input type="text"
                                                                                            ng-model="kat.code_registrasi"
                                                                                            ng-change="updateCodeRegistrasi()"/>
                                                /{{kat.code_category}}
                                            </td>
                                        </tr>
                                    </table>
                                    <br>
                                    <button ng-click="resetCat()" class="mdl-button mdl-js-button mdl-button--accent">
                                        Reset
                                    </button>
                                </div>
                            </div>
                            <br><br>

                            <div class="p-20--small" ng-controller="SelectController">
                                <p>Members
                                    <span
                                        data-toggle="modal" data-target="#modalMember"
                                        data-upgraded=",MaterialButton,MaterialRipple"
                                        class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                    <i class="material-icons">add</i>
                    <span class="mdl-button__ripple-container">
                      <span
                          style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);"
                          class="mdl-ripple is-animating">
                      </span>
                    </span>
                </span>
                                    <span
                                        data-toggle="modal" data-target="#modalInsMem"
                                        data-upgraded=",MaterialButton,MaterialRipple"
                                        class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                    <i class="material-icons">search</i>
                    <span class="mdl-button__ripple-container">
                      <span
                          style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);"
                          class="mdl-ripple is-animating">
                      </span>
                    </span>
                </span>
                                </p>

                                <div>
                                    <table ng-table="tableParams" template-pagination="custom/pager" id="memberTable"
                                           class="table mdl-data-table">
                                        <tr ng-repeat="item in $data">
                                            <td data-title="'NO'">No</td>
                                            <td data-title="'MEMBER'">{{item.name}}</td>
                                            <!-- <td data-title="'Action'" sortable="'action'">{{item.action}}</td> -->
                                        </tr>
                                        <tr ng-repeat="mem in dataMember" class="eusiMember">
                                            <td>{{$index+1}}</td>
                                            <td>{{mem.name}}</td>
                                        </tr>
                                    </table>
                                    <button ng-click="resetMember()"
                                            class="mdl-button mdl-js-button mdl-button--accent">Reset
                                    </button>
                                </div>
                            </div>
                            <br><br>

                            <div class="p-20--small" ng-controller="SelectController">
                                <p>Conductors
                                    <span
                                        data-toggle="modal" data-target="#modalInsKon"
                                        data-upgraded=",MaterialButton,MaterialRipple"
                                        class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                        <i class="material-icons">search</i>
                        <span class="mdl-button__ripple-container">
                          <span
                              style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);"
                              class="mdl-ripple is-animating">
                          </span>
                        </span>
                    </span>
                                </p>
                                <div>
                                    <table ng-table="tableParams" template-pagination="custom/pager" id="conductorTable"
                                           class="table mdl-data-table">
                                        <tr ng-repeat="item in $data">
                                            <td data-title="'NO'">NO</td>
                                            <td data-title="'NAME'" sortable="'nama_konduktor'">
                                                {{item.nama_konduktor}}
                                            </td>
                                            <!-- <td data-title="'Action'" sortable="'action'">{{item.action}}</td> -->
                                        </tr>
                                        <tr ng-repeat="kon in dataKonduktor" class="eusiKonduktor">
                                            <td>{{$index+1}}</td>
                                            <td>{{kon.name}}</td>
                                        </tr>
                                    </table>
                                </div>
                                <button ng-click="resetKonduktor()" class="mdl-button mdl-js-button mdl-button--accent">
                                    Reset
                                </button>
                            </div>
                            <br><br>

                            <div class="p-20--small" ng-controller="SelectController">
                                <p>Managers
                                    <span data-toggle="modal" data-target="#modalInsMan"
                                          data-upgraded=",MaterialButton,MaterialRipple"
                                          class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                                      <i class="material-icons">search</i>
                                      <span class="mdl-button__ripple-container">
                                        <span
                                            style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);"
                                            class="mdl-ripple is-animating">
                                        </span>
                                      </span>
                                  </span>
                                </p>
                                <div>
                                    <table ng-table="tableParams" template-pagination="custom/pager" id="managerTable"
                                           class="table mdl-data-table">
                                        <tr ng-repeat="item in $data">
                                            <td data-title="'NO'">NO</td>
                                            <td data-title="'NAME'" sortable="'nama_kategori'">{{item.nama_kategori}}
                                            </td>
                                            <!-- <td data-title="'Action'" sortable="'action'">{{item.action}}</td> -->
                                        </tr>
                                        <tr ng-repeat="man in dataManager" class="eusiManager">
                                            <td>{{$index+1}}</td>
                                            <td>{{man.name}}</td>
                                        </tr>
                                    </table>
                                </div>
                                <button ng-click="resetManager()" class="mdl-button mdl-js-button mdl-button--accent">
                                    Reset
                                </button>
                            </div>
                            <br><br>

                            <div class="p-20--small" ng-controller="SelectController">
                                <p>Songs
                                    <span
                                        data-toggle="modal" data-target="#modalSongs"
                                        data-upgraded=",MaterialButton,MaterialRipple"
                                        class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                                        <i class="material-icons">search</i>
                                        <span class="mdl-button__ripple-container">
                                            <span style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);"  class="mdl-ripple is-animating">
                                            </span>
                                        </span>
                                    </span>
                                </p>
                                <div>
                                    <table ng-table="tableParams" template-pagination="custom/pager" id="managerTable"
                                           class="table mdl-data-table">
                                        <tr ng-repeat="item in $data">

                                            <td data-title="'NO'">NO</td>
                                            <td data-title="'NAME'" sortable="'song_name'">{{item.title}}</td>
                                            <td data-title="'Jumlah Partitur Diterima'" sortable="'song_name'">{{item.jumlah_partitur_diterima}}</td>
                                            <td data-title="'Surati Izin Komposer'" sortable="'song_name'">{{item.surati_izin_komposer}}</td>
                                            <td data-title="'Durasi'" sortable="'song_name'">{{item.durasi}}</td>
                                            <td data-title="'Persetujuan Artistic Commite'" sortable="'song_name'">{{item.persetujuan_artistic_commite}}</td>
                                            <!-- <td data-title="'Action'" sortable="'action'">{{item.action}}</td> -->
                                        </tr>
                                        <tr ng-repeat="song in dataSongs" class="eusiSong">
                                            <td>{{$index+1}}</td>
                                            <td>{{song.title}}</td>
                                            <td><input type="checkbox"
                                                    ng-model="song.jumlah_partitur_diterima"
                                                    ng-change="updateSongDetail()"
                                                    ng-true-value="1"
                                                    ng-false-value="0"/></td>
                                            <td><input type="checkbox"
                                                    ng-model="song.surati_izin_komposer"
                                                    ng-change="updateSongDetail()"
                                                    ng-true-value="1"
                                                    ng-false-value="0"/></td>
                                            <td><input type="text"
                                                    ng-model="song.durasi"
                                                    ng-change="updateSongDetail()"/></td>
                                            <td><input type="checkbox"
                                                    ng-model="song.persetujuan_artistic_commite"
                                                    ng-change="updateSongDetail()"
                                                    ng-true-value="1"
                                                    ng-false-value="0"/></td>
                                        </tr>
                                    </table>
                                </div>
                                <button ng-click="resetSong()" class="mdl-button mdl-js-button mdl-button--accent">
                                    Reset
                                </button>
                            </div>


                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <textarea class="mdl-textfield__input" type="text" rows="4" id="street_address"
                                          ng-model="street_address"></textarea>
                                <label class="mdl-textfield__label" for="street_address">Street Address</label>
                            </div>
                            <div class="mdl-textfield mdl-js-textfield">
                                <p>Country</p>
                                <select class="mdl-textfield__input" id="country" ng-model="country">
                                    <option value="">- Select Country -</option>
                                    <?php foreach ($get_country as $rows) { ?>
                                        <option value="<?php echo $rows->name ?>"><?php echo $rows->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="mdl-textfield mdl-js-textfield">
                                <p>State</p>
                                <select class="mdl-textfield__input" id="state" ng-model="state">
                                    <option value="">- Select State -</option>
                                    <?php foreach ($get_state as $rows) { ?>
                                        <option value="<?php echo $rows->name ?>"><?php echo $rows->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="city" ng-model="city"/>
                                <label class="mdl-textfield__label" for="city">City Name</label>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text"
                                       pattern="^\s*[a-zA-Z0-9\--_-.]+@[a-zA-Z0-9\--_-.]+\.[a-zA-Z]{2,4}\s*$" id="email"
                                       ng-model="email"/>
                                <label class="mdl-textfield__label" for="email">Email</label>
                                <span class="mdl-textfield__error">Please supply a valid email address.</span>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <textarea class="mdl-textfield__input" type="text" rows="4" id="notes"
                                          ng-model="notes"></textarea>
                                <label class="mdl-textfield__label" for="notes">Notes</label>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">

                                <label for="has_surat_keterangan_usia"  style="padding-right: 20px;">Surat Keterangan Usia</label>
                                <input type="checkbox" rows="4" id="has_surat_keterangan_usia"
                                       ng-model="has_surat_keterangan_usia" ng-true-value="'1'"
                                       ng-false-value="'0'"></input>
                                <input class="mdl-textfield__input"
                                       type="text"
                                       id="has_surat_keterangan_usia_date"
                                       pikaday="datepicker2"
                                       format="DD-MM-YYYY"
                                       on-select="onPikadaySelect(pikaday, date)"
                                       theme="material-lite" ng-model="has_surat_keterangan_usia_date" name="has_surat_keterangan_usia_date "/>


                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">

                                <label for="has_bukti_registrasi"  style="padding-right: 20px;">Bukti Registrasi</label>
                                <input type="checkbox" rows="4" id="has_bukti_registrasi"
                                       ng-model="has_bukti_registrasi" ng-true-value="'1'" ng-false-value="'0'"></input>
                                <input class="mdl-textfield__input"
                                       type="text"
                                       id="has_bukti_registrasi_date"
                                       pikaday="datepicker2"
                                       format="DD-MM-YYYY"
                                       on-select="onPikadaySelect(pikaday, date)"
                                       theme="material-lite" ng-model="has_bukti_registrasi_date" name="has_bukti_registrasi_date "/>

                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <label for="has_biograpy"  style="padding-right: 20px;">Biograpy</label>
                                <input type="checkbox" rows="4" id="has_biograpy" ng-model="has_biograpy"
                                       ng-true-value="'1'" ng-false-value="'0'"></input>
                                <input class="mdl-textfield__input"
                                       type="text"
                                       id="has_biograpy_date"
                                       pikaday="datepicker2"
                                       format="DD-MM-YYYY"
                                       on-select="onPikadaySelect(pikaday, date)"
                                       theme="material-lite" ng-model="has_biograpy_date" name="has_biograpy_date"/>
                            </div>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <label for="has_audio_record"  style="padding-right: 20px;">Audio Record</label>
                                <input type="checkbox" rows="4" id="has_audio_record" ng-model="has_audio_record"
                                       ng-true-value="'1'" ng-false-value="'0'"></input>
                                <input class="mdl-textfield__input"
                                       type="text"
                                       id="has_audio_record_date"
                                       pikaday="datepicker2"
                                       format="DD-MM-YYYY"
                                       on-select="onPikadaySelect(pikaday, date)"
                                       theme="material-lite" ng-model="has_audio_record_date" name="has_audio_record_date "/>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <p>Photo</p>
                                <img ng-if="photo!=''" src="<?php echo base_url('assets/uploads/img/default.jpg'); ?>"
                                     height="200px" width="200px">
                                <img ng-show="f.progress >= 100"
                                     src="<?php echo base_url('assets/uploads/img/choirdetail/{{photo}}'); ?>"
                                     height="200px" width="200px">
                                <input type="hidden" value="default.jpg" ng-model="photo">
                                <!-- mdl-button mdl-js-button mdl-button--raised mdl-button--colored -->
                                <button
                                    class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect"
                                    style="top:53px;margin-left:10px;" type="file"
                                    ngf-select="uploadFiles($file, $invalidFiles)"
                                    accept="image/*" id="userfile" ngf-max-size="2MB">
                                    Select File
                                </button>

                            </div>
                            <div class="m-t-20">
                                <button ng-click="insert_data()" ng-show="myform.$valid"
                                        type="submit"
                                        class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                                    Save
                                </button>
                                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                                    Reset
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>


<!-- Modal Insert Kategori -->
<div class="modal fade" data-backdrop="" id="modalKategori" tabindex="-1" role="dialog" aria-labelledby="labelKategori"
     ng-controller="InsertCatChoir">
    <div class="modal-dialog" style="margin-top: 100px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="labelKategori">Add Category</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-md-6">
                            Name : <input class="form-control" type="text" ng-model="kategori.nameCat">
                        </div>
                        <div class="col-md-6">
                            Type :
                            <select class="form-control" ng-model="kategori.typeCat">
                                <option value="">-Select Type-</option>
                                <option value="Championship">Championship</option>
                                <option value="Competition">Competition</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            Age : <input class="form-control" type="text" ng-model="kategori.ageCat">
                        </div>
                        <div class="col-md-6">
                            Minimum Singer : <input class="form-control" type="number" ng-model="kategori.minsingCat">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            Max Perform Time : <input class="form-control" type="number" ng-model="kategori.maxperfCat">
                        </div>
                        <div class="col-md-6">
                            Num of Song : <input class="form-control" type="number" ng-model="kategori.numsongCat">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            Num of Acapela Song : <input class="form-control" type="number"
                                                         ng-model="kategori.numacapCat">
                        </div>
                        <div class="col-md-6">
                            With Amplification :
                            <select class="form-control" ng-model="kategori.ampliCat">
                                <option value="">-Select With Aplification-</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            Notes : <textarea class="form-control" ng-model="kategori.notesCat"></textarea>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <!-- <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="statKategori">
                  <input type="checkbox" id="statKategori" class="mdl-switch__input" checked>
                  <span class="mdl-switch__label"></span>
                </label> -->
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" ng-click="saveCat()">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Add Kategori -->
<div class="modal fade" data-backdrop="" id="modalInsKategori" tabindex="-1" role="dialog"
     aria-labelledby="labelKategori" ng-controller="PilihCatChoir">
    <div class="modal-dialog" style="margin-top: 100px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="labelKategori">Choose Category</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-body">
                                <input type="text" ng-model="tableParams.filter()['search']"
                                       class="mdl-textfield__input" placeholder="Search data" autofocus/>
                                <table ng-table="tableParams" template-pagination="custom/pager"
                                       class="table mdl-data-table mdl-data-table--selectable fullwidth">
                                    <tr ng-repeat="item in $data">
                                        <td style="text-align:center">
                                            <input type="checkbox" checklist-model="catData.id_choir_category"
                                                   checklist-value="item.id_choir_category">
                                            <!-- <input type="checkbox" class="mdl-checkbox__input" name="id_candj[]" value="{{item.id_choir_category}}"> -->
                                        </td>
                                        <td width="50" data-title="'ID'">{{$index+1}}</td>
                                        <td data-title="'NAME'" filter="{ 'name': 'text' }" sortable="'name'"
                                            class="mdl-data-table__cell--non-numeric">{{item.name}}
                                        </td>
                                        <td data-title="'TYPE'" filter="{ 'city': 'text' }" sortable="'city'"
                                            class="mdl-data-table__cell--non-numeric">{{item.type}}
                                        </td>
                                        <!-- <td data-title="'STATUS'" sortable="'status'" class="mdl-data-table__cell--non-numeric">
                                          <p ng-if="item.status==1">Aktif</p>
                                          <p ng-if="item.status==0">Tidak Aktif</p>
                                        </td> -->
                                    </tr>
                                    <tr ng-show="loading">
                                        <td colspan="6" style="text-align:center;">
                                            <div id="p2"
                                                 class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
                                        </td>
                                    </tr>
                                </table>

                                <script type="text/ng-template" id="custom/pager">
                                    <div ng-if="params.data.length" class="ml-data-table-pager p-10">
                                        <div ng-if="params.settings().counts.length" class="f-right">
                                            <button ng-class="{'active':params.count() == 10}"
                                                    ng-click="params.count(10)" class="mdl-button">10
                                            </button>
                                            <button ng-class="{'active':params.count() == 25}"
                                                    ng-click="params.count(25)" class="mdl-button">25
                                            </button>
                                            <button ng-class="{'active':params.count() == 50}"
                                                    ng-click="params.count(50)" class="mdl-button">50
                                            </button>
                                            <button ng-class="{'active':params.count() == 100}"
                                                    ng-click="params.count(100)" class="mdl-button">100
                                            </button>
                                        </div>
                                        <span ng-repeat="page in pages"
                                              ng-class="{'disabled': !page.active, 'previous': page.type == 'prev', 'next': page.type == 'next'}"
                                              ng-switch="page.type">
                <button ng-switch-when="prev" ng-click="params.page(page.number)" class="mdl-button">&laquo;</button>
                <button ng-switch-when="first" ng-click="params.page(page.number)" class="mdl-button"><span
                        ng-bind="page.number"></span></button>
                <button ng-switch-when="page" ng-click="params.page(page.number)" class="mdl-button"><span
                        ng-bind="page.number"></span></button>
                <button ng-switch-when="more" ng-click="params.page(page.number)" class="mdl-button">&#8230;</button>
                <button ng-switch-when="last" ng-click="params.page(page.number)" class="mdl-button"><span
                        ng-bind="page.number"></span></button>
                <button ng-switch-when="next" ng-click="params.page(page.number)" class="mdl-button">&raquo;</button>
              </span>
                                    </div>
                                </script>

                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <pre>{{catData.id_choir_category|json}}</pre>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" ng-click="pilih()" data-dismiss="modal">Pilih</button>
            </div>
        </div>
    </div>
</div>



<?php $modals['member'];?>
<?php $modals['manager'];?>
<?php $modals['conductor'];?>
<?php $modals['song'];?>
<?php $modals['lattest_choir'];?>
