<!-- Modal Add Choose Konduktor -->
<div class="modal fade" data-backdrop="" id="modalInsKon" tabindex="-1" role="dialog" aria-labelledby="labelKon"
     ng-controller="PilihKonChoir">
    <div class="modal-dialog" style="margin-top: 100px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="labelMember">Choose Conductor</h4> 
            </div>
            <div class="modal-body">
                <div class="row" ng-if="show == 'list'">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-body">
                                <input type="text" ng-model="tableParams.filter()['search']"
                                       class="mdl-textfield__input" placeholder="Search data" autofocus/>
                                <table ng-table="tableParams" template-pagination="custom/pager"
                                       class="table mdl-data-table mdl-data-table--selectable fullwidth">
                                    <tr ng-repeat="item in $data">
                                        <td style="text-align:center">
                                            <input type="checkbox" checklist-model="konData.id_conductor"
                                                   checklist-value="item.id_conductor">
                                            <!-- <input type="checkbox" class="mdl-checkbox__input" name="id_candj[]" value="{{item.id_choir_category}}"> -->
                                        </td>
                                        <td width="50" data-title="'ID'">{{$index+1}}</td>
                                        <td data-title="'NAME'" filter="{ 'name': 'text' }" sortable="'name'"
                                            class="mdl-data-table__cell--non-numeric">{{item.name}}
                                        </td>
                                        <td data-title="'INSTITUTION'" filter="{ 'institution': 'text' }"
                                            sortable="'institution'" class="mdl-data-table__cell--non-numeric">
                                            {{item.institution}}
                                        </td>
                                        <!-- <td data-title="'STATUS'" sortable="'status'" class="mdl-data-table__cell--non-numeric">
                                          <p ng-if="item.status==1">Aktif</p>
                                          <p ng-if="item.status==0">Tidak Aktif</p>
                                        </td> -->
                                    </tr>
                                    <tr ng-show="loading">
                                        <td colspan="6" style="text-align:center;">
                                            <div id="p2"
                                                 class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
                                        </td>
                                    </tr>
                                </table>
                                <script type="text/ng-template" id="custom/pager">
                                    <div ng-if="params.data.length" class="ml-data-table-pager p-10">
                                        <div ng-if="params.settings().counts.length" class="f-right">
                                            <button ng-class="{'active':params.count() == 10}"
                                                    ng-click="params.count(10)" class="mdl-button">10
                                            </button>
                                            <button ng-class="{'active':params.count() == 25}"
                                                    ng-click="params.count(25)" class="mdl-button">25
                                            </button>
                                            <button ng-class="{'active':params.count() == 50}"
                                                    ng-click="params.count(50)" class="mdl-button">50
                                            </button>
                                            <button ng-class="{'active':params.count() == 100}"
                                                    ng-click="params.count(100)" class="mdl-button">100
                                            </button>
                                        </div>
                                        <span ng-repeat="page in pages"
                                              ng-class="{'disabled': !page.active, 'previous': page.type == 'prev', 'next': page.type == 'next'}"
                                              ng-switch="page.type">
                                            <button ng-switch-when="prev" ng-click="params.page(page.number)" class="mdl-button">&laquo;</button>
                                            <button ng-switch-when="first" ng-click="params.page(page.number)" class="mdl-button"><span
                                                    ng-bind="page.number"></span></button>
                                            <button ng-switch-when="page" ng-click="params.page(page.number)" class="mdl-button"><span
                                                    ng-bind="page.number"></span></button>
                                            <button ng-switch-when="more" ng-click="params.page(page.number)" class="mdl-button">&#8230;</button>
                                            <button ng-switch-when="last" ng-click="params.page(page.number)" class="mdl-button"><span
                                                    ng-bind="page.number"></span></button>
                                            <button ng-switch-when="next" ng-click="params.page(page.number)" class="mdl-button">&raquo;</button>
                                        </span>
                                    </div>
                                </script>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
                <div class ="row" ng-if="show == 'form'">
                    <div class="col-lg-12">
                        <form name='konductor'>
                            <div class="row">
                                <div class="col-md-12">
                                    Name : <input class="form-control" type="text" ng-model="conductorData.name" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    Appellation : 
                                    <select name='gender'  class="form-control" ng-model="conductorData.appellation" required>
                                    <option value="" >Pilih Appellation</option>
                                    <option value="0" >Mr.</option>
                                    <option value="1" >Ms.</option>
                                    <option value="2" >Mrs.</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    Gender : 
                                    <select name='gender' class="form-control" ng-model="conductorData.gender" required>
                                    <option value="" >Pilih Gender</option>
                                    <option value="male" >Male</option>
                                    <option value="female" >Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    First Title : <input class="form-control" type="text" ng-model="conductorData.first_title" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    Last Title : <input class="form-control" type="text" ng-model="conductorData.last_title" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    Street address :  <textarea class="form-control"  ng-model="conductorData.street_address"> {{conductorData.last_title}}</textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    Country : 
                                    <select name='gender' class="form-control" ng-model="conductorData.country" required>
                                    <option value="" >Pilih Country</option>
                                    <option value="country.id"  ng-repeat="country in countrys">{{country.name}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    State : 
                                    <select name='gender' class="form-control" ng-model="conductorData.state" required>
                                    <option value="" >Pilih State</option>
                                    <option value="state.id"  ng-repeat="state in states">{{state.name}}</option>
                                    
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    City Name<input class="form-control" type="text" ng-model="conductorData.city" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    Phone  : <input class="form-control" type="text" ng-model="conductorData.phone" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    Email  : <input class="form-control" type="text" ng-model="conductorData.email" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    Social Media  : <input class="form-control" type="text" ng-model="conductorData.social_media" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    Websites : <input class="form-control" type="text" ng-model="conductorData.website" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    Place Of Brith : <input class="form-control" type="text" ng-model="conductorData.place_of_birth" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                     Date Of Brith : 
                                        <input class="form-control"
                                        type="text"
                                        id="date_of_birth"
                                        pikaday="datepicker2"
                                        format="YYYY-MM-DD"
                                        on-select="onPikadaySelect(pikaday, date)"
                                        theme="material-lite" ng-model="conductorData.date_of_birth" name="date_of_birth "/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    Notes :  <textarea class="form-control"  ng-model="conductorData.notes"> {{conductorData.last_title}}</textarea>
                                </div>
                            </div>
                        </form>

                                
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                    <button type="button" class="btn btn-success" ng-click="saveData()"  ng-if="show== 'form'">Simpan</button>
                    <button type="button" class="btn btn-danger" ng-click="changeForm('list')" ng-if="show== 'form'">Batal</button>
                    <button type="button" class="btn btn-success" ng-click="changeForm('form')" ng-if="show== 'list'">Baru</button>
                    <button type="button" class="btn btn-primary" ng-click="pilih()" data-dismiss="modal"  ng-if="show== 'list'">Pilih</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>