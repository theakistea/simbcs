
<!-- Modal Add Choose Songs -->
<div class="modal fade" data-backdrop="" id="modalSongs" tabindex="-1" role="dialog" aria-labelledby="labelMan"
     ng-controller="PilihSongs">
    <div class="modal-dialog" style="margin-top: 100px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="labelMember">Choose Song</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-body">
                                <input type="text" ng-model="tableParams.filter()['search']"
                                       class="mdl-textfield__input" placeholder="Search data" autofocus/>
                                <table ng-table="tableParams" template-pagination="custom/pager"
                                       class="table mdl-data-table mdl-data-table--selectable fullwidth">
                                    <tr ng-repeat="item in $data">
                                        <td style="text-align:center">
                                            <input type="checkbox" checklist-model="dataSongs.id_songs"
                                                   checklist-value="item.id_songs">
                                            <!-- <input type="checkbox" class="mdl-checkbox__input" name="id_candj[]" value="{{item.id_choir_category}}"> -->
                                        </td>
                                        <td width="50" data-title="'ID'">{{$index+1}}</td>
                                        <td data-title="'NAME'" filter="{ 'name': 'text' }" sortable="'name'"
                                            class="mdl-data-table__cell--non-numeric">{{item.title}}
                                        </td>
                                        <td data-title="'COMPOSER'" filter="{ 'name': 'text' }" sortable="'name'"
                                            class="mdl-data-table__cell--non-numeric">{{item.composer}}
                                        </td>
                                        <!-- <td data-title="'STATUS'" sortable="'status'" class="mdl-data-table__cell--non-numeric">
                                          <p ng-if="item.status==1">Aktif</p>
                                          <p ng-if="item.status==0">Tidak Aktif</p>
                                        </td> -->
                                    </tr>
                                    <tr ng-show="loading">
                                        <td colspan="6" style="text-align:center;">
                                            <div id="p2"
                                                 class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
                                        </td>
                                    </tr>
                                </table>

                                <script type="text/ng-template" id="custom/pager">
                                    <div ng-if="params.data.length" class="ml-data-table-pager p-10">
                                        <div ng-if="params.settings().counts.length" class="f-right">
                                            <button ng-class="{'active':params.count() == 10}"
                                                    ng-click="params.count(10)" class="mdl-button">10
                                            </button>
                                            <button ng-class="{'active':params.count() == 25}"
                                                    ng-click="params.count(25)" class="mdl-button">25
                                            </button>
                                            <button ng-class="{'active':params.count() == 50}"
                                                    ng-click="params.count(50)" class="mdl-button">50
                                            </button>
                                            <button ng-class="{'active':params.count() == 100}"
                                                    ng-click="params.count(100)" class="mdl-button">100
                                            </button>
                                        </div>
                                        <span ng-repeat="page in pages"
                                              ng-class="{'disabled': !page.active, 'previous': page.type == 'prev', 'next': page.type == 'next'}"
                                              ng-switch="page.type">
                <button ng-switch-when="prev" ng-click="params.page(page.number)" class="mdl-button">&laquo;</button>
                <button ng-switch-when="first" ng-click="params.page(page.number)" class="mdl-button"><span
                        ng-bind="page.number"></span></button>
                <button ng-switch-when="page" ng-click="params.page(page.number)" class="mdl-button"><span
                        ng-bind="page.number"></span></button>
                <button ng-switch-when="more" ng-click="params.page(page.number)" class="mdl-button">&#8230;</button>
                <button ng-switch-when="last" ng-click="params.page(page.number)" class="mdl-button"><span
                        ng-bind="page.number"></span></button>
                <button ng-switch-when="next" ng-click="params.page(page.number)" class="mdl-button">&raquo;</button>
              </span>
                                    </div>
                                </script>

                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <pre>{{dataSongs.id_songs|json}}</pre>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" ng-click="pilih()" data-dismiss="modal">Pilih</button>
            </div>
        </div>
    </div>
</div>