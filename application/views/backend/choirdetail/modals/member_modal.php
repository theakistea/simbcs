
<!-- Modal Member -->
<div class="modal fade" data-backdrop="" id="modalMember" tabindex="-1" role="dialog" aria-labelledby="labelMember"
     ng-controller="InsertMemberChoir">
    <div class="modal-dialog" style="margin-top: 100px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="labelKategori">Add Member</h4>
            </div>
            <div class="modal-body">
                <form name="inputMember">
                    <div class="row">
                        <div class="col-md-6">
                            Name : <input class="form-control" type="text" ng-model="member.name" required>
                        </div>
                        <div class="col-md-6">
                            Institution : <input class="form-control" type="text" ng-model="member.institution"
                                                 required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            Gender :
                            <select class="form-control" ng-model="member.gender" required>
                                <option value="">-Select Gender-</option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            Appellation :
                            <select class="form-control" ng-model="member.appellation" required>
                                <option value="">-Select Appellation-</option>
                                <option value="0">Mr.</option>
                                <option value="1">Ms.</option>
                                <option value="2">Mrs.</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            First Title : <input class="form-control" type="text" ng-model="member.first_title"
                                                 required>
                        </div>
                        <div class="col-md-6">
                            Last Title : <input class="form-control" type="text" ng-model="member.last_title" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            Street Address : <textarea class="form-control" required
                                                       ng-model="member.street_address"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            Country :
                            <select class="form-control" ng-model="member.country" required>
                                <option value="">-Select Country-</option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            State :
                            <select class="form-control" ng-model="member.state" required>
                                <option value="">-Select State-</option>
                                <option value="0">Mr.</option>
                                <option value="1">Ms.</option>
                                <option value="2">Mrs.</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            City Name : <input class="form-control" type="text" ng-model="member.city" required>
                        </div>
                        <div class="col-md-6">
                            Phone : <input class="form-control" type="text" ng-model="member.phone" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            Email : <input class="form-control" type="email" ng-model="member.email" required>
                        </div>
                        <div class="col-md-6">
                            Social Media : <input class="form-control" type="text" ng-model="member.social_media"
                                                  required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            Website : <input class="form-control" type="text" ng-model="member.website" required>
                        </div>
                        <div class="col-md-6">
                            Place of Birth :

                            <input class="form-control" type="text" ng-model="member.place_of_birth" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            Date of Birth :
                            <input class="form-control"
                                   type="text"
                                   id="date_of_birth"
                                   pikaday="datepicker2"
                                   format="YYYY-MM-DD"
                                   on-select="onPikadaySelect(pikaday, date)" required
                                   theme="material-lite" ng-model="member.date_of_birth" name="date_of_birth "/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            Notes : <textarea class="form-control" ng-model="member.notes"></textarea>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <!-- <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="statKategori">
                  <input type="checkbox" id="statKategori" class="mdl-switch__input" checked>
                  <span class="mdl-switch__label"></span>
                </label> -->
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" ng-show="inputMember.$valid" class="btn btn-primary" ng-click="saveCat()">Save
                    changes
                </button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Add Choose Member -->
<div class="modal fade" data-backdrop="" id="modalInsMem" tabindex="-1" role="dialog" aria-labelledby="labelMem"
     ng-controller="PilihMemChoir">
    <div class="modal-dialog" style="margin-top: 100px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="labelMember">Choose Member</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-body">
                                <input type="text" ng-model="tableParams.filter()['search']"
                                       class="mdl-textfield__input" placeholder="Search data" autofocus/>
                                <table ng-table="tableParams" template-pagination="custom/pager"
                                       class="table mdl-data-table mdl-data-table--selectable fullwidth">
                                    <tr ng-repeat="item in $data">
                                        <td style="text-align:center">
                                            <input type="checkbox" checklist-model="memData.id_member"
                                                   checklist-value="item.id_member">
                                            <!-- <input type="checkbox" class="mdl-checkbox__input" name="id_candj[]" value="{{item.id_choir_category}}"> -->
                                        </td>
                                        <td width="50" data-title="'ID'">{{$index+1}}</td>
                                        <td data-title="'NAME'" filter="{ 'name': 'text' }" sortable="'name'"
                                            class="mdl-data-table__cell--non-numeric">{{item.name}}
                                        </td>
                                        <td data-title="'INSTITUTION'" filter="{ 'institution': 'text' }"
                                            sortable="'institution'" class="mdl-data-table__cell--non-numeric">
                                            {{item.institution}}
                                        </td>
                                        <!-- <td data-title="'STATUS'" sortable="'status'" class="mdl-data-table__cell--non-numeric">
                                          <p ng-if="item.status==1">Aktif</p>
                                          <p ng-if="item.status==0">Tidak Aktif</p>
                                        </td> -->
                                    </tr>
                                    <tr ng-show="loading">
                                        <td colspan="6" style="text-align:center;">
                                            <div id="p2"
                                                 class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
                                        </td>
                                    </tr>
                                </table>

                                <script type="text/ng-template" id="custom/pager">
                                    <div ng-if="params.data.length" class="ml-data-table-pager p-10">
                                        <div ng-if="params.settings().counts.length" class="f-right">
                                            <button ng-class="{'active':params.count() == 10}"
                                                    ng-click="params.count(10)" class="mdl-button">10
                                            </button>
                                            <button ng-class="{'active':params.count() == 25}"
                                                    ng-click="params.count(25)" class="mdl-button">25
                                            </button>
                                            <button ng-class="{'active':params.count() == 50}"
                                                    ng-click="params.count(50)" class="mdl-button">50
                                            </button>
                                            <button ng-class="{'active':params.count() == 100}"
                                                    ng-click="params.count(100)" class="mdl-button">100
                                            </button>
                                        </div>
                                        <span ng-repeat="page in pages"
                                              ng-class="{'disabled': !page.active, 'previous': page.type == 'prev', 'next': page.type == 'next'}"
                                              ng-switch="page.type">
                                            <button ng-switch-when="prev" ng-click="params.page(page.number)" class="mdl-button">&laquo;</button>
                                            <button ng-switch-when="first" ng-click="params.page(page.number)" class="mdl-button"><span
                                                    ng-bind="page.number"></span></button>
                                            <button ng-switch-when="page" ng-click="params.page(page.number)" class="mdl-button"><span
                                                    ng-bind="page.number"></span></button>
                                            <button ng-switch-when="more" ng-click="params.page(page.number)" class="mdl-button">&#8230;</button>
                                            <button ng-switch-when="last" ng-click="params.page(page.number)" class="mdl-button"><span
                                                    ng-bind="page.number"></span></button>
                                            <button ng-switch-when="next" ng-click="params.page(page.number)" class="mdl-button">&raquo;</button>
                                        </span>
                                    </div>
                                </script>

                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <pre>{{memData.id_member|json}}</pre>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" ng-click="pilih()" data-dismiss="modal">Pilih</button>
            </div>
        </div>
    </div>
</div>