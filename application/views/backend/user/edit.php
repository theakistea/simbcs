<section ng-controller="UpdateDataUserController" class="text-fields">
  <div class="mdl-color--amber ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">User Management</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Edit Data</h3>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/User">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">

            <form name="myform">
				<input type="hidden" name="idUser" ng-model="idUser" value="<?php echo $rows->idUser ?>">
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="username" id="username" ng-model="username" value="<?php echo $rows->username ?>" required />
                <label class="mdl-textfield__label" for="username">Username</label>
				<span  class="mdl-textfield__error" >Please input the username!</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="firstName" ng-model="firstName"  value="<?php echo $rows->firstName ?>"/>
                <label class="mdl-textfield__label" for="firstName">First Name</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="lastName" ng-model="lastName"  value="<?php echo $rows->lastName ?>"/>
                <label class="mdl-textfield__label" for="lastName">Last Name</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text"  pattern="^\s*[a-zA-Z0-9\--_-.]+@[a-zA-Z0-9\--_-.]+\.[a-zA-Z]{2,4}\s*$" id="email" ng-model="email"  value="<?php echo $rows->email ?>"/>
                <label class="mdl-textfield__label" for="email">Email</label>
				<span class="mdl-textfield__error">Please supply a valid email address.</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="password" id="password" ng-model="password"  value="<?php echo $rows->password ?>"/>
                <label class="mdl-textfield__label" for="password">Password</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>User Group</p>
				<select class="mdl-textfield__input" id="idUserGroup" ng-model="idUserGroup">
						<option value="idUserGroup"><?php echo $rows->permissionName ?></option>
						<?php foreach($get_user_group as $rows){ ?>
							<option value="<?php echo $rows['idUserGroup'];?>"><?php echo $rows['permissionName'];?></option>
						<?php } ?>
                </select>
              </div>
              <div class="m-t-20">
                <button ng-click="update_data()" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Save
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
	
    </div>

  </div>  
</section>
