<section ng-controller="InsertDataEventPackageB" class="text-fields">
  <div class="mdl-color--amber ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Event Package B</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Add Data</h3>
          <p>An overview of basic form styles and elements.</p>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/EventPackageB">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form name="myform">
			<div class="p-20--small">
                
                <p>Event
                <span 
                  data-toggle="modal" data-target="#modalSearchEvent"
                  data-upgraded=",MaterialButton,MaterialRipple" 
                  class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                    <i class="material-icons">search</i>
                    <span class="mdl-button__ripple-container">
                      <span style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);" class="mdl-ripple is-animating">
                      </span>
                    </span>
                </span>
                </p>
              </div>
              <div class="p-20--small">
                <p>Event Name : </p><p id="event"></p>
              </div>
              <div class="p-20--small">
                <p>Choir's Name: </p><p id="choir"></p>
              </div>
              <div class="p-20--small">
                <p>Number of Singers: </p><p id="nos"></p>
              </div>

              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-dirty">
                <input class="mdl-textfield__input" type="text" name="nom" id="nom" ng-change="cout_total_participant()" ng-model="nom"  required pattern="[0-9]*"/>
                <label class="mdl-textfield__label" for="nom">Number of Musicians</label>
				<span class="mdl-textfield__error">Please input number of musicians</span>
                
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-dirty">
                <input class="mdl-textfield__input" type="text" name="conductor" id="conductor" ng-change="cout_total_participant()" ng-model="conductor"  required pattern="[0-9]*"/>
                <label class="mdl-textfield__label" for="conductor">Conductor</label>
				<span class="mdl-textfield__error">Please input number of conductor</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-dirty">
                <input class="mdl-textfield__input" type="text" name="officials" id="officials" ng-change="cout_total_participant()" ng-model="officials"  required pattern="[0-9]*"/>
                <label class="mdl-textfield__label" for="officials">Officials</label>
				<span class="mdl-textfield__error">Please input number of officials</span>
              </div>

              <p id="tot">Totals Participans : {{total_participant}}</p>

              <p>T-Shirts size in cm</p>
              <table ng-table="tableParams" template-pagination="custom/pager" id="tshirt" class="table mdl-data-table">
                  <tr ng-repeat="item in $data" >
                    <td data-title="'NO'">NO</td>
                    <td data-title="'MEMBER NAME'">{{item.nama_kategori}}</td>
                    <td data-title="'SIZE'">SIZE</td>
                  </tr>
                </table>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="flight_detail" id="flight_detail" ng-model="flight_detail" />
                <label class="mdl-textfield__label" for="flight_detail">Flight Detail</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="bus_detail" id="bus_detail" ng-model="bus_detail" />
                <label class="mdl-textfield__label" for="bus_detail">Bus Detail</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <input class="mdl-textfield__input"
                       type="text"
                       id="check_in"
                       pikaday="datepicker2"
                       format="DD-MM-YYYY"
                       on-select="onPikadaySelect(pikaday, date)"
                       theme="material-lite" ng-model="check_in" name="check_in "/>
                <label class="mdl-textfield__label" for="check_in">Check In</label>
                <span class="mdl-textfield__error">Please input check in!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <uib-timepicker ng-model="time_in" show-meridian="false"></uib-timepicker>
                <input type="hidden" value="-" id="time_in" class="mdl-textfield__input" />
                
                <label class="mdl-textfield__label" for="time_in">Time In</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <input class="mdl-textfield__input"
                       type="text"
                       id="check_out"
                       pikaday="datepicker2"
                       format="DD-MM-YYYY"
                       on-select="onPikadaySelect(pikaday, date)"
                       theme="material-lite" ng-model="check_out" name="check_out "/>
                <label class="mdl-textfield__label" for="check_out">Check Out</label>
                <span class="mdl-textfield__error">Please input check out!</span>
      
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <uib-timepicker ng-model="time_out"  show-meridian="false"></uib-timepicker>
        <input type="hidden" value="-" id="time_out" class="mdl-textfield__input" />
                <label class="mdl-textfield__label" for="time_out">Time Out</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" id="staying_address" ng-model="staying_address" ></textarea>
                <label class="mdl-textfield__label" for="staying_address">Staying Address</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="single_room" ng-model="single_room" />
                <label class="mdl-textfield__label" for="single_room">Single Room</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="single_room_person" ng-model="single_room_person" />
                <label class="mdl-textfield__label" for="single_room_person">Single Room Person</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="two_bedded_room" ng-model="two_bedded_room" />
                <label class="mdl-textfield__label" for="two_bedded_room">2 Bedded Room</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="two_bedded_room_person" ng-model="two_bedded_room_person" />
                <label class="mdl-textfield__label" for="two_bedded_room_person">2 Bedded Room Person</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="three_bedded_room" ng-model="three_bedded_room" />
                <label class="mdl-textfield__label" for="three_bedded_room">3 Bedded Room</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="three_bedded_room_person" ng-model="three_bedded_room_person" />
                <label class="mdl-textfield__label" for="three_bedded_room_person">3 Bedded Room Person</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="four_bedded_room" ng-model="four_bedded_room" />
                <label class="mdl-textfield__label" for="four_bedded_room">4 Bedded Room</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="four_bedded_room_person" ng-model="four_bedded_room_person" />
                <label class="mdl-textfield__label" for="four_bedded_room_person">4 Bedded Room Person</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="hallal_food" ng-model="hallal_food" />
                <label class="mdl-textfield__label" for="hallal_food">Need Hallal Food</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="vegetarian_food" ng-model="vegetarian_food" />
                <label class="mdl-textfield__label" for="vegetarian_food">Need Vegetarian Food</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="los_information" id="los_information" ng-model="los_information" />
                <label class="mdl-textfield__label" for="los_information">LO's Information</label>
              </div>
			  <h4>Account Information</h4>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
                <input class="mdl-textfield__input is-dirty" type="text" id="price_per_person" ng-model="price_per_person" numformat="number" ng-change="cout_total_participant()"/>
                <label class="mdl-textfield__label" for="price_per_person">Price per Person</label>
                <span class="mdl-textfield__error">Please input price per person</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-dirty"  ng-class="is-dirty">
                <input class="mdl-textfield__input is-dirty" type="text" id="sub_total" readonly value="{{sub_total|number}}" />
                <label class="mdl-textfield__label" for="sub_total">Sub Total</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input " type="text" pattern="[0-9]*" id="phone" ng-model="phone" />
                <label class="mdl-textfield__label" for="phone">Phone</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="contact_person" id="contact_person" ng-model="contact_person" />
                <label class="mdl-textfield__label" for="contact_person">Contact Person</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text"  pattern="^\s*[a-zA-Z0-9\--_-.]+@[a-zA-Z0-9\--_-.]+\.[a-zA-Z]{2,4}\s*$" id="email" ng-model="email"  required/>
                <label class="mdl-textfield__label" for="email">Email</label>
				<span class="mdl-textfield__error">Please supply a valid email address.</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" id="address" ng-model="address" ></textarea>
                <label class="mdl-textfield__label" for="address">Address</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" id="notes" ng-model="notes" ></textarea>
                <label class="mdl-textfield__label" for="notes">Notes</label>
              </div>
			  <p>Active</p>
			  <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="status">
				  <input type="checkbox" id="status" class="mdl-switch__input" checked>
				  <span class="mdl-switch__label"></span>
			  </label>
              <div class="m-t-20">
                <button ng-click="insert_data()" ng-show="myform.$valid" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Save
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>
<!-- Modal -->
<div class="modal fade" data-backdrop="" id="modalSearchEvent" tabindex="-1" role="dialog" aria-labelledby="labelMan" ng-controller="SearchEventController2">
  <div class="modal-dialog" style="margin-top: 100px;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="labelMember">Select Event</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
              <div class="box">
                <div class="box-body">
                  <input type="text" ng-model="tableParams.filter()['search']" class="mdl-textfield__input" placeholder="Search data" autofocus />
                  <table ng-table="tableParams" template-pagination="custom/pager" class="table mdl-data-table mdl-data-table--selectable fullwidth">
                    <tr ng-repeat="item in $data">
                      <td style="text-align:center">
                        <input type="radio" ng-model="event.name" ng-value="item.idna">
                          <!-- <input type="checkbox" class="mdl-checkbox__input" name="id_candj[]" value="{{item.id_choir_category}}"> -->
                      </td>
                      <td width="50" data-title="'ID'">{{$index+1}}</td>
                      <td data-title="'NAME'" filter="{ 'title': 'text' }" sortable="'title'" class="mdl-data-table__cell--non-numeric">{{item.title}}</td>
                      <td data-title="'CHOIR'" filter="{ 'choir_name': 'text' }" sortable="'choir_name'" class="mdl-data-table__cell--non-numeric">{{item.choir_name}}</td>
                      <!-- <td data-title="'STATUS'" sortable="'status'" class="mdl-data-table__cell--non-numeric">
                        <p ng-if="item.status==1">Aktif</p>
                        <p ng-if="item.status==0">Tidak Aktif</p>
                      </td> -->
                    </tr>
                    <tr ng-show="loading">
                      <td colspan="6" style="text-align:center;">
                        <div id="p2" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
                      </td>
                    </tr>
                  </table>

          <script type="text/ng-template" id="custom/pager">
            <div ng-if="params.data.length" class="ml-data-table-pager p-10">
              <div ng-if="params.settings().counts.length" class="f-right">
                <button ng-class="{'active':params.count() == 10}" ng-click="params.count(10)" class="mdl-button">10</button>
                <button ng-class="{'active':params.count() == 25}" ng-click="params.count(25)" class="mdl-button">25</button>
                <button ng-class="{'active':params.count() == 50}" ng-click="params.count(50)" class="mdl-button">50</button>
                <button ng-class="{'active':params.count() == 100}" ng-click="params.count(100)" class="mdl-button">100</button>
              </div>
              <span ng-repeat="page in pages"
                  ng-class="{'disabled': !page.active, 'previous': page.type == 'prev', 'next': page.type == 'next'}"
                  ng-switch="page.type">
                <button ng-switch-when="prev" ng-click="params.page(page.number)" class="mdl-button">&laquo;</button>
                <button ng-switch-when="first" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="page" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="more" ng-click="params.page(page.number)" class="mdl-button">&#8230;</button>
                <button ng-switch-when="last" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="next" ng-click="params.page(page.number)" class="mdl-button">&raquo;</button>
              </span>
            </div>
          </script>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <pre>{{event.name|json}}</pre>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" ng-click="pilih()" data-dismiss="modal">Pilih</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  
  // function cek(){
  //   var nos = ( $("#nos").html()!='' ) ? $("#nos").html() : 0;
  //   var nom = ( $("#nom").val()!='' ) ? $("#nom").val() : 0;
  //   var conductor = ( $("#conductor").val()!='' ) ? $("#conductor").val() : 0;
  //   var officials = ( $("#officials").val()!='' ) ? $("#officials").val() : 0;

  //   var hasil = parseInt(nos) + parseInt(nom) + parseInt(conductor) + parseInt(officials);

  //   console.log(nos + ":"+ nom + ":"+ conductor + ":"+ officials);

  //   $("#tot").html("Totals Participans : "+ hasil);

  // }

</script>