<section ng-controller="UpdateDataEventPackageB" class="text-fields">
  <div class="mdl-color--amber ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Event Package B</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Add Data</h3>
          <p>An overview of basic form styles and elements.</p>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/EventPackageB">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form name="myform">
			<div class="p-20--small">
                
              </div>
              <div class="p-20--small">
                <p style="color:blue;"><strong>Event's Name</strong></p>
				<input class="mdl-textfield__input" type="text" name="title" id="title" ng-model="title" readonly/>
              </div>
              <div class="p-20--small">
                <p style="color:blue;"><strong>Choir's Name</strong></p>
				<input class="mdl-textfield__input" type="text" name="choir_name" id="choir_name" ng-model="choir_name" readonly/>
              </div>
              <div class="p-20--small">
                <p>Number of Singers: </p><p id="nos"></p>
              </div>

              <div class="p-20--small">
                <p style="color:blue;"><strong>Number of Singers: </strong></p><p id="nos"><?php echo $rows->num_of_singer; ?></p>
              </div>

              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Musicians</strong></p>
                <input class="mdl-textfield__input" type="text" name="nom" id="nom" ng-change="cout_total_participant()" ng-model="nom" />
				<span class="mdl-textfield__error">Please input number of musician!</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Conductors</strong></p>
                <input class="mdl-textfield__input" type="text" name="conductor" id="conductor" ng-change="cout_total_participant()" ng-model="conductor" />
				<span class="mdl-textfield__error">Please input number of conductor!</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Officials</strong></p>
                <input class="mdl-textfield__input" type="text" name="officials" id="officials" ng-change="cout_total_participant()" ng-model="officials" />
				<span class="mdl-textfield__error">Please input number of official!</span>
              </div>

              <p id="tot">Totals Participans : {{total_participant}}</p>

              <p>T-Shirts size in cm</p>
              <table ng-table="tableParams" template-pagination="custom/pager" id="tshirt" class="table mdl-data-table">
                  <tr class="top">
                    <td data-title="'NO'">NO</td>
                    <td data-title="'MEMBER NAME'">{{item.nama_kategori}}</td>
                    <td data-title="'SIZE'">SIZE</td>
                  </tr>
                </table>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Flight Detail</strong></p>
                <input class="mdl-textfield__input" type="text" name="flight_detail" id="flight_detail" ng-model="flight_detail" />
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Bus Detail</strong></p>
                <input class="mdl-textfield__input" type="text" name="bus_detail" id="bus_detail" ng-model="bus_detail" />
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
			  <p style="color:blue;"><strong>Check In</strong></p>
                <input class="mdl-textfield__input"
                       type="text"
                       id="check_in"
                       pikaday="datepicker2" required
                       format="DD-MM-YYYY"
                       on-select="onPikadaySelect(pikaday, date)"
                       theme="material-lite" ng-model="check_in" name="check_in "/>
                       
				<span class="mdl-textfield__error">Please input date check in!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Time In</strong></p>
                <uib-timepicker ng-model="time_in" show-meridian="false"></uib-timepicker>
                <input type="hidden" value="-" id="time_in" class="mdl-textfield__input" />
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
			  <p style="color:blue;"><strong>Check Out</strong></p>
                <input class="mdl-textfield__input"
                       type="text"
                       id="check_out" required
                       pikaday="datepicker2"
                       format="DD-MM-YYYY"
                       on-select="onPikadaySelect(pikaday, date)"
                       theme="material-lite" ng-model="check_out" name="check_out "/>
				<span class="mdl-textfield__error">Please input date check out!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Time Out</strong></p>
                <uib-timepicker ng-model="time_out" show-meridian="false"></uib-timepicker>
                <input type="hidden" value="-" id="time_out" class="mdl-textfield__input" />
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Staying Address</strong></p>
                <textarea class="mdl-textfield__input" type="text" rows="4" id="staying_address" ng-model="staying_address" ></textarea>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Single Room</strong></p>
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="single_room" ng-model="single_room" />
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Single Room Person</strong></p>
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="single_room_person" ng-model="single_room_person" />
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>2 Bedded Room</strong></p>
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="two_bedded_room" ng-model="two_bedded_room" />
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>2 Bedded Room Person</strong></p>
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="two_bedded_room_person" ng-model="two_bedded_room_person" />
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>3 Bedded Room</strong></p>
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="three_bedded_room" ng-model="three_bedded_room" />
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>3 Bedded Room Person</strong></p>
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="three_bedded_room_person" ng-model="three_bedded_room_person" />
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>4 Bedded Room</strong></p>
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="four_bedded_room" ng-model="four_bedded_room" />
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>4 Bedded Room Person</strong></p>
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="four_bedded_room_person" ng-model="four_bedded_room_person" />
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Need Hallal Food</strong></p>
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="hallal_food" ng-model="hallal_food" />
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Need Vegetarian Food</strong></p>
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="vegetarian_food" ng-model="vegetarian_food" />
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>LO's Information</strong></p>
                <input class="mdl-textfield__input" type="text" name="los_information" id="los_information" ng-model="los_information" />
              </div>
			  <h4>Account Information</h4>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Price Per Person</strong></p>
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="price_per_person"  ng-change="cout_total_participant()" ng-model="price_per_person" numformat="number"/>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"  ng-class="is-dirty">
			  <p style="color:blue;"><strong>Sub Total</strong></p>
                <input class="mdl-textfield__input" type="text" readonly id="sub_total"  value="{{sub_total|number}}"/>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Phone</strong></p>
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="phone" ng-model="phone" />
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Contact Person</strong></p>
                <input class="mdl-textfield__input" type="text" name="contact_person" id="contact_person" ng-model="contact_person" />
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Email</strong></p>
                <input class="mdl-textfield__input" type="text"  pattern="^\s*[a-zA-Z0-9\--_-.]+@[a-zA-Z0-9\--_-.]+\.[a-zA-Z]{2,4}\s*$" id="email" ng-model="email"/>
				<span class="mdl-textfield__error">Please supply a valid email address.</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Address</strong></p>
                <textarea class="mdl-textfield__input" type="text" rows="4" id="address" ng-model="address" ></textarea>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Notes</strong></p>
                <textarea class="mdl-textfield__input" type="text" rows="4" id="notes" ng-model="notes" ></textarea>
              </div>
              <div class="m-t-20">
                <button ng-click="update_data()"  ng-show="myform.$valid" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Save
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>
<!-- Modal -->
<div class="modal fade" data-backdrop="" id="modalSearchEvent" tabindex="-1" role="dialog" aria-labelledby="labelMan" ng-controller="SearchEventController2">
  <div class="modal-dialog" style="margin-top: 100px;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="labelMember">Select Event</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
              <div class="box">
                <div class="box-body">
                  <input type="text" ng-model="tableParams.filter()['search']" class="mdl-textfield__input" placeholder="Search data" autofocus />
                  <table ng-table="tableParams" template-pagination="custom/pager" class="table mdl-data-table mdl-data-table--selectable fullwidth">
                    <tr ng-repeat="item in $data">
                      <td style="text-align:center">
                        <input type="radio" ng-model="event.name" ng-value="item.idna">
                          <!-- <input type="checkbox" class="mdl-checkbox__input" name="id_candj[]" value="{{item.id_choir_category}}"> -->
                      </td>
                      <td width="50" data-title="'ID'">{{$index+1}}</td>
                      <td data-title="'NAME'" filter="{ 'name': 'text' }" sortable="'name'" class="mdl-data-table__cell--non-numeric">{{item.title}}</td>
                      <td data-title="'CHOIR'" filter="{ 'choir': 'text' }" sortable="'choir'" class="mdl-data-table__cell--non-numeric">{{item.choir_name}}</td>
                      <!-- <td data-title="'STATUS'" sortable="'status'" class="mdl-data-table__cell--non-numeric">
                        <p ng-if="item.status==1">Aktif</p>
                        <p ng-if="item.status==0">Tidak Aktif</p>
                      </td> -->
                    </tr>
                    <tr ng-show="loading">
                      <td colspan="6" style="text-align:center;">
                        <div id="p2" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
                      </td>
                    </tr>
                  </table>

          <script type="text/ng-template" id="custom/pager">
            <div ng-if="params.data.length" class="ml-data-table-pager p-10">
              <div ng-if="params.settings().counts.length" class="f-right">
                <button ng-class="{'active':params.count() == 10}" ng-click="params.count(10)" class="mdl-button">10</button>
                <button ng-class="{'active':params.count() == 25}" ng-click="params.count(25)" class="mdl-button">25</button>
                <button ng-class="{'active':params.count() == 50}" ng-click="params.count(50)" class="mdl-button">50</button>
                <button ng-class="{'active':params.count() == 100}" ng-click="params.count(100)" class="mdl-button">100</button>
              </div>
              <span ng-repeat="page in pages"
                  ng-class="{'disabled': !page.active, 'previous': page.type == 'prev', 'next': page.type == 'next'}"
                  ng-switch="page.type">
                <button ng-switch-when="prev" ng-click="params.page(page.number)" class="mdl-button">&laquo;</button>
                <button ng-switch-when="first" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="page" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="more" ng-click="params.page(page.number)" class="mdl-button">&#8230;</button>
                <button ng-switch-when="last" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="next" ng-click="params.page(page.number)" class="mdl-button">&raquo;</button>
              </span>
            </div>
          </script>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <pre>{{event.name|json}}</pre>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" ng-click="pilih()" data-dismiss="modal">Pilih</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  
  // function cek(){
  //   var nos = ( $("#nos").html()!='' ) ? $("#nos").html() : 0;
  //   var nom = ( $("#nom").val()!='' ) ? $("#nom").val() : 0;
  //   var conductor = ( $("#conductor").val()!='' ) ? $("#conductor").val() : 0;
  //   var officials = ( $("#officials").val()!='' ) ? $("#officials").val() : 0;

  //   var hasil = parseInt(nos) + parseInt(nom) + parseInt(conductor) + parseInt(officials);

  //   console.log(nos + ":"+ nom + ":"+ conductor + ":"+ officials);

  //   $("#tot").html("Totals Participans : "+ hasil);

  // }

</script>