<section ng-controller="UpdateDataCandjController" class="text-fields">
  <div class="mdl-color--cyan ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Clinnicians and Juries</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Edit Data</h3>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/Candj">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form name="myform">
			<input class="mdl-textfield__input" type="hidden" id="date_modify" ng-model="date_modify" value="<?php echo date('d-m-y'); ?>" />
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="name" ng-model="name" value="<?php echo $rows->name ?>" required />
                <label class="mdl-textfield__label" for="name">Name</label>
				        <span  class="mdl-textfield__error" >Please input the name!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Gender</p>
                <select 
                  class="mdl-textfield__input" 
                  id="gender" 
                  ng-model="gender"
                  ng-options="gender.value as gender.name for gender in genders">
                </select>
                
				        <span  class="mdl-textfield__error" >Please Select Gender!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Appellation</p>
                <select 
                  class="mdl-textfield__input" 
                  id="appellation" 
                  ng-model="appellation" 
                  ng-options="appellation.value as appellation.name for appellation in appellations">
                </select>
				        <span  class="mdl-textfield__error" >Please Select Appellation!</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="first_title" ng-model="first_title" value="<?php echo $rows->first_title ?>"  />
                <label class="mdl-textfield__label" for="first_title">First Title</label>
				        <span  class="mdl-textfield__error" >Please input first title!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="first_title" ng-model="last_title" value="<?php echo $rows->last_title ?>" />
                <label class="mdl-textfield__label" for="last_title">Last Title</label>
				        <span  class="mdl-textfield__error" >Please input last title!</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" id="street_address" ng-model="street_address" require><?php echo $rows->street_address ?></textarea>
                <label class="mdl-textfield__label" for="street_address">Street Address</label>
				        <span  class="mdl-textfield__error" >Please input street address</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Country</p>
                <select 
                  class="mdl-textfield__input" 
                  id="country" 
                  ng-model="country"  
                  ng-options="country.sortname as country.name for country in allCountry">
                </select>
                <span  class="mdl-textfield__error" >Please select country!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>State</p>
                <select 
                  class="mdl-textfield__input" 
                  id="state" 
                  ng-model="state" 
                  ng-options="state.id as state.name for state in allState">
                </select>
				        <span  class="mdl-textfield__error" >Please select state!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="city" ng-model="city" value="<?php echo $rows->city ?>" />
                <label class="mdl-textfield__label" for="city">City Name</label>
				        <span  class="mdl-textfield__error" >Please input city name</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Address status</p>
                <select 
                  class="mdl-textfield__input" 
                  id="address_status" 
                  ng-model="address_status"   
                  ng-options="address_status.value as address_status.name for address_status in statuses">
                </select>
				        <span  class="mdl-textfield__error" >Please input city name</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="phone" ng-model="phone" value="<?php echo $rows->phone ?>" />
                <label class="mdl-textfield__label" for="phone">Phone</label>
				        <span  class="mdl-textfield__error" >Please input valid phone!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="email" pattern="^\s*[a-zA-Z0-9\--_-.]+@[a-zA-Z0-9\--_-.]+\.[a-zA-Z]{2,4}\s*$"  ng-model="email" value="<?php echo $rows->email ?>"  />
                <label class="mdl-textfield__label" for="email">Email</label>
				        <span  class="mdl-textfield__error" >Please input valid email!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="social_media" ng-model="social_media" value="<?php echo $rows->social_media ?>" />
                <label class="mdl-textfield__label" for="social_media">Social Media</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="website" ng-model="website" value="<?php echo $rows->website ?>" />
                <label class="mdl-textfield__label" for="website">Website</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="place_of_birth" ng-model="place_of_birth" value="<?php echo $rows->place_of_birth ?>"  />
                <label class="mdl-textfield__label" for="place_of_birth">Place of Birth</label>
				        <span  class="mdl-textfield__error" >Please input brith place</span>
              </div>
			        <div class="mdl-textfield mdl-js-textfield">
                <p>Date of Birth</p>
                <input class="mdl-textfield__input"
                       type="text"
                       id="date_of_birth" 
                       pikaday="datepicker2"
                       format="DD-MM-YYYY"
                       on-select="onPikadaySelect(pikaday, date)"
                       theme="material-lite" ng-model="date_of_birth" value="<?php echo $rows->date_of_birth; ?>" />
				        <span  class="mdl-textfield__error" >Please input the name!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" id="notes" ng-model="notes" ><?php echo $rows->notes ?></textarea>
                <label class="mdl-textfield__label" for="notes">Notes</label>
              </div>

				<span class="">Max 300 character Current : {{notes.length}}</span>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <p>Photo</p>
                <img ng-if="photo!=''" src="<?php echo base_url('assets/uploads/img/candj/{{photo}}'); ?>" height="200px" width="200px">
                <input type="hidden" ng-model="photo">
				<button ng-if="photo!=''" ng-click="delete_photo(photo)" id="photo" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" style="top:95px;margin-left:10px;">
                  Delete
                </button>
                <!-- mdl-button mdl-js-button mdl-button--raised mdl-button--colored -->
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" type="file" ngf-select="uploadFiles($file, $invalidFiles)"
                accept="image/*" ngf-max-height="1000" id="userfile" ngf-max-size="2MB" style="margin-left: 10px;top:53px;">
                  Select File
                </button>
              </div>
              {{myform.$errors}}

			  <div class="mdl-textfield mdl-js-textfield">
                <p>Status</p>
                <select 
                  class="mdl-textfield__input" 
                  id="status" 
                  ng-model="status" 
                  ng-options="status.value as status.name for status in statuses">
                </select>
              </div>
                <button ng-click="update_data()" type="submit" ng-show="myform.$valid" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Update
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>
