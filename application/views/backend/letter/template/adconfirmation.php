<html>
	<head>
		<title>Letter</title>
	</head>
	<style>
	body {
		width: 100%;
		height: 100%;
		margin: 0;
		padding: 0;
		background-color: #FFF;
		font: 10pt "Calibri";
				font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
	}
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 180mm;
        min-height: 297mm;
        padding: 3mm;
        margin: 2mm auto;
        border: 0px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px black solid;
        height: 257mm;
        outline: 2cm #FFF solid;
    }

    dim {
        color:#eee;
    }
    table.bordered {
        border-collapse: collapse;
    }
    table.bordered {
        border: 1px solid black;
    }

    table.bordered  td { 
        border: 1px solid black;
    }

    table.bordered  th { 
        border: 1px solid black;

        text-decoration: none;
        
    }

    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 210mm;
            height: 297mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
    table.categori {

    }
	</style>

    
	<?php foreach ($choirs as $choir){?>
	    <?php foreach ($choir['choir']['data'] as $choirData){
            $code_transaksi =   $event->code_event. $event->date_start_formated ."/" . $choirData->type_prefix."-".$choirData->code_registrasi."/". $choirData->code_category;
            ?>
        	<body>
            
            <div class="book">
            <?php 
            //for($i=1;$i<=$jumlah_hal;$i++):
            ?>
            <div class="page" style="page-break-after: always">
                <center><img src="./logobcs.png"></center>
                <p style="text-align:right">Bandung, <?php echo date("d M Y");?></p>
                <h3>KONFIRMASI DATA ARTISTIC<br><?php  echo $code_transaksi?></h3>
                Yth. Bapak/Ibu <?php echo $choir['data']->name;?><br>
                <?php echo $choir['data']->street_address;?><br>
                <?php echo $choir['data']->city;?><br>
                <br>
                Dengan Hormat,<br>
                <div style="text-align: justify;text-justify: inter-word;">
                    <?php
                                        $rep = str_replace("--event_name",$event->title,$letter->konten);
                                        $rep = str_replace("--date_start",$event->date_start,$rep);
                                        $rep = str_replace("--date_finish",$event->date_finish,$rep);
                                        $rep = str_replace("--last_confirmation_date",$choirData->choir_name,$rep);
                                        $rep = str_replace("--receiver",$choir['type'] == '1' ? 'Conductor' : 'Choir Manager',$rep);
										$rep = str_replace("--choir_name", $choirData->choir_name ,$rep);
                                        $rep = str_replace("--transaction_code",$code_transaksi,$rep);

                                        echo $rep;
                                        
                    ?>
                    <table >
                            <tr>
                                <td valign="top" width="200px">
                                    Conductor 
                                </td>
                                <td valign="top">
                                :
                                </td>
                                <td>

                                <?php foreach ($choir['choir']['conductors'] as $key => $value) {
                                    ?>
                                <?php echo $value->name ?><br>
                                
                                <?php } ?>
                                </td>
                            </tr> 
                            <tr>
                                <td valign="top" width="200px">
                                    Choir Manager 
                                </td>
                                <td valign="top">
                                :
                                </td>
                                <td>

                                <?php foreach ($choir['choir']['managers'] as $key => $value) {
                                    ?>
                                <?php echo $value->name ?><br>
                                
                                <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="200px">
                                    Administration Document 
                                </td>
                                <td valign="top" width="10px">
                                :
                                </td>
                                <td>
                                    Form -> OK | 
                                    Choir Photo -> <?php echo $choirData->photo == '' ? 'NOT OK' : 'OK'?> |
                                    Audio Record -> <?php echo $choirData->has_audio_record == 0 ? 'NOT OK' : 'OK'?> |
                                    Biograpy -> <?php echo $choirData->has_biograpy == 0 ? 'NOT OK' : 'OK'?> |
                                    Surat Ket. Usia -> <?php echo $choirData->has_surat_keterangan_usia == 0 ? 'NOT OK' : 'OK'?> |
                                    Bukti Registrasi -> <?php echo $choirData->has_bukti_registrasi == 0  ? 'NOT OK' : 'OK'?> |
                                    Conductor Photo -> <?php echo $choirData->notes == ''  ? 'NOT OK' : 'OK'?> 
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="200px">
                                    <?php $eventPackage = $choir['choir']['eventPackage'] ?>
                                    Total Person <b>(<?php echo($eventPackage ? $eventPackage->num_of_musician + $eventPackage->num_of_singer + $eventPackage->num_of_conductor + $eventPackage->num_of_official :0) ?>)</b>
                                </td>
                                <td valign="top">
                                :
                                </td>
                                <td>
                                    Singers -> <?php echo( $eventPackage ? $eventPackage->num_of_singer : 0) ?> |
                                    Musician -> <?php echo( $eventPackage ? $eventPackage->num_of_musician: 0) ?> |
                                    Conductor -> <?php echo( $eventPackage ? $eventPackage->num_of_conductor: 0) ?> |
                                    Official -> <?php echo( $eventPackage ? $eventPackage->num_of_official: 0) ?> |
                                    
                                </td>
                            </tr>
                    
                    </table>
                    <br>

                    <table class="bordered" width="100%">
                        <tr>
                            <td width="20%" >
                                Kategori
                            </td>

                            <td >
                                ->  <?php echo $choirData->categoryname  != ''  ? $choirData->categoryname : 'NOT OK'?> 
                            </td>
                            <td width="40%">
                                Tanda Tangan<br>Conductor : 
                            </td>
                        </tr>
                    </table>
                    <br>

                    <?php $songs = $choir['choir']['songs'] ?>
                    <table class="bordered" width="100%">
                        <tr>
                            <th >
                                Program Lagu sesuai Urutan : 
                            </th>

                            <th width="10%">
                                Iringan
                            </th>
                            <th width="10%">
                                Jumlah Partitur Diteriama
                            </th>
                            <th width="10%">
                                Surat Izin Komposer
                            </th>
                            <th width="10%">
                                Durasi
                            </th>
                            <th width="10%" >
                                Persetujuan Artistic Commite
                            </th>
                        </tr>
                        <?php foreach ($songs as $key => $value) {
                            ?>
                        <tr>
                                <th >
                                    <?php echo ($value->title);?> -> 
                                    <?php echo ($value->composer);?>
                                </th>

                                <th width="10%">
                                    <?php echo ($value->accompanied_value == 'none' ? 'NOT OK' : 'OK') ;?>
                                </th>
                                <th width="10%">
                                    <?php echo ($value->jumlah_partitur_diterima != '1' ? 'NOT OK' : 'OK') ;?>
                                </th>
                                <th width="10%">
                                    <?php echo ($value->surati_izin_komposer != '1' ? 'NOT OK' : 'OK') ;?>
                                </th>
                                <th width="10%" >
                                    <?php echo ($value->durasi == '' ? 'NOT OK' : $value->durasi) ;?>
                                </th>
                                <th width="10%" >
                                    <?php echo ($value->persetujuan_artistic_commite != '1' ? 'NOT OK' : 'OK') ;?>
                                </th>
                                </tr>
                            <?php 
                            # code...
                        }?>
                    </table>


                    Terimakasih atas partisipasi dan kerjasamanya yang baik,<br>
                    Salam Sukses Selalu
                    <br><br><br><br>

                    <table width="100%">
                        <tr>
                            <td  valign="top" align="center">
                                Admin Secretaty <br>
                                <br><br>
                                <br><br><br><br>
                                <u>Thresia Yuni Atika</u><br>

                                (08221645054)
                            </td>
                            <td  valign="top" align="center">
                            <dim>kota , dd mm</dim> - <?php  echo(date('Y'))?><br>
                            <br>
                        <dim> <?php echo ("Tanda Tangan<br>". ($isKonduktor  ? "Conductor " : "Official"))?> </dim
                            <br>
                            <br>
                            <br><br>
                                <dim><u>Nama Lengkap</u></dim>
                            <br>
                            </td>
                        </tr>
                    <table>
                    
                    <center>
                        <b><?php echo strtoupper($company[0]['mailing_address']);?></b>
                        <div style="font-size:8pt">
                            <?php echo $company[0]['street_address'];?><br>
                            phone & fax: <?php echo $company[0]['fax_number'];?> | <?php echo $company[0]['phone_number'];?> | +62 823 1846 4046 | <a href="mailto:<?php echo $company[0]['email_company'];?>"><?php echo $company[0]['email_company'];?></a> | <a href="http://bandungchoral.com">www.bandungchoral.com</a>
                        </div>
                    </center>
                </div>
            </div>
            <?php //endfor;?>
            <!--<div class="page">
                <div class="subpage">Page 2/2</div>    
            </div>-->
            </div>
	    </body>

        <?php } ?>
    <?php } ?>
</html>