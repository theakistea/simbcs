<html>
	<head>
		<title>Letter</title>
	</head>
	<style>
	body {
		width: 100%;
		height: 100%;
		margin: 0;
		padding: 0;
		background-color: #FFF;
		font: 10pt "Calibri";
				font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
	}
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 180mm;
        min-height: 297mm;
        padding: 3mm;
        margin: 2mm auto;
        border: 0px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px black solid;
        height: 257mm;
        outline: 2cm #FFF solid;
    }

    dim {
        color:#eee;
    }
    table.bordered {
        border-collapse: collapse;
    }
    table.bordered {
        border: 1px solid black;
    }

    table.bordered  td { 
        border: 1px solid black;
    }

    table.bordered  th { 
        border: 1px solid black;

        text-decoration: none;
        
    }

    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 210mm;
            height: 297mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
    table.categori {

    }
    table th  {
        background-color: #eee;
		font: 10pt "Calibri";
    }
    table td   {
		font: 10pt "Calibri";
    }
    p   {
		font: 10pt "Calibri";
    }
	</style>

    
	<?php foreach ($choirs as $choir){?>
	    <?php foreach ($choir['choir']['data'] as $choirData){
            $code_transaksi =   $event->code_event. $event->date_start_formated ."/" . $choirData->type_prefix."-".$choirData->code_registrasi."/". $choirData->code_category;
            ?>
        	<body>
            
            <div class="book">
            <?php 
            //for($i=1;$i<=$jumlah_hal;$i++):
            ?>
            <div class="page" style="page-break-after: always">
                <center><img src="./logobcs.png"></center>
                <p style="text-align:right">Bandung, <?php echo date("d M Y");?></p>
                <h3>KONFIRMASI DATA AKHIR<br><?php  echo $code_transaksi?><?php  print_r($choir['choir']['data'][0]->choir_name);?></h3>
                Yth. Bapak/Ibu <?php echo $choir['data']->name;?><br>
                <?php echo $choir['data']->street_address;?><br>
                <?php echo $choir['data']->city;?><br>
                <br>
                Dengan Hormat,<br>
                <div style="text-align: justify;text-justify: inter-word;font: 10pt 'Calibri' " >
                    <?php
                                        $rep = str_replace("--event_name",$event->title,$letter->konten);
                                        $rep = str_replace("--date_start",$event->date_start,$rep);
                                        $rep = str_replace("--date_finish",$event->date_finish,$rep);
                                        $rep = str_replace("--last_confirmation_date",$choirData->choir_name,$rep);
                                        $rep = str_replace("--receiver",$choir['type'] == '1' ? 'Conductor' : 'Choir Manager',$rep);
                                        $rep = str_replace("--choir_name", $choirData->choir_name ,$rep);
                                        $rep = str_replace("--transaction_code",$code_transaksi,$rep);
                                        echo $rep;
                                        
                    ?>
                    <table class="datapaduansuara bordered">
                            <tr>
                                <th colspan="3" style="text-align:left;">
                                    DATA PADUAN SUARA
                                </th>
                            </tr>
                            <tr>
                                <td valign="top" width="200px">
                                    Dirigen 
                                </td>
                                <td valign="top">
                                :
                                </td>
                                <td>

                                <?php foreach ($choir['choir']['conductors'] as $key => $value) {
                                    ?>
                                <?php echo $value->name ?><br>
                                
                                <?php } ?>
                                </td>
                            </tr> 
                            <tr>
                                <td valign="top" width="200px">
                                    Manager 
                                </td>
                                <td valign="top">
                                :
                                </td>
                                <td>

                                <?php foreach ($choir['choir']['managers'] as $key => $value) {
                                    ?>
                                <?php echo $value->name ?><br>
                                
                                <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="200px">
                                    Dokumen Registrasi
                                </td>
                                <td valign="top" width="10px">
                                :
                                </td>
                                <td>
                                    Form -> OK | 
                                    Choir Photo -> <?php echo $choirData->photo == '' ? 'NOT OK' : 'OK'?> |
                                    Audio Record -> <?php echo $choirData->has_audio_record == 0 ? 'NOT OK' : 'OK'?> |
                                    Biograpy -> <?php echo $choirData->has_biograpy == 0 ? 'NOT OK' : 'OK'?> |
                                    Surat Ket. Usia -> <?php echo $choirData->has_surat_keterangan_usia == 0 ? 'NOT OK' : 'OK'?> |
                                    Bukti Registrasi -> <?php echo $choirData->has_bukti_registrasi == 0  ? 'NOT OK' : 'OK'?> |
                                    Conductor Photo -> <?php echo $choirData->notes == ''  ? 'NOT OK' : 'OK'?> 
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="200px">
                                    <?php $eventPackage = $choir['choir']['eventPackage'] ?>
                                    Jumlah Peserta <b>(<?php echo($eventPackage ? $eventPackage->num_of_musician + $eventPackage->num_of_singer + $eventPackage->num_of_conductor + $eventPackage->num_of_official :0) ?>)</b>
                                </td>
                                <td valign="top">
                                :
                                </td>
                                <td>
                                    Singers -> <?php echo( $eventPackage ? $eventPackage->num_of_singer : 0) ?> |
                                    Musician -> <?php echo( $eventPackage ? $eventPackage->num_of_musician: 0) ?> |
                                    Conductor -> <?php echo( $eventPackage ? $eventPackage->num_of_conductor: 0) ?> |
                                    Official -> <?php echo( $eventPackage ? $eventPackage->num_of_official: 0) ?> |
                                    
                                </td>
                            </tr>
                    
                    </table>
                    <br>

                    <table class="bordered" width="100%">
                        <tr>
                            <th width="50%" >
                                DATA ARTISTIK CHOIR COMPETITION
                                <br>
                                Kategori -> <?php echo $choirData->categoryname  != ''  ? $choirData->categoryname : 'NOT OK'?> 
                            </th>

                            <th width="50%">
                                Tanda Tangan<br>Conductor : 
                            </th>
                        </tr>
                    </table>
                    <br>

                    <?php $songs = $choir['choir']['songs'] ?>
                    <table class="bordered" width="100%">
                        <tr>
                            <td >
                                Program Lagu sesuai Urutan : 
                            </td>

                            <td width="10%">
                                Iringan
                            </td>
                            <td width="10%">
                                Jumlah Partitur Diteriama
                            </td>
                            <td width="10%">
                                Surat Izin Komposer
                            </td>
                            <td width="10%">
                                Durasi
                            </td>
                            <td width="10%" >
                                Persetujuan Artistic Commite
                            </th>
                        </tr>
                        <?php foreach ($songs as $key => $value) {
                            ?>
                        <tr>
                                <td >
                                    <?php echo ($value->title);?> -> 
                                    <?php echo ($value->composer);?>
                                </td>

                                <td width="10%">
                                    <?php echo ($value->accompanied_value == 'none' ? 'NOT OK' : 'OK') ;?>
                                </td>
                                <td width="10%">
                                    <?php echo ($value->jumlah_partitur_diterima == '' ? 'NOT OK' : $value->jumlah_partitur_diterima ) ;?>
                                </td>
                                <td width="10%">
                                    <?php echo ($value->surati_izin_komposer == '1' ? 'OK' : 'NOT OK') ;?>
                                </td>
                                <td width="10%" >
                                    <?php echo ($value->durasi == '' ? 'NOT OK' : $value->durasi) ;?>
                                </td>
                                <td width="10%" >
                                    <?php echo ($value->persetujuan_artistic_commite == '1' ? 'OK' : 'NOT OK') ;?>
                                </td>
                                </tr>
                            <?php 
                            # code...
                        }?>
                    </table>
                    <br>

                    <?php $eventPackage = $choir['choir']['eventActivity']; ?> 
                    <table class="bordered" width="100%">
                    <tr>
                                <th colspan="3" style="text-align:left;">
                                    JADWAL REGISTRASI ULANG
                                </th>
                            </tr>
                        <tr>
                            <td width="20%" >
                                Hari ,  Tanggal
                            </td>
                            <td width="50%" >
                                Lokasi
                            </td>
                            <td width="50%" >
                                Waktu
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <?php echo ( $eventPackage ? $eventPackage->reg_date : '' ) ?>
                            </td>
                            <td >
                                <?php 
                                $venueUji  = array_search('1', array_column($venue, 'purpose')); 
                                echo $venueUji != -1 ? $venue[$venueUji]['venue'] : ''; ?>
                            </td>
                            <td >
                                <?php echo ( $eventPackage ? $eventPackage->reg_time_start : '') ?> ~ 
                                <?php echo ( $eventPackage ? $eventPackage->reg_time_finish : '') ?>
                            </td>
                        </tr>
                    </table>
                    <br>

                    <table class="bordered" width="100%">
                    <tr>
                                <th colspan="5" style="text-align:left;">
                                    JADWAL UJI COBA PANGGUNG :
                                </th>
                            </tr>
                        <tr>
                            <td width="40%" >
                                Venue
                            </td>
                            <td width="15%" >
                                Date
                            </td>
                            <td width="15%" >
                                Check In 
                            </td>
                            <td width="15%" >
                                Start on stage 
                            </td>
                            <td width="15%" >
                                Finish
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <?php 
                                $venueUji  = array_search('2', array_column($venue, 'purpose')); 
                                echo $venueUji != -1 ? $venue[$venueUji]['venue'] : ''; ?>
                            </td>
                            <td >
                                <?php echo ( $eventPackage ?$eventPackage->stage_rehearseal_date : '') ?>
                            </td>
                            <td >
                                <?php echo ( $eventPackage ?$eventPackage->stage_rehearseal_time_check_in : '') ?>
                            </td>
                            <td >
                                <?php echo ( $eventPackage ?$eventPackage->stage_rehearseal_time_start : '') ?>
                            </td>
                            <td >
                                <?php echo ( $eventPackage ?$eventPackage->stage_rehearseal_time_finish : '') ?>
                            </td>
                        </tr>
                    </table>
                    <br>

                    <table class="bordered" width="100%">
                    <tr>
                                <th colspan="7" style="text-align:left;">
                                    JADWAL KOMPETISI :
                                </th>
                            </tr>
                        <tr>
                            <td width="30%" >
                                Venue
                            </td>
                            <td width="10%" >
                                Date
                            </td>
                            <td width="10%" >
                                Check In 
                            </td>
                            <td width="10%" >
                                Preparation Room
                            </td>
                            <td width="10%" >
                                Back Stage 
                            </td>
                            <td width="10%" >
                                On Stage 
                            </td>
                            <td width="10%" >
                                Finish
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <?php 
                                $venueUji  = array_search('3', array_column($venue, 'purpose')); 
                                echo $venueUji != -1 ? $venue[$venueUji]['venue'] : ''; ?>
                            </td>
                            <td >
                                
                                <?php echo ( $eventPackage ?$eventPackage->reg_date : '') ?>
                            </td>
                            <td >
                               
                                <?php echo ( $eventPackage ?$eventPackage->re_reg_time : '') ?>
                            </td>
                            <td >
                                <?php echo ( $eventPackage ?$eventPackage->re_reg_time_preparation : '') ?>
                                
                            </td>
                            <td >
                                
                                <?php echo ( $eventPackage ?$eventPackage->re_reg_time_backstage : '') ?>
                            </td>
                            <td >
                                
                                <?php echo ( $eventPackage ?$eventPackage->re_reg_time_start : '') ?>
                            </td>
                            <td >
                                
                                <?php echo ( $eventPackage ?$eventPackage->re_reg_time_finish : '') ?>
                            </td>
                        </tr>
                    </table>
                    <br>


                    <table class="bordered" width="100%">
                    <tr>
                                <th colspan="4" style="text-align:left;">
                                    JADWAL CHOIR CLINIC :
                                </th>
                            </tr>
                        <tr>
                            <td width="40%" >
                                Venue
                            </td>
                            <td width="20%" >
                                Date
                            </td>
                            <td width="20%" >
                                Start
                            </td>
                            <td width="20%" >
                                Finish
                            </td>
                        </tr>
                        <tr>
                            <td >
                            <?php 
                                $venueUji  = array_search('4', array_column($venue, 'purpose')); 
                                echo $venueUji != -1 ? $venue[$venueUji]['venue'] : ''; ?>
                            </td>
                            <td >
                                <?php echo ( $eventPackage ?$eventPackage->choir_clinic_date : '') ?>
                            </td>
                            <td >
                                <?php echo ( $eventPackage ?$eventPackage->choir_clinic_time_start : '') ?>
                            </td>
                            <td >
                                <?php echo ( $eventPackage ?$eventPackage->choir_clinic_time_finish  : '') ?>
                            </td>
                        </tr>
                    </table>
                    <br>
                 
                    
                    <center>
                        <b><?php echo strtoupper($company[0]['mailing_address']);?></b>
                        <div style="font-size:8pt">
                            <?php echo $company[0]['street_address'];?><br>
                            phone & fax: <?php echo $company[0]['fax_number'];?> | <?php echo $company[0]['phone_number'];?> | +62 823 1846 4046 | <a href="mailto:<?php echo $company[0]['email_company'];?>"><?php echo $company[0]['email_company'];?></a> | <a href="http://bandungchoral.com">www.bandungchoral.com</a>
                        </div>
                    </center>
                </div>
            </div>
            <?php //endfor;?>
            <!--<div class="page">
                <div class="subpage">Page 2/2</div>    
            </div>-->
                <div class="page" style="page-break-after: always">
                    <center><img src="./logobcs.png"></center>
                       <p>
                    MOHON DIPERHATIKAN :
                    <ol>
                    <li> Registrasi ulang wajib dilakukan oleh Manager / Dirigen dari paduan suara. Mohon membawa surat konfirmasi data akhir ini ketika datang ke meja registrasi.</li>
                    <li> Tidak ada penggantian jadwal uji coba panggung / kompetisi jika peserta tidak hadir di waktu yang sudah dijadwalkan.</li>
                    <li> Seluruh tim paduan suara wajib hadir tepat waktu pada saat check in kompetisi, supaya mempunyai cukup waktu
                    dan tidak terburu-buru menjelang waktu tampil, juga agar siap seandainya terjadi pergesaeran waktu tampil lebih cepat.</li>
                    <li> Seluruh anggota Paduan Suara harus sudah siap saat tiba di tempat festival pada waktu yang sudah ditentukan. Panitia tidak menyediakan tempat untuk ganti baju/kostum atau pun penitipan barang.</li>
                    <li> Selama pelaksanaan festival, peserta harus selalu menggunakan Name Tag. Mohon menjaga baik-baik Name Tag paduan suara anda. Panitia tidak menyediakan penggantian jika Name Tag hilang.</li>
                    <li> Atas permintaan pengelola gedung, tidak diperkenankan makan ataupun membawa makanan ke dalam area festival.</li>
                    <li> Seluruh lokasi penyelenggaraan 5th BICF 2016 adalah daerah bebas rokok. Seluruh peserta dan tim pendukung tidak
                    diperkenankan merokok di alam ruangan dan di lingkungan lokasi kegiatan.</li>
                    <li> Anak di bawah umur 6 tahun dilarang masuk ke area kompetisi.</li>
                    <li> Peserta wajib menjaga kebersihan, kesopanan dan ketenangan di seluruh lokasi kegiatan 5th BICF 2016.</li>
                    <li> Jadwal Award Ceremony (pengumuman prestasi) untuk masing-masing kategori kompetisi tercantum pada General Schedule.</li>
                    <li> Setiap Paduan Suara/ Vocal Group yang mendapatkan nilai lebih dari 33,01 (Competition Gold Lv.IV) akan mendapatkan kualifikasi untuk langsung berpartisipasi pada Bali International Choir Championship. 2 buah lagu dari competition dan 1 buah lagu baru yang sudah dikonfirmasikan sebelumnya ke panitia)</li>
                    <li> Paduan Suara/ Vocal Group yang berhasil meraih kualifikasi Bali Championship akan dihubungi langsung oleh panitia untuk konfirmasi jadwal penampilan selanjutnya.</li>
                    <li> Setiap group yang mendapatkan Gold Medal dengan nilai tertinggi pada masing-masing kategori championship berhak tampil pada babak Grand Prix Championship. (memilih 2 buah lagu yang sudah dinyanyikan sebelumnya).</li>
                    <li> Medali dan sertifikat untuk masing-masing penyanyi/anggota grup peserta dapat dipesan dengan mengisi formulir pemesanan yang terpisah.</li>
                    <li> Jagalah barang bawaan anda baik-baik, jangan sampai tertinggal atau hilang. Panitia tidak bertanggung jawab jika terjadi kehilangan. Peserta disarankan tidak membawa barang-barang berharga ke lokasi 5th BICF 2016.</li>
                    Terimakasih atas partisipasi dan kerjasamanya yang baik.</li>
                    </ol>
                    </p>

                    Terimakasih atas partisipasi dan kerjasamanya yang baik,<br>
                    Salam Sukses Selalu
                    <br><br><br><br>

                    <table width="100%">
                        <tr>
                            <td  valign="top" align="center">
                                Admin Secretaty <br>
                                <br><br>
                                <br><br><br><br>
                                <u>Thresia Yuni Atika</u><br>

                                (08221645054)
                            </td>
                            <td  valign="top" align="center">
                            <dim>kota , dd mm</dim> - <?php  echo(date('Y'))?><br>
                            <br>
                        <dim> <?php echo ("Tanda Tangan<br>". ($isKonduktor  ? "Conductor " : "Official"))?> </dim
                            <br>
                            <br>
                            <br><br>
                                <dim><u>Nama Lengkap</u></dim>
                            <br>
                            </td>
                        </tr>
                    <table>
                    <center>
                        <b><?php echo strtoupper($company[0]['mailing_address']);?></b>
                        <div style="font-size:8pt">
                            <?php echo $company[0]['street_address'];?><br>
                            phone & fax: <?php echo $company[0]['fax_number'];?> | <?php echo $company[0]['phone_number'];?> | +62 823 1846 4046 | <a href="mailto:<?php echo $company[0]['email_company'];?>"><?php echo $company[0]['email_company'];?></a> | <a href="http://bandungchoral.com">www.bandungchoral.com</a>
                        </div>
                    </center>
                </div>

            </div>
        </body>
        <?php } ?>
    <?php } ?>
</html>