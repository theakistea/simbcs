<html>
	<head>
		<title>Letter</title>
	</head>
	<style>
	body {
		width: 100%;
		height: 100%;
		margin: 0;
		padding: 0;
		background-color: #FFF;
		font: 10pt "Calibri";
				font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
	}
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 180mm;
        min-height: 297mm;
        padding: 20mm;
        margin: 10mm auto;
        border: 0px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px black solid;
        height: 257mm;
        outline: 2cm #FFF solid;
    }
    
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 210mm;
            height: 297mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
	</style>
	<body>
	<div class="book">
	<?php 
	//for($i=1;$i<=$jumlah_hal;$i++):
	?>
    <div class="page" style="page-break-after: always">
        <center><img src="<?php echo base_url();?>assets/img/logobcs.png"></center>
		<p style="text-align:right">Bandung, <?php echo date("d M Y");?></p>
		Nomor 	: <?php echo $event2['choir_name'];; echo date('/m/Y/'); echo substr($event->title,0,3); ?><br>
		Perihal : <?php echo $event->title;?><br><br>
		Yth. Bapak/Ibu <?php echo $event2['name'];?><br>
		<?php echo $event->street_address;?><br><br>
		<br>
		Salam sejahtera,<br>
		<div style="text-align: justify;text-justify: inter-word;">
			<?php
                                $rep = str_replace("--event_name",$event->title,$letter->konten);
                                $rep2 = str_replace("--member_name",$event2['name'],$rep);
                                $rep3 = str_replace("--host",$event->host,$rep2);
                                $rep4 = str_replace("--date_start",$event->date_start,$rep3);
                                $rep5 = str_replace("--date_finish",$event->date_finish,$rep4);
                                $rep6 = str_replace("--pic",$event->PIC,$rep5);
                                echo $rep6;
                                
			?>
			<br><br>
			<br><br>
			<br>
			Hormat Kami,
			<br><br><br>
			<br><br><br>
			<strong>Tommyanto Kandisaputra</strong><br>
			<i>Pimpinan Bandung Choral Society</i><br><br>
			<br><br><br><br><br><br>
			<center>
				<b><?php echo strtoupper($company[0]['mailing_address']);?></b>
				<div style="font-size:8pt">
					<?php echo $company[0]['street_address'];?><br>
					phone & fax: <?php echo $company[0]['fax_number'];?> | <?php echo $company[0]['phone_number'];?> | +62 823 1846 4046 | <a href="mailto:<?php echo $company[0]['email_company'];?>"><?php echo $company[0]['email_company'];?></a> | <a href="http://bandungchoral.com">www.bandungchoral.com</a>
				</div>
			</center>
		</div>
    </div>
	</div>
	</body>
</html>