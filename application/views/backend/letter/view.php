<section ng-controller="TablesDataLetterController" class="tables-data">
  <div class="mdl-color--amber ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Type of Letter</h3>
      <h4 class="mdl-color-text--amber-100 m-b-20 no-m-t w100">Click 'Nomor' to view detail.</h4>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-20--small">

        <div class="table-search">
        </div>

        <div class="mdl-textfield mdl-js-textfield">
          <input type="text" ng-model="tableParams.filter()['search']" class="mdl-textfield__input" placeholder="Search data" autofocus />
        </div>
		
        <div class=" mdl-color-text--blue-grey-400">
          <div class="m-t-30">
            <ul class="list-bordered">
              <li><a href="#/Letter/add">
                <i class="material-icons m-r-5 f11">add</i>
                Create a new item
              </a></li>
              <!--li><a href="">
                <i class="material-icons m-r-5 f11">file_download</i>
                Export selected items
              </a></li-->
            </ul>
          </div>
        </div>

      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col  mdl-cell--12-col-tablet mdl-cell--12-col-phone">
      <div class="p-20 ml-card-holder ml-card-holder-first">

        <div class="mdl-card mdl-shadow--1dp m-b-30">

          <table ng-table="tableParams" template-pagination="custom/pager" class="table mdl-data-table mdl-data-table--selectable fullwidth">
            <tr ng-repeat="item in $data">
              <td width="50" data-title="'ID'">{{$index+1}}</td>
              <td data-title="'TEMPLATE NAME'" filter="{ 'template_name': 'text' }" sortable="'template_name'" class="mdl-data-table__cell--non-numeric"><a href="#/Letter/edit/{{item.id_letter}}">{{item.template_name}}</a></td>
              <td data-title="'TYPE'" sortable="'type'" class="mdl-data-table__cell--non-numeric">{{item.type}}</td>
              <!--td data-title="'AKSI'" sortable="'aksi'" class="mdl-data-table__cell--non-numeric">
                <!-- <button data-toggle="modal" data-target="#modalMember" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Lihat
                </button> -->
                <!--a class="btn btn-info" href="./letter/download/{{item.id_letter}}" >
                  Unduh
                </a>
              </td>
              <!-- <td data-title="'STATUS'" sortable="'status'" class="mdl-data-table__cell--non-numeric">
                <p ng-if="item.status==1">Aktif</p>
                <p ng-if="item.status==0">Tidak Aktif</p>
              </td> -->
            </tr>
            <tr ng-show="loading">
              <td colspan="6" style="text-align:center;">
                <div id="p2" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
              </td>
            </tr>
          </table>

          <script type="text/ng-template" id="custom/pager">
            <div ng-if="params.data.length" class="ml-data-table-pager p-10">
              <div ng-if="params.settings().counts.length" class="f-right">
                <button ng-class="{'active':params.count() == 10}" ng-click="params.count(10)" class="mdl-button">10</button>
                <button ng-class="{'active':params.count() == 25}" ng-click="params.count(25)" class="mdl-button">25</button>
                <button ng-class="{'active':params.count() == 50}" ng-click="params.count(50)" class="mdl-button">50</button>
                <button ng-class="{'active':params.count() == 100}" ng-click="params.count(100)" class="mdl-button">100</button>
              </div>
              <span ng-repeat="page in pages"
                  ng-class="{'disabled': !page.active, 'previous': page.type == 'prev', 'next': page.type == 'next'}"
                  ng-switch="page.type">
                <button ng-switch-when="prev" ng-click="params.page(page.number)" class="mdl-button">&laquo;</button>
                <button ng-switch-when="first" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="page" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="more" ng-click="params.page(page.number)" class="mdl-button">&#8230;</button>
                <button ng-switch-when="last" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="next" ng-click="params.page(page.number)" class="mdl-button">&raquo;</button>
              </span>
            </div>
          </script>
        </div>

      </div>
    </div>

  </div>

</section>

<div class="modal fade bs-example-modal-lg" data-backdrop="" id="modalMember" tabindex="-1" role="dialog" aria-labelledby="labelKategori">
  <div class="modal-dialog" style="margin-top: 100px;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="labelKategori">Add Member</h4>
      </div>
      <div class="modal-body">
        
        
      </div>
      <div class="modal-footer">
        <!-- <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="statKategori">
          <input type="checkbox" id="statKategori" class="mdl-switch__input" checked>
          <span class="mdl-switch__label"></span>
        </label> -->
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" ng-click="saveCat()">Save changes</button>
      </div>
    </div>
  </div>