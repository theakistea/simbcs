<section ng-controller="UpdateDataLetterController" class="text-fields">
  <div class="mdl-color--amber ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Letter</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Edit Data</h3>
          <p>An overview of basic form styles and elements.</p>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/Letter">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">

            <form name="myform">
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><b>Template Name</b></p>
                <input class="mdl-textfield__input" type="text" name="template_name" ng-model="template_name" required />
              </div>
			  
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
<div>
<div class="mdl-textfield mdl-js-textfield">
                <p>Type</p>
			        	<select class="mdl-textfield__input" id="type" ng-model="type">
                    <option value="">- Select Type -</option>
                    <option value="satu">Satu</option>
                    <option value="dua">Dua</option>
                    <option value="templatebali">Template Bali</option>
                    <option value="adconfirmation">Confirmation Of Artistic Data</option>
                    <option value="finalconfirmation">Final Confirmation</option>
                </select>
              </div>
<p style="color:blue;"><b>Konten</b></p>
                      <p><strong>Variables For Content</strong></p>
                      <p>*)Event's Name : --event_name</p>
                      <p>*)Member's Name: --member_name</p>
                      <p>*)Host: --host</p>
                      <p>*)Date Start: --date_start</p>
                      <p>*)Date Finish: --date_finish</p>
                      <p>*)PIC: --pic</p>
                      <p>*)Manager: --manager</p>
                      <p>*)Receiver: --receiver</p>
                      <p>*)ChoirName: --choir_name</p>
              </div>
                <textarea class="mdl-textfield__input" ckeditor="editorOptions" type="text" rows="4" id="konten" ng-model="konten" ><?php echo $rows[0]['konten'];?></textarea>
              
              
                 </div>
			        
			      

              <div class="m-t-20">
                <button ng-click="update_data()" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Save
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>