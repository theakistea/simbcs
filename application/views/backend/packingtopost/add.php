<section ng-controller="InsertDataChoirDetailController" class="text-fields">
  <div class="mdl-color--amber ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Choir's Detail</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Add Data</h3>
          <p>An overview of basic form styles and elements.</p>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/ChoirDetail">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form name="myform">
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="name" id="name" ng-model="name" />
                <label class="mdl-textfield__label" for="name">Choir Name</label>
				<span  class="mdl-textfield__error" >Please input the name!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Institution</p>
				<select class="mdl-textfield__input" id="institution" ng-model="institution">
						<?php foreach($get_institution as $rows){ ?>
							<option value="<?php echo $rows->institution?>"><?php echo $rows->institution?></option>
						<?php } ?>
                </select>
              </div>
				
			   <div class="p-30 p-20--small" ng-controller="cacheCategoriesController">
                <p>Categories</p>
				
					<table ng-table="tableParams" template-pagination="custom/pager" class="table mdl-data-table">
					<tr ng-repeat="item in $data" >
					  <td data-title="'ID'" filter="{ 'id_choir_category': 'text' }" sortable="'id_choir_category'" class="mdl-data-table__cell--non-numeric">{{item.id_choir_category}}</td>
					  <td data-title="'CATEGORY'" filter="{ 'name': 'text' }" sortable="'name'" class="mdl-data-table__cell--non-numeric"><a href="#/Choir/edit/{{item.id_choir_category}}">{{item.name}}</a></td>
					</tr>
				  </table>
              </div>
			  <form ng-controller="InsertDataCacheCategoryController">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input class="mdl-textfield__input" type="text" name="category_name" id="category_name" ng-model="category_name" />
					<label class="mdl-textfield__label" for="category_name">Category Name</label>
				</div>
					<button ng-click="insert_data_cache()" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
						Add
					</button>
				</form>
			  <div class="p-30 p-20--small" ng-controller="cacheMemberController">
                <p>Member</p>
					<table ng-table="tableParams" template-pagination="custom/pager" class="table mdl-data-table">
					<tr ng-repeat="item in $data" >
					  <td data-title="'ID'" filter="{ 'id_member': 'text' }" sortable="'id_member'" class="mdl-data-table__cell--non-numeric">{{item.id_member}}</td>
					  <td data-title="'CATEGORY'" filter="{ 'name': 'text' }" sortable="'name'" class="mdl-data-table__cell--non-numeric"><a href="#/Choir/edit/{{item.id_member}}">{{item.name}}</a></td>
					</tr>
				  </table>
              </div>
			  
			  <div class="p-30 p-20--small" ng-controller="cacheConductorController">
                <p>Conductors</p>
					<table ng-table="tableParams" template-pagination="custom/pager" class="table mdl-data-table">
					<tr ng-repeat="item in $data" >
					  <td data-title="'ID'" filter="{ 'id_conductor': 'text' }" sortable="'id_conductor'" class="mdl-data-table__cell--non-numeric">{{item.id_conductor}}</td>
					  <td data-title="'CATEGORY'" filter="{ 'name': 'text' }" sortable="'name'" class="mdl-data-table__cell--non-numeric"><a href="#/Choir/edit/{{item.id_conductor}}">{{item.name}}</a></td>
					</tr>
				  </table>
              </div>
			  
			  <div class="p-30 p-20--small" ng-controller="cacheManagerController">
                <p>Managers</p>
					<table ng-table="tableParams" template-pagination="custom/pager" class="table mdl-data-table">
					<tr ng-repeat="item in $data" >
					  <td data-title="'ID'" filter="{ 'id_manager': 'text' }" sortable="'id_manager'" class="mdl-data-table__cell--non-numeric">{{item.id_manager}}</td>
					  <td data-title="'CATEGORY'" filter="{ 'name': 'text' }" sortable="'name'" class="mdl-data-table__cell--non-numeric"><a href="#/Member/edit/{{item.id_manager}}">{{item.name}}</a></td>
					</tr>
				  </table>
              </div>
              

              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" id="street_address" ng-model="street_address" ></textarea>
                <label class="mdl-textfield__label" for="street_address">Street Address</label>
              </div>
				<div class="mdl-textfield mdl-js-textfield">
                <p>Country</p>
				<select class="mdl-textfield__input" id="country" ng-model="country">
						<?php foreach($get_country as $rows){ ?>
							<option value="a"><?php echo $rows->name?></option>
						<?php } ?>
                </select>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>State</p>
				<select class="mdl-textfield__input" id="state" ng-model="state">
                    <?php foreach($get_state as $rows){ ?>
							<option value="<?php echo $rows->name?>"><?php echo $rows->name?></option>
						<?php } ?>
                </select>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="city" ng-model="city" />
                <label class="mdl-textfield__label" for="city">City Name</label>
              </div>
			  
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text"  pattern="^\s*[a-zA-Z0-9\--_-.]+@[a-zA-Z0-9\--_-.]+\.[a-zA-Z]{2,4}\s*$" id="email" ng-model="email"  required/>
                <label class="mdl-textfield__label" for="email">Email</label>
				<span class="mdl-textfield__error">Please supply a valid email address.</span>
              </div>

			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" id="notes" ng-model="notes" ></textarea>
                <label class="mdl-textfield__label" for="notes">Notes</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="file" id="photo" name="photo" ng-model="photo" />
                <label class="mdl-textfield__label" for="photo">Photo</label>
              </div>
			  <p>Active</p>
			  <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="status">
				  <input type="checkbox" id="status" class="mdl-switch__input" checked>
				  <span class="mdl-switch__label"></span>
			  </label>
              <div class="m-t-20">
                <button ng-click="insert_data()" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Save
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>
