<section ng-controller="UpdateDataPackingToPostController" class="text-fields">
  <div class="mdl-color--amber ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Event Member</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Edit Data</h3>
          <p>An overview of basic form styles and elements.</p>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/PackingToPost">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form>
			  <!--input type="hidden" value="<?php// echo $rows->id_trs_choir_event ?>" name="id_trs_choir_event">
			  <input type="hidden" value="<?php //echo $rows->id_trs_choir ?>" name="id_trs_choir"-->
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <label style="color:blue;">Event Name</label><br>
			        {{title}}
        </div>

        <div class="mdl-textfield mdl-js-textfield">
            <input type="text" ng-model="getTrsChoir.filter()['search']" class="mdl-textfield__input" placeholder="Search data" autofocus />
        </div>
        <div>
          <div ng-repeat="i in confirmdata">  
            <h4>Choir : {{i.choir_name}}</h4>
            <p>Receiver : 
            
            <select ng-model ='confirmrechiver[$index]' class='form-control' ng-change="updateconfirmParams()" ng-if="i.recheiver.length > 0">
               <option value>--Select Recheiver--</option>
               <option ng-repeat="r in i.recheiver" value="{{r.id+'-'+r.type+'-'+r.choir_id}}">{{r.name}}</option>
            </select>
          </div>
        </div>


          <table ng-table="getTrsChoir" template-pagination="custom/pager" class="table mdl-data-table mdl-data-table--selectable fullwidth" style="display:none">
              <tr>
                <td colspan="8" style="text-align:center;">
                    <div id="p2" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
                </td>
              </tr>
          </table>
                                                
        <script type="text/ng-template" id="custom/pager">
            <div ng-if="params.data.length" class="ml-data-table-pager p-10">
            <span ng-repeat="page in pages"
                ng-class="{'disabled': !page.active, 'previous': page.type == 'prev', 'next': page.type == 'next'}"
                ng-switch="page.type">
                <button ng-switch-when="prev" ng-click="params.page(page.number)" class="mdl-button">&laquo;</button>
                <button ng-switch-when="first" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="page" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="more" ng-click="params.page(page.number)" class="mdl-button">&#8230;</button>
                <button ng-switch-when="last" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="next" ng-click="params.page(page.number)" class="mdl-button">&raquo;</button>
            </span>
            </div>
        </script>



        <div class="mdl-textfield mdl-js-textfield">
          <p>Template</p>
                <select  class="mdl-textfield__input"   id="template" name="template" ng-model="template">
                  <option value="">--Select Template--</option>
                  <?php foreach($template as $rows){ ?>
                  <option value="<?php echo $rows->id_letter; ?>"><?php echo $rows->template_name; ?></option>
                  <?php } ?>
                </select>
              </div>
              <a ng-show ="confirmrechiverString != '' && (template  && template != '')" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" href="<?php echo site_url('PackingToPost/download_confirmationad/'.$this->uri->segment(3));?>/{{template}}?data={{confirmrechiverString}}" target="blank">
                PRINT
              </a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>