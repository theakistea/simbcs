<section ng-controller="InsertDataMusicianController" class="text-fields">
  <div class="mdl-color--indigo ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Musician</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Add Data</h3>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/Musician">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form name="myform">
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="name" id="name" ng-model="name" required />
                <label class="mdl-textfield__label" for="name">Name</label>
				<span  class="mdl-textfield__error" >Please input the name!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Gender</p>
				<select class="mdl-textfield__input" id="gender" ng-model="gender" required>
                    <option value="">- Select Gender -</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                </select>
				<span class="mdl-textfield__error">Please select gender.</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Appellation</p>
				<select class="mdl-textfield__input" id="appellation" ng-model="appellation" required>
                    <option value="">- Select Appellation -</option>
                    <option value="0">Mr.</option>
                    <option value="1">Ms.</option>
                    <option value="2">Mrs.</option>
                </select>
				<span class="mdl-textfield__error">Please select Appellation.</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="first_title" ng-model="first_title" />
                <label class="mdl-textfield__label" for="first_title">First Title</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="first_title" ng-model="last_title" />
                <label class="mdl-textfield__label" for="last_title">Last Title</label>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" id="street_address" ng-model="street_address" ></textarea>
                <label class="mdl-textfield__label" for="street_address">Street Address</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Country</p>
				<select class="mdl-textfield__input" id="country" ng-model="country" >
            <option value="">- Select Country -</option>
            <?php foreach($get_country as $rows){ ?>
              <option value="<?php echo $rows['sortname'];?>"><?php echo $rows['name'];?></option>
            <?php } ?>
                </select>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>State</p>
				<select class="mdl-textfield__input" id="state" ng-model="state">
          <option value="">- Select State -</option>
                    <?php foreach($get_state as $rows){ ?>
              <option value="<?php echo $rows['id'];?>"><?php echo $rows['name'];?></option>
            <?php } ?>
                </select>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="city" ng-model="city" />
                <label class="mdl-textfield__label" for="city">City Name</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="phone" ng-model="phone" />
                <label class="mdl-textfield__label" for="phone">Phone</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text"  pattern="^\s*[a-zA-Z0-9\--_-.]+@[a-zA-Z0-9\--_-.]+\.[a-zA-Z]{2,4}\s*$" id="email" ng-model="email" />
                <label class="mdl-textfield__label" for="email">Email</label>
				<span class="mdl-textfield__error">Please supply a valid email address.</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="social_media" ng-model="social_media" />
                <label class="mdl-textfield__label" for="social_media">Social Media</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="website" ng-model="website" />
                <label class="mdl-textfield__label" for="website">Website</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="place_of_birth" ng-model="place_of_birth" />
                <label class="mdl-textfield__label" for="place_of_birth">Place of Birth</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Date of Birth</p>
                <input class="mdl-textfield__input"
                       type="text"
                       id="date_of_birth"
                       pikaday="datepicker2"
                       format="YYYY-MM-DD"
                       on-select="onPikadaySelect(pikaday, date)"
                       theme="material-lite" ng-model="date_of_birth" name="date_of_birth "/>
                
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" id="notes" ng-model="notes" ></textarea>
                <label class="mdl-textfield__label" for="notes">Notes</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <p>Photo</p>
                <img ng-if="photo!=''" src="<?php echo base_url('assets/uploads/img/default.jpg'); ?>" height="200px" width="200px">
				<img ng-show="f.progress >= 100" src="<?php echo base_url('assets/uploads/img/musician/{{photo}}'); ?>" height="200px" width="200px">
                <input type="hidden" ng-model="photo">
                <!-- mdl-button mdl-js-button mdl-button--raised mdl-button--colored -->
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" style="top:53px;margin-left:10px;" type="file" ngf-select="uploadFiles($file, $invalidFiles)"
                    accept="image/*" id="userfile" ngf-max-size="2MB">
                Select File</button>
                
              </div>

              <div class="m-t-20">
                <button ng-click="insert_data()" ng-show="myform.$valid" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Save
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>
