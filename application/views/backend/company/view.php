<section ng-controller="UpdateDataCompanyController" class="text-fields">
  <div class="mdl-color--amber ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Company Data</h3>
      <h4 class="mdl-color-text--amber-100 m-b-20 no-m-t w100">Work with large data sets in tables with NG-Table</h4>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Company Data</h3>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form ng-model="myform">
			<p>Main Information</p>
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" ng-model="phone_number" pattern="[0-9]*"  value="<?php echo $rows->phone_number ?>" type="text" ng-model="phone_number"/>
                <label class="mdl-textfield__label" for="phone_number">Phone Number</label>
				        <span  class="mdl-textfield__error" >Please input phone number!</span>

            </div>
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" ng-model="fax_number" value="<?php echo $rows->fax_number ?>" />
                <label class="mdl-textfield__label" for="fax_number">Fax Number</label>
            </div>
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" ng-model="mailing_address" ><?php echo $rows->mailing_address ?></textarea>
                <label class="mdl-textfield__label" for="mailing_address">Mailing Address</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" ng-model="street_address"><?php echo $rows->street_address ?></textarea>
                <label class="mdl-textfield__label" for="street_address">Street Address</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" ng-model="email_company"   pattern="^\s*[a-zA-Z0-9\--_-.]+@[a-zA-Z0-9\--_-.]+\.[a-zA-Z]{2,4}\s*$" value="<?php echo $rows->email_company ?>" />
                <label class="mdl-textfield__label" for="email_company">Email</label>
				        <span  class="mdl-textfield__error" >Please input valid email address !</span>
              </div>
			  <p>Other Information</p>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" ng-model="contact_person" value="<?php echo $rows->contact_person ?>" />
                <label class="mdl-textfield__label" for="contact_person">Contact Person</label>
				        <span  class="mdl-textfield__error" >Please input brith place!</span>
            </div>
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" ng-model="phone_cp" value="<?php echo $rows->phone_cp ?>" />
                <label class="mdl-textfield__label" for="phone_cp">Phone</label>
				        <span  class="mdl-textfield__error" >Please input brith place!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" ng-model="email_cp"   pattern="^\s*[a-zA-Z0-9\--_-.]+@[a-zA-Z0-9\--_-.]+\.[a-zA-Z]{2,4}\s*$" value="<?php echo $rows->email_cp ?>" />
                <label class="mdl-textfield__label" for="email_cp">Email</label>
                <span  class="mdl-textfield__error" >Please input valid email address!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" ng-model="notes" ><?php echo $rows->notes ?></textarea>
                <label class="mdl-textfield__label" for="notes">Notes/Motto</label>
              </div>
                <button ng-click="update_data()"  type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Save
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>
