<section ng-controller="UpdateDataSongsCategoryController" class="text-fields">
  <div class="mdl-color--deep-purple ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Songs Category</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Edit Data</h3>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/SongsCategory">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form name="myform">
			<input class="mdl-textfield__input" type="hidden" id="date_modify" ng-model="date_modify" value="<?php echo date('d-m-y'); ?>" />
			<input class="mdl-textfield__input" type="hidden" id="id_songs_category" ng-model="id_songs_category" value="<?php echo $rows->id_songs_category ?>" />
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="title_category" ng-model="title_category" value="<?php echo $rows->title_category ?>" />
                <label class="mdl-textfield__label" for="title_category">Title</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" id="notes" ng-model="notes" ><?php echo $rows->notes ?></textarea>
                <label class="mdl-textfield__label" for="notes">Notes</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Status</p>
                <select 
                  class="mdl-textfield__input" 
                  id="status" 
                  ng-model="status" 
                  ng-options="status.value as status.name for status in statuses">
                </select>
              </div>
                <button ng-click="update_data()" type="submit"  ng-show="myform.$valid"class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Update
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>
