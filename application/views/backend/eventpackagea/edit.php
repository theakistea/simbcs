<section ng-controller="UpdateDataEventPackageA" class="text-fields">
  <div class="mdl-color--amber ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Event Package A</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Edit Data</h3>
          <p>An overview of basic form styles and elements.</p>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/EventPackageA">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form name="myform">
              <div class="p-20--small">
              </div>
              <div class="p-20--small">
                <p style="color:blue;"><strong>Event's Name</strong></p>
				<input class="mdl-textfield__input" type="text" name="title" id="title" ng-model="title" readonly/>
              </div>
              <div class="p-20--small">
                <p style="color:blue;"><strong>Choir's Name</strong></p>
				<input class="mdl-textfield__input" type="text" name="choir_name" id="choir_name" ng-model="choir_name" readonly/>
              </div>
              <div class="p-20--small">
                <p style="color:blue;"><strong>Number of Singers: </strong></p><p id="nos"><?php echo $rows->num_of_singer; ?></p>
              </div>

              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"  ng-class="is-dirty">
			  <p style="color:blue;"><strong>Musicians</strong></p>
                <input class="mdl-textfield__input" type="text" name="nom" id="nom" ng-change="cout_total_participant()" ng-model="nom" required pattern="[0-9]*"/>

	              <span class="mdl-textfield__error">Please supply number of Musician.</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" ng-class="is-dirty">
			  <p style="color:blue;"><strong>Conductors</strong></p>
                <input class="mdl-textfield__input" type="text" name="conductor" id="conductor" ng-change="cout_total_participant()" ng-model="conductor" required pattern="[0-9]*"/>
              
	              <span class="mdl-textfield__error">Please supply number of Coductor.</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" ng-class="is-dirty">
			  <p style="color:blue;"><strong>Officials</strong></p>
                <input class="mdl-textfield__input" type="text" name="officials" id="officials" ng-change="cout_total_participant()" ng-model="officials" required pattern="[0-9]*"/>
                <span class="mdl-textfield__error">Please supply number of Officials.</span></div>

              <p id="tot">Totals Participans : {{total_participant}}</p>

              <p style="color:blue;"><strong>T-shirts Size</strong></p>
              <table ng-table="tableParams" id="tshirt" class="table mdl-data-table">
                  <tr class="top">
                    <td data-title="'NO'">{{user.no}}</td>
                    <td data-title="'MEMBER NAME'">{{user.member}}</td>
                    <td data-title="'SIZE'">{{user.ukuran}}</td>
                  </tr>
                </table>

              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Flight Detail</strong></p>
                <input class="mdl-textfield__input" type="text" name="flight_detail" id="flight_detail" ng-model="flight_detail" />
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Staying Address</strong></p>
                <textarea class="mdl-textfield__input" type="text" rows="4" id="staying_address" ng-model="staying_address" ></textarea>
              </div>
			  <h4>Account Information</h4>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"  ng-class="is-dirty">
			  <p style="color:blue;"><strong>Price Per Person</strong></p>
                <input class="mdl-textfield__input" type="text"  id="price_per_person"  ng-change="cout_total_participant()" required  numformat="number" ng-model="price_per_person" />
				<span class="mdl-textfield__error">Please input price per person!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"  ng-class="is-dirty" required pattern="[0-9]*">
			  <p style="color:blue;"><strong>Sub Total</strong></p>
                <input class="mdl-textfield__input" type="text"  readonly id="sub_total" ng-value="sub_total|number" />
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
			  <p style="color:blue;"><strong>Due Date</strong></p>
                <input class="mdl-textfield__input"
                       type="text" required
                       id="due_date"
                       pikaday="datepicker2"
                       format="DD-MM-YYYY"
                       on-select="onPikadaySelect(pikaday, date)"
                       theme="material-lite" ng-model="due_date" name="due_date "/>
				<span class="mdl-textfield__error">Please input due date.</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Phone</strong></p>
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="phone" ng-model="phone" />
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Contact Person</strong></p>
                <input class="mdl-textfield__input" type="text" name="contact_person" id="contact_person" required ng-model="contact_person" />
				<span class="mdl-textfield__error">Please input contact person</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Email</strong></p>
                <input class="mdl-textfield__input" type="text"  pattern="^\s*[a-zA-Z0-9\--_-.]+@[a-zA-Z0-9\--_-.]+\.[a-zA-Z]{2,4}\s*$" id="email" ng-model="email" />
				<span class="mdl-textfield__error">Please supply a valid email address.</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				<p style="color:blue;"><strong>Address</strong></p>
                <textarea class="mdl-textfield__input" type="text" rows="4" id="address" ng-model="address" ></textarea>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				<p style="color:blue;"><strong>Notes</strong></p>
                <textarea class="mdl-textfield__input" type="text" rows="4" id="notes" ng-model="notes" ></textarea>
              </div>
              <div class="m-t-20">
                <button ng-click="update_data()" ng-show="myform.$valid" type="submit" id="insert" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Save
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>

