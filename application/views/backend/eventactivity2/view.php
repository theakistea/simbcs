<section ng-controller="TablesDataEventActivity2Controller" class="tables-data">
  <div class="mdl-color--amber ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Event Activity 2</h3>
      <h4 class="mdl-color-text--amber-100 m-b-20 no-m-t w100">Click name to view detail.</h4>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-20--small">

        <div class="table-search">
        </div>

        <div class="mdl-textfield mdl-js-textfield">
          <input type="text" ng-model="tableParams.filter()['search']" class="mdl-textfield__input" placeholder="Search data" autofocus />
        </div>

        <div class=" mdl-color-text--blue-grey-400">
          <div class="m-t-30">
            <ul class="list-bordered">
              <li><a href="#/EventActivity2/add">
                <i class="material-icons m-r-5 f11">add</i>
                Create a new item
              </a></li>
			  <li><a href="" ng-click="change_status()">
                <i class="material-icons m-r-5 f11">check</i>
                Active/Deactive Selected
              </a></li>
              <!--li><a href="">
                <i class="material-icons m-r-5 f11">file_download</i>
                Export selected items
              </a></li-->
            </ul>
          </div>
        </div>

      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col  mdl-cell--12-col-tablet mdl-cell--12-col-phone">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp m-b-30">
          <table ng-table="tableParams" template-pagination="custom/pager" class="table mdl-data-table mdl-data-table--selectable fullwidth">
            <tr ng-repeat="item in $data" >
              <td style="text-align:center">
                  <input type="checkbox" class="mdl-checkbox__input" name="node[]" value="{{item.id_pd_event_activity_2}}">
              </td>
              <td data-title="'CHOIR NAME'" filter="{ 'choir_name': 'text' }" sortable="'choir_name'" class="mdl-data-table__cell--non-numeric"><a href="#/EventActivity2/edit/{{item.id_pd_event_activity_2}}">{{item.choir_name}}</a></td>
              <td data-title="'EVENT'" filter="{ 'title': 'text' }" sortable="'title'" class="mdl-data-table__cell--non-numeric">{{item.title}}</td>
              <td data-title="'STAGE REHEARSEAL DATE'" filter="{ 'stage_rehearseal_date': 'text' }" sortable="'stage_rehearseal_date'" class="mdl-data-table__cell--non-numeric">{{item.stage_rehearseal_date}}</td>
              <td data-title="'PERFORMANCE SCHEDULE'" sortable="'performance_schedule'" class="mdl-data-table__cell--non-numeric">{{item.performance_schedule}}</td>
            </tr>
          </table>

          <script type="text/ng-template" id="custom/pager">
            <div ng-if="params.data.length" class="ml-data-table-pager p-10">
              <div ng-if="params.settings().counts.length" class="f-right">
                <button ng-class="{'active':params.count() == 10}" ng-click="params.count(10)" class="mdl-button">10</button>
                <button ng-class="{'active':params.count() == 25}" ng-click="params.count(25)" class="mdl-button">25</button>
                <button ng-class="{'active':params.count() == 50}" ng-click="params.count(50)" class="mdl-button">50</button>
                <button ng-class="{'active':params.count() == 100}" ng-click="params.count(100)" class="mdl-button">100</button>
              </div>
              <span ng-repeat="page in pages"
                  ng-class="{'disabled': !page.active, 'previous': page.type == 'prev', 'next': page.type == 'next'}"
                  ng-switch="page.type">
                <button ng-switch-when="prev" ng-click="params.page(page.number)" class="mdl-button">&laquo;</button>
                <button ng-switch-when="first" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="page" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="more" ng-click="params.page(page.number)" class="mdl-button">&#8230;</button>
                <button ng-switch-when="last" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="next" ng-click="params.page(page.number)" class="mdl-button">&raquo;</button>
              </span>
            </div>
          </script>
        </div>

      </div>
    </div>

  </div>

</section>