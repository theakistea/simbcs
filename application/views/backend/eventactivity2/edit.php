<section ng-controller="UpdateDataEventActivity2" class="text-fields">
  <div class="mdl-color--amber ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Event Activity {{total_participant}}</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Add Data</h3>
          <p>An overview of basic form styles and elements.</p>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/EventActivity2">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
	
    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form name="myform">
			<div class="p-20--small">
                <p style="color:blue;"><strong>Event's Name</strong></p>
				<input class="mdl-textfield__input" type="text" name="title" id="title" ng-model="title" readonly/>
              </div>
              <div class="p-20--small">
                <p style="color:blue;"><strong>Choir's Name</strong></p>
				<input class="mdl-textfield__input" type="text" name="choir_name" id="choir_name" ng-model="choir_name" readonly/>
              </div>
              <div class="p-20--small">
                <p style="color:blue;"><strong>Number of Singers: </strong></p><p id="nos">{{num_of_singer}}</p>
              </div>

             <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Musicians</strong></p>
                <input class="mdl-textfield__input" type="text" name="nom" id="nom"  ng-change="cout_total_participant()"  required ng-model="nom" />
                
                <span class="mdl-textfield__error">Please input number of musicians!</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Conductors</strong></p>
                <input class="mdl-textfield__input" type="text" name="conductor" id="conductor"  ng-change="cout_total_participant()" required  ng-model="conductor" />
                <span class="mdl-textfield__error">Please input number of conductors!</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Officials</strong></p>
                <input class="mdl-textfield__input" type="text" name="officials" id="officials"  ng-change="cout_total_participant()"  required ng-model="officials" />
                <span class="mdl-textfield__error">Please input number of officials!</span>
              </div>

              <p id="tot">Totals Participans : {{total_participant}}</p>

			  <div class="mdl-textfield mdl-js-textfield">
			  <p style="color:blue;"><strong>Stage Rehearseal Date</strong></p>
                <input class="mdl-textfield__input"
                       type="text"
                       id="stage_rehearseal_date"
                       pikaday="datepicker2"
                       format="DD-MM-YYYY"
                       on-select="onPikadaySelect(pikaday, date)"
                       theme="material-lite" ng-model="stage_rehearseal_date" name="stage_rehearseal_date"/>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Reg Time</strong></p>
                <uib-timepicker ng-model="reg_time" show-meridian="false"></uib-timepicker>
                <input type="hidden" value="-" id="reg_time" class="mdl-textfield__input" />
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Reg Time Start (HH:MM)</strong></p>
                <uib-timepicker ng-model="reg_time_start" show-meridian="false"></uib-timepicker>
                <input type="hidden" value="-" id="reg_time_start" class="mdl-textfield__input" />
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Reg Time Finish (HH:MM)</strong></p>
        
                <uib-timepicker ng-model="reg_time_finish" show-meridian="false"></uib-timepicker>
                <input type="hidden" value="-" id="reg_time_finish" class="mdl-textfield__input" />
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
			  <p style="color:blue;"><strong>Performance Schedule</strong></p>
                <input class="mdl-textfield__input"
                       type="text"
                       id="performance_schedule"
                       pikaday="datepicker2"
                       format="DD-MM-YYYY"
                       on-select="onPikadaySelect(pikaday, date)"
                       theme="material-lite" ng-model="performance_schedule" name="performance_schedule"/>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Performance Schedule Time</strong></p>
                <input class="mdl-textfield__input" type="text" name="performance_schedule_time" id="performance_schedule_time" ng-model="performance_schedule_time" />
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Re-Reg Time</strong></p>
                <uib-timepicker ng-model="re_reg_time" show-meridian="false"></uib-timepicker>
                <input type="hidden" value="-" id="re_reg_time" class="mdl-textfield__input" />
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Re-Reg TIme Start</strong></p>
                <uib-timepicker ng-model="re_reg_time_start" show-meridian="false"></uib-timepicker>
                <input type="hidden" value="-" id="re_reg_time_start" class="mdl-textfield__input" />
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Re-Reg Time Finish</strong></p>
                <uib-timepicker ng-model="re_reg_time_finish" show-meridian="false"></uib-timepicker>
                <input type="hidden" value="-" id="re_reg_time_finish" class="mdl-textfield__input" />
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Re-Reg Preparation</strong></p>
                <uib-timepicker ng-model="re_reg_time_preparation" show-meridian="false"></uib-timepicker>
                <input type="hidden" value="-" id="re_reg_time_preparation" class="mdl-textfield__input" />
        
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Re-Reg Backstage</strong></p>
                <uib-timepicker ng-model="re_reg_time_backstage" show-meridian="false"></uib-timepicker>
                <input type="hidden" value="-" id="re_reg_time_backstage" class="mdl-textfield__input" />
        
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			  <p style="color:blue;"><strong>Notes</strong></p>
                <textarea class="mdl-textfield__input" type="text" rows="4" name="notes" id="notes" ng-model="notes" ></textarea>
              </div>
              <div class="m-t-20">
                <button ng-click="update_data()" ng-show="myform.$valid"  type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Save
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>
<!-- Modal -->
<div class="modal fade" data-backdrop="" id="modalSearchEvent" tabindex="-1" role="dialog" aria-labelledby="labelMan" ng-controller="SearchEventController3">
  <div class="modal-dialog" style="margin-top: 100px;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="labelMember">Select Event</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
              <div class="box">
                <div class="box-body">
                  <input type="text" ng-model="tableParams.filter()['search']" class="mdl-textfield__input" placeholder="Search data" autofocus />
                  <table ng-table="tableParams" template-pagination="custom/pager" class="table mdl-data-table mdl-data-table--selectable fullwidth">
                    <tr ng-repeat="item in $data">
                      <td style="text-align:center">
                        <input type="radio" ng-model="event.name" ng-value="item.idna">
                          <!-- <input type="checkbox" class="mdl-checkbox__input" name="id_candj[]" value="{{item.id_choir_category}}"> -->
                      </td>
                      <td width="50" data-title="'ID'">{{$index+1}}</td>
                      <td data-title="'NAME'" filter="{ 'name': 'text' }" sortable="'name'" class="mdl-data-table__cell--non-numeric">{{item.title}}</td>
                      <td data-title="'CHOIR'" filter="{ 'choir': 'text' }" sortable="'choir'" class="mdl-data-table__cell--non-numeric">{{item.choir_name}}</td>
                      <!-- <td data-title="'STATUS'" sortable="'status'" class="mdl-data-table__cell--non-numeric">
                        <p ng-if="item.status==1">Aktif</p>
                        <p ng-if="item.status==0">Tidak Aktif</p>
                      </td> -->
                    </tr>
                    <tr ng-show="loading">
                      <td colspan="6" style="text-align:center;">
                        <div id="p2" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
                      </td>
                    </tr>
                  </table>

          <script type="text/ng-template" id="custom/pager">
            <div ng-if="params.data.length" class="ml-data-table-pager p-10">
              <div ng-if="params.settings().counts.length" class="f-right">
                <button ng-class="{'active':params.count() == 10}" ng-click="params.count(10)" class="mdl-button">10</button>
                <button ng-class="{'active':params.count() == 25}" ng-click="params.count(25)" class="mdl-button">25</button>
                <button ng-class="{'active':params.count() == 50}" ng-click="params.count(50)" class="mdl-button">50</button>
                <button ng-class="{'active':params.count() == 100}" ng-click="params.count(100)" class="mdl-button">100</button>
              </div>
              <span ng-repeat="page in pages"
                  ng-class="{'disabled': !page.active, 'previous': page.type == 'prev', 'next': page.type == 'next'}"
                  ng-switch="page.type">
                <button ng-switch-when="prev" ng-click="params.page(page.number)" class="mdl-button">&laquo;</button>
                <button ng-switch-when="first" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="page" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="more" ng-click="params.page(page.number)" class="mdl-button">&#8230;</button>
                <button ng-switch-when="last" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="next" ng-click="params.page(page.number)" class="mdl-button">&raquo;</button>
              </span>
            </div>
          </script>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <pre>{{event.name|json}}</pre>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" ng-click="pilih()" data-dismiss="modal">Pilih</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  
  function cek(){
    var nos = ( $("#nos").html()!='' ) ? $("#nos").html() : 0;
    var nom = ( $("#nom").val()!='' ) ? $("#nom").val() : 0;
    var conductor = ( $("#conductor").val()!='' ) ? $("#conductor").val() : 0;
    var officials = ( $("#officials").val()!='' ) ? $("#officials").val() : 0;

    var hasil = parseInt(nos) + parseInt(nom) + parseInt(conductor) + parseInt(officials);

    console.log(nos + ":"+ nom + ":"+ conductor + ":"+ officials);

    $("#tot").html("Totals Participans : "+ hasil);

  }

</script>