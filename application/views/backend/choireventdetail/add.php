<section ng-controller="InsertDataChoirEventDetailController" class="text-fields">
  <div class="mdl-color--amber ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Choir's Event Detail</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Add Data</h3>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/ChoirEventDetail">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form name="myform">
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                      <input class="mdl-textfield__input" type="text" name="title" id="title" ng-model="title" required/>
                      <label class="mdl-textfield__label" for="title">Title</label>
                      <span  class="mdl-textfield__error" >Please input the title!</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                      <input class="mdl-textfield__input" type="text" name="code_event" id="code_event" ng-model="code_event" required/>
                      <label class="mdl-textfield__label" for="code_event">Event Code</label>
                      <span  class="mdl-textfield__error" >Please input the event code!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Grade</p>
				<select class="mdl-textfield__input" id="grade" ng-model="grade" required>
						<option value="">- Select Grade -</option>
						<?php foreach($get_grade as $rows){ ?>
							<option value="<?php echo $rows->name?>"><?php echo $rows->name?></option>
						<?php } ?>
                </select>
                <span  class="mdl-textfield__error" >Please Select Grade!</span>
        
              </div>
			   <div class="mdl-textfield mdl-js-textfield">
                <input class="mdl-textfield__input"
                       type="text"
                       id="date_start"
                       pikaday="datepicker2" required
                       format="DD-MM-YYYY"
                       on-select="onPikadaySelect(pikaday, date)"
                       theme="material-lite" ng-model="date_start" name="date_start "/>
                       <span  class="mdl-textfield__error" >Please input start date!</span>
        
                <span  class="mdl-textfield__error" >Please input the title!</span>
                <label class="mdl-textfield__label" for="date_start">Date Start</label>
              </div>
				 <div class="mdl-textfield mdl-js-textfield">
                <input class="mdl-textfield__input"
                       type="text"
                       id="date_finish"
                       pikaday="datepicker2"
                       format="DD-MM-YYYY" required
                       on-select="onPikadaySelect(pikaday, date)"
                       theme="material-lite" ng-model="date_finish" name="date_finish "/>
                
                <span  class="mdl-textfield__error" >Please input finish date!</span>
                <label class="mdl-textfield__label" for="date_finish">Date Finish</label>
              </div>
			<div class="mdl-textfield mdl-js-textfield">
                <p>Country</p>
				<select class="mdl-textfield__input" id="country" ng-model="country"  required>
				<option value="">- Select Country -</option>
						<?php foreach($get_country as $rows){ ?>
							<option value="<?php echo $rows->name?>"><?php echo $rows->name?></option>
						<?php } ?>
                </select>
                
                <span  class="mdl-textfield__error" >Please select country</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>State</p>
				<select class="mdl-textfield__input" id="state" ng-model="state"  required>
				<option value="">- Select State -</option>
                    <?php foreach($get_state as $rows){ ?>
							<option value="<?php echo $rows->name?>"><?php echo $rows->name?></option>
						<?php } ?>
                </select>
                
                <span  class="mdl-textfield__error" >Please select state!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="city" ng-model="city"  required/>
                
                <span  class="mdl-textfield__error" >Please input city name!</span>
                <label class="mdl-textfield__label" for="city">City Name</label>
              </div>
			  
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="host" ng-model="host"  required/>
                <label class="mdl-textfield__label" for="host">Host By</label>
             
                <span  class="mdl-textfield__error" >Please input host by!</span>
                 </div>
			  
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="PIC" ng-model="PIC"  required/>
                
                <span  class="mdl-textfield__error" >Please input pic!</span>
                <label class="mdl-textfield__label" for="PIC">PIC</label>
              </div>
			  
		<div class="p-20--small">
                
                <p>Venue and Type
                <span 
                  data-toggle="modal" data-target="#modalVenueAndType"
                  data-upgraded=",MaterialButton,MaterialRipple" 
                  class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored" ng-click="resetVenueField()">
                    <i class="material-icons">search</i>
                    <span class="mdl-button__ripple-container">
                      <span style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);" class="mdl-ripple is-animating">
                      </span>
                    </span>
                </span>
                </p>
                <div class="">
                <table ng-table="tableParams" template-pagination="custom/pager" id="venueTable" class="table mdl-data-table">
                  <tr ng-repeat="item in $data" >
                    <td data-title="'NO'">No</td>
                    <td data-title="'Venue'">{{item.venue}}</td>
                    <td data-title="'Nama Kategori'">{{item.type}}</td>
                    <!-- <td data-title="'Action'" sortable="'action'">{{item.action}}</td> -->
                  </tr>
                  <tr ng-repeat="venue in eusiVenue" class="eusiVenue">
                    <td>{{$index+1}}</td>
					<td data-title="'Venue'">{{venue.name}}</td>
                    <td>{{venue.type}}</td>
                  </tr>
                </table>
                <button ng-click="resetVenue()" class="mdl-button mdl-js-button mdl-button--accent">Reset</button>
                </div>
            </div><br><br>
		  
		 <div class="p-20--small">
                
                <p>Clinnician's and Juries
                <!--span 
                  data-toggle="modal" data-target="#modalCandj"
                  data-upgraded=",MaterialButton,MaterialRipple" 
                  class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                    <i class="material-icons">add</i>
                    <span class="mdl-button__ripple-container">
                      <span style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);" class="mdl-ripple is-animating">
                      </span>
                    </span>
                </span-->
                <span 
                  data-toggle="modal" data-target="#modalInsCandj"
                  data-upgraded=",MaterialButton,MaterialRipple" 
                  class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                    <i class="material-icons">search</i>
                    <span class="mdl-button__ripple-container">
                      <span style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);" class="mdl-ripple is-animating">
                      </span>
                    </span>
                </span>
                </p>
                <div class="">
                <table ng-table="tableParams" template-pagination="custom/pager" id="candjTable" class="table mdl-data-table">
                  <tr ng-repeat="item in $data" >
                    <td data-title="'NO'">No</td>
                    <td data-title="'NAME'">{{item.name}}</td>
                    <!-- <td data-title="'Action'" sortable="'action'">{{item.action}}</td> -->
                  </tr>
                  <tr ng-repeat="candj in isiCandj" class="eusiCandj">
                    <td>{{$index+1}}</td>
                    <td>{{candj.name}}</td>
                  </tr>
                </table>
                <button ng-click="resetCandj()" class="mdl-button mdl-js-button mdl-button--accent">Reset</button>
                </div>
            </div><br><br>
		  
		  <div class="p-20--small">
                
                <p>Choir
                </p>
                <div class="">
                <table ng-table="tableParams" template-pagination="custom/pager" id="choirTable" class="table mdl-data-table">
                  <tr ng-repeat="item in $data" >
                    <td data-title="'NO'">No</td>
                    <td data-title="'Choir Name'">{{item.choir_name}}</td>
                    <!-- <td data-title="'Action'" sortable="'action'">{{item.action}}</td> -->
                  </tr>
                  <tr ng-repeat="choir in isiChoir" class="eusiChoir">
                    <td>{{$index+1}}</td>
                    <td>{{choir.choir_name}}</td>
                  </tr>
                </table>
                <button ng-click="resetChoir()" class="mdl-button mdl-js-button mdl-button--accent">Reset</button>
                </div>
            </div><br><br>
		  
		  <div class="p-20--small">
                 
                <p>Committees
                <span 
                  data-toggle="modal" data-target="#modalInsCommittee"
                  data-upgraded=",MaterialButton,MaterialRipple" 
                  class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                    <i class="material-icons">search</i>
                    <span class="mdl-button__ripple-container">
                      <span style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);" class="mdl-ripple is-animating">
                      </span>
                    </span>
                </span>
                </p>
                <div class="">
                <table ng-table="tableParams" template-pagination="custom/pager" id="comTable" class="table mdl-data-table">
                  <tr ng-repeat="item in $data" >
                    <td data-title="'NO'">No</td>
                    <td data-title="'Committees'">{{item.name}}</td>
                    <!-- <td data-title="'Action'" sortable="'action'">{{item.action}}</td> -->
                  </tr>
                  <tr ng-repeat="com in isiCom" class="eusiCom">
                    <td>{{$index+1}}</td>
                    <td>{{com.name}}</td>
                  </tr>
                </table>
                <button ng-click="resetCom()" class="mdl-button mdl-js-button mdl-button--accent">Reset</button>
                </div>
            </div><br><br>
		  
			<div class="p-20--small">
                
                <p>Sponsor
                <span 
                  data-toggle="modal" data-target="#modalSponsor"
                  data-upgraded=",MaterialButton,MaterialRipple" 
                  class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored" ng-click="resetSponsor()">
                    <i class="material-icons"  ng-click="resetSponsor()">add</i>
                    <span class="mdl-button__ripple-container">
                      <span style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);" class="mdl-ripple is-animating">
                      </span>
                    </span>
                </span>
                </p>
                <div class="">
                <table ng-table="tableParams" template-pagination="custom/pager" id="sponsorTable" class="table mdl-data-table">
                  <tr ng-repeat="item in $data" >
                    <td data-title="'NO'">No</td>
                    <td data-title="'Sponsor'">{{item.sponsor}}</td>
                    <td data-title="'As'">{{item.sponsor_as}}</td>
                    <!-- <td data-title="'Action'" sortable="'action'">{{item.action}}</td> -->
                  </tr>
                  <tr ng-repeat="sponsor in isiSponsor" class="eusiSponsor">
                    <td>{{$index+1}}</td>
                    <td>{{sponsor.name}}</td>
                    <td>{{sponsor.sponsor_as}}</td>
                  </tr>
                </table>
                <button ng-click="resetSponsor()" class="mdl-button mdl-js-button mdl-button--accent">Reset</button>
                </div>
            </div><br><br>
		  
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" id="notes" ng-model="notes" ></textarea>
                <label class="mdl-textfield__label" for="notes">Notes</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				<p>Poster</p>
        <img ng-if="photo!=''" src="<?php echo base_url('assets/uploads/img/default.jpg'); ?>" height="200px" width="200px">
                <img ng-show="f.progress >= 100" src="<?php echo base_url('assets/uploads/img/choireventdetail/{{photo}}'); ?>" height="200px" width="200px">
                <input type="hidden" value="default.jpg" ng-model="photo">
                <!-- mdl-button mdl-js-button mdl-button--raised mdl-button--colored -->
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" style="top:53px;margin-left:10px;" type="file" ngf-select="uploadFiles($file, $invalidFiles)"
                    accept="image/*" id="userfile" ngf-max-size="2MB">
                Select File</button>
                
         </div>
			  <p>Active</p>
			  <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="status">
				  <input type="checkbox" id="status" class="mdl-switch__input" checked>
				  <span class="mdl-switch__label"></span>
			  </label>
              <div class="m-t-20">
                <button ng-click="insert_data()" ng-show="myform.$valid" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Save
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>
<!-- Modal Sponsor -->
<div class="modal fade" data-backdrop="" id="modalSponsor" tabindex="-1" role="dialog" aria-labelledby="labelSponsor" ng-controller="sponsorChoirEvent">
  <div class="modal-dialog" style="margin-top: 100px;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="labelSponsor">Add Sponsor</h4>
      </div>
      <div class="modal-body">
        <form>
        <div class="row">
          <div class="col-md-6">
            Sponsor : <input class="form-control" type="text" ng-model="sponsor.name">
          </div>
		  <div class="col-md-6">
            As : <input class="form-control" type="text" ng-model="sponsor.as">
          </div>
		</div>
      </div>
	  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" ng-click="pilih()" data-dismiss="modal">Add</button>
      </div>
    </div>
   </div>
</div>
<!-- Modal Venue and Type -->
<div class="modal fade" data-backdrop="" id="modalVenueAndType" tabindex="-1" role="dialog" aria-labelledby="labelVenue" ng-controller="PilihVenueAndType">
  <div class="modal-dialog" style="margin-top: 100px;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="labelVenue">Add Venue and Type</h4>
      </div>
      <div class="modal-body">
        <form>
        <div class="row">
          <div class="col-md-6">
            Venue : <input class="form-control" type="text" ng-model="venue.name">
            Choir Category :
            <select class="form-control" ng-model="venue.type">
              <option value="">- Select Category -</option>
              <?php foreach($get_kategori as $rows){ ?>
                <option value="<?php echo $rows->name ?>"><?php echo $rows->name?></option>
              <?php } ?>
            </select>

            Venue Purpose :
            <select class="form-control" ng-model="venue.purpose">
              <option value="">- Select Category -</option>
              <option value="1">Registration</option>
              <option value="2">Uji coba panggung</option>
              <option value="3">Competition</option>
              <option value="4">Choir clinic</option>
              
            </select>
          </div>
          
	    	</div>
    </div>
	  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" ng-click="pilih()" data-dismiss="modal">Add</button>
      </div>
    </div>
   </div>
</div>

<!-- Modal CANDJ -->
<div class="modal fade" data-backdrop="" id="modalCandj" tabindex="-1" role="dialog" aria-labelledby="labelCandj">
  <div class="modal-dialog" style="margin-top: 100px;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="labelCandj">Add Clinnician's and Juries</h4>
      </div>
      <div class="modal-body">
        <form>
        <div class="row">
          <div class="col-md-6">
            Name : <input class="form-control" type="text" ng-model="candj.name">
          </div>
          <div class="col-md-6">
            Institution : <input class="form-control" type="text" ng-model="candj.institution">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            Gender : 
              <select class="form-control" ng-model="candj.gender">
                <option value="">-Select Gender-</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
              </select>
          </div>
          <div class="col-md-6">
            Appellation : 
              <select class="form-control" ng-model="candj.appellation">
                <option value="">-Select Appellation-</option>
                <option value="0">Mr.</option>
                <option value="1">Ms.</option>
                <option value="2">Mrs.</option>
              </select>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            First Title : <input class="form-control" type="text" ng-model="candj.ageCat">
          </div>
          <div class="col-md-6">
            Last Title : <input class="form-control" type="text" ng-model="candj.minsingCat">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            Street Address : <textarea class="form-control" ng-model="candj.notesCat"></textarea>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            Country : 
              <select class="form-control" ng-model="candj.gender">
                <option value="">-Select Country-</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
              </select>
          </div>
          <div class="col-md-6">
            State : 
              <select class="form-control" ng-model="candj.gender">
                <option value="">-Select State-</option>
                <option value="0">Mr.</option>
                <option value="1">Ms.</option>
                <option value="2">Mrs.</option>
              </select>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            City Name : <input class="form-control" type="text" ng-model="candj.numacapCat">
          </div>
          <div class="col-md-6">
            Phone : <input class="form-control" type="text" ng-model="candj.numacapCat">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            Email : <input class="form-control" type="email" ng-model="candj.numacapCat">
          </div>
          <div class="col-md-6">
            Social Media : <input class="form-control" type="text" ng-model="candj.numacapCat">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            Website : <input class="form-control" type="email" ng-model="candj.numacapCat">
          </div>
          <div class="col-md-6">
            Place of Birth : <input class="form-control" type="text" ng-model="candj.numacapCat">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            Date of Birth : <input class="form-control" type="email" ng-model="candj.numacapCat">
          </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" ng-click="saveCat()">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Add Choose CANDJ -->
<div class="modal fade" data-backdrop="" id="modalInsCandj" tabindex="-1" role="dialog" aria-labelledby="labelCandj" ng-controller="PilihCandjChoirEvent">
  <div class="modal-dialog" style="margin-top: 100px;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="labelCandj">Choose Clinnician's and Juries</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
              <div class="box">
                <div class="box-body">
                  <input type="text" ng-model="tableParams.filter()['search']" class="mdl-textfield__input" placeholder="Search data" autofocus />
                  <table ng-table="tableParams" template-pagination="custom/pager" class="table mdl-data-table mdl-data-table--selectable fullwidth">
                    <tr ng-repeat="item in $data">
                      <td style="text-align:center">
                        <input type="checkbox" checklist-model="candjData.id_candj" checklist-value="item.id_candj">
                      </td>
                      <td width="50" data-title="'ID'">{{$index+1}}</td>
                      <td data-title="'NAME'" filter="{ 'name': 'text' }" sortable="'name'" class="mdl-data-table__cell--non-numeric">{{item.name}}</td>
                      <td data-title="'NOTES'" filter="{ 'notes': 'text' }" sortable="'notes'" class="mdl-data-table__cell--non-numeric">{{item.notes}}</td>
                      <!-- <td data-title="'STATUS'" sortable="'status'" class="mdl-data-table__cell--non-numeric">
                        <p ng-if="item.status==1">Aktif</p>
                        <p ng-if="item.status==0">Tidak Aktif</p>
                      </td> -->
                    </tr>
                    <tr ng-show="loading">
                      <td colspan="6" style="text-align:center;">
                        <div id="p2" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
                      </td>
                    </tr>
                  </table>

          <script type="text/ng-template" id="custom/pager">
            <div ng-if="params.data.length" class="ml-data-table-pager p-10">
              <div ng-if="params.settings().counts.length" class="f-right">
                <button ng-class="{'active':params.count() == 10}" ng-click="params.count(10)" class="mdl-button">10</button>
                <button ng-class="{'active':params.count() == 25}" ng-click="params.count(25)" class="mdl-button">25</button>
                <button ng-class="{'active':params.count() == 50}" ng-click="params.count(50)" class="mdl-button">50</button>
                <button ng-class="{'active':params.count() == 100}" ng-click="params.count(100)" class="mdl-button">100</button>
              </div>
              <span ng-repeat="page in pages"
                  ng-class="{'disabled': !page.active, 'previous': page.type == 'prev', 'next': page.type == 'next'}"
                  ng-switch="page.type">
                <button ng-switch-when="prev" ng-click="params.page(page.number)" class="mdl-button">&laquo;</button>
                <button ng-switch-when="first" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="page" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="more" ng-click="params.page(page.number)" class="mdl-button">&#8230;</button>
                <button ng-switch-when="last" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="next" ng-click="params.page(page.number)" class="mdl-button">&raquo;</button>
              </span>
            </div>
          </script>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <pre>{{candjData.id_candj|json}}</pre>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" ng-click="pilih()" data-dismiss="modal">Pilih</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Add Choose Choir -->
<div class="modal fade" data-backdrop="" id="modalInsChoir" tabindex="-1" role="dialog" aria-labelledby="labelChoir" ng-controller="PilihChoirChoirEvent">
  <div class="modal-dialog" style="margin-top: 100px;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="labelChoir">Choose Choir</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
              <div class="box">
                <div class="box-body">
                  <input type="text" ng-model="tableParams.filter()['search']" class="mdl-textfield__input" placeholder="Search data" autofocus />
                  <table ng-table="tableParams" template-pagination="custom/pager" class="table mdl-data-table mdl-data-table--selectable fullwidth">
                    <tr ng-repeat="item in $data">
                      <td style="text-align:center">
                        <input type="checkbox" checklist-model="choirData.id_trs_choir" checklist-value="item.id_trs_choir">
                          <!-- <input type="checkbox" class="mdl-checkbox__input" name="id_candj[]" value="{{item.id_choir_category}}"> -->
                      </td>
                      <td width="50" data-title="'ID'">{{$index+1}}</td>
                      <td data-title="'NAME'" filter="{ 'choir_name': 'text' }" sortable="'choir_name'" class="mdl-data-table__cell--non-numeric">{{item.choir_name}}</td>
                      <td data-title="'INSTITUTION'" filter="{ 'institution': 'text' }" sortable="'institution'" class="mdl-data-table__cell--non-numeric">{{item.institution}}</td>
                      <!-- <td data-title="'STATUS'" sortable="'status'" class="mdl-data-table__cell--non-numeric">
                        <p ng-if="item.status==1">Aktif</p>
                        <p ng-if="item.status==0">Tidak Aktif</p>
                      </td> -->
                    </tr>
                    <tr ng-show="loading">
                      <td colspan="6" style="text-align:center;">
                        <div id="p2" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
                      </td>
                    </tr>
                  </table>

          <script type="text/ng-template" id="custom/pager">
            <div ng-if="params.data.length" class="ml-data-table-pager p-10">
              <div ng-if="params.settings().counts.length" class="f-right">
                <button ng-class="{'active':params.count() == 10}" ng-click="params.count(10)" class="mdl-button">10</button>
                <button ng-class="{'active':params.count() == 25}" ng-click="params.count(25)" class="mdl-button">25</button>
                <button ng-class="{'active':params.count() == 50}" ng-click="params.count(50)" class="mdl-button">50</button>
                <button ng-class="{'active':params.count() == 100}" ng-click="params.count(100)" class="mdl-button">100</button>
              </div>
              <span ng-repeat="page in pages"
                  ng-class="{'disabled': !page.active, 'previous': page.type == 'prev', 'next': page.type == 'next'}"
                  ng-switch="page.type">
                <button ng-switch-when="prev" ng-click="params.page(page.number)" class="mdl-button">&laquo;</button>
                <button ng-switch-when="first" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="page" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="more" ng-click="params.page(page.number)" class="mdl-button">&#8230;</button>
                <button ng-switch-when="last" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="next" ng-click="params.page(page.number)" class="mdl-button">&raquo;</button>
              </span>
            </div>
          </script>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <pre>{{choirData.id_trs_choir|json}}</pre>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" ng-click="pilih()" data-dismiss="modal">Pilih</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Add Choose Committee -->
<div class="modal fade" data-backdrop="" id="modalInsCommittee" tabindex="-1" role="dialog" aria-labelledby="labelComm" ng-controller="PilihComChoirEvent">
  <div class="modal-dialog" style="margin-top: 100px;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="labelComm">Choose Committee</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
              <div class="box">
                <div class="box-body">
                  <input type="text" ng-model="tableParams.filter()['search']" class="mdl-textfield__input" placeholder="Search data" autofocus />
                  <table ng-table="tableParams" template-pagination="custom/pager" class="table mdl-data-table mdl-data-table--selectable fullwidth">
                    <tr ng-repeat="item in $data">
                      <td style="text-align:center">
                        <input type="checkbox" checklist-model="comData.id_committee" checklist-value="item.id_committee">
                      </td>
                      <td width="50" data-title="'ID'">{{$index+1}}</td>
                      <td data-title="'NAME'" filter="{ 'name': 'text' }" sortable="'name'" class="mdl-data-table__cell--non-numeric">{{item.name}}</td>
                      <td data-title="'INSTITUTION'" filter="{ 'institution': 'text' }" sortable="'institution'" class="mdl-data-table__cell--non-numeric">{{item.institution}}</td>
                      <!-- <td data-title="'STATUS'" sortable="'status'" class="mdl-data-table__cell--non-numeric">
                        <p ng-if="item.status==1">Aktif</p>
                        <p ng-if="item.status==0">Tidak Aktif</p>
                      </td> -->
                    </tr>
                    <tr ng-show="loading">
                      <td colspan="6" style="text-align:center;">
                        <div id="p2" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
                      </td>
                    </tr>
                  </table>

          <script type="text/ng-template" id="custom/pager">
            <div ng-if="params.data.length" class="ml-data-table-pager p-10">
              <div ng-if="params.settings().counts.length" class="f-right">
                <button ng-class="{'active':params.count() == 10}" ng-click="params.count(10)" class="mdl-button">10</button>
                <button ng-class="{'active':params.count() == 25}" ng-click="params.count(25)" class="mdl-button">25</button>
                <button ng-class="{'active':params.count() == 50}" ng-click="params.count(50)" class="mdl-button">50</button>
                <button ng-class="{'active':params.count() == 100}" ng-click="params.count(100)" class="mdl-button">100</button>
              </div>
              <span ng-repeat="page in pages"
                  ng-class="{'disabled': !page.active, 'previous': page.type == 'prev', 'next': page.type == 'next'}"
                  ng-switch="page.type">
                <button ng-switch-when="prev" ng-click="params.page(page.number)" class="mdl-button">&laquo;</button>
                <button ng-switch-when="first" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="page" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="more" ng-click="params.page(page.number)" class="mdl-button">&#8230;</button>
                <button ng-switch-when="last" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="next" ng-click="params.page(page.number)" class="mdl-button">&raquo;</button>
              </span>
            </div>
          </script>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <pre>{{comData.id_committee|json}}</pre>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" ng-click="pilih()" data-dismiss="modal">Pilih</button>
      </div>
    </div>
  </div>
</div>
