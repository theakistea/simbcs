<section ng-controller="UpdateDataConductorController" class="text-fields">
  <div class="mdl-color--lime ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Conductor</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Edit Data</h3>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/conductor">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form name="myform">
      <input class="mdl-textfield__input" type="hidden" id="date_modify" ng-model="date_modify" value="<?php echo date('d-m-y'); ?>" />
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="name" ng-model="name" value="<?php echo $rows->name ?>" required />
                <label class="mdl-textfield__label" for="name">Name</label>
        <span class="mdl-textfield__error">Please input name!</span>
              </div>
        <div class="mdl-textfield mdl-js-textfield">
                <p>Gender</p>
                <select 
                  class="mdl-textfield__input" 
                  id="gender" 
                  ng-model="gender" 
                  ng-options="gender.value as gender.name for gender in genders">
                </select>
              </div>
        <div class="mdl-textfield mdl-js-textfield">
                <p>Appellation</p>
                <select 
                  class="mdl-textfield__input" 
                  id="appellation" 
                  ng-model="appellation" 
                  ng-options="appellation.value as appellation.name for appellation in appellations">
                </select>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="first_title" ng-model="first_title"  value="<?php echo $rows->first_title ?>" />
                <label class="mdl-textfield__label" for="first_title">First Title</label>
        <span class="mdl-textfield__error">Please input first title!.</span>
              </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="first_title" ng-model="last_title"  value="<?php echo $rows->last_title ?>" />
                <label class="mdl-textfield__label" for="last_title">Last Title</label>
        <span class="mdl-textfield__error">Please input last title!.</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" id="street_address" ng-model="street_address" ><?php echo $rows->street_address ?></textarea>
                <label class="mdl-textfield__label" for="street_address">Street Address</label>
              </div>
        <div class="mdl-textfield mdl-js-textfield">
                <p>Country</p>
                <select 
                  class="mdl-textfield__input" 
                  id="country" 
                  ng-model="country" 
                  ng-options="country.sortname as country.name for country in allCountry">
                </select>
              </div>
        <div class="mdl-textfield mdl-js-textfield">
                <p>State</p>
                <select 
                  class="mdl-textfield__input" 
                  id="state" 
                  ng-model="state" 
                  ng-options="state.id as state.name for state in allState">
                </select>
              </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="city" ng-model="city" value="<?php echo $rows->city ?>" required />
                <label class="mdl-textfield__label" for="city">City Name</label>
        <span class="mdl-textfield__error">Please input city name.</span>
              </div>
        <div class="mdl-textfield mdl-js-textfield">
                <p>Address status</p>
                <select 
                  class="mdl-textfield__input" 
                  id="address_status" 
                  ng-model="address_status" 
                  ng-options="address_status.value as address_status.name for address_status in statuses">
                </select>
              </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="phone" ng-model="phone" required pattern="[0-9]*" value="<?php echo $rows->phone ?>" />
                <label class="mdl-textfield__label" for="phone">Phone</label>
        <span class="mdl-textfield__error">Please supply a valid phone number.</span>
              </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="email" ng-model="email" required  pattern="^\s*[a-zA-Z0-9\--_-.]+@[a-zA-Z0-9\--_-.]+\.[a-zA-Z]{2,4}\s*$"  value="<?php echo $rows->email ?>" />
                <label class="mdl-textfield__label" for="email">Email</label>
        <span class="mdl-textfield__error">Please supply a valid email address.</span>
              </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="social_media" ng-model="social_media" value="<?php echo $rows->social_media ?>" />
                <label class="mdl-textfield__label" for="social_media">Social Media</label>
              </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="website" ng-model="website" value="<?php echo $rows->website ?>" />
                <label class="mdl-textfield__label" for="website">Website</label>
              </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="place_of_birth"  ng-model="place_of_birth" value="<?php echo $rows->place_of_birth ?>" />
                <label class="mdl-textfield__label" for="place_of_birth">Place of Birth</label>
        <span class="mdl-textfield__error">Please input brith place!</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield">
                <p>Date of Birth</p>
                <input class="mdl-textfield__input"
                       type="text"
                       id="date_of_birth"
                       pikaday="datepicker2"
                       format="YYYY-MM-DD"
                       on-select="onPikadaySelect(pikaday, date)"
                       theme="material-lite" ng-model="date_of_birth" value="<?php echo $rows->date_of_birth; ?>" />
        <span class="mdl-textfield__error">Please input brith date.</span>
                
              </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" id="notes" ng-model="notes" ><?php echo $rows->notes ?></textarea>
                <label class="mdl-textfield__label" for="notes">Notes</label>
              </div>

              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <p>Photo</p>
                <img ng-if="photo!=''" src="<?php echo base_url('assets/uploads/img/conductor/{{photo}}'); ?>" height="200px" width="200px">
                <input type="hidden" ng-model="photo">
				<button ng-if="photo!=''" ng-click="delete_photo(photo)" id="photo" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" style="top:95px;margin-left:10px;">
                  Delete
                </button>
                <!-- mdl-button mdl-js-button mdl-button--raised mdl-button--colored -->
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" style="margin-left: 10px;top:53px;" type="file" ngf-select="uploadFiles($file, $invalidFiles)"
                accept="image/*" ngf-max-height="1000" id="userfile" ngf-max-size="1MB">
                  Select File
                </button>
                
              </div>

        <div class="mdl-textfield mdl-js-textfield">
                <p>Status</p>
                <select 
                  class="mdl-textfield__input" 
                  id="status" 
                  ng-model="status" 
                  ng-options="status.value as status.name for status in statuses">
                </select>
              </div>
                <button ng-click="update_data()" ng-show="myform.$valid" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Update
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>