<section ng-controller="UpdateDataChoirController" class="text-fields">
  <div class="mdl-color--amber ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Choir Category</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Edit Data</h3>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/Choir">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form name="myform">
			<input class="mdl-textfield__input" type="hidden" id="date_modify" ng-model="date_modify" value="<?php echo date('d-m-y'); ?>" />
			        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="name" id="name" ng-model="name" value="<?php echo $rows->name ?>" required />
                <label class="mdl-textfield__label" for="name">Name</label>
			        	<span  class="mdl-textfield__error" >Please input the name!</span>
              </div>
              
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="code_category" id="code_category" ng-model="code_category" value="<?php echo $rows->code_category ?>" required />
                <label class="mdl-textfield__label" for="code_category">Category Code</label>
				        <span  class="mdl-textfield__error" >Please input the categry code!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Type</p>
                <select 
                  class="mdl-textfield__input" 
                  id="type" 
                  ng-model="type" 
                  ng-options="type.value as type.name for type in types">
                </select>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="age" ng-model="age" ng-min="1" ng-max="100" min="1" max="100"  value="<?php echo $rows->age ?>" required />
                <label class="mdl-textfield__label" for="age">Age</label>
				<span class="mdl-textfield__error">Digits only Or Exceed Maximum ! </span>
              </div>
              
				<span class="">Age Minimum 1 And Maximum 100</span>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="min_singer" ng-model="min_singer"   value="<?php echo $rows->min_singer ?>" required />
                <label class="mdl-textfield__label" for="min_singer">Minimum Singer</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="max_perform_time" ng-model="max_perform_time" value="<?php echo $rows->max_perform_time ?>"  required />
                <label class="mdl-textfield__label" for="max_perform_time">Max Perform Time</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="num_of_song" ng-model="num_of_song" value="<?php echo $rows->num_of_song ?>" required />
                <label class="mdl-textfield__label" for="num_of_song">Number of Song</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="num_of_acapela_song" ng-model="num_of_acapela_song" value="<?php echo $rows->num_of_acapela_song ?>" required />
                <label class="mdl-textfield__label" for="num_of_acapela_song">Number of Acapela Song</label>
				<span class="mdl-textfield__error">Digits only!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>With Amplification</p>
                <select 
                  class="mdl-textfield__input" 
                  id="with_amplification" 
                  ng-model="with_amplification" 
                  ng-options="with_amplification.value as with_amplification.name for with_amplification in with_amplifications">
                </select>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input"  rows="4" id="notes" ng-model="notes" ><?php echo $rows->notes ?></textarea>
                <label class="mdl-textfield__label" for="notes">Notes</label>
              </div>
              
				<span class="">Max 300 character Current : {{notes.length}}</span>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Status</p>
                <select 
                  class="mdl-textfield__input" 
                  id="status" 
                  ng-model="status" 
                  ng-options="status.value as status.name for status in statuses">
                </select>
              </div>
              <div class="m-t-20">
                <button ng-click="update_data()"  ng-show="myform.$valid" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Update
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>
