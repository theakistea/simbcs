<section ng-controller="UpdateDataSongsController" class="text-fields">
  <div class="mdl-color--pink ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Songs</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Edit Data</h3>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/songs">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form name="myform">
			<input class="mdl-textfield__input" type="hidden" id="date_modify" ng-model="date_modify" value="<?php echo date('d-m-y'); ?>" />
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="title" id="title" ng-model="title" value="<?php echo $rows->title ?>" required />
                <label class="mdl-textfield__label" for="title">Title</label>
				<span  class="mdl-textfield__error" >Please input the title!</span>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="composer" ng-model="composer" value="<?php echo $rows->composer ?>" />
                <label class="mdl-textfield__label" for="composer">Composer</label>
              </div>

              
        <div class="mdl-textfield mdl-js-textfield">
                <p>Year</p>
                <div class="row">
                  <div class="col-lg-6">
                    
                    <select class="mdl-textfield__input" id="type" ng-model="year_start">
                        <option value="">- Select Year -</option>
                        <?php 
                          for ($i=  date('Y') ; $i >= 1800; $i --) {
                            echo("<option value='$i'>$i</option>");
                          }
                        ?>
                    </select>
                  </div>
                  
                  <div class="col-lg-6">
                    
                    <select class="mdl-textfield__input" id="type" ng-model="year_end">
                        <option value="">- Select Year -</option>
                        <?php 
                          for ($i=  date('Y') ; $i >= 1800; $i --) {
                            echo("<option value='$i'>$i</option>");
                          }
                        ?>
                    </select>
                  </div>
                </div>
              </div>

             
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Type of Songs</p>
                <input class="mdl-textfield__input" max="100" type="text" id="type" ng-model="type" />
              </div>
 
		  <div class="p-20--small">
                
                <p>Song's Category
                <span 
                  data-toggle="modal" data-target="#modalTypeAndSong"
                  data-upgraded=",MaterialButton,MaterialRipple" 
                  class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                    <i class="material-icons">search</i>
                    <span class="mdl-button__ripple-container">
                      <span style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);" class="mdl-ripple is-animating">
                      </span>
                    </span>
                </span>
                </p>
                <div class="">
                <table ng-table="tableParams" template-pagination="custom/pager" id="kategoriTable" class="table mdl-data-table">
                  <tr ng-repeat="item in $data" >
                    <td data-title="'NO'">No</td>
                    <td data-title="'Committees'">{{item.comittee_name}}</td>
                    <!-- <td data-title="'Action'" sortable="'action'">{{item.action}}</td> -->
                  </tr>
                  <tr ng-repeat="comm in dataCategory" class="eusi">
                    <td>{{$index+1}}</td>
                    <td>{{comm.title_category}}</td>
                  </tr>
                </table>
                <button ng-click="resetCat()" class="mdl-button mdl-js-button mdl-button--accent">Reset</button>
                </div>
            </div><br><br>


              
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="publisher" ng-model="publisher" value="<?php echo $rows->publisher ?>"/>
                <label class="mdl-textfield__label" for="publisher">Publisher</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Accompanied</p>
                <select 
                  class="mdl-textfield__input" 
                  id="accompanied_type" 
                  ng-model="accompanied_type" 
                  ng-options="accompanied_type.value as accompanied_type.name for accompanied_type in accompanied">
                </select>
              </div>
			   <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="accompanied_value" value="<?php echo $rows->accompanied_value;?>" ng-model="accompanied_value" />
                <label class="mdl-textfield__label" for="accompanied_value">Accompanied Value</label>
              </div>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" id="notes" ng-model="notes" ></textarea>
                <label class="mdl-textfield__label" for="notes">Notes</label>
              </div>
              
				<span class="">Max 300 character Current : {{notes.length}}</span>
			  <p>Song Sheet File</p>
        <a ng-if="file_not_balok!='none'" target="blank" href="<?php echo base_url('assets/uploads/file/songs/balok/'.$rows->file_not_balok);?>">
          <?php echo $rows->file_not_balok;?>
        </a>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
         <input type="hidden" ng-model="balok">
          <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" style="top:0" type="file_not_balok" ngf-select="uploadFileBalok($file, $invalidFiles)"
                    accept="application/pdf" id="userfile" ngf-max-size="2MB">
                Select File</button> 

                <!-- <input class="mdl-textfield__input" type="file" id="file_not_balok" name="file_not_balok" ng-model="file_not_balok" /> -->
              </div>
			  <p style="margin-top: 20px">Not Angka</p>
        <a ng-if="file_not_angka!='none'" target="blank" href="<?php echo base_url('assets/uploads/file/songs/angka/'.$rows->file_not_angka);?>">
          <?php echo $rows->file_not_angka;?>
        </a>
			  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input type="hidden" ng-model="angka">
          <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" style="top:0" type="file_not_angka" ngf-select="uploadFileAngka($file, $invalidFiles)"
                    accept="application/pdf" id="userfile" ngf-max-size="2MB">
                Select File</button>
                <!-- <input class="mdl-textfield__input" type="file" id="file_not_angka" name="file_not_angka" ng-model="file_not_angka" /> -->
              </div>
			  <div class="mdl-textfield mdl-js-textfield">
                <p>Status</p>
                <select 
                  class="mdl-textfield__input" 
                  id="status" 
                  ng-model="status" 
                  ng-options="status.value as status.name for status in statuses">
                </select>
              </div>
              <div class="m-t-20">
                <button ng-click="update_data()"  ng-show="myform.$valid" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Save
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>


<!-- Modal Add Choose Committee -->
<div class="modal fade" data-backdrop="" id="modalTypeAndSong" tabindex="-1" role="dialog" aria-labelledby="labelComm" ng-controller="PilihCategory">
  <div class="modal-dialog" style="margin-top: 100px;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="labelComm">Choose Category</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
              <div class="box">
                <div class="box-body">
                  <input type="text" ng-model="tableParams.filter()['search']" class="mdl-textfield__input" placeholder="Search data" autofocus />
                  <table ng-table="tableParams" template-pagination="custom/pager" class="table mdl-data-table mdl-data-table--selectable fullwidth">
                    <tr ng-repeat="item in $data">
                      <td style="text-align:center">
                      
                        <input type="checkbox" checklist-model="dataCategory.id_category" checklist-value="item.id_songs_category">
                      </td>
                      <td width="50" data-title="'ID'">{{$index+1}}</td>
                      <td data-title="'NAME'" filter="{ 'title_category': 'text' }" sortable="'name'" class="mdl-data-table__cell--non-numeric">{{item.title_category}}</td>
                      <!-- <td data-title="'STATUS'" sortable="'status'" class="mdl-data-table__cell--non-numeric">
                        <p ng-if="item.status==1">Aktif</p>
                        <p ng-if="item.status==0">Tidak Aktif</p>
                      </td> -->
                    </tr>
                    <tr ng-show="loading">
                      <td colspan="6" style="text-align:center;">
                        <div id="p2" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
                      </td>
                    </tr>
                  </table>

          <script type="text/ng-template" id="custom/pager">
            <div ng-if="params.data.length" class="ml-data-table-pager p-10">
              <div ng-if="params.settings().counts.length" class="f-right">
                <button ng-class="{'active':params.count() == 10}" ng-click="params.count(10)" class="mdl-button">10</button>
                <button ng-class="{'active':params.count() == 25}" ng-click="params.count(25)" class="mdl-button">25</button>
                <button ng-class="{'active':params.count() == 50}" ng-click="params.count(50)" class="mdl-button">50</button>
                <button ng-class="{'active':params.count() == 100}" ng-click="params.count(100)" class="mdl-button">100</button>
              </div>
              <span ng-repeat="page in pages"
                  ng-class="{'disabled': !page.active, 'previous': page.type == 'prev', 'next': page.type == 'next'}"
                  ng-switch="page.type">
                <button ng-switch-when="prev" ng-click="params.page(page.number)" class="mdl-button">&laquo;</button>
                <button ng-switch-when="first" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="page" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="more" ng-click="params.page(page.number)" class="mdl-button">&#8230;</button>
                <button ng-switch-when="last" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="next" ng-click="params.page(page.number)" class="mdl-button">&raquo;</button>
              </span>
            </div>
          </script>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <pre>{{dataCategory.id_category|json}}</pre>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" ng-click="pilih()" data-dismiss="modal">Pilih</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Add Choose Member -->
