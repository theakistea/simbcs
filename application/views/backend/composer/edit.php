<section ng-controller="UpdateDataComposerController" class="text-fields">
  <div class="mdl-color--teal ml-header relative clear">
    <div class="p-20">
      <h3 class="mdl-color-text--white m-t-20 m-b-5">Composer's and Arrangers</h3>
    </div>
  </div>

  <div class="mdl-grid mdl-grid--no-spacing">

    <div class="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-color--grey-100">
      <div class="p-40 p-r-20 p-20--small">
        <div class=" mdl-color-text--blue-grey-400">
          <h3><i class="material-icons f-left m-r-5">format_align_left</i> Edit Data</h3>
          <div class="m-t-30">
            <ul class="list-bordered">
              <li>
                <a href="#/Composer">
                  <i class="material-icons m-r-5 f11">arrow_back</i>
                  Back to Data
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l">
      <div class="p-20 ml-card-holder ml-card-holder-first">
        <div class="mdl-card mdl-shadow--1dp">
          <div class="p-30">
            <form name="myform">
      <input class="mdl-textfield__input" type="hidden" id="date_modify" ng-model="date_modify" value="<?php echo date('d-m-y'); ?>" />
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="name" ng-model="name" value="<?php echo $rows->name ?>" required/>
                <label class="mdl-textfield__label" for="name">Name</label>
                <span  class="mdl-textfield__error" >Please input the name!</span>
              </div>
        <div class="mdl-textfield mdl-js-textfield">
                <p>Gender</p>
                <select 
                  class="mdl-textfield__input" 
                  id="gender" 
                  ng-model="gender" 
                  ng-options="gender.value as gender.name for gender in genders">
                </select>
              </div>
        <div class="mdl-textfield mdl-js-textfield">
                <p>Appellation</p>
                <select 
                  class="mdl-textfield__input" 
                  id="appellation" 
                  ng-model="appellation" 
                  ng-options="appellation.value as appellation.name for appellation in appellations">
                </select>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="first_title" ng-model="first_title" value="<?php echo $rows->first_title ?>"  />
                <label class="mdl-textfield__label" for="first_title">First Title</label>
                <span  class="mdl-textfield__error" >Please input first name!</span>
              </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="first_title" ng-model="last_title" value="<?php echo $rows->last_title ?>" />
                <label class="mdl-textfield__label" for="last_title">Last Title</label>
                <span  class="mdl-textfield__error" >Please input last name!</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <p>Street Address</p>
                <textarea class="mdl-textfield__input" type="text" rows="4" id="street_address" ng-model="street_address" required><?php echo $rows->street_address ?></textarea>
                <span  class="mdl-textfield__error" >Please input street address!</span>
              </div>
        <div class="mdl-textfield mdl-js-textfield">
                <p>Country</p>
                <select 
                  class="mdl-textfield__input" 
                  id="country" 
                  ng-model="country" 
                  ng-options="country.sortname as country.name for country in allCountry">
                </select>
              </div>
        <div class="mdl-textfield mdl-js-textfield">
                <p>State</p>
                <select 
                  class="mdl-textfield__input" 
                  id="state" 
                  ng-model="state" 
                  ng-options="state.id as state.name for state in allState">
                </select>
              </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="city" ng-model="city" value="<?php echo $rows->city ?>" required />
                <label class="mdl-textfield__label" for="city">City Name</label>
                <span  class="mdl-textfield__error" >Please input city name!</span>
              </div>
        <div class="mdl-textfield mdl-js-textfield">
                <p>Address status</p>
                <select 
                  class="mdl-textfield__input" 
                  id="address_status" 
                  ng-model="address_status" 
                  ng-options="address_status.value as address_status.name for address_status in statuses">
                </select>
              </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="phone" pattern="[0-9]*" ng-model="phone" value="<?php echo $rows->phone ?>" />
                <label class="mdl-textfield__label" for="phone">Phone</label>
                <span  class="mdl-textfield__error" >Please input valid phone!</span>
              </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="email"  pattern="^\s*[a-zA-Z0-9\--_-.]+@[a-zA-Z0-9\--_-.]+\.[a-zA-Z]{2,4}\s*$"  ng-model="email" value="<?php echo $rows->email ?>" />
                <label class="mdl-textfield__label" for="email">Email</label>
                <span  class="mdl-textfield__error" >Please input valid email!</span>
              </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="social_media" ng-model="social_media" value="<?php echo $rows->social_media ?>" />
                <label class="mdl-textfield__label" for="social_media">Social Media</label>
              </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="website" ng-model="website" value="<?php echo $rows->website ?>" />
                <label class="mdl-textfield__label" for="website">Website</label>
              </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="place_of_birth" ng-model="place_of_birth" value="<?php echo $rows->place_of_birth ?>" />
                <label class="mdl-textfield__label" for="place_of_birth">Place of Birth</label>
                <span  class="mdl-textfield__error" >Please input brith place!</span>
              </div>
              <div class="mdl-textfield mdl-js-textfield">
                <p>Date of Birth</p>
                <input class="mdl-textfield__input"
                       type="text"
                       id="date_of_birth"
                       pikaday="datepicker2"
                       format="YYYY-MM-DD"
                       on-select="onPikadaySelect(pikaday, date)"
                       theme="material-lite" ng-model="date_of_birth" value="<?php echo $rows->date_of_birth; ?>" />
                <span  class="mdl-textfield__error" >Please input brith date!</span>
                
              </div>
			  <div class="p-20--small">
                
                <p>Songs
                <span 
                  data-toggle="modal" data-target="#modalInsSongs"
                  data-upgraded=",MaterialButton,MaterialRipple" 
                  class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                    <i class="material-icons">search</i>
                    <span class="mdl-button__ripple-container">
                      <span style="width: 160.392px; height: 160.392px; transform: translate(-50%, -50%) translate(26px, 29px);" class="mdl-ripple is-animating">
                      </span>
                    </span>
                </span>
                </p>
                <div class="">
                <table ng-table="tableParams" template-pagination="custom/pager" id="songsTable" class="table mdl-data-table">
                  <tr ng-repeat="item in $data" >
                    <td data-title="'NO'">No</td>
                    <td data-title="'TITLE'">{{item.title}}</td>
                    <td data-title="'TYPE'">{{item.type}}</td>
                    <!-- <td data-title="'Action'" sortable="'action'">{{item.action}}</td> -->
                  </tr>
			
                  <tr class="eusiSongs" ng-repeat="song in songss">
                    <td>{{$index+1}}</td>
                    <td>{{song.title}}</td>
                    <td>{{song.type}}</td>
                    
                  </tr>
				  
                  <tr ng-repeat="songs in isiSongs" class="eusiSongs">
                    <td>{{$index+1}}</td>
                    <td>{{songs.title}}</td>
                    <td>{{songs.type}}</td>
                  </tr>
                </table>
                <button ng-click="resetSongs()" class="mdl-button mdl-js-button mdl-button--accent">Reset</button>
                </div>
            </div><br><br>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="4" id="notes" ng-model="notes" ><?php echo $rows->notes ?></textarea>
                <label class="mdl-textfield__label" for="notes">Notes</label>
              </div>

              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <p>Photo</p>
                <img ng-if="photo!=''" src="<?php echo base_url('assets/uploads/img/composer/{{photo}}'); ?>" height="200px" width="200px">
                <input type="hidden" ng-model="photo">
				<button ng-if="photo!=''" ng-click="delete_photo(photo)" id="photo" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" style="top:95px;margin-left:10px;">
                  Delete
                </button>
                <!-- mdl-button mdl-js-button mdl-button--raised mdl-button--colored -->
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" style="margin-left: 10px;top:53px;" type="file" ngf-select="uploadFiles($file, $invalidFiles)"
                accept="image/*" ngf-max-height="1000" id="userfile" ngf-max-size="1MB">
                  Select File
                </button>
              </div>

        <div class="mdl-textfield mdl-js-textfield">
                <p>Status</p>
                <select 
                  class="mdl-textfield__input" 
                  id="status" 
                  ng-model="status" 
                  ng-options="status.value as status.name for status in statuses">
                </select>
              </div>
                <button ng-click="update_data()" ng-show="myform.$valid" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                  Update
                </button>
                <button type="reset" class="mdl-button mdl-js-button mdl-js-ripple-effect">
                  Reset
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>  
</section>
<!-- Modal Add songsData -->
<div class="modal fade" data-backdrop="" id="modalInsSongs" tabindex="-1" role="dialog" aria-labelledby="labelsongsData" ng-controller="PilihSongsComposer">
  <div class="modal-dialog" style="margin-top: 100px;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="labelsongsData">Choose Song</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
              <div class="box">
                <div class="box-body">
                  <input type="text" ng-model="tableParams.filter()['search']" class="mdl-textfield__input" placeholder="Search data" autofocus />
                  <table ng-table="tableParams" template-pagination="custom/pager" class="table mdl-data-table mdl-data-table--selectable fullwidth">
                    <tr ng-repeat="item in $data">
                      <td style="text-align:center">
                        <input type="checkbox" checklist-model="songsData.id_songs" checklist-value="item.id_songs">
                          <!-- <input type="checkbox" class="mdl-checkbox__input" name="id_candj[]" value="{{item.id_songsData}}"> -->
                      </td>
					  
                      <td width="50" data-title="'ID'">{{$index+1}}</td>
                      <td data-title="'TITLE'" filter="{ 'title': 'text' }" sortable="'title'" class="mdl-data-table__cell--non-numeric">{{item.title}}</td>
                      <td data-title="'TYPE'" filter="{ 'type': 'text' }" sortable="'type'" class="mdl-data-table__cell--non-numeric">{{item.type}}</td>
                      <!-- <td data-title="'STATUS'" sortable="'status'" class="mdl-data-table__cell--non-numeric">
                        <p ng-if="item.status==1">Aktif</p>
                        <p ng-if="item.status==0">Tidak Aktif</p>
                      </td> -->
                    </tr>
					
                    <tr ng-show="loading">
                      <td colspan="6" style="text-align:center;">
                        <div id="p2" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
                      </td>
                    </tr>
                  </table>

          <script type="text/ng-template" id="custom/pager">
            <div ng-if="params.data.length" class="ml-data-table-pager p-10">
              <div ng-if="params.settings().counts.length" class="f-right">
                <button ng-class="{'active':params.count() == 10}" ng-click="params.count(10)" class="mdl-button">10</button>
                <button ng-class="{'active':params.count() == 25}" ng-click="params.count(25)" class="mdl-button">25</button>
                <button ng-class="{'active':params.count() == 50}" ng-click="params.count(50)" class="mdl-button">50</button>
                <button ng-class="{'active':params.count() == 100}" ng-click="params.count(100)" class="mdl-button">100</button>
              </div>
              <span ng-repeat="page in pages"
                  ng-class="{'disabled': !page.active, 'previous': page.type == 'prev', 'next': page.type == 'next'}"
                  ng-switch="page.type">
                <button ng-switch-when="prev" ng-click="params.page(page.number)" class="mdl-button">&laquo;</button>
                <button ng-switch-when="first" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="page" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="more" ng-click="params.page(page.number)" class="mdl-button">&#8230;</button>
                <button ng-switch-when="last" ng-click="params.page(page.number)" class="mdl-button"><span ng-bind="page.number"></span></button>
                <button ng-switch-when="next" ng-click="params.page(page.number)" class="mdl-button">&raquo;</button>
              </span>
            </div>
          </script>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <pre style="display:none;">{{songsData.id_songs|json}}</pre>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" ng-click="pilih()" data-dismiss="modal">Pilih</button>
      </div>
    </div>
  </div>
</div>
