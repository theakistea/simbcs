<?php
class Muser extends CI_Model {
	
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
	}

	public function getUserGroup(){
		$query = $this->db->get("user_group");
		$query = $query->result_array();

		return $query;
	}
	
}