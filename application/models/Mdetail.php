<?php
class Mdetail extends CI_Model {
	
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}

	function get_member(){
		$query = "SELECT * FROM mst_member";
		$sql = $this->db->query($query);
		return $sql->result();
	}
	function get_memberAktif(){
		return $this->db->get_where('mst_member',array('status'=>1))->result();

	}function get_candjAktif(){
		return $this->db->get_where('mst_candj',array('status'=>1))->result();

	}
	function get_songsAktif(){
		return $this->db->get_where('mst_songs',array('status'=>1))->result();

	}
	function get_comAktif(){
		return $this->db->get_where('mst_committee',array('status'=>1))->result();
	}
	function get_conductorAktif(){
		return $this->db->get_where('mst_conductor',array('status'=>1))->result();
	}
	function get_managerAktif(){
		return $this->db->get_where('mst_manager',array('status'=>1))->result();
	}
	function get_choirAktif(){
		$query = "SELECT * FROM trs_choir WHERE status = 1";
		$sql = $this->db->query($query);
		return $sql->result();
	}
	
	function getOneMember($where=array()){
		return $this->db->get_where('mst_member',$where)->result_array();
	}
	function getOneMember2($where=array()){
		$ambil = $this->db->query("SELECT * FROM mst_member INNER JOIN trs_dtl_choir_mem ON trs_dtl_choir_mem.id_member = mst_member.id_member INNER JOIN trs_choir ON trs_choir.id_trs_choir = trs_dtl_choir_mem.id_trs_choir WHERE mst_member.id_member ='$where'")->result_array();
		return $ambil;
	}
	function getOneSongs($where=array()){
		return $this->db->get_where('mst_songs',$where)->result_array();
	}
	function getOneConductor($where=array()){
		return $this->db->get_where('mst_conductor',$where)->result_array();
	}
	function getOneManager($where=array()){
		return $this->db->get_where('mst_manager',$where)->result_array();
	}
	
	function get_grade(){
		$query = "SELECT name FROM mst_event_grade";
		$sql = $this->db->query($query);
		return $sql->result();
	}
	
	function get_institution(){
		$query = "SELECT institution FROM mst_member";
		$sql = $this->db->query($query);
		return $sql->result();
	}
	
}