<?php
class User_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
	}
	function tambah_data(){
		$data = array(
        'username' => $this->input->post('username'),
        'firstName' => $this->input->post('firstName'),
        'lastName' => $this->input->post('lastName'),
        'email' =>  $this->input->post('email'),
        'password' =>  $this->input->post('password'),
        'idUserGroup' =>  $this->input->post('idUserGroup'),
        'active' => 1
		);
		$this->db->insert('user', $data);
	}
	function update_data(){
		$this->db->set('username',$this->input->post('username'));
		$this->db->set('firstName',$this->input->post('firstName'));
		$this->db->set('lastName',$this->input->post('lastName'));
		$this->db->set('email',$this->input->post('email'));
		$this->db->set('password',$this->input->post('password'));
		$this->db->where('idUser', $this->input->post('idUser') );
		$this->db->update('user'); 
	}
	function get_data(){
		$this->db->where('active', '1');
		$this->db->join('user_group', 'user.idUserGroup = user_group.idUserGroup');
		$query = $this->db->get('user');
		return $query->result();
	}
	function get_id(){
		$this->db->where('idUser', $this->input->post('idUser'));
		$query = $this->db->get('user');
		return $query->result();
	}
	function hapus_data(){
		$this->db->where('idUser', $this->input->post('idUser'));
		$this->db->delete('user');
	}
}
