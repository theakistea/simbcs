<?php
class Master_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
	}
	function tambah_data_candj(){
        $nmfile = date('dmy').time('hms'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/uploads/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '1288'; //lebar maksimum 1288 px
        $config['max_height']  = '768'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
		$this->load->library('upload',$config);
         if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                        $data = array('photo' => $this->upload->data());
                }
		$data = array(
		'id_candj' => $this->input->post('id'),
        'name' => $this->input->post('name'),
        'gender' => $this->input->post('gender'),
        'appellation' => $this->input->post('appellation'),
        'first_title' =>  $this->input->post('first_title'),
        'last_title' =>  $this->input->post('last_title'),
        'street_address' =>  $this->input->post('street_address'),
        'city' =>  $this->input->post('city'),
        'state' =>  $this->input->post('state'),
        'state' =>  $this->input->post('state'),
        'country' =>  $this->input->post('country'),
        'address_status' =>  $this->input->post('address_status'),
        'website' =>  $this->input->post('website'),
        'place_of_birth' =>  $this->input->post('place_of_birth'),
        'date_of_birth' =>  $this->input->post('date_of_birth'),
        'notes' =>  $this->input->post('notes'),
        'user_added' =>  $this->input->post('username'),
        'status' =>  1
		);
		$this->db->insert('mst_candj', $data);
		$data_phone = array(
			'phone' => $this->input->post('phone'),
			'id_master_all' => $this->input->post('id'),
			'status_phone' => 1
		);
		$this->db->insert('phone', $data_phone);
		$data_email = array(
			'email' => $this->input->post('email'),
			'id_master_all' => $this->input->post('id'),
			'status_email' => 1
		);
		$this->db->insert('email', $data_email);
		$data_social = array(
			'social_media' => $this->input->post('social_media'),
			'id_master_all' => $this->input->post('id'),
			'status_social' => 1
		);
		$this->db->insert('social_media', $data_social);
	}
	function update_data_candj(){
		$this->db->where('id_candj', $this->input->post('id_candj'));
		$this->db->set('name',$this->input->post('name'));
		$this->db->update('mst_candj'); 
	}
	function update_status_candj($id_candj){
		foreach($this->input->post('node') as $change){
			$this->db->where('id_candj', $change);
			$this->db->set('status',0);
			$this->db->update('mst_candj'); 
		}
	}
	function update_status_candj_active(){
		foreach($this->input->post('node') as $change){
			$this->db->where('id_candj', $change);
			$this->db->set('status',1);
			$this->db->update('mst_candj'); 
		}
	}
	function get_data_candj(){	
		$this->db->where('status_phone', '1');
		$this->db->where('status_email', '1');
		$this->db->where('status_social', '1');
		$this->db->join('phone', 'phone.id_master_all = mst_candj.id_candj');
		$this->db->join('email', 'email.id_master_all = mst_candj.id_candj');
		$this->db->join('social_media', 'social_media.id_master_all = mst_candj.id_candj');
		$query = $this->db->get('mst_candj');
		return $query->result();
	}
	function get_id_candj(){
		$query = "SELECT id_candj FROM mst_candj WHERE SUBSTRING(id_candj,4,2)= SUBSTRING(date_added,6,2) ORDER BY id_candj DESC LIMIT 1";
		$sql = $this->db->query($query);
		return $sql->result();
	}
	function show_data_candj($id_candj){
		$this->db->where('id_candj', $id_candj);
		$this->db->where('status_phone', '1');
		$this->db->where('status_email', '1');
		$this->db->where('status_social', '1');
		$this->db->join('phone', 'phone.id_master_all = mst_candj.id_candj');
		$this->db->join('email', 'email.id_master_all = mst_candj.id_candj');
		$this->db->join('social_media', 'social_media.id_master_all = mst_candj.id_candj');
		$query = $this->db->get('mst_candj');
		return $query->result();
	}
	function get_data_committee(){
		$this->db->where('status_phone', '1');
		$this->db->join('phone', 'phone.id_master_all = mst_committee.id_committee');
		$query = $this->db->get('mst_committee');
		return $query->result();
	}
	function show_data_committee(){
		$this->db->where('status_phone', '1');
		$this->db->join('phone', 'phone.id_master_all = mst_committee.id_committee');
		$query = $this->db->get('mst_committee');
		return $query->result();
	}
	function get_data_conductor(){
		$this->db->where('status_phone', '1');
		$this->db->join('phone', 'phone.id_master_all = mst_conductor.id_conductor');
		$query = $this->db->get('mst_conductor');
		return $query->result();
	}
	function show_data_conductor(){
		$this->db->where('status_phone', '1');
		$this->db->join('phone', 'phone.id_master_all = mst_conductor.id_conductor');
		$query = $this->db->get('mst_conductor');
		return $query->result();
	}
	function get_data_manager(){
		$this->db->where('status_phone', '1');
		$this->db->join('phone', 'phone.id_master_all = mst_manager.id_manager');
		$query = $this->db->get('mst_manager');
		return $query->result();
	}
	function show_data_manager(){
		$this->db->where('status_phone', '1');
		$this->db->join('phone', 'phone.id_master_all = mst_manager.id_manager');
		$query = $this->db->get('mst_manager');
		return $query->result();
	}
	function get_data_member(){
		$this->db->where('status_phone', '1');
		$this->db->join('phone', 'phone.id_master_all = mst_member.id_member');
		$query = $this->db->get('mst_member');
		return $query->result();
	}
	function show_data_member(){
		$this->db->where('status_phone', '1');
		$this->db->join('phone', 'phone.id_master_all = mst_member.id_member');
		$query = $this->db->get('mst_member');
		return $query->result();
	}
	function get_data_musician(){
		$this->db->where('status_phone', '1');
		$this->db->join('phone', 'phone.id_master_all = mst_musician.id_musician');
		$query = $this->db->get('mst_musician');
		return $query->result();
	}
	function show_data_musician(){
		$this->db->where('status_phone', '1');
		$this->db->join('phone', 'phone.id_master_all = mst_musician.id_musician');
		$query = $this->db->get('mst_musician');
		return $query->result();
	}
	function get_data_officials(){
		$this->db->where('status_phone', '1');
		$this->db->join('phone', 'phone.id_master_all = mst_officials.id_officials');
		$query = $this->db->get('mst_officials');
		return $query->result();
	}
	function show_data_officials(){
		$this->db->where('status_phone', '1');
		$this->db->join('phone', 'phone.id_master_all = mst_officials.id_officials');
		$query = $this->db->get('mst_officials');
		return $query->result();
	}
}
