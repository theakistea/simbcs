<?php
class Mcountry extends CI_Model {
	
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
	}

	public function getCountry(){
		$query = $this->db->get("countries");
		$query = $query->result_array();

		return $query;
	}

	public function getState(){
		$query = $this->db->get("states");
		$query = $query->result_array();

		return $query;
	}

	function get_country(){
		$query = "SELECT name FROM countries";
		$sql = $this->db->query($query);
		return $sql->result();
	}
	function get_state(){
		$query = "SELECT name FROM states";
		$sql = $this->db->query($query);
		return $sql->result();
	}
	
}