<?php
class Mcategory extends CI_Model {
	
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
	}

	function get_category(){
		$query = "SELECT title_category,id_songs_category FROM mst_songs_category";
		$sql = $this->db->query($query);
		return $sql->result();
	}
	
	function get_choircategoryAktif(){
		// $this->db->order_by('cat_order  asc');
		return $this->db->get_where('mst_choir_category',array('status'=>1))->result();
	}

	function getOneCatChoir($where=array()){
		return $this->db->get_where('mst_choir_category',$where)->result_array();
	}
}