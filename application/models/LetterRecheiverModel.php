<?php
class LetterRecheiverModel extends CI_Model {

    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
    }

    public function getParticipant($event_id , $type){

        $result = array(
            'conductors' => [],
            'managers' => [],
            'members' => []
        );
        $this->db->select("choir.id_trs_choir");
        $this->db->from("trs_choir as choir");
        $this->db->join('(SELECT id_trs_choir , id_trs_choir_event FROM trs_dtl_choir_event_choir group by id_trs_choir , id_trs_choir_event) as nest_choid_event' , "nest_choid_event.id_trs_choir =  choir.id_trs_choir");
        $this->db->where(array('nest_choid_event.id_trs_choir_event' => $event_id));
        $this->db->group_by('choir.id_trs_choir');
        $choirs = $this->db->get()->result_array();



        $choirIds = [];


        foreach ($choirs as $choir){
            array_push($choirIds , $choir['id_trs_choir']);
        }

        if(is_array($type)){
            if(in_array($type , 3)){
                $result['members'] = ($this->getMember($choirIds));
            }
            if(in_array($type , 1)){
                $result['conductors'] = ($this->getConductor($choirIds));
            }
            if(in_array($type , 2)){
                $result['managers'] = ($this->getManager($choirIds));
            }
        } else {
            if($type == '*'){
                $result['conductors'] = ($this->getConductor($choirIds));
                $result['managers'] = ($this->getManager($choirIds));
                $result['members'] = ($this->getMember($choirIds));
            }
        }
        return $result;


    }

    public function getParticipantByChoir($choirId , $type){
        

        $choirIds = [0=> $choirId];

        if(is_array($type)){
            if(in_array($type , 3)){
                $result['members'] = ($this->getMember($choirIds));
            }
            if(in_array($type , 1)){
                $result['conductors'] = ($this->getConductor($choirIds));
            }
            if(in_array($type , 2)){
                $result['managers'] = ($this->getManager($choirIds));
            }
        } else {
            if($type == '*'){
                $result['conductors'] = ($this->getConductor($choirIds));
                $result['managers'] = ($this->getManager($choirIds));
                $result['members'] = ($this->getMember($choirIds));
            }
        }
        return $result;


    }

    public function getConductor ($choirid){
        $this->db->select('conductor.* , conductor.id_conductor  as id ,  ("1") as type , nest_choid_event.id_trs_choir as choir_id  , (SELECT  choir_name FROM trs_choir WHERE trs_choir.id_trs_choir = nest_choid_event.id_trs_choir)  as choir_name') ;
        $this->db->from('mst_conductor as conductor');
        $this->db->join('trs_dtl_choir_con as choir_con' , "conductor.id_conductor =  choir_con.id_conductor");
        $this->db->join('(SELECT id_trs_choir , id_trs_choir_event FROM trs_dtl_choir_event_choir group by id_trs_choir , id_trs_choir_event ) as nest_choid_event' , "nest_choid_event.id_trs_choir =  choir_con.id_trs_choir");
        if(is_array($choirid)){

            $this->db->where_in('nest_choid_event.id_trs_choir', $choirid);
        }else{
            $this->db->where(array('nest_choid_event.id_trs_choir' => $choirid));
        }
        $this->db->group_by('conductor.id_conductor');
        return $this->db->get()->result_array();
    }
    public function getManager ($choirid){

        $this->db->select('manager.* , manager.id_manager  as id ,  ("2") as type , nest_choid_event.id_trs_choir as choir_id , (SELECT  choir_name FROM trs_choir WHERE trs_choir.id_trs_choir = nest_choid_event.id_trs_choir)  as choir_name' );
        $this->db->from('mst_manager as manager');
        $this->db->join('trs_dtl_choir_man as choir_man' , "manager.id_manager =  choir_man.id_manager");
        $this->db->join('(SELECT id_trs_choir , id_trs_choir_event FROM trs_dtl_choir_event_choir group by id_trs_choir , id_trs_choir_event) as nest_choid_event' , "nest_choid_event.id_trs_choir =  choir_man.id_trs_choir");
        if(is_array($choirid)){
            $this->db->where_in('nest_choid_event.id_trs_choir', $choirid);
        }else{
            $this->db->where(array('nest_choid_event.id_trs_choir' => $choirid));
        }
        $this->db->group_by('manager.id_manager');
        return $this->db->get()->result_array();
    }

    public function getMember($choirid){

        $this->db->select('member.* , member.id_member  as id ,  ("3") as type ,nest_choid_event.id_trs_choir as choir_id , (SELECT  choir_name FROM trs_choir WHERE trs_choir.id_trs_choir = nest_choid_event.id_trs_choir) as choir_name' );
        $this->db->from('mst_member as member');
        $this->db->join('trs_dtl_choir_mem choir_mem' , "member.id_member =  choir_mem.id_member");
        $this->db->join('(SELECT id_trs_choir , id_trs_choir_event FROM trs_dtl_choir_event_choir group by id_trs_choir , id_trs_choir_event) as nest_choid_event' , "nest_choid_event.id_trs_choir =  choir_mem.id_trs_choir");
        if(is_array($choirid)){
            $this->db->where_in('nest_choid_event.id_trs_choir', $choirid);
        }else{
            $this->db->where(array('nest_choid_event.id_trs_choir' => $choirid));
        }
        $this->db->group_by('member.id_member');
        return $this->db->get()->result_array();
    }
}