
alter table mst_songs add column notes varchar(300);
alter table mst_songs add column year_start varchar(4);
alter table mst_songs add column year_end varchar(4);


CREATE TABLE `dtl_songs_category` (
	`id_dtl_songs_category` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_songs_category` Int( 11 ) NOT NULL,
	`id_song` Int( 11 ) NOT NULL,
	PRIMARY KEY ( `id_dtl_songs_category` ) )