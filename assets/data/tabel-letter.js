(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('TablesDataLetterController', ['$scope', '$http', 'ngTableParams', '$filter', TablesDataLetterController]);

  function TablesDataLetterController($scope, $http, ngTableParams, $filter, $location, $routeParams) {
    $scope.loading = true;

    var data = [];

    $scope.candjData = {
          id_candj: []
        };

    $scope.change_status = function(){
      console.log("Change status");
      console.log($scope.candjData.id_candj);
      // console.log($scope.candjData.id_candj.length);

      $http.post("Candj/aktif",$scope.candjData.id_candj).success(function(res){
        console.log(res);
         alert("Status changed!");
        datana();
      }).error(function(err){
        console.log(err);
      });
    }

    datana();
	
    function datana(){
      $http.get("Letter/data_angularnya").success(function(result){
          $scope.loading = false;
          data=result;
          console.log(data);
          $scope.data = data;

          /* jshint newcap: false */
          $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,
            sorting: {
              template_name: 'desc'     // initial sorting
            }
          }, {
            filterDelay: 50,
            total: data.length, // length of data
            getData: function ($defer, params) {
              var searchStr = params.filter().search;
              var mydata = [];

              if (searchStr) {
                searchStr = searchStr.toLowerCase();
                mydata = data.filter(function (item) {
                  return item.id_letter.toLowerCase().indexOf(searchStr) > -1 || item.nomor.toLowerCase().indexOf(searchStr) > -1;
                });

              } else {
                mydata = data;
              }

              mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
              $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
      });
    };

  }


})();
