(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('TablesDataPackingToSmsController', ['$scope', '$http', 'ngTableParams', '$filter', TablesDataPackingToSmsController]);

  function TablesDataPackingToSmsController($scope, $http, ngTableParams, $filter, $location, $routeParams) {

    var data = [];
	
    $http.get("PackingToSms/data_angularnya").success(function(result){
        data=result;
        $scope.data = data;

        /* jshint newcap: false */
        $scope.tableParams = new ngTableParams({
          page: 1,            // show first page
          count: 10,
          sorting: {
            title: 'desc'     // initial sorting
          }
        }, {
          filterDelay: 50,
          total: data.length, // length of data
          getData: function ($defer, params) {
            var searchStr = params.filter().search;
            var mydata = [];

            if (searchStr) {
              searchStr = searchStr.toLowerCase();
              mydata = data.filter(function (item) {
                return item.id_packing_to_sms.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
              });

            } else {
              mydata = data;
            }

            mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
            $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
    });

  }


})();
