(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('TablesDataChoirEventDetailController', ['$scope', '$http', 'ngTableParams', '$filter', TablesDataChoirDetailController]);

  function TablesDataChoirDetailController($scope, $http, ngTableParams, $filter, $location, $routeParams) {

    var data = [];
    
    $scope.choirData = {
          id_trs_choir_event: []
        };

    

    $scope.change_status = function(){
      console.log("Change status");
      console.log($scope.choirData.id_trs_choir_event);
      // console.log($scope.candjData.id_candj.length);

      $http.post("ChoirEventDetail/aktif",$scope.choirData.id_trs_choir_event).success(function(res){
        console.log(res);
         alert("Status changed!");
        datana();
      }).error(function(err){
        console.log(err);
      });
    }


    $http.get("ChoirEventDetail/data_angularnya").success(function(result){
        data=result;
console.log(data);
        $scope.data = data;

        /* jshint newcap: false */
        $scope.tableParams = new ngTableParams({
          page: 1,            // show first page
          count: 10,
          sorting: {
            title: 'desc'     // initial sorting
          }
        }, {
          filterDelay: 50,
          total: data.length, // length of data
          getData: function ($defer, params) {
            var searchStr = params.filter().search;
            var mydata = [];

            if (searchStr) {
              searchStr = searchStr.toLowerCase();
              mydata = data.filter(function (item) {
                return item.id_trs_choir_event.toLowerCase().indexOf(searchStr) > -1 || item.title.toLowerCase().indexOf(searchStr) > -1;
              });

            } else {
              mydata = data;
            }

            mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
            $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
    });

  }


})();
