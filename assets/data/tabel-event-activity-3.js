(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('TablesDataEventActivity3Controller', ['$scope', '$http', 'ngTableParams', '$filter', TablesDataEventActivity3Controller]);

  function TablesDataEventActivity3Controller($scope, $http, ngTableParams, $filter, $location, $routeParams) {

    var data = [];
	
    $http.get("EventActivity3/data_angularnya").success(function(result){
        data=result;
        $scope.data = data;

        /* jshint newcap: false */
        $scope.tableParams = new ngTableParams({
          page: 1,            // show first page
          count: 10,
          sorting: {
            choir_name: 'desc'     // initial sorting
          }
        }, {
          filterDelay: 50,
          total: data.length, // length of data
          getData: function ($defer, params) {
            var searchStr = params.filter().search;
            var mydata = [];

            if (searchStr) {
              searchStr = searchStr.toLowerCase();
              mydata = data.filter(function (item) {
                return item.id_pd_event_activity_3.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
              });

            } else {
              mydata = data;
            }

            mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
            $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
    });

  }


})();
