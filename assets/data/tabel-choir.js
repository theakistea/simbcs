(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('TablesDataChoirController', ['$scope', '$http', 'ngTableParams', '$filter', TablesDataChoirController]);

  function TablesDataChoirController($scope, $http, ngTableParams, $filter, $location, $routeParams) {
    $scope.loading = true;

    var data = [];

    $scope.choirData = {
          id_choir_category: []
        };

    $scope.change_status = function(){
      console.log("Change status");
      console.log($scope.choirData.id_choir_category);
      // console.log($scope.candjData.id_candj.length);

      $http.post("Choir/aktif",$scope.choirData.id_choir_category).success(function(res){
        console.log(res);
         alert("Status changed!");
        datana();
      }).error(function(err){
        console.log(err);
      });
    }

    datana();
	
    function datana(){
      $http.get("Choir/data_angularnya").success(function(result){
          $scope.loading = false;
          data=result;
          console.log(data);
          $scope.data = data;

          /* jshint newcap: false */
          $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,
            sorting: {
              name: 'desc'     // initial sorting
            }
          }, {
            filterDelay: 50,
            total: data.length, // length of data
            getData: function ($defer, params) {
              var searchStr = params.filter().search;
              var mydata = [];

              if (searchStr) {
                searchStr = searchStr.toLowerCase();
                mydata = data.filter(function (item) {
                  return item.id_choir_category.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
                });

              } else {
                mydata = data;
              }
              console.log(data)

              mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
              $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
      });
    };

  }

})();
