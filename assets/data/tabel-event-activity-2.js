(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('TablesDataEventActivity2Controller', ['$scope', '$http', 'ngTableParams', '$filter', TablesDataEventActivity2Controller]);

  function TablesDataEventActivity2Controller($scope, $http, ngTableParams, $filter, $location, $routeParams) {

    var data = [];
	
    $http.get("EventActivity2/data_angularnya").success(function(result){
        data=result;
        $scope.data = data;
        console.log("Data >  " , data)
        /* jshint newcap: false */
        $scope.tableParams = new ngTableParams({
          page: 1,            // show first page
          count: 10,
          sorting: {
            choir_name: 'desc'     // initial sorting
            }
          }, {
          filterDelay: 50,
          total: data.length, // length of data
          getData: function ($defer, params) {
            var searchStr = params.filter().search;
            var mydata = [];

            if (searchStr) {
              searchStr = searchStr.toLowerCase();
              mydata = data.filter(function (item) {
                return item.id_pd_event_activity_2.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
              });

            } else {
              mydata = data;
            }

            mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
            $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
    });

  }


})();
