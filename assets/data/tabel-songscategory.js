(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('TablesDataSongsCategoryController', ['$scope', '$http', 'ngTableParams', '$filter', TablesDataSongsCategoryController]);

  function TablesDataSongsCategoryController($scope, $http, ngTableParams, $filter, $location, $routeParams) {
    $scope.loading = true;

    var data = [];

    $scope.songsCategoryData = {
          id_songs_category: []
        };

    $scope.change_status = function(){
      console.log("Change status");
      console.log($scope.songsCategoryData.id_songs_category);
      // console.log($scope.songsCategoryData.id_songs_category.length);

      $http.post("SongsCategory/aktif",$scope.songsCategoryData.id_songs_category).success(function(res){
        console.log(res);
         alert("Status changed!");
        datana();
      }).error(function(err){
        console.log(err);
      });
    }

    datana();
	
    function datana(){
      $http.get("SongsCategory/data_angularnya").success(function(result){
          $scope.loading = false;
          data=result;
          console.log(data);
          $scope.data = data;

          /* jshint newcap: false */
          $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,
            sorting: {
              title_category: 'asc'     // initial sorting
            }
          }, {
            filterDelay: 50,
            total: data.length, // length of data
            getData: function ($defer, params) {
              var searchStr = params.filter().search;
              var mydata = [];

              if (searchStr) {
                searchStr = searchStr.toLowerCase();
                mydata = data.filter(function (item) {
                  return item.id_songs_category.toLowerCase().indexOf(searchStr) > -1 || item.title_category.toLowerCase().indexOf(searchStr) > -1;
                });

              } else {
                mydata = data;
              }

              mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
              $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
      });
    };

  }

})();
