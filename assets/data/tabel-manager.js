(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('TablesDataManagerController', ['$scope', '$http', 'ngTableParams', '$filter', TablesDataManagerController]);

  function TablesDataManagerController($scope, $http, ngTableParams, $filter, $location, $routeParams) {
    $scope.loading = true;

    var data = [];

    $scope.managerData = {
          id_manager: []
        };

    $scope.change_status = function(){
      console.log("Change status");
      console.log($scope.managerData.id_manager);
      // console.log($scope.managerData.id_manager.length);

      $http.post("Manager/aktif",$scope.managerData.id_manager).success(function(res){
        console.log(res);
         alert("Status changed!");
        datana();
      }).error(function(err){
        console.log(err);
      });
    }

    datana();
	
    function datana(){
      $http.get("Manager/data_angularnya").success(function(result){
          $scope.loading = false;
          data=result;
          console.log(data);
          $scope.data = data;

          /* jshint newcap: false */
          $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,
            sorting: {
              name: 'desc'     // initial sorting
            }
          }, {
            filterDelay: 50,
            total: data.length, // length of data
            getData: function ($defer, params) {
              var searchStr = params.filter().search;
              var mydata = [];

              if (searchStr) {
                searchStr = searchStr.toLowerCase();
                mydata = data.filter(function (item) {
                  return item.id_manager.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
                });

              } else {
                mydata = data;
              }

              mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
              $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
      });
    };

  }

})();
