(function() {
  'use strict';

  // routes
  angular
    .module('material-lite')
    .config(['$routeProvider', routeProvider])
    .run(['$route', routeRunner]);

  function routeProvider($routeProvider) {

    $routeProvider.when('/', {
      templateUrl: 'Admin/dashboard'

    }).when('/:folder/:tpl/:var1/:var2', {
        templateUrl: function(attr){
          return attr.folder + '/' + attr.tpl + '/' + attr.var1 + '/' + attr.var2;
        }

    }).when('/:folder/:tpl/:var1', {
        templateUrl: function(attr){
          return attr.folder + '/' + attr.tpl + '/' + attr.var1;
        }

    }).when('/:folder/:tpl', {
        templateUrl: function(attr){
          return attr.folder + '/' + attr.tpl;
        }

    }).when('/:tpl', {
      templateUrl: function(attr){
        return attr.tpl;
      }

    }).otherwise({ redirectTo: '/' });
  }

  function routeRunner($route) {
    // $route.reload();
  }

})();
