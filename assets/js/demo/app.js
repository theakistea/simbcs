(function() {
  'use strict';

  var app = angular.module('material-lite', [
    'app.constants',

    'ngRoute',
    'ngAnimate',
    'ngSanitize', // Required by angular-ui-select

    'angular.mdl', // Required to make MDL components work with angular
    'ml.chat',
    'ml.menu',
    'ml.svg-map',
    'ml.todo',
    'ui.select', // Enhanced select element
    'ngFileUpload', // File uploader
    'ngWig', // Text editor
    'pikaday', // Datepicker 
    'ui.bootstrap',
    'dnTimepicker',
    'dateParser',
    'ngPlaceholders',
    'ngTable',

    'uiGmapgoogle-maps',

    'gridshore.c3js.chart', // C3 chart directives

    'angularGrid',
    'checklist-model',
    'LocalStorageModule', // Required by todo module
	'ngCkeditor',
  ]);

  app.service('popupDataResolverService', function() {
      var stringValue = 'test string value';
      var objectValue = {
          data: 'test object value'
      };
      var data = {}
      
      return {
          getString: function() {
              return stringValue;
          },
          setString: function(value) {
              stringValue = value;
          },
          getObject: function() {
              return objectValue;
          },
          getData:  function() {
            return data;
          },
          setData:  function(newData) {
            return data = newData;
          }
      }
  });

})();
