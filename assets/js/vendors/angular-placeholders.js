/**
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 * This is based, in part, on [fkadeveloper](https://github.com/fkadeveloper)'s
 * [lorem.js](https://github.com/fkadeveloper/loremjs).
 */
angular.module( 'ngPlaceholders', [] )

  .factory( 'PlaceholderTextService', function () {

    var words = ['lorem', 'dictum'
    ];


    var name = {
      'first_name': ['Coba','Zula'],
      'last_name': ['ob']
      };

    var icons = [
      'coba','coba'
    ];


    var img = [
      'ad.svg', 'button.svg', 'converse.svg', 'fire extinguisher.svg', 'lamp.svg', 'passport.svg', 'skate.svg',
      'theatre.svg', 'turntable.svg', 'wacom.svg', 'bill.svg', 'buzzer.svg','conveyor.svg', 'helmet.svg', 'luggage.svg',
      'presentation.svg','smart watch.svg', 'tie.svg', 'umbrella.svg', 'workspace.svg', 'bowling.svg', 'calculator.svg',
      'demoltion.svg', 'icecream.svg', 'microscope.svg', 'server.svg', 'switcher.svg', 'tower.svg', 'vespa.svg', 'wrench.svg'
    ];

    var colors = [
      'pink','red','purple','indigo','blue',
      'light-blue','cyan','teal','green','light-green',
      'lime','yellow','amber','orange','deep-orange'
    ];
    var colorVariation = [
      '300', '400', '500', '600', '700'
    ];



    function randomInt ( min, max ) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    return {

      randomColor: function(){
        return colors[randomInt(0, colors.length-1)];
      },

      createFirstname: function(){
        return name.first_name[randomInt(0, name.first_name.length - 1)];
      },

      createLastname: function(){
        return name.last_name[randomInt(0, name.last_name.length - 1)];
      },

      createIcon: function(color){
        color = color || false;

        if(!color){
          return '<i class="material-icons">'+icons[randomInt(0, icons.length - 1)]+'</i>';
        }

        var c = 'mdl-color-text--'+colors[randomInt(0, colors.length-1)]+'-'+colorVariation[randomInt(0, colorVariation.length-1)];

        return '<i class="material-icons '+c+'">'+icons[randomInt(0, icons.length - 1)]+'</i>';
      },

      createImg: function(){
        return img[randomInt(0, img.length - 1)];
      },

      createName: function(){
        return this.createFirstname()+' '+this.createLastname();
      },

      createSentence: function ( sentenceLength ) {
        var wordIndex,
          sentence;

        // Determine how long the sentence should be. Do it randomly if one was not
        // provided.
        sentenceLength = sentenceLength || randomInt( 5, 20 );

        // Now we determine were we are going to start in the array randomly. We
        // are just going to take a slice of the array, so we have to ensure
        // whereever we start has enough places left in the array to accommodate
        // the random sentence length from above.
        wordIndex = randomInt(0, words.length - sentenceLength - 1);

        // And pull out the words, join them together, separating words by spaces
        // (duh), and removing any commas that may appear at the end of the
        // sentence. Finally, add a period.
        sentence = words.slice(wordIndex, wordIndex + sentenceLength)
          .join(' ')
          .replace(/\,$/g, '') + '.';

        // Capitalize the first letter - it is a sentence, after all.
        sentence = sentence.charAt(0).toUpperCase() + sentence.slice(1);

        return sentence;
      },
      createSentences: function ( numSentences ) {
        var sentences = [],
          i = 0;

        // Determine how many sentences we should do. Do it randomly if one was not
        // provided.
        numSentences = numSentences || randomInt( 3, 5 );

        // For each paragraph, we should generate between 3 and 5 sentences.
        for ( i = 0; i < numSentences; i++ ) {
          sentences.push( this.createSentence() );
        }

        // And then we just return the array of sentences, concatenated with spaces.
        return sentences.join( ' ' );
      },
      createParagraph: function ( numSentences, html ) {
        var sentences = this.createSentences( numSentences );

        // Make the sentences into a paragraph and return.
        if(html){
          return '<p>' + sentences + '</p>';
        }

        return sentences + "\n";
      },
      createParagraphs: function ( numParagraphs, numSentences, html ) {
        var paragraphs = [],
          i = 0;

        numParagraphs = numParagraphs || randomInt( 3, 7 );

        // Create the number of paragraphs requested.
        for ( i = 0; i < numParagraphs; i++ ) {
          paragraphs.push( this.createParagraph( numSentences, html ) );
        }

        // Return the paragraphs, concatenated with newlines.
        return paragraphs.join( '\n' );
      }
    };
  })

  .directive( 'placeholderText', [ 'PlaceholderTextService', function ( PlaceholderTextService ) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs, ngModel) {

        numParagraphs = attrs.sentences || 1;
        numSentences = attrs.paragraphs || 6;
        html = attrs.html || true;

        element.html(
          PlaceholderTextService.createParagraphs(numParagraphs, numSentences, true)
        );

      }
    };
  }])

  .directive( 'placeholderTitle', [ 'PlaceholderTextService', function ( PlaceholderTextService ) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.html(
          PlaceholderTextService.createSentence(5)
        );
      }
    };
  }])

  .directive( 'placeholderName', [ 'PlaceholderTextService', function ( PlaceholderTextService ) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs, ngModel) {
        element.html(
          PlaceholderTextService.createName()
        );
      }
    };
  }])

  .directive( 'placeholderP', [ 'PlaceholderTextService', function ( PlaceholderTextService ) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs, ngModel) {
        element.html(
          PlaceholderTextService.createParagraph(1, 3, false)
        );
      }
    };
  }])

  .directive( 'placeholderImg', [ 'PlaceholderTextService', function ( PlaceholderTextService ) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs, ngModel) {
        element.attr('src', 'assets/img/icons/ballicons/'+PlaceholderTextService.createImg());
      }
    };
  }])

  .directive( 'placeholderIcon', [ 'PlaceholderTextService', function ( PlaceholderTextService ) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs, ngModel) {
        element.html(
          PlaceholderTextService.createIcon(false)
        );
      }
    };
  }])

  .directive( 'placeholderForm', [ 'PlaceholderTextService', function ( PlaceholderTextService ) {
    return {
      restrict: 'C',
      link: function(scope, element, attrs, ngModel) {

        angular.forEach(element.find('input, textarea'), function(el){
          elem = angular.element(el);

          switch (el.type) {
            case 'textarea':
              elem.text(PlaceholderTextService.createParagraphs(1, 4));
              break;
            case 'text':
              elem.attr('value', PlaceholderTextService.createSentence());
              break;
            case 'password':
              elem.attr('value', 'nakama?');
              break;
            case 'checkbox':
              elem.attr('checked','checked');
              break;
          }

          if(elem.val()){
            elem.parent().addClass('filled');
          }
        });

      }
    };
  }])


  .directive( 'placeholderImage', function () {
    return {
      restrict: 'A',
      scope: { dimensions: '@placeholderImage' },
      link: function( scope, element, attr ) {

        // A reference to a canvas that we can reuse
        var canvas;
        var config = {
          text_size: 10,
          fill_color: '#EEEEEE',
          text_color: '#AAAAAA'
        };

        /**
         * When the provided dimensions change, re-pull the width and height and
         * then redraw the image.
         */
        scope.$watch('dimensions', function () {
          if( ! angular.isDefined( scope.dimensions ) ) {
            return;
          }
          var matches = scope.dimensions.match( /^(\d+)x(\d+)$/ ),
            dataUrl;

          if(  ! matches ) {
            console.error('Expected \'000x000\'. Got ' + scope.dimensions);
            return;
          }

          // Grab the provided dimensions.
          scope.size = { w: matches[1], h: matches[2] };

          // FIXME: only add these if not already present
          element.prop( 'title', scope.dimensions );
          element.prop( 'alt', scope.dimensions );

          // And draw the image, getting the returned data URL.
          dataUrl = drawImage();

          // If this is an `img` tag, set the src as the data URL. Else, we set
          // the CSS `background-image` property to same.
          if ( element.prop( 'tagName' ) === 'IMG' ) {
            element.prop( 'src', dataUrl );
          } else {
            element.css( 'background-image', 'url("' + dataUrl + '")' );
          }
        });

        /**
         * Calculate the maximum height of the text we can draw, based on the
         * requested dimensions of the image.
         */
        function getTextSize() {
          var dimension_arr = [scope.size.h, scope.size.w].sort(),
            maxFactor = Math.round(dimension_arr[1] / 16);

          return Math.max(config.text_size, maxFactor);
        }

        /**
         * Using the HTML5 canvas API, draw a placeholder image of the requested
         * size with text centered vertically and horizontally that specifies its
         * dimensions. Returns the data URL that can be used as an `img`'s `src`
         * attribute.
         */
        function drawImage() {
          // Create a new canvas if we don't already have one. We reuse the canvas
          // when if gets redrawn so as not to be wasteful.
          canvas = canvas || document.createElement( 'canvas' );

          // Obtain a 2d drawing context on which we can add the placeholder
          // image.
          var context = canvas.getContext( '2d' ),
            text_size,
            text;

          // Set the canvas to the appropriate size.
          canvas.width = scope.size.w;
          canvas.height = scope.size.h;

          // Draw the placeholder image square.
          // TODO: support other shapes
          // TODO: support configurable colors
          context.fillStyle = config.fill_color;
          context.fillRect( 0, 0, scope.size.w, scope.size.h );

          // Add the dimension text.
          // TODO: support configurable font
          // FIXME: ensure text will fit and resize if it doesn't
          text_size = getTextSize();
          text = scope.dimensions;
          context.fillStyle = config.text_color;
          context.textAlign = 'center';
          context.textBaseline = 'middle';
          context.font = 'bold '+text_size+'pt sans-serif';

          // If the text is too long to fit, reduce it until it will.
          if (context.measureText( text ).width / scope.size.w > 1) {
            text_size = config.text_size / (context.measureText( text ).width / scope.size.w);
            context.font = 'bold '+text_size+'pt sans-serif';
          }

          // Finally, draw the text in its calculated position.
          context.fillText( scope.dimensions, scope.size.w / 2, scope.size.h / 2 );

          // Get the data URL and return it.
          return canvas.toDataURL('image/png');
        }
      }
    };
  });