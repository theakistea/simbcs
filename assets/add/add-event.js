(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('InsertDataEventController', ['$scope', '$http', '$location', InsertDataEventController]);

  function InsertDataEventController($scope, $http, $location) {
		
        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.insert_data=function(){
        
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("Event/insert_data",{
				//'id_candj':$scope.id_candj,
				'name':$scope.name,
				'notes':$scope.notes
				}).success(function(data,status,headers,config){
                //Beritahu jika data sudah berhasil di input
                alert("Data Berhasil Di Input");
                $location.path("Event");
            });
        }

  }

})();
