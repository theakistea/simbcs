(function() {
  'use strict';

  angular
    .module('material-lite')
	//controller add
    .controller('InsertDataChoirEventDetailController', ['$scope', '$http', '$location', '$timeout', 'localStorageService','Upload', InsertDataChoirEventDetailController])
	
	// controller pilih
    .controller('PilihCandjChoirEvent', ['$scope', '$http', '$timeout','localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihCandjChoirEvent])
	.controller('PilihChoirChoirEvent', ['$scope', '$http', '$timeout','localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihChoirChoirEvent])
	.controller('PilihComChoirEvent', ['$scope', '$http', '$timeout','localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihComChoirEvent])
  ;

  function InsertDataChoirEventDetailController($scope, $http, $location, $timeout, localStorageService,Upload) {		
        $scope.photo = 'default.jpg';
		
		$scope.uploadFiles = function(file, errFiles){
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if(file){
            file.upload = Upload.upload({
                url: 'ChoirEventDetail/upload',
                data: {file: file} // attribut name file dengan value file
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
                $scope.photo = response.data;
                console.log(response);
            }, function (response) {
                if (response.status > 0)
                   $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * 
                evt.loaded / evt.total));
            });
        }   
    }
		//var judul = $scope.title;
		$scope.insert_data=function(){
			//Inisialisasi
			var dataVenue = JSON.parse(localStorageService.get('dataVenue')) || [];
			var dataCandj = JSON.parse(localStorageService.get('dataCandj')) || [];
			var dataChoir = JSON.parse(localStorageService.get('dataChoir')) || [];
			var dataCom = JSON.parse(localStorageService.get('dataCom')) || [];
			var dataSponsor = JSON.parse(localStorageService.get('dataSponsor')) || [];
        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        if(dataVenue.length!=0){
		  //insert
            $http.post("ChoirEventDetail/insert_data", {
					'title':$scope.title,
    				'grade':$scope.grade,
    				'photo':$scope.photo,
    				'country':$scope.country,
    				'state':$scope.state,
    				'city':$scope.city,
    				'host':$scope.host,
    				'date_start':$scope.date_start,
    				'date_finish':$scope.date_finish,
    				'code_event':$scope.code_event,
					'dataVenue':dataVenue,
					'dataCandj':dataCandj,
					'dataSponsor':dataSponsor,
					'dataCom':dataCom,
					'dataChoir':dataChoir,
    				'PIC':$scope.PIC,
    				'notes':$scope.notes
            }).success(function(data,status,headers,config){
               
               localStorageService.remove('dataVenue');
               localStorageService.remove('dataCom');
               localStorageService.remove('dataChoir');
               localStorageService.remove('dataCandj');
               localStorageService.remove('dataSponsor');
               alert("Data Berhasil Di Input");
              $location.path("ChoirEventDetail");
            }).error(function(err){
              console.log(err);
            });
          }
		}
		//kosongkan local storage
		localStorage.clear();
		
		$scope.resetSponsor = function(){
          localStorageService.remove('dataSponsor');
          $("#sponsorTable .eusiSponsor").remove();
        }
		$scope.resetCandj = function(){
          localStorageService.remove('dataCandj');
          $("#candjTable .eusiCandj").remove();
        }
		$scope.resetVenue = function(){
          localStorageService.remove('dataVenue');
          $("#venueTable .eusiVenue").remove();
        }
		$scope.resetChoir = function(){
          localStorageService.remove('dataChoir');
          $("#choirTable .eusiChoir").remove();
        }
		$scope.resetCom = function(){
          localStorageService.remove('dataCom');
          $("#comTable .eusiCom").remove();
        }

     
        

		//Isi
        $scope.eusiVenue = JSON.parse(localStorageService.get('dataVenue')) || [];
        $scope.isiCandj = JSON.parse(localStorageService.get('dataCandj')) || [];
        $scope.isiChoir = JSON.parse(localStorageService.get('dataChoir')) || [];
        $scope.isiCom = JSON.parse(localStorageService.get('dataCom')) || [];
        $scope.isiSponsor = JSON.parse(localStorageService.get('dataSponsor')) || [];
        
  }
  
  function PilihCandjChoirEvent($scope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams){
    // Inisialisasi
      var dataCandj = JSON.parse(localStorageService.get('dataCandj')) || [];
      var no_candj=dataCandj.length+1;
      var data = [];

        $scope.candjData = {
              id_candj: []
            };
    
        $http.get("ChoirEventDetail/getAllCandjChoirEvent").success(function(result){
          $scope.loading = false;
          data=result;
          // console.log(data);
          $scope.data = data;

          /* jshint newcap: false */
          $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,
            sorting: {
              firstname: 'asc'     // initial sorting
            }
          }, {
            filterDelay: 50,
            total: data.length, // length of data
            getData: function ($defer, params) {
              var searchStr = params.filter().search;
              var mydata = [];

              if (searchStr) {
                searchStr = searchStr.toLowerCase();
                mydata = data.filter(function (item) {
                  return item.id_candj.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
                });

              } else {
                mydata = data;
              }

              mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
              $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
      });
  
      $scope.pilih = function(){
         console.log($scope.candjData.id_candj.length);
          // Remove dulu
          localStorageService.remove('dataCandj');
          $("#candjTable .eusiCandj").remove();
          no_candj=1;
		  
          if($scope.candjData.id_candj.length!=0){
            for(var i=0;i<$scope.candjData.id_candj.length;i++){
              $http.get("ChoirEventDetail/getOneCandj/"+$scope.candjData.id_candj[i]).success(function(data,status,headers,config){
                 console.log(data);
                // Nyimpen ke localstorage 
                // retrieve it (Or create a blank array if there isn't any info saved yet),
                var datana = JSON.parse(localStorageService.get('dataCandj')) || [];
                // add to it,
                datana.push({id: data[0].id_candj, name: data[0].name});
                // then put it back.
                localStorageService.set('dataCandj', JSON.stringify(datana));
                // append data tabel
                $("#candjTable tr:last").after("<tr class='eusiCandj'><td>"+no_candj+"</td><td>"+data[0].name+"<input type='hidden' name='id_candj' value='"+data[0].id_candj+"'></td></tr>");
                no_candj++;
              }).error(function(err){
                console.log(err);
              });
            }
          }
      }
  }
  function PilihChoirChoirEvent($scope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams){
    // Inisialisasi
      var dataChoir = JSON.parse(localStorageService.get('dataChoir')) || [];
      var no_choir=dataChoir.length+1;
      var data = [];

        $scope.choirData = {
              id_trs_choir: []
            };
    
        $http.get("ChoirEventDetail/getAllChoirChoirEvent").success(function(result){
          $scope.loading = false;
          data=result;
          // console.log(data);
          $scope.data = data;

          /* jshint newcap: false */
          $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,
            sorting: {
              firstname: 'asc'     // initial sorting
            }
          }, {
            filterDelay: 50,
            total: data.length, // length of data
            getData: function ($defer, params) {
              var searchStr = params.filter().search;
              var mydata = [];

              if (searchStr) {
                searchStr = searchStr.toLowerCase();
                mydata = data.filter(function (item) {
                  return item.id_trs_choir.toLowerCase().indexOf(searchStr) > -1 || item.choir_name.toLowerCase().indexOf(searchStr) > -1;
                });

              } else {
                mydata = data;
              }

              mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
              $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
      });
  
      $scope.pilih = function(){
         console.log($scope.choirData.id_trs_choir.length);
          // Remove dulu
          localStorageService.remove('dataChoir');
          $("#choirTable .eusiChoir").remove();
          no_choir=1;
		  
          if($scope.choirData.id_trs_choir.length!=0){
            for(var i=0;i<$scope.choirData.id_trs_choir.length;i++){
              $http.get("ChoirEventDetail/getOneChoir/"+$scope.choirData.id_trs_choir[i]).success(function(data,status,headers,config){
                 console.log(data);
                // Nyimpen ke localstorage 
                // retrieve it (Or create a blank array if there isn't any info saved yet),
                var datana = JSON.parse(localStorageService.get('dataChoir')) || [];
                // add to it,
                datana.push({id2: data[0].id_trs_choir,id: data[0].id_trs_choir_event, name: data[0].choir_name});
                // then put it back.
                localStorageService.set('dataChoir', JSON.stringify(datana));
                // append data tabel
                $("#choirTable tr:last").after("<tr class='eusiChoir'><td>"+no_choir+"</td><td>"+data[0].choir_name+"<input type='hidden' name='id_trs_choir' value='"+data[0].id_trs_choir+"'></td></tr>");
                no_choir++;
              }).error(function(err){
                console.log(err);
              });
            }
          }
      }
  }
  
  function PilihComChoirEvent($scope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams){
    // Inisialisasi
      var dataCom = JSON.parse(localStorageService.get('dataCom')) || [];
      var no_com=dataCom.length+1;
      var data = [];

        $scope.comData = {
              id_committee: []
            };
    
        $http.get("ChoirEventDetail/getAllComChoirEvent").success(function(result){
          $scope.loading = false;
          data=result;
          // console.log(data);
          $scope.data = data;

          /* jshint newcap: false */
          $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,
            sorting: {
              firstname: 'asc'     // initial sorting
            }
          }, {
            filterDelay: 50,
            total: data.length, // length of data
            getData: function ($defer, params) {
              var searchStr = params.filter().search;
              var mydata = [];

              if (searchStr) {
                searchStr = searchStr.toLowerCase();
                mydata = data.filter(function (item) {
                  return item.id_committee.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
                });

              } else {
                mydata = data;
              }

              mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
              $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
      });
  
      $scope.pilih = function(){
        // console.log($scope.manData.id_manager.length);
          // Remove dulu
          localStorageService.remove('dataCom');
          $timeout(function () {
              $scope.tampilTabCom = false;
          });
          $("#comTable .eusiCom").remove();
          no_com=1;
          if($scope.comData.id_committee.length!=0){
            for(var i=0;i<$scope.comData.id_committee.length;i++){
              $http.get("ChoirEventDetail/getOneCom/"+$scope.comData.id_committee[i]).success(function(data,status,headers,config){
                // console.log(data);
                // Nyimpen ke localstorage 
                // retrieve it (Or create a blank array if there isn't any info saved yet),
                var datana = JSON.parse(localStorageService.get('dataCom')) || [];
                // add to it,
                datana.push({id: data[0].id_committee, name: data[0].name, gender: data[0].gender});
                // then put it back.
                localStorageService.set('dataCom', JSON.stringify(datana));
                // append data tabel
                $("#comTable tr:last").after("<tr class='eusiCom'><td>"+no_com+"</td><td>"+data[0].name+"<input type='hidden' name='id_committee' value='"+data[0].id_committee+"'></td></tr>");
                no_com++;
              }).error(function(err){
                console.log(err);
              });
            }
          }
      }
  }
  
  
})();
