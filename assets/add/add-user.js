(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('InsertDataUserController', ['$scope', '$http', '$location', '$timeout', 'Upload', InsertDataUserController]);

  function InsertDataUserController($scope, $http, $location, $timeout, Upload) {
		
        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.insert_data=function(){
        
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("User/insert_data",{
				'username':$scope.username,
				'firstName':$scope.firstName,
				'lastName':$scope.lastName,
				'password':$scope.password,
				'idUserGroup':$scope.idUserGroup,
				'email':$scope.email
			}).success(function(data,status,headers,config){
                //Beritahu jika data sudah berhasil di input
                alert("Data Berhasil Di Input");
                $location.path("User");
            });
        }

  }

})();
