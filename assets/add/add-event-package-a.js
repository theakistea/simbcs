(function () {
  'use strict';

  angular
    .module('material-lite')
    .controller('InsertDataEventPackageA', ['$scope', '$http', '$location', 'dataService', InsertDataEventPackageA])

    // controller tambah
    .controller('SearchEventController', ['$scope', '$http', '$timeout', 'localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', 'dataService', SearchEventController])

    .service('dataService', function () {
      // private variable
      var _dataObj = {};

      // public API
      this.dataObj = _dataObj;
    })
    ;

  function InsertDataEventPackageA($scope, $http, $location, dataService) {
    // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
   
    $scope.total_participant;
    $scope.nom;
    $scope.conductor;
    $scope.officials;
    $scope.price_per_person;
    $scope.sub_total  = 0;

    $scope.cout_total_participant = function () {
      $scope.total_participant = Number(dataService.dataObj.number_of_singer ? dataService.dataObj.number_of_singer : 0) +  Number($scope.nom ? $scope.nom : 0) + Number($scope.conductor ? $scope.conductor : 0) + Number($scope.officials ? $scope.officials : 0);
      $scope.sub_total = Number($scope.total_participant ? $scope.total_participant :  0) * Number($scope.price_per_person ? $scope.price_per_person : 0)

      };
    
    $scope.insert_data = function () {
      if (dataService.dataObj != undefined) {
        //console.log("Id Event : "+dataService.dataObj.id_trs_choir_event);
        //console.log("Id Choir : "+dataService.dataObj.id_trs_choir);
        // ukuran
        
        var ukuranna = [];
        for (var i = 1; i <= dataService.dataObj.member.length; i++) {
          //console.log($("#ukuran"+i).val());
          ukuranna.push($("#ukuran" + i).val());
        }
        console.log(ukuranna);

        console.log(dataService.dataObj.member);

        $http.post("EventPackageA/insert_data", {
          'id_trs_choir_event': dataService.dataObj.id_trs_choir_event,
          'id_trs_choir': dataService.dataObj.id_trs_choir,
          'member': dataService.dataObj.member,
          'ukuran': ukuranna,
          'num_of_singer': dataService.dataObj.number_of_singer,
          'num_of_musician': $scope.nom,
          'num_of_conductor': $scope.conductor,
          'num_of_official': $scope.officials,
          'flight_detail': $scope.flight_detail,
          'staying_address': $scope.staying_address,
          'price_per_person': $scope.price_per_person,
          'sub_total': $scope.sub_total,
          'due_date': $scope.due_date,
          'contact_person': $scope.contact_person,
          'phone': $scope.phone,
          'email': $scope.email,
          'address': $scope.address,
          'notes': $scope.notes
        }).success(function (data, status, headers, config) {
          alert("Data Berhasil Di Input");
          $location.path("EventPackageA");
        }).error(function (err) {
          console.log(err);
        });


      }
    }

    $scope.nos = dataService.dataObj.number_of_singer;

    $scope.parseInt = function (value) {
      return (value != undefined) ? parseInt(value) : 0;
    }

    // var timer = null;

    // timer = setInterval(function(){
    //   var id_event = dataService.dataObj.id_event;
    //   // adaw(id_event);
    //   start(dataService.dataObj.id_event,dataService.dataObj.id_choir);
    // },1000);

    // function start(id_event,id_choir){
    //   $.get("EventPackageA/getMemberSelected/"+id_event+"/"+id_choir, function(res){
    //     // console.log(id_event+" "+id_choir);
    //     $scope.isiMember = [];
    //     if(res.length!=0){
    //       // console.log("Stop");
    //       $scope.isiMember = res;
    //       // clearInterval(timer);
    //       timer = null
    //     }
    //   }, "json" );
    // }
  }

  function SearchEventController($scope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams, dataService) {
    // Inisialisasi
    var data = [];

    $scope.event = {
      name: []
    };


    $http.get("EventPackageA/getEvent").success(function (result) {
      $scope.loading = false;

      console.log(result);

      for (var i = 0; i < result.length; i++) {

        data.push({
          title: result[i].title,
          choir_name: result[i].choir_name,
          idna: { id_trs_choir_event: result[i].id_trs_choir_event, id_trs_choir: result[i].id_trs_choir }
        });
      }
      // console.log(data);

      $scope.data = result;

      /* jshint newcap: false */
      $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10,
        sorting: {
          title: 'desc'     // initial sorting
        }
      }, {
          filterDelay: 50,
          total: data.length, // length of data
          getData: function ($defer, params) {
            var searchStr = params.filter().search;
            var mydata = [];

            if (searchStr) {
              searchStr = searchStr.toLowerCase();
              mydata = data.filter(function (item) {
                return item.choir_name.toLowerCase().indexOf(searchStr) > -1 || item.title.toLowerCase().indexOf(searchStr) > -1;
              });

            } else {
              mydata = data;
            }

            mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
            $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
    });

    $scope.pilih = function () {

      $http.get("EventPackageA/getOneChoirEvent/" + $scope.event.name.id_trs_choir_event + "/" + $scope.event.name.id_trs_choir).success(function (res) {
        dataService.dataObj = {
          id_trs_choir_event: $scope.event.name.id_trs_choir_event,
          id_trs_choir: $scope.event.name.id_trs_choir,
          title: res.title,
          choir_name: res.choir_name,
          number_of_singer: res.number_of_singer,
          member: res.member
        };
        //	 console.log('jhjh' + res.id_event);
        console.log(res);
        // $("#event").val(res.title);
        // $("#choir").val(res.choir_name);
        $("#event").html(res.title);
        $("#choir").html(res.choir_name);
        $("#id_trs_choir_event").val(res.id_trs_choir_event);
        $("#id_trs_choir").val(res.id_trs_choir);
        $("#choir").html(res.choir_name);
        $("#nos").html(res.number_of_singer);

        // remove heula
        $("#tshirt .eusiMember").remove();
        // add deui
        for (var i = 1; i <= dataService.dataObj.member.length; i++) {
          $("#tshirt tr:last").after("<tr class='eusiMember'><td>" + i + "</td><td>" + res.member[i - 1].name + "</td><td><input name='ukuran' type='text' id='ukuran" + i + "'></td></tr>");
        }

        // console.log(res.member);
      }).error(function (err) {
        console.log(err);
      });


    }

  }

})();
