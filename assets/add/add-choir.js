(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('InsertDataChoirController', ['$scope', '$http', '$location', InsertDataChoirController]);

  function InsertDataChoirController($scope, $http, $location) {
		
        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.insert_data=function(){
        
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("Choir/insert_data",{
				//'id_candj':$scope.id_candj,
				'name':$scope.name,
				'type':$scope.type,
				'age':$scope.age,
				'min_singer':$scope.min_singer,
				'max_perform_time': $scope.max_perform_time,
				'num_of_song':$scope.num_of_song,
				'num_of_acapela_song':$scope.num_of_acapela_song,
				'with_amplification':$scope.with_amplification,
				'code_category':$scope.code_category,
				'notes':$scope.notes
				}).success(function(data,status,headers,config){
                //Beritahu jika data sudah berhasil di input
                alert("Data Berhasil Di Input");
                $location.path("Choir");
            });
        }

  }

})();
