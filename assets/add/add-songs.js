(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('InsertDataSongsController', ['$rootScope','$scope', '$http', '$location', '$timeout', 'Upload', 'localStorageService', InsertDataSongsController]);

  function InsertDataSongsController($rootScope, $scope, $http, $location, $timeout, Upload, localStorageService) {

    console.log('add songs');

    localStorageService.remove('dataCategory');

    $scope.balok = "none";
    $scope.angka = "none";

    $scope.uploadFileBalok = function(file, errFiles){
          $scope.f = file;
          $scope.errFile = errFiles && errFiles[0];
          
          if (file) {
              file.upload = Upload.upload({
                  url: 'songs/uploadBalok',
                  data: {fileBalok: file} // attribut name file dengan value file
              });

              file.upload.then(function (response) {
                  $timeout(function () {
                      file.result = response.data;
                  });
                  $scope.balok = response.data;
                  console.log(response);
              }, function (response) {
                  if (response.status > 0)
                     $scope.errorMsg = response.status + ': ' + response.data;
              }, function (evt) {
                  file.progress = Math.min(100, parseInt(100.0 * 
                  evt.loaded / evt.total));
              });
          }   
      }

      
    $rootScope.$on('updateSelectData', function (event, args) {
      console.log("updated")
      $scope.dataCategory = [];
      $scope.dataCategory = JSON.parse(localStorageService.get('dataCategory')) || [];

      console.log($scope.dataCategory)
     
    });
    

      $scope.uploadFileAngka = function(file, errFiles){
          $scope.f = file;
          $scope.errFile = errFiles && errFiles[0];
          if (file) {
              file.upload = Upload.upload({
                  url: 'songs/uploadAngka',
                  data: {fileAngka: file} // attribut name file dengan value file
              });

              file.upload.then(function (response) {
                  $timeout(function () {
                      file.result = response.data;
                  });
                  $scope.angka = response.data;
                  console.log(response);
              }, function (response) {
                  if (response.status > 0)
                     $scope.errorMsg = response.status + ': ' + response.data;
              }, function (evt) {
                  file.progress = Math.min(100, parseInt(100.0 * 
                  evt.loaded / evt.total));
              });
          }   
      }
		
        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.insert_data=function(){
          $http.post("songs/insert_data",{
            'title':$scope.title,
            'type' :$scope.type,
            'id_songs_category':$scope.id_songs_category,
            'composer':$scope.composer,
            'publisher':$scope.publisher,
            'accompanied_type':$scope.accompanied_type,
            'accompanied_value':$scope.accompanied_value,
            'balok' : $scope.balok,
            'angka' : $scope.angka,
            'notes' : $scope.notes,
            'year_start' : $scope.year_start,
            'year_end' : $scope.year_end,
            'data_category' : $scope.dataCategory,

          }).success(function(data,status,headers,config){
            //Beritahu jika data sudah berhasil di input
            alert("Data Berhasil Di Input");
            $location.path("songs");
          });
        };
  }

})();
