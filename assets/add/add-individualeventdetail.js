(function() {
  'use strict';

  angular
    .module('material-lite')
	//controller add
    .controller('InsertDataIndividualEventDetailController', ['$scope', '$http', '$location', '$timeout', 'localStorageService','Upload', InsertDataIndividualEventDetailController])
	
	// controller pilih
    .controller('PilihCandjChoirEvent', ['$scope', '$http', '$timeout','localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihCandjChoirEvent])
  ;

  function InsertDataIndividualEventDetailController($scope, $http, $location, $timeout, localStorageService,Upload) {		
        $scope.photo = 'default.jpg';
		
		$scope.uploadFiles = function(file, errFiles){
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if(file){
            file.upload = Upload.upload({
                url: 'IndividualEventDetail/upload',
                data: {file: file} // attribut name file dengan value file
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
                $scope.photo = response.data;
                console.log(response);
            }, function (response) {
                if (response.status > 0)
                   $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * 
                evt.loaded / evt.total));
            });
        }   
    }
		//var judul = $scope.title;
		$scope.insert_data=function(){
        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
		var dataVenue = JSON.parse(localStorageService.get('dataVenue')) || [];
        if(dataVenue.length!=0){
			//Inisialisasi
			var dataVenue = JSON.parse(localStorageService.get('dataVenue')) || [];
			var dataCandj = JSON.parse(localStorageService.get('dataCandj')) || [];
			var dataSponsor = JSON.parse(localStorageService.get('dataSponsor')) || [];
		  //insert
            $http.post("IndividualEventDetail/insert_data", {
					'title':$scope.title,
    				'grade':$scope.grade,
    				'country':$scope.country,
    				'state':$scope.state,
    				'city':$scope.city,
    				'host':$scope.host,
    				'date_start':$scope.date_start,
    				'date_finish':$scope.date_finish,
					'dataVenue':dataVenue,
					'dataCandj':dataCandj,
					'dataSponsor':dataSponsor,
    				'PIC':$scope.PIC,
    				'notes':$scope.notes
            }).success(function(data,status,headers,config){
               
               localStorageService.remove('dataVenue');
               localStorageService.remove('dataCandj');
               localStorageService.remove('dataSponsor');
               alert("Data Berhasil Di Input");
              $location.path("IndividualEventDetail");
            }).error(function(err){
              console.log(err);
            });
          }
		}

        $scope.reset = function(){
          localStorageService.remove('kategoriChoir');
          $timeout(function () {
              $scope.tampilTabCat = false;
          });
          $("#kategoriTable .eusi").remove();
        }
		$scope.resetSponsor = function(){
          localStorageService.remove('dataSponsor');
          $("#sponsorTable .eusiSponsor").remove();
        }
		$scope.resetVenue = function(){
          localStorageService.remove('dataVenue');
          $("#venueTable .eusiVenue").remove();
        }
        

        // Isi kategori
        $scope.isiKategori = JSON.parse(localStorageService.get('kategoriChoir')) || [];
        $scope.isiMember = JSON.parse(localStorageService.get('detailMember')) || [];
        // cek table
        /*if($scope.isiKategori.length==0){
          $scope.tampilTabCat = false;
        }else{
          $scope.tampilTabCat = true;
        }*/

        $scope.tambahCat = function(){
          console.log($scope.cat);
          if($scope.cat!=undefined){

            $http.get("ChoirDetail/getOneCatChoir/"+$scope.cat).success(function(data,status,headers,config){
              console.log(data);
              // Nyimpen ke localstorage 
              // retrieve it (Or create a blank array if there isn't any info saved yet),
              var categories = JSON.parse(localStorageService.get('kategoriChoir')) || [];
              // add to it,
              categories.push({id: data[0].id_choir_category, name: data[0].name, type: data[0].type});
              // then put it back.
              localStorageService.set('kategoriChoir', JSON.stringify(categories));
              // append data tabel
              $("#kategoriTable tr:last").after("<tr class='eusi'><td>"+no+"</td><td>"+data[0].name+"<input type='hidden' name='id_choir_category' value='"+data[0].id_choir_category+"'></td></tr>");
              no++;
            }).error(function(err){
              console.log(err);
            });
            
          }
        }
		
		$scope.tambahMem = function(){
          console.log($scope.mem);
          if($scope.mem!=undefined){

            $http.get("ChoirDetail/getOneMember/"+$scope.mem).success(function(data,status,headers,config){
              console.log(data);
              // Nyimpen ke localstorage 
              // retrieve it (Or create a blank array if there isn't any info saved yet),
              var members = JSON.parse(localStorageService.get('detailMember')) || [];
              // add to it,
              members.push({id: data[0].id_member, name: data[0].name, type: data[0].type});
              // then put it back.
              localStorageService.set('detailMember', JSON.stringify(members));
              // append data tabel
              $("#memberTable tr:last").after("<tr class='eusiMember'><td>"+no+"</td><td>"+data[0].name+"<input type='hidden' name='id_member' value='"+data[0].id_member+"'></td></tr>");
              no++;
            }).error(function(err){
              console.log(err);
            });
            
          }
        }
  }
  // Kategori
  function InsertCatChoir($scope, $http, $timeout, localStorageService) {
    // Inisialisasi
    $scope.kategori = {};
    var kategoriChoir = JSON.parse(localStorageService.get('kategoriChoir')) || [];
    var no=kategoriChoir.length+1;

    $timeout(function () {
        $scope.tampilTabCat = true;
    });
    
    $scope.saveCat = function(){
      console.log($scope.kategori);

      // Post
      $http.post("choir/insert_data",{
        'name':$scope.kategori.nameCat,
        'type':$scope.kategori.typeCat,
        'age':$scope.kategori.ageCat,
        'min_singer':$scope.kategori.minsingCat,
        'max_perform_time':$scope.kategori.maxperfCat,
        'num_of_song':$scope.kategori.numsongCat,
        'num_of_acapela_song':$scope.kategori.numacapCat,
        'with_amplification':$scope.kategori.ampliCat,
        'notes':$scope.kategori.notesCat
      }).success(function(data,status,headers,config){
          console.log("last id : " +data);
          console.log("no : " +no);

          // Nyimpen ke localstorage 
          // retrieve it (Or create a blank array if there isn't any info saved yet),
          var categories = JSON.parse(localStorageService.get('kategoriChoir')) || [];
          // add to it,
          categories.push({id: data, name: $scope.kategori.nameCat, type: $scope.kategori.typeCat});
          // then put it back.
          localStorageService.set('kategoriChoir', JSON.stringify(categories));

          //Beritahu jika data sudah berhasil di input
          // alert("Category successfully added.");

          // Append data
          // <td><button ng-click='removeItemCat("+parseInt(no-1)+")'>Remove</button></td>
          $("#kategoriTable tr:last").after("<tr><td>"+no+"</td><td>"+$scope.kategori.nameCat+"</td></tr>");
          no++;

          // Set bekas nya kosongin
          $scope.kategori = {};
          $('#modalChoir').modal('hide');
      }).error(function(err){
        console.log(err);
      });
        
    }
  }

  // Member
  function InsertMemberChoir($scope, $http, $timeout, localStorageService) {
    // Inisialisasi
    $scope.member = {};
    var memberChoir = JSON.parse(localStorageService.get('memberChoir')) || [];
    var no=memberChoir.length+1;

    $timeout(function () {
        $scope.tampilTabMem = true;
    });
    
    $scope.saveCat = function(){
      console.log($scope.member);

      // Post
      $http.post("Member/insert_data",{
        
        'name':$scope.member.name,
        'institution':$scope.member.institution,
        'gender':$scope.member.gender,
        'appellation':$scope.member.appellation,
        'first_title':$scope.member.first_title,
        'last_title':$scope.member.last_title,
        'street_address':$scope.member.street_address,
        'country':$scope.member.country,
        'state':$scope.member.state,
        'city':$scope.member.city,
        'phone':$scope.member.phone,
        'email':$scope.member.email,
        'social_media':$scope.member.social_media,
        'website':$scope.member.website,
        'place_of_birth':$scope.member.place_of_birth,
        'date_of_birth':$scope.member.date_of_birth,
        'notes':$scope.member.notes
      }).success(function(data,status,headers,config){
          console.log("last id : " +data);
          console.log("no : " +no);

          // Nyimpen ke localstorage 
          // retrieve it (Or create a blank array if there isn't any info saved yet),
          var categories = JSON.parse(localStorageService.get('memberChoir')) || [];
          // add to it,
          categories.push({id: data, name: $scope.member.nameCat, type: $scope.member.typeCat});
          // then put it back.
          localStorageService.set('memberChoir', JSON.stringify(categories));

          //Beritahu jika data sudah berhasil di input
          // alert("Category successfully added.");

          // Append data
          // <td><button ng-click='removeItemCat("+parseInt(no-1)+")'>Remove</button></td>
          $("#kategoriTable tr:last").after("<tr><td>"+no+"</td><td>"+$scope.member.nameCat+"</td></tr>");
          no++;

          // Set bekas nya kosongin
          $scope.member = {};
          $('#modalMember').modal('hide');
      }).error(function(err){
        console.log(err);
      });
        
    }
  }
  
  function PilihCandjChoirEvent($scope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams){
    // Inisialisasi
      var dataCandj = JSON.parse(localStorageService.get('dataCandj')) || [];
      var no_candj=dataCandj.length+1;
      var data = [];

        $scope.candjData = {
              id_candj: []
            };
    
        $http.get("IndividualEventDetail/getAllCandjChoirEvent").success(function(result){
          $scope.loading = false;
          data=result;
          // console.log(data);
          $scope.data = data;

          /* jshint newcap: false */
          $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,
            sorting: {
              firstname: 'asc'     // initial sorting
            }
          }, {
            filterDelay: 50,
            total: data.length, // length of data
            getData: function ($defer, params) {
              var searchStr = params.filter().search;
              var mydata = [];

              if (searchStr) {
                searchStr = searchStr.toLowerCase();
                mydata = data.filter(function (item) {
                  return item.id_candj.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
                });

              } else {
                mydata = data;
              }

              mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
              $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
      });
  
      $scope.pilih = function(){
        // console.log($scope.manData.id_manager.length);
          // Remove dulu
          localStorageService.remove('dataCandj');
          $timeout(function () {
              $scope.tampilTabCandj = false;
          });
          $("#candjTable .eusiCandj").remove();
          no_candj=1;
          if($scope.candjData.id_candj.length!=0){
            for(var i=0;i<$scope.candjData.id_candj.length;i++){
              $http.get("IndividualEventDetail/getOneCandj/"+$scope.candjData.id_candj[i]).success(function(data,status,headers,config){
                // console.log(data);
                // Nyimpen ke localstorage 
                // retrieve it (Or create a blank array if there isn't any info saved yet),
                var datana = JSON.parse(localStorageService.get('dataCandj')) || [];
                // add to it,
                datana.push({id: data[0].id_candj, name: data[0].name, gender: data[0].gender});
                // then put it back.
                localStorageService.set('dataCandj', JSON.stringify(datana));
                // append data tabel
                $("#candjTable tr:last").after("<tr class='eusiCandj'><td>"+no_candj+"</td><td>"+data[0].name+"<input type='hidden' name='id_candj' value='"+data[0].id_candj+"'></td></tr>");
                no_candj++;
              }).error(function(err){
                console.log(err);
              });
            }
          }
      }
  }
  

  
})();
