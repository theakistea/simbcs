(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('InsertDataOfficialsController', ['$scope', '$http', '$location', '$timeout', 'Upload', InsertDataOfficialsController]);

  function InsertDataOfficialsController($scope, $http, $location, $timeout, Upload) {

  	$scope.photo = 'default.jpg';

	$scope.uploadFiles = function(file, errFiles){
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: 'officials/upload',
                data: {file: file} // attribut name file dengan value file
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
                $scope.photo = response.data;
                console.log(response);
            }, function (response) {
                if (response.status > 0)
                   $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * 
                evt.loaded / evt.total));
            });
        }   
    }
		
        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.insert_data=function(){
        
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("Officials/insert_data",{
				//'id_candj':$scope.id_candj,
				'name':$scope.name,
				'gender':$scope.gender,
				'appellation':$scope.appellation,
				'first_title':$scope.first_title,
				'last_title':$scope.last_title,
				'street_address':$scope.street_address,
				'city':$scope.city,
				'state':$scope.state,
				'country':$scope.country,
				'website':$scope.website,
				'place_of_birth':$scope.place_of_birth,
				'date_of_birth':$scope.date_of_birth,
				'photo':$scope.photo,
				'notes':$scope.notes,
                'photo':$scope.photo,
				'phone':$scope.phone,
				'email':$scope.email,
				'social_media':$scope.social_media
			}).success(function(data,status,headers,config){
                //Beritahu jika data sudah berhasil di input
                alert("Data Berhasil Di Input");
                $location.path("Officials");
            });
        }

  }

})();
