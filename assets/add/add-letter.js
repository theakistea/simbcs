(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('InsertDataLetterController', ['$scope', '$http', '$location', '$timeout', InsertDataLetterController]);

  function InsertDataLetterController($scope, $http, $location, $timeout) {

		$scope.editorOptions = {
                language: 'id'
               // uiColor: '#000000'
            };
            $scope.$on("ckeditor.ready", function( event ) {
                $scope.isReady = true;
            });
        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.insert_data=function(){
        
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("Letter/insert_data",{
				//'id_candj':$scope.id_candj,
				'template_name':$scope.template_name,
				'konten':$scope.konten,
				'type'	: $scope.type
			}).success(function(data,status,headers,config){
                //Beritahu jika data sudah berhasil di input
                alert("Data Berhasil Di Input");
                $location.path("Letter");
            });
        }

  }

})();
