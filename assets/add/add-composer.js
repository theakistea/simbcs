(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('InsertDataComposerController', ['$scope', '$http', '$location','localStorageService', '$timeout', 'Upload', InsertDataComposerController])	
	  .controller('PilihSongsComposer', ['$scope', '$http', '$timeout','localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihSongsComposer]);
  function InsertDataComposerController($scope, $http, $location, localStorageService, $timeout, Upload) {
	localStorage.clear();
	$scope.resetSongs = function(){
              localStorageService.remove('songsComposer');
              $timeout(function () {
                  $scope.tampilTabSongs = false;
              });
              $("#songsTable .eusiSongs").remove();
            }
  	$scope.photo = 'default.jpg';

  	$scope.uploadFiles = function(file, errFiles){
          $scope.f = file;
          $scope.errFile = errFiles && errFiles[0];
          if (file) {
              file.upload = Upload.upload({
                  url: 'Composer/upload',
                  data: {file: file} // attribut name file dengan value file
              });

              file.upload.then(function (response) {
                  $timeout(function () {
                      file.result = response.data;
                  });
                  $scope.photo = response.data;
                  console.log(response);
              }, function (response) {
                  if (response.status > 0)
                     $scope.errorMsg = response.status + ': ' + response.data;
              }, function (evt) {
                  file.progress = Math.min(100, parseInt(100.0 * 
                  evt.loaded / evt.total));
              });
          }   
      }
		
        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.insert_data=function(){
            var songsComposer = JSON.parse(localStorageService.get('songsComposer')) || [];

            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
      			if(songsComposer.length!=0){
                  $http.post("Composer/insert_data", {
      				'name':$scope.name,
      				'gender':$scope.gender,
      				'appellation':$scope.appellation,
      				'first_title':$scope.first_title,
      				'last_title':$scope.last_title,
      				'street_address':$scope.street_address,
      				'city':$scope.city,
      				'state':$scope.state,
      				'country':$scope.country,
      				'website':$scope.website,
      				'place_of_birth':$scope.place_of_birth,
      				'date_of_birth':$scope.date_of_birth,
      				'userfile':$scope.userfile,
      				'photo':$scope.photo,
      				'notes':$scope.notes,
      				'phone':$scope.phone,
      				'email':$scope.email,
      				'social_media':$scope.social_media,
      				'songsComposer':songsComposer
                  }).success(function(data,status,headers,config){
                     localStorageService.remove('songsComposer');
                     alert("Data Berhasil Di Input");
                    $location.path("Composer");
                  }).error(function(err){
                    console.log(err);
                  });
                }

            // Isi tabel
            $scope.isiSongs = JSON.parse(localStorageService.get('songsComposer')) || [];


        }	
  }
  
		

  
  function PilihSongsComposer($scope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams){
	  
      // Inisialisasi
      var songsComposer = JSON.parse(localStorageService.get('songsComposer')) || [];
      var no=songsComposer.length+1;
      var data = [];

        $scope.songsData = {
              id_songs: []
            };
    
        $http.get("Composer/getAllSongsComposer").success(function(result){
          $scope.loading = false;
          data=result;
          // console.log(data);
          $scope.data = data;

          /* jshint newcap: false */
          $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,
            sorting: {
              firstname: 'asc'     // initial sorting
            }
          }, {
            filterDelay: 50,
            total: data.length, // length of data
            getData: function ($defer, params) {
              var searchStr = params.filter().search;
              var mydata = [];

              if (searchStr) {
                searchStr = searchStr.toLowerCase();
                mydata = data.filter(function (item) {
                  return item.id_songs.toLowerCase().indexOf(searchStr) > -1 || item.title.toLowerCase().indexOf(searchStr) > -1;
                });

              } else {
                mydata = data;
              }

              mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
              $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
      });
  
      $scope.pilih = function(){
        //console.log($scope.songsData.id_songs.length);
          // Remove dulu
          localStorageService.remove('songsComposer');
          $timeout(function () {
              $scope.tampilTabSongs = false;
          });
          $("#songsTable .eusiSongs").remove();
          no=1;
          if($scope.songsData.id_songs.length!=0){
            for(var i=0;i<$scope.songsData.id_songs.length;i++){
              $http.get("Composer/getOneSongsComposer/"+$scope.songsData.id_songs[i]).success(function(data,status,headers,config){
                console.log(data);
                // Nyimpen ke localstorage 
                // retrieve it (Or create a blank array if there isn't any info saved yet),
                var datana = JSON.parse(localStorageService.get('songsComposer')) || [];
                // add to it,
                datana.push({id: data[0].id_songs, title: data[0].title, type: data[0].type});
                // then put it back.
                localStorageService.set('songsComposer', JSON.stringify(datana));
                // append data tabel
                $("#songsTable tr:last").after("<tr class='eusiSongs'><td>"+no+"</td><td>"+data[0].title+"<input type='hidden' title='id_songs' value='"+data[0].id_songs+"'></td><td>"+data[0].type+"</td></tr>");
                no++;
              }).error(function(err){
                console.log(err);
              });
            }
          }
      }
      
  }
  

})();
