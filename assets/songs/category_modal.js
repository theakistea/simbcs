(function () {
'use strict';
angular.module('material-lite').controller('PilihCategory', ['$scope', '$rootScope', '$http', '$timeout', 'localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihCategory])
  
  // controller pilih Songs choir
  function PilihCategory($scope, $rootScope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams) {
    // Inisialisasi
    var dataCategory = JSON.parse(localStorageService.get('dataCategory')) || [];
    var no_man = dataCategory.length + 1;
    var data = [];

    $scope.dataCategory = {
      id_category: []
    };

    $http.get("songs/getAllCategory").success(function (result) {
      var dataCategory = JSON.parse(localStorageService.get('dataCategory')) || [];
      var id_category = []
      for(var i = 0; i < dataCategory.length; i ++){
        id_category.push(dataCategory[i].id_songs_category)

      }
      console.log(id_category)
      $scope.dataCategory.id_category = id_category
      $scope.loading = false;
      data = result;
      // console.log(data);
      $scope.data = data;

      /* jshint newcap: false */
      $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10,
        sorting: {
          firstname: 'asc'     // initial sorting
        }
      }, {
          filterDelay: 50,
          total: data.length, // length of data
          getData: function ($defer, params) {
            var searchStr = params.filter().search;
            var mydata = [];

            if (searchStr) {
              searchStr = searchStr.toLowerCase();
              mydata = data.filter(function (item) {
                return item.id_category.toLowerCase().indexOf(searchStr) > -1 || item.title_category.toLowerCase().indexOf(searchStr) > -1;
              });

            } else {
              mydata = data;
            }

            mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
            $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
    });

    $scope.pilih = function () {
      // console.log($scope.manData.id_manager.length);
      // Remove dulu
      localStorageService.remove('dataCategory');
      $timeout(function () {
        $scope.tampilTabMan = false;
      });
      no_man = 1;
      if ($scope.dataCategory.id_category.length != 0) {
        for (var i = 0; i < $scope.dataCategory.id_category.length; i++) {
          $http.get("songs/getOneCategory/" + $scope.dataCategory.id_category[i]).success(function (data, status, headers, config) {
            // console.log(data);
            // Nyimpen ke localstorage 
            // retrieve it (Or create a blank array if there isn't any info saved yet),
            var datana = JSON.parse(localStorageService.get('dataCategory')) || [];
            // add to it,
            console.log(data);
            datana.push({ id_songs_category: data[0].id_songs_category, title_category: data[0].title_category });
            // then put it back.
            localStorageService.set('dataCategory', JSON.stringify(datana));
            // append data tabel
            no_man++;

            $rootScope.$emit('updateSelectData', []);
          }).error(function (err) {
            console.log(err);
          });
        }
      }
    }
  }
})()