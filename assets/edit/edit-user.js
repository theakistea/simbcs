(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataUserController', ['$scope', '$http', '$location', '$routeParams', '$timeout', 'Upload', UpdateDataUserController]);

  function UpdateDataUserController($scope, $http, $location, $routeParams, $timeout, Upload) {

      $http.get("User/data_single/"+$routeParams.var1).success(function(result){
          
            // Pilihan Status
            $scope.actives = [{
                name: 'Active',
                value: 1
            },{
                name: 'Deactive',
                value: 0
            }];
            $scope.active = parseInt(result[0]['active']); // Selected status      

            $scope.idUser  = result[0]['idUser'];
            $scope.username  = result[0]['username'];
            $scope.lastName  = result[0]['lastName'];
            $scope.firstName  = result[0]['firstName'];
            $scope.password  = result[0]['password'];
            $scope.email  = result[0]['email'];
            $scope.idUserGroup  = result[0]['idUserGroup'];
            
        });

        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.update_data=function(){
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("User/update_data",{
    			'id_User':$routeParams.var1,
    			'username':$scope.username,
    			'lastName':$scope.lastName,
    			'firstName':$scope.firstName,
    			'password':$scope.password,
    			'idUserGroup':$scope.idUserGroup,
    			'email':$scope.email
			}).success(function(data,active,headers,config){
                //Beritahu jika data sudah berhasil di input
                alert("Update Success!");

                $location.path("User");
            });
        }

  }

})();