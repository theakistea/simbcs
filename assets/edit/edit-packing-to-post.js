(function () {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataPackingToPostController', ['$scope', '$filter' ,  '$http', '$location', 'ngTableParams' , '$routeParams', '$timeout', 'Upload', 'localStorageService', UpdateDataPackingToPostController])

  function UpdateDataPackingToPostController($scope, $filter ,  $http, $location,  ngTableParams  , $routeParams, $timeout, Upload, localStorageService) {
    // Kosongin semua localstorage pas pertama di load

    $scope.formData = {member : [] , template: ''};
    localStorage.clear();
    $scope.resetMember = function () {
      localStorageService.remove('dataMember');
      $timeout(function () {
        $scope.tampilTabMem = false;
      });
      $("#memberTable .eusiMember").remove();
    }


    $scope.confirmdata = []
    $scope.invitationData = {members  : [] ,managers  : [] ,conductors  : []  };

    console.log($routeParams.tpl)

    $scope.memberCheked = false
    $scope.conductorChecked = false
    $scope.managerChecked = false
    $scope.memberTypes = ""
    $scope.memberType = []

    $scope.selectedChoirId = 0;
    $scope.searchChoirName = 0;


    $scope.getLetterInvitation = function () {

      $http.get("PackingToPost/data_single/" + $routeParams.var1 ).success(function (result) {
        $http.get("PackingToPost/getLetterParticipantChoir/" + $scope.selectedChoirId).success(function (res) {
          


          $scope.invitationData.conductors = res.conductors
          $scope.invitationData.managers = res.managers
          $scope.invitationData.members = res.members

          angular.forEach( $scope.invitationData.members ,  function (data , key) {
            $scope.invitationData.members[key].checked = false
          })
          angular.forEach( $scope.invitationData.conductors ,  function (data , key) {
            $scope.invitationData.conductors[key].checked = false
          })
          angular.forEach( $scope.invitationData.managers ,  function (data , key) {
            $scope.invitationData.managers[key].checked = false
          })




        });
        $scope.id_trs_choir_event = $routeParams.var1;
        $scope.title = result[0] ? result[0]['title'] : 0;



      });
    }

    $scope.getChoirOnEvent = function () {
      var data = [];
      $http.get("ChoirDetail/getChoirOnEvent/" +  $routeParams.var1 ).success(function (result) {
          $scope.loading = false;
          data=result;
          console.log(data);
          $scope.data = data;

          /* jshint newcap: false */
          $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,
            sorting: {
              choir_name: 'asc'     // initial sorting
            }
          }, {
            filterDelay: 50,
            total: data.length, // length of data
            getData: function ($defer, params) {
              var searchStr = params.filter().search;
              var mydata = [];

              if (searchStr) {
                searchStr = searchStr.toLowerCase();
                mydata = data.filter(function (item) {
                  return item.id_trs_choir.toLowerCase().indexOf(searchStr) > -1 || item.choir_name.toLowerCase().indexOf(searchStr) > -1;
                });

              } else {
                mydata = data;
              }

              mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
              $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
      })
    }

    $scope.checkAll = function (what) {
      var type = []
      if (what == 'member') {
        if($filter('filter')($scope.memberType, '3'))
          $scope.memberType.push('3')
        angular.forEach( $scope.invitationData.members ,  function (data , key) {
          $scope.invitationData.members[key].checked = !$scope.memberCheked
        })
        $scope.memberCheked = !$scope.memberCheked
      }
      if (what == 'conductor') {
        if($filter('filter')($scope.memberType, '1'))
          $scope.memberType.push('1')
        angular.forEach( $scope.invitationData.conductors ,  function (data , key) {
          $scope.invitationData.conductors[key].checked = !$scope.conductorChecked
        })
        $scope.conductorChecked = !$scope.conductorChecked
      }
      if (what == 'manager') {
        if($filter('filter')($scope.memberType, '2'))
          $scope.memberType.push('2')
        angular.forEach( $scope.invitationData.managers ,  function (data , key) {
          $scope.invitationData.managers[key].checked = !$scope.managerChecked
        })
        $scope.managerChecked = !$scope.managerChecked
      }
      $scope.memberTypes = $scope.memberType.join('-');
      $scope.memberTypes += '-' + $scope.selectedChoirId
    }

    $scope.check = function (what){
      console.log(what)
      if(what == 'member'){
        var found = $filter('filter')($scope.invitationData.members, {checked: false});
        // $scope.memberCheked = !(found.length > 0);
      }
      if(what == 'conductor'){
        var found = $filter('filter')($scope.invitationData.conductors, {checked: false});
        // $scope.conductorChecked = !(found.length > 0);
      }
      if(what == 'manager'){
        var found = $filter('filter')($scope.invitationData.managers, {checked: false});
        // $scope.managerChecked = !(found.length > 0);
      }
    }
    $scope.pilihChoir = function (id) {
      console.log(id)
      $scope.selectedChoirId = id
      $scope.getLetterInvitation()
    }

    if($routeParams.tpl== 'edit'){
      $scope.getChoirOnEvent()
      $scope.getLetterInvitation()

    }else if ($routeParams.tpl == 'confirm'){
        $http.get("PackingToPost/count_trs_choir_from_trs_choir_event/" + $routeParams.var1)
              .success(function (count) {
               
        $scope.getTrsChoir = new ngTableParams({
            page: 1,            // show first page
            count: 10,
            sorting: {
              choir_name: 'asc'     // initial sorting
            }
          }, {
            filterDelay: 50,
            total: count,
            getData: function ($defer, params) {

              $http.get("PackingToPost/get_trs_choir_from_trs_choir_event/" + $routeParams.var1 + "/" + params.page() + "/" + params.filter().search)
              .success(function (result) {
                $scope.confirmdata = result
                $defer.resolve(result)
                console.log(result)
              })
  

              // var searchStr = params.filter().search;
              // var mydata = [];

              // if (searchStr) {
              //   searchStr = searchStr.toLowerCase();
              //   mydata = data.filter(function (item) {
              //     return item.id_trs_choir.toLowerCase().indexOf(searchStr) > -1 || item.choir_name.toLowerCase().indexOf(searchStr) > -1;
              //   });

              // } else {
              //   mydata = data;
              // }
              
              // mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
              // $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }});
        })
      console.log('dadad')

    }


    // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
    $scope.update_data = function () {

      var mem = JSON.parse(localStorageService.get('dataMember')) || [];
      console.log(mem);

      $http.post("PackingToPost/download", {
        'id_trs_choir_event': $routeParams.var1,
        'id_letter': $scope.template,
        'status': $scope.status
      }).success(function (data, status, headers, config) {

        //Beritahu jika data sudah berhasil di input
        alert("Data Downloaded and Updated!");
        //$location.path("PackingToPost");
      });
    }
    $scope.sendLetter = function () {


      var member = $filter('filter')($scope.invitationData.members, {checked: true});
      var conductor = $filter('filter')($scope.invitationData.conductors, {checked: true});
      var manager = $filter('filter')($scope.invitationData.managers, {checked: true});

      $http.post("PackingToPost/download/" +$routeParams.var1 + "/" +  $scope.template , {
        'id_trs_choir_event': $routeParams.var1,
        'id_letter': $scope.template,
        'status': $scope.status,
        'members' : member,
        'conductors' : conductor,
        'managers' : manager


      }).success(function (data, status, headers, config) {


        //Beritahu jika data sudah berhasil di input
        alert("Data Downloaded and Updated!");
        var blob = new Blob([data], {type: "application/pdf"});
        var objectUrl = URL.createObjectURL(blob);
        window.open(objectUrl);

        //$location.path("PackingToPost");
      });
    }
    $scope.send_to_email = function(){
      $http.get("PackingToPost/send_to_email/" + $scope.id_trs_choir_event + "/" + $scope.formData.template).success(function (data, status, headers, config) {
        $scope.data.push(data[0]);

        alert("Data Has Sended and Updated!");
      }).error(function (err) {
        console.log(err);
      });

    }

    $scope.send_to_email = function(){
      $http.get("PackingToPost/send_to_email/" + $scope.id_trs_choir_event + "/" + $scope.formData.template).success(function (data, status, headers, config) {
        $scope.data.push(data[0]);

        alert("Data Has Sended and Updated!");
      }).error(function (err) {
        console.log(err);
      });

    }

    $scope.confirmrechiver = []
    $scope.confirmrechiverString = ""
    $scope.updateconfirmParams = function(){
      console.log($scope.confirmrechiver)
      $scope.confirmrechiverString = $scope.confirmrechiver.join(',')


    }
  }
  

})();

