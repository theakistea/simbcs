(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataCommitteeController', ['$scope', '$http', '$location', '$routeParams', '$timeout', 'Upload', UpdateDataCommitteeController]);

  function UpdateDataCommitteeController($scope, $http, $location, $routeParams, $timeout, Upload) {

    $scope.uploadFiles = function(file, errFiles){
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: 'committee/upload',
                data: {file: file} // attribut name file dengan value file
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
                $scope.photo = response.data;
                console.log(response);
            }, function (response) {
                if (response.status > 0)
                   $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * 
                evt.loaded / evt.total));
            });
        }   
    }
	$scope.delete_photo = function(filename){
        console.log(filename);
        
        $http.post("Committee/deletePhoto",{filename:filename}).success(function(data,status,headers,config){
            if(data==1){
                $scope.photo = "";
                $("#photo").hide();
            }
        }).error(function(err){
            console.log(err);
        });
    }
        
        $http.get("Committee/data_single/"+$routeParams.var1).success(function(result){
            // Foto
            $scope.photo = (result[0]['photo']==undefined)? '' : result[0]['photo'];

            // Pilihan gender
            $scope.genders = [{
                name: 'Male',
                value: 'male'
            },{
                name: 'Female',
                value: 'female'
            }];
            $scope.gender = result[0]['gender'].toLowerCase(); // Selected gender

            // Pilihan appellation
            $scope.appellations = [{
                name: 'Mr.',
                value: 0
            },{
                name: 'Ms.',
                value: 1
            },{
                name: 'Mrs.',
                value: 2
            }];
            $scope.appellation = parseInt(result[0]['appellation']); // Selected appellation

            // Pilihan country
            $http.get("committee/getCountry").success(function(resDataCountry){
                $scope.allCountry = resDataCountry;
            }).error(function(){
                $scope.allCountry = {};
            });
            $scope.country = result[0]['country']; // Selected country

            // Get state
            $http.get("committee/getState").success(function(resDataState){
                $scope.allState = resDataState;
            }).error(function(){
                $scope.allState = {};
            });
            $scope.state = result[0]['state']; // Selected state

            // Pilihan Status
            $scope.statuses = [{
                name: 'Active',
                value: 1
            },{
                name: 'Deactive',
                value: 0
            }];
            $scope.address_status = parseInt(result[0]['address_status']); // Selected address_status
            $scope.status = parseInt(result[0]['status']); // Selected status     

            $scope.name  = result[0]['name'];
            $scope.first_title = result[0]['first_title'];
            $scope.last_title = result[0]['last_title'];
            $scope.gender = result[0]['gender'].toLowerCase();
            $scope.street_address = result[0]['street_address'];
            $scope.city = result[0]['city'];
            $scope.website = result[0]['website'];
            $scope.email = result[0]['email'];
            $scope.phone = result[0]['phone'];
            $scope.social_media = result[0]['social_media'];
            $scope.place_of_birth = result[0]['place_of_birth'];
            $scope.date_of_birth = result[0]['date_of_birth'];
            $scope.notes = result[0]['notes'];
        });

        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.update_data=function(){
            console.log($scope.name);
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("Committee/update_data",{
    			'id_committee':$routeParams.var1,
    			'name':$scope.name,
    			'gender':$scope.gender,
    			'appellation':$scope.appellation,
    			'first_title':$scope.first_title,
    			'last_title':$scope.last_title,
    			'city':$scope.city,
    			'state':$scope.state,
    			'country':$scope.country,
    			'address_status':$scope.address_status,
    			'phone':$scope.phone,
    			'email':$scope.email,
    			'social_media':$scope.social_media,
    			'website':$scope.website,
    			'place_of_birth':$scope.place_of_birth,
    			'date_of_birth':$scope.date_of_birth,
    			'notes':$scope.notes,
                'photo':$scope.photo,
    			'status':$scope.status,
    			'street_address':$scope.street_address
			}).success(function(data,status,headers,config){
                // console.log(data);
                //Beritahu jika data sudah berhasil di input
                alert("Update Success!");
                $location.path("committee");
            });
        }

  }

})();
