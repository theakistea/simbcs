(function () {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataChoirEventDetailController', ['$scope', '$http', '$location', '$routeParams', '$timeout', 'Upload', 'localStorageService', UpdateDataChoirEventDetailController])

    .controller('PilihCandjChoirEvent', ['$scope', '$http', '$timeout', 'localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihCandjChoirEvent])
    .controller('PilihChoirChoirEvent', ['$scope', '$http', '$timeout', 'localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihChoirChoirEvent])
    .controller('PilihComChoirEvent', ['$scope', '$http', '$timeout', 'localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihComChoirEvent])
    .controller('PilihVenueAndType', ['$scope', '$http', '$timeout', 'localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihVenueAndType])
    .controller('sponsorChoirEvent', ['$scope', '$http', '$timeout', 'localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', sponsorChoirEvent])
    ;

  function UpdateDataChoirEventDetailController($scope, $http, $location, $routeParams, $timeout, Upload, localStorageService) {
    // Kosongin semua localstorage pas pertama di load
    localStorage.clear();

    // reset table pake tombol
    $scope.resetSponsor = function () {
      localStorageService.remove('dataSponsor');
      $("#sponsorTable .eusiSponsor").remove();
    }
    $scope.resetCandj = function () {
      localStorageService.remove('dataCandj');
      $("#candjTable .eusiCandj").remove();
    }
    $scope.resetVenue = function () {
      localStorageService.remove('dataVenue');
      $("#venueTable .eusiVenue").remove();
    }
    $scope.resetChoir = function () {
      localStorageService.remove('dataChoir');
      $("#choirTable .eusiChoir").remove();
    }
    $scope.resetCom = function () {
      localStorageService.remove('dataCom');
      $("#comTable .eusiCom").remove();
    }

    $scope.uploadFiles = function (file, errFiles) {
      $scope.f = file;
      $scope.errFile = errFiles && errFiles[0];
      if (file) {
        file.upload = Upload.upload({
          url: 'ChoirEventDetail/upload',
          data: { file: file } // attribut name file dengan value file
        });

        file.upload.then(function (response) {
          $timeout(function () {
            file.result = response.data;
          });
          $scope.photo = response.data;
          console.log(response);
        }, function (response) {
          if (response.status > 0)
            $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
          file.progress = Math.min(100, parseInt(100.0 *
            evt.loaded / evt.total));
        });
      }
    }
    $scope.delete_photo = function (filename) {
      console.log(filename);

      $http.post("ChoirEventDetail/deletePhoto", { filename: filename }).success(function (data, status, headers, config) {
        if (data == 1) {
          $scope.photo = "";
          $("#photo").hide();
        }
      }).error(function (err) {
        console.log(err);
      });
    }



    $http.get("ChoirEventDetail/data_single/" + $routeParams.var1).success(function (result) {
      // Foto
      $scope.photo = (result[0]['photo'] == undefined) ? '' : result[0]['photo'];
      $scope.code_event = result[0]['code_event']

      $scope.appellation = parseInt(result[0]['appellation']); // Selected appellation

      // Pilihan country
      $http.get("ChoirEventDetail/get_country").success(function (resDataCountry) {
        $scope.allCountry = resDataCountry;
      }).error(function () {
        $scope.allCountry = {};
      });
      $scope.country = result[0]['country']; // Selected country

      // Get state
      $http.get("ChoirEventDetail/get_state").success(function (resDataState) {
        $scope.allState = resDataState;
      }).error(function () {
        $scope.allState = {};
      });
      $scope.state = result[0]['state']; // Selected state

      // Pilihan Status
      $scope.statuses = [{
        name: 'Active',
        value: 1
      }, {
          name: 'Deactive',
          value: 0
        }];

      $scope.status = parseInt(result[0]['status']); // Selected status      
      // Choir
      $http.get("ChoirEventDetail/getChoir/" + $routeParams.var1).success(function (res) {
        $scope.choirs = res;
        //console.log(res.length);
        for (var i = 0; i < res.length; i++) {
          var datana = JSON.parse(localStorageService.get('dataChoir')) || [];
          // add to it,
          datana.push({ id: res[i].id_trs_choir_event, id2: res[i].id_trs_choir, name: res[i].choir_name });
          // then put it back.
          localStorageService.set('dataChoir', JSON.stringify(datana));
        }
      });
      // Venue
      $http.get("ChoirEventDetail/getVenue/" + $routeParams.var1).success(function (res) {
        $scope.venues = res;
        //console.log(res.length);
        for (var i = 0; i < res.length; i++) {
          var datana = JSON.parse(localStorageService.get('dataVenue')) || [];
          // add to it,
          datana.push({ venue: res[i].venue, type: res[i].kategori , purpose: res[i].purpose });
          // then put it back.
          localStorageService.set('dataVenue', JSON.stringify(datana));
        }
      });
      // Sponsor
      $http.get("ChoirEventDetail/getSponsor/" + $routeParams.var1).success(function (res) {
        $scope.spons = res;
        //console.log(res.length);
        for (var i = 0; i < res.length; i++) {
          var datana = JSON.parse(localStorageService.get('dataSponsor')) || [];

          // add to it,
          datana.push({ sponsor: res[i].sponsor, sponsor_as: res[i].sebagai });
          // then put it back.
          localStorageService.set('dataSponsor', JSON.stringify(datana));
        }
      });
      // Candj
      $http.get("ChoirEventDetail/getCandj/" + $routeParams.var1).success(function (res) {
        $scope.candjs = res;
        //console.log(res);
        for (var i = 0; i < res.length; i++) {
          var datana = JSON.parse(localStorageService.get('dataCandj')) || [];
          // add to it,
          datana.push({ id: res[i].id_candj, name: res[i].name });
          // then put it back.
          localStorageService.set('dataCandj', JSON.stringify(datana));
        }
      });
      // Committee
      $http.get("ChoirEventDetail/getComm/" + $routeParams.var1).success(function (res) {
        $scope.Coms = res;
        //console.log(res.length);
        for (var i = 0; i < res.length; i++) {
          var datana = JSON.parse(localStorageService.get('dataCom')) || [];
          // add to it,
          datana.push({ id: res[i].id_committee, name: res[i].name });
          // then put it back.
          localStorageService.set('dataCom', JSON.stringify(datana));
        }
      });
      $scope.id_trs_choir_event = result[0]['id_trs_choir_event'];
      $scope.title = result[0]['title'];
      $scope.grade = result[0]['grade'];
      
      $scope.date_start = result[0]['date_start'];
      $scope.date_finish = result[0]['date_finish'];
      $scope.host = result[0]['host'];
      $scope.PIC = result[0]['PIC'];
      $scope.city = result[0]['city'];
      $scope.status = result[0]['status'];
      $scope.date_modify = result[0]['date_modify'];
      $scope.notes = result[0]['notes'];

      (function(){
        $('#grade').val($scope.grade)
      })

    });

    // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
    $scope.update_data = function () {
      var dataChoir = JSON.parse(localStorageService.get('dataChoir')) || [];
      var dataCandj = JSON.parse(localStorageService.get('dataCandj')) || [];
      var dataSponsor = JSON.parse(localStorageService.get('dataSponsor')) || [];
      var dataVenue = JSON.parse(localStorageService.get('dataVenue')) || [];
      var dataCom = JSON.parse(localStorageService.get('dataCom')) || [];
      console.log($scope.photo);
      //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
      //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
      $http.post("ChoirEventDetail/update_data", {
        'id_trs_choir_event': $scope.id_trs_choir_event,
        'title': $scope.title,
        'grade': $scope.grade,
        'host': $scope.host,
        'PIC': $scope.PIC,
        'country': $scope.country,
        'state': $scope.state,
        'date_start': $scope.date_start,
        'date_finish': $scope.date_finish,
        'city': $scope.city,
        'photo': $scope.photo,
        'notes': $scope.notes,
        'dataChoir': dataChoir,
        'dataCandj': dataCandj,
        'dataSponsor': dataSponsor,
        'dataVenue': dataVenue,
        'dataCom': dataCom,
        'code_event': $scope.code_event
      }).success(function (data, status, headers, config) {
        localStorageService.remove('dataCandj');
        localStorageService.remove('dataChoir');
        localStorageService.remove('dataCom');
        localStorageService.remove('dataSponsor');
        localStorageService.remove('dataVenue');
        //Beritahu jika data sudah berhasil di input
        alert("Update Success!");

        $location.path("ChoirEventDetail");
      });
    }

  }

  function PilihCandjChoirEvent($scope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams) {
    // Inisialisasi
    var dataCandj = JSON.parse(localStorageService.get('dataCandj')) || [];
    var no_candj = dataCandj.length + 1;
    var data = [];

    $scope.candjData = {
      id_candj: []
    };
    
     $http.get("ChoirEventDetail/getCandj/" + $routeParams.var1).success(function (res) {
      var id_candj = []
      for (var i = 0; i < res.length; i++) {
        id_candj.push(res[i].id_candj)
      }
      $scope.candjData.id_candj = id_candj
    });

    /* ambil semua songs berdasarkan composer
      $http.get("ChoirEventDetail/getAllCandjOn/"+$routeParams.var1).success(function(resSongs){
          for (var i=0; i < resSongs.length; i++) {
              // Buat di ceklis kalo udah ada di database
              $scope.songsData.id_candj.push(resSongs[i]['id_candj']);
          };
      });
  */
    $http.get("ChoirEventDetail/getAllCandjChoirEvent").success(function (result) {
      $scope.loading = false;
      data = result;
      // console.log(data);
      $scope.data = data;

      /* jshint newcap: false */
      $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10,
        sorting: {
          firstname: 'asc'     // initial sorting
        }
      }, {
          filterDelay: 50,
          total: data.length, // length of data
          getData: function ($defer, params) {
            var searchStr = params.filter().search;
            var mydata = [];

            if (searchStr) {
              searchStr = searchStr.toLowerCase();
              mydata = data.filter(function (item) {
                return item.id_candj.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
              });

            } else {
              mydata = data;
            }

            mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
            $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
    });

    $scope.pilih = function () {
      console.log($scope.candjData.id_candj.length);
      // Remove dulu
      localStorageService.remove('dataCandj');
      $("#candjTable .eusiCandj").remove();
      no_candj = 1;

      if ($scope.candjData.id_candj.length != 0) {
        for (var i = 0; i < $scope.candjData.id_candj.length; i++) {
          $http.get("ChoirEventDetail/getOneCandj/" + $scope.candjData.id_candj[i]).success(function (data, status, headers, config) {
            console.log(data);
            // Nyimpen ke localstorage 
            // retrieve it (Or create a blank array if there isn't any info saved yet),
            var datana = JSON.parse(localStorageService.get('dataCandj')) || [];
            // add to it,
            datana.push({ id: data[0].id_candj, name: data[0].name });
            // then put it back.
            localStorageService.set('dataCandj', JSON.stringify(datana));
            // append data tabel
            $("#candjTable tr:last").after("<tr class='eusiCandj'><td>" + no_candj + "</td><td>" + data[0].name + "<input type='hidden' name='id_candj' value='" + data[0].id_candj + "'></td></tr>");
            no_candj++;
          }).error(function (err) {
            console.log(err);
          });
        }
      }
    }
  }
  function PilihChoirChoirEvent($scope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams) {
    // Inisialisasi
    var dataChoir = JSON.parse(localStorageService.get('dataChoir')) || [];
    var no_choir = dataChoir.length + 1;
    var data = [];

    $scope.choirData = {
      id_trs_choir: []
    };

    $http.get("ChoirEventDetail/getChoir/" + $routeParams.var1).success(function (res) {
      var id_trs_choir = []
      for (var i = 0; i < res.length; i++) {
        id_trs_choir.push(res[i].id_trs_choir)
      }
      $scope.choirData.id_trs_choir = id_trs_choir
    });

    $http.get("ChoirEventDetail/getAllChoirChoirEvent").success(function (result) {
      $scope.loading = false;
      data = result;
      // console.log(data);

      $scope.data = data;

      /* jshint newcap: false */
      $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10,
        sorting: {
          firstname: 'asc'     // initial sorting
        }
      }, {
          filterDelay: 50,
          total: data.length, // length of data
          getData: function ($defer, params) {
            var searchStr = params.filter().search;
            var mydata = [];

            if (searchStr) {
              searchStr = searchStr.toLowerCase();
              mydata = data.filter(function (item) {
                return item.id_trs_choir.toLowerCase().indexOf(searchStr) > -1 || item.choir_name.toLowerCase().indexOf(searchStr) > -1;
              });

            } else {
              mydata = data;
            }

            mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
            $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
    });

    $scope.pilih = function () {
      console.log($scope.choirData.id_trs_choir.length);
      // Remove dulu
      localStorageService.remove('dataChoir');
      $("#choirTable .eusiChoir").remove();
      no_choir = 1;

      if ($scope.choirData.id_trs_choir.length != 0) {
        for (var i = 0; i < $scope.choirData.id_trs_choir.length; i++) {
          $http.get("ChoirEventDetail/getOneChoir/" + $scope.choirData.id_trs_choir[i]).success(function (data, status, headers, config) {
            console.log(data);
            // Nyimpen ke localstorage 
            // retrieve it (Or create a blank array if there isn't any info saved yet),
            var datana = JSON.parse(localStorageService.get('dataChoir')) || [];
            // add to it,
            datana.push({ id2: data[0].id_trs_choir, id: data[0].id_trs_choir_event, name: data[0].choir_name });
            // then put it back.
            localStorageService.set('dataChoir', JSON.stringify(datana));
            // append data tabel
            $("#choirTable tr:last").after("<tr class='eusiChoir'><td>" + no_choir + "</td><td>" + data[0].choir_name + "<input type='hidden' name='id_trs_choir' value='" + data[0].id_trs_choir + "'></td></tr>");
            no_choir++;
          }).error(function (err) {
            console.log(err);
          });
        }
      }
    }
  }

  function PilihComChoirEvent($scope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams) {
    // Inisialisasi
    var dataCom = JSON.parse(localStorageService.get('dataCom')) || [];
    var no_com = dataCom.length + 1;
    var data = [];

    $scope.comData = {
      id_committee: []
    };
    $http.get("ChoirEventDetail/getComm/" + $routeParams.var1).success(function (res) {
      var id_committee = []
      for (var i = 0; i < res.length; i++) {
        id_committee.push(res[i].id_committee)
      }
      $scope.comData.id_committee = id_committee
    });

    $http.get("ChoirEventDetail/getAllComChoirEvent").success(function (result) {
      $scope.loading = false;
      data = result;
      // console.log(data);
      $scope.data = data;

      /* jshint newcap: false */
      $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10,
        sorting: {
          firstname: 'asc'     // initial sorting
        }
      }, {
          filterDelay: 50,
          total: data.length, // length of data
          getData: function ($defer, params) {
            var searchStr = params.filter().search;
            var mydata = [];

            if (searchStr) {
              searchStr = searchStr.toLowerCase();
              mydata = data.filter(function (item) {
                return item.id_committee.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
              });

            } else {
              mydata = data;
            }

            mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
            $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
    });

    $scope.pilih = function () {
      // console.log($scope.manData.id_manager.length);
      // Remove dulu
      localStorageService.remove('dataCom');
      $timeout(function () {
        $scope.tampilTabCom = false;
      });
      $("#comTable .eusiCom").remove();
      no_com = 1;
      if ($scope.comData.id_committee.length != 0) {
        for (var i = 0; i < $scope.comData.id_committee.length; i++) {
          $http.get("ChoirEventDetail/getOneCom/" + $scope.comData.id_committee[i]).success(function (data, status, headers, config) {
            // console.log(data);
            // Nyimpen ke localstorage 
            // retrieve it (Or create a blank array if there isn't any info saved yet),
            var datana = JSON.parse(localStorageService.get('dataCom')) || [];
            // add to it,
            datana.push({ id: data[0].id_committee, name: data[0].name, gender: data[0].gender });
            // then put it back.
            localStorageService.set('dataCom', JSON.stringify(datana));
            // append data tabel
            $("#comTable tr:last").after("<tr class='eusiCom'><td>" + no_com + "</td><td>" + data[0].name + "<input type='hidden' name='id_committee' value='" + data[0].id_committee + "'></td></tr>");
            no_com++;
          }).error(function (err) {
            console.log(err);
          });
        }
      }
    }
  }

  function sponsorChoirEvent($scope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams) {
    // Inisialisasi
    var dataSponsor = JSON.parse(localStorageService.get('dataSponsor')) || [];
    var no = dataSponsor.length + 1;
    var data = [];
    $scope.eusiSponsor = dataSponsor;
    no = 1;
    $scope.pilih = function () {
      // console.log($scope.manData.id_manager.length);
      // Remove dulu
      //localStorageService.remove('dataSponsor');
      //$("#sponsorTable .eusiSponsor").remove();
      // Nyimpen ke localstorage 
      // retrieve it (Or create a blank array if there isn't any info saved yet),
      var datana = JSON.parse(localStorageService.get('dataSponsor')) || [];
      // add to it,
      datana.push({ sponsor: $scope.sponsor.name, sponsor_as: $scope.sponsor.as });
      // then put it back.
      localStorageService.set('dataSponsor', JSON.stringify(datana));
      // append data tabel
      $("#sponsorTable tr:last").after("<tr class='eusiSponsor'><td>" + no + "</td><td>" + $scope.sponsor.name + "</td><td>" + $scope.sponsor.as + "</td></tr>");
      no++;
      
      $scope.sponsor.name =""
      $scope.sponsor.type = ""
    }
  }

  function PilihVenueAndType($scope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams) {
    // Inisialisasi
    var dataVenue = JSON.parse(localStorageService.get('dataVenue')) || [];
    var no = dataVenue.length + 1;
    var data = [];
    $scope.eusiVenue = dataVenue;
    no = 1;
    $scope.pilih = function () {
      // Nyimpen ke localstorage 
      // retrieve it (Or create a blank array if there isn't any info saved yet),
      var datana = JSON.parse(localStorageService.get('dataVenue')) || [];
      // add to it,
      datana.push({ venue: $scope.venue.name, type: $scope.venue.type ,purpose: $scope.venue.purpose });
      // then put it back.
      localStorageService.set('dataVenue', JSON.stringify(datana));
      // append data tabel
      $("#venueTable tr:last").after("<tr class='eusiVenue'><td>" + no + "</td><td>" + $scope.venue.name + "</td><td>" + $scope.venue.type + "</td><<td>" + $scope.venue.purpose + "</td></tr>");
      no++;
      $scope.venue.name =""
      $scope.venue.type = ""
      $scope.venue.purpose = ""
    }
  }

})();
