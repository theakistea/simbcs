(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataEventActivity3', ['$scope', '$http', '$location', '$routeParams', '$timeout', 'Upload', 'dataService', UpdateDataEventActivity3])
	.service('dataService', function() {
      // private variable
      var _dataObj = {};

      // public API
      this.dataObj = _dataObj;
    })
	;

  function UpdateDataEventActivity3($scope, $http, $location, $routeParams, $timeout, Upload, dataService) {      
         $scope.nom ;
        $scope.conductor ; 
        $scope.officials;
        $scope.total_participant = 0
        $scope.cout_total_participant = function () {
            $scope.total_participant = Number($scope.num_of_singer ? $scope.num_of_singer : 0) + Number($scope.nom ? $scope.nom : 0) + Number($scope.conductor ? $scope.conductor : 0) + Number($scope.officials ? $scope.officials : 0);
            $scope.sub_total = Number($scope.total_participant ? $scope.total_participant :  0) * Number($scope.price_per_person ? $scope.price_per_person : 0)
        };
        $http.get("EventActivity3/data_single/"+$routeParams.var1).success(function(result){
        //  
		  dataService.dataObj = {
            member : result,
			ukuran : result
          };
		  //console.log(result);
		  // remove heula
          $("#tshirt .eusiMember").remove();
          // add deui
          for(var i=1;i<=result.length;i++){
            $("#tshirt tr:last").after("<tr class='eusiMember'><td>"+i+"</td><td>"+result[i-1].member+"</td><td><input name='ukuran' ng-model='ukuran' type='text' value='"+result[i-1].ukuran+"' id='ukuran"+i+"'></td></tr>");
          }
		    //console.log(dataService.dataObj.member);
            //console.log(dataService.dataObj.ukuran);
			// Pilihan Status
            $scope.statuses = [{
                name: 'Active',
                value: 1
            },{
                name: 'Deactive',
                value: 0
            }];

            $scope.title  = result[0]['title'];
            $scope.choir_name  = result[0]['choir_name'];
            $scope.num_of_singer  = result[0]['num_of_singer'];
            $scope.nom  = result[0]['num_of_musician'];
            $scope.conductor  = result[0]['num_of_conductor'];
            $scope.officials  = result[0]['num_of_official'];
            $scope.stage_rehearseal_date  = result[0]['stage_rehearseal_date'];

            $scope.performance_schedule  = result[0]['performance_schedule'];
            $scope.performance_schedule_time  = result[0]['performance_schedule_time'];

            $scope.reg_time  = new Date("October 13, 2014 " +  result[0]['reg_time']+ ":00");
            $scope.reg_time_start  = new Date("October 13, 2014 " +  result[0]['reg_time_start']+ ":00");
            $scope.reg_time_finish  = new Date("October 13, 2014 " +  result[0]['reg_time_finish']+ ":00");
            $scope.re_reg_time  = new Date("October 13, 2014 " +  result[0]['re_reg_time']+ ":00");
            $scope.re_reg_time_start  = new Date("October 13, 2014 " +  result[0]['re_reg_time_start']+ ":00");
            $scope.re_reg_time_finish  = new Date("October 13, 2014 " +  result[0]['re_reg_time_finish']+ ":00");
            $scope.re_reg_time_preparation  = new Date("October 13, 2014 " +  result[0]['re_reg_time_preparation']+ ":00");
            $scope.re_reg_time_backstage  = new Date("October 13, 2014 " +  result[0]['re_reg_time_backstage']+ ":00");
            $scope.notes = result[0]['notes'];
            $scope.cout_total_participant()
        });

        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.update_data=function(){
			var ukuranna = [];
			for(var i=1;i<=(dataService.dataObj.member).length;i++){
              //console.log($("#ukuran"+i).val());
			  ukuranna.push($("#ukuran"+i).val());
            }
			console.log(ukuranna);
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("EventActivity3/update_data",{
    		'id_pd_event_activity_3':$routeParams.var1,
    		'num_of_singer':$scope.num_of_singer ,
              'num_of_musician':$scope.nom,
              'num_of_conductor':$scope.conductor,
              'num_of_official':$scope.officials,
              'stage_rehearseal_date':$scope.stage_rehearseal_date,
              'reg_time':moment($scope.reg_time).format("HH:mm"),
              'reg_time_start' :moment($scope.reg_time_start).format("HH:mm"),
              'reg_time_finish' :moment($scope.reg_time_finish).format("HH:mm"),
              'performance_schedule' :$scope.performance_schedule,
              'performance_schedule_time' :$scope.performance_schedule_time,
              're_reg_time' :moment($scope.re_reg_time).format("HH:mm"),
              're_reg_time_start' :moment($scope.re_reg_time_start).format("HH:mm"),
              're_reg_time_finish' :moment($scope.re_reg_time_finish).format("HH:mm"),
              're_reg_time_preparation' :moment($scope.re_reg_time_preparation).format("HH:mm"),
              're_reg_time_backstage' :moment($scope.re_reg_time_backstage).format("HH:mm"),
              'notes': $scope.notes
			}).success(function(data,status,headers,config){
                //Beritahu jika data sudah berhasil di input
                alert("Update Success!");

                $location.path("EventActivity3");
            });
        }

  }

})();