(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataEventActivity1', ['$scope', '$http', '$location', '$routeParams', '$timeout', 'Upload', 'dataService', UpdateDataEventActivity1])
	.service('dataService', function() {
      // private variable
      var _dataObj = {};

      // public API
      this.dataObj = _dataObj;
    })
	;

  function UpdateDataEventActivity1($scope, $http, $location, $routeParams, $timeout, Upload, dataService) {      
        
        $scope.nom ;
        $scope.conductor ; 
        $scope.officials;
        $scope.total_participant = 0
        $scope.cout_total_participant = function () {
            $scope.total_participant = Number($scope.num_of_singer ? $scope.num_of_singer : 0) + Number($scope.nom ? $scope.nom : 0) + Number($scope.conductor ? $scope.conductor : 0) + Number($scope.officials ? $scope.officials : 0);
            $scope.sub_total = Number($scope.total_participant ? $scope.total_participant :  0) * Number($scope.price_per_person ? $scope.price_per_person : 0)
        };

        $http.get("EventActivity1/data_single/"+$routeParams.var1).success(function(result){
        //  
		  dataService.dataObj = {
            member : result,
			ukuran : result
          };
		  //console.log(result);
		  // remove heula
          $("#tshirt .eusiMember").remove();
          // add deui
          for(var i=1;i<=result.length;i++){
            $("#tshirt tr:last").after("<tr class='eusiMember'><td>"+i+"</td><td>"+result[i-1].member+"</td><td><input name='ukuran' ng-model='ukuran' type='text' value='"+result[i-1].ukuran+"' id='ukuran"+i+"'></td></tr>");
          }
		    //console.log(dataService.dataObj.member);
            //console.log(dataService.dataObj.ukuran);
			// Pilihan Status
            $scope.statuses = [{
                name: 'Active',
                value: 1
            },{
                name: 'Deactive',
                value: 0
            }];

            $scope.title  = result[0]['title'];
            $scope.choir_name  = result[0]['choir_name'];
            $scope.num_of_singer  = result[0]['num_of_singer'];
            $scope.nom  = result[0]['num_of_musician'];
            $scope.conductor  = result[0]['num_of_conductor'];
            $scope.officials  = result[0]['num_of_official'];
            $scope.stage_rehearseal_date  = result[0]['stage_rehearseal_date'];
            $scope.stage_rehearseal_time_check_in  = new Date("October 13, 2014 " +  result[0]['stage_rehearseal_time_check_in']+ ":00");
            $scope.stage_rehearseal_time_start  = new Date("October 13, 2014 " +  result[0]['stage_rehearseal_time_start']+ ":00");
            $scope.stage_rehearseal_time_finish  = new Date("October 13, 2014 " +  result[0]['stage_rehearseal_time_finish']+ ":00");
            
            $scope.performance_schedule  = result[0]['performance_schedule'];
            $scope.performance_schedule_time  = result[0]['performance_schedule_time'];

            $scope.reg_date  = result[0]['reg_date'];
            $scope.reg_time_start  = new Date("October 13, 2014 " +  result[0]['reg_time_start']+ ":00");
            $scope.reg_time_finish  = new Date("October 13, 2014 " +  result[0]['reg_time_finish']+ ":00");


            $scope.choir_clinic_date  = result[0]['choir_clinic_date'];
            $scope.choir_clinic_time_start  = new Date("October 13, 2014 " +  result[0]['choir_clinic_time_start']+ ":00");
            $scope.choir_clinic_time_finish  = new Date("October 13, 2014 " +  result[0]['choir_clinic_time_finish']+ ":00");



            $scope.re_reg_time  = new Date("October 13, 2014 " +  result[0]['re_reg_time']+ ":00");
            $scope.re_reg_time_start  = new Date("October 13, 2014 " +  result[0]['re_reg_time_start']+ ":00");
            $scope.re_reg_time_finish  = new Date("October 13, 2014 " +  result[0]['re_reg_time_finish']+ ":00");
            $scope.re_reg_time_preparation  = new Date("October 13, 2014 " +  result[0]['re_reg_time_preparation']+ ":00");
            $scope.re_reg_time_backstage  = new Date("October 13, 2014 " +  result[0]['re_reg_time_backstage']+ ":00");
            $scope.notes = result[0]['notes'];
            $scope.cout_total_participant()

            //     // Stage Rehearseal Date (Jadwal Uji Coba Panggung)
					// 'stage_rehearseal_date' 			=> isset($data['stage_rehearseal_date']) ? $data['stage_rehearseal_date'] : '' , 
					// 'stage_rehearseal_time_check_in' 	=> isset($data['stage_rehearseal_time_check_in']) ? $data['stage_rehearseal_time_check_in'] : '' , 
					// 'stage_rehearseal_time_start'		=> isset($data['stage_rehearseal_time_start']) ? $data['stage_rehearseal_time_start'] : '' , 
					// 'stage_rehearseal_time_finish' 		=> isset($data['stage_rehearseal_time_finish']) ? $data['stage_rehearseal_time_finish'] : '' , 
					// //Registration Info (Jadwal registrasi ulang)

					// 'reg_date' 					=> isset($data['reg_time']) ? $data['reg_time'] : '' ,
					// 'reg_time_start' 			=> isset($data['reg_time_start']) ? $data['reg_time_start'] : '' ,
					// 'reg_time_finish' 			=> isset($data['reg_time_finish']) ? $data['reg_time_finish'] : '' ,
					// //Choir clinic info (Jadwal Choir Clinic)
					// 'choir_clinic_date' 		=> isset($data['choir_clinic_date']) ? $data['choir_clinic_date'] : '' ,
					// 'choir_clinic_time_start' 	=> isset($data['choir_clinic_time_start']) ? $data['choir_clinic_time_start'] : '' ,
					// 'choir_clinic_time_finish' 	=> isset($data['choir_clinic_time_finish']) ? $data['choir_clinic_time_finish'] : '' ,
					// 'performance_schedule' 		=> isset($data['performance_schedule']) ? $data['performance_schedule'] : '' ,
					// 'performance_schedule_time' => isset($data['performance_schedule_time']) ? $data['performance_schedule_time'] : '' ,
        });

        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.update_data=function(){
			var ukuranna = [];
			for(var i=1;i<=(dataService.dataObj.member).length;i++){
              //console.log($("#ukuran"+i).val());
			  ukuranna.push($("#ukuran"+i).val());
            }
			console.log(ukuranna);
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("EventActivity1/update_data",{
              'id_pd_event_activity_1' : $routeParams.var1,
    		  'id_trs_choir_event':dataService.dataObj.id_trs_choir_event,
              'id_trs_choir':dataService.dataObj.id_trs_choir,
              'member':dataService.dataObj.member,
              'ukuran'                      :ukuranna,
              'num_of_singer'               :dataService.dataObj.number_of_singer,
              'num_of_musician'             :$scope.nom,
              'num_of_conductor'            :$scope.conductor,
              'num_of_official'             :$scope.officials,

              'stage_rehearseal_date'           :$scope.stage_rehearseal_date,
              'stage_rehearseal_time_check_in'  :moment($scope.stage_rehearseal_time_check_in).format("HH:mm"),
              'stage_rehearseal_time_start'     :moment($scope.stage_rehearseal_time_start).format("HH:mm"),
              'stage_rehearseal_time_finish'    :moment($scope.stage_rehearseal_time_finish).format("HH:mm"),

              'reg_date'                    :$scope.reg_date,
              'reg_time_start'              :moment($scope.reg_time_start).format("HH:mm"),
              'reg_time_finish'             :moment($scope.reg_time_finish).format("HH:mm"),
              'performance_schedule'        :$scope.performance_schedule,
              'performance_schedule_time'   :$scope.performance_schedule_time,
              're_reg_time'                 :moment($scope.re_reg_time).format("HH:mm"),
              're_reg_time_start'           :moment($scope.re_reg_time_start).format("HH:mm"),
              're_reg_time_finish'          :moment($scope.re_reg_time_finish).format("HH:mm"),
              're_reg_time_preparation'     :moment($scope.re_reg_time_preparation).format("HH:mm"),
              're_reg_time_backstage'       :moment($scope.re_reg_time_backstage).format("HH:mm"),
              'choir_clinic_date'           :$scope.choir_clinic_date,
              'choir_clinic_time_start'     :moment($scope.choir_clinic_time_start).format("HH:mm"),
              'choir_clinic_time_finish'    :moment($scope.choir_clinic_time_finish).format("HH:mm"),
              'notes': $scope.notes
			}).success(function(data,status,headers,config){
                //Beritahu jika data sudah berhasil di input
                alert("Update Success!");

                $location.path("EventActivity1");
            });
        }

  }

})();