(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataComposerController', ['$scope', '$http', '$location', '$routeParams', '$timeout', 'Upload','localStorageService', UpdateDataComposerController])
	.controller('PilihSongsComposer', ['$scope', '$http', '$timeout','localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihSongsComposer]);

  function UpdateDataComposerController($scope, $http, $location, $routeParams, $timeout, Upload,localStorageService) {
	// Kosongin semua localstorage pas pertama di load

        localStorage.clear();
		$scope.resetSongs = function(){
              localStorageService.remove('songsComposer');
              $timeout(function () {
                  $scope.tampilTabSongs = false;
              });
              $("#songsTable .eusiSongs").remove();
            }
			
    $scope.uploadFiles = function(file, errFiles){
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: 'Composer/upload',
                data: {file: file} // attribut name file dengan value file
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
                $scope.photo = response.data;
                console.log(response);
            }, function (response) {
                if (response.status > 0)
                   $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * 
                evt.loaded / evt.total));
            });
        }   
    }
	$scope.delete_photo = function(filename){
        console.log(filename);
        
        $http.post("Composer/deletePhoto",{filename:filename}).success(function(data,status,headers,config){
            if(data==1){
                $scope.photo = "";
                $("#photo").hide();
            }
        }).error(function(err){
            console.log(err);
        });
    }
        
        $http.get("Composer/data_single/"+$routeParams.var1).success(function(result){
            // Foto
            $scope.photo = (result[0]['photo']==undefined)? '' : result[0]['photo'];

            // Pilihan gender
            $scope.genders = [{
                name: 'Male',
                value: 'male'
            },{
                name: 'Female',
                value: 'female'
            }];
            $scope.gender = result[0]['gender'].toLowerCase(); // Selected gender

            // Pilihan appellation
            $scope.appellations = [{
                name: 'Mr.',
                value: 0
            },{
                name: 'Ms.',
                value: 1
            },{
                name: 'Mrs.',
                value: 2
            }];
            $scope.appellation = parseInt(result[0]['appellation']); // Selected appellation

            // Pilihan country
            $http.get("Composer/getCountry").success(function(resDataCountry){
                $scope.allCountry = resDataCountry;
            }).error(function(){
                $scope.allCountry = {};
            });
            $scope.country = result[0]['country']; // Selected country

            // Get state
            $http.get("Composer/getState").success(function(resDataState){
                $scope.allState = resDataState;
            }).error(function(){
                $scope.allState = {};
            });
            $scope.state = result[0]['state']; // Selected state

            // Pilihan Status
            $scope.statuses = [{
                name: 'Active',
                value: 1
            },{
                name: 'Deactive',
                value: 0
            }];
            $scope.address_status = parseInt(result[0]['address_status']); // Selected address_status
            $scope.status = parseInt(result[0]['status']); // Selected status    
			
			// Song
			$http.get("Composer/getSong/"+$routeParams.var1).success(function(res){
				$scope.songss = res;
				
				//console.log(res.length);
				
				for(var i=0;i<res.length;i++){
					var datana = JSON.parse(localStorageService.get('songsComposer')) || [];
					// add to it,
					datana.push({id: res[0].id_songs, title: res[0].title, type: res[0].type});
					// then put it back.
					localStorageService.set('songsComposer', JSON.stringify(datana));
				}
			});
			

            $scope.name  = result[0]['name'];
            $scope.first_title = result[0]['first_title'];
            $scope.last_title = result[0]['last_title'];
            $scope.street_address = result[0]['street_address'];
            $scope.city = result[0]['city'];
            $scope.website = result[0]['website'];
            $scope.email = result[0]['email'];
            $scope.phone = result[0]['phone'];
            $scope.social_media = result[0]['social_media'];
            $scope.place_of_birth = result[0]['place_of_birth'];
            $scope.date_of_birth = result[0]['date_of_birth'];
            $scope.notes = result[0]['notes'];
        });

        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.update_data=function(){
			var songsComposer = JSON.parse(localStorageService.get('songsComposer')) || [];
            console.log($scope.date_of_birth);
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("Composer/update_data",{
    			'id_composer_and_arranger':$routeParams.var1,
    			'name':$scope.name,
    			'gender':$scope.gender,
    			'appellation':$scope.appellation,
    			'first_title':$scope.first_title,
    			'last_title':$scope.last_title,
    			'city':$scope.city,
    			'state':$scope.state,
    			'country':$scope.country,
    			'address_status':$scope.address_status,
    			'phone':$scope.phone,
    			'email':$scope.email,
    			'social_media':$scope.social_media,
    			'website':$scope.website,
    			'place_of_birth':$scope.place_of_birth,
    			'date_of_birth':$scope.date_of_birth,
    			'notes':$scope.notes,
                'photo':$scope.photo,
    			'status':$scope.status,
				'songsComposer':songsComposer,
    			'street_address':$scope.street_address
			}).success(function(data,status,headers,config){
                // console.log(data);
                //Beritahu jika data sudah berhasil di input
				localStorageService.remove('songsComposer');
				$timeout(function () {
                  $scope.tampilTabSongs = false;
              });
				$("#songsTable .eusiSongs").remove();
                alert("Update Success!");
                $location.path("Composer");
            });
        }

  }
  
  function PilihSongsComposer($scope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams){
      // Inisialisasi
      var songsComposer = JSON.parse(localStorageService.get('songsComposer')) || [];
      var no=songsComposer.length+1;
      var data = [];

        $scope.songsData = {
              id_songs: []
            };
			
			// ambil semua songs berdasarkan composer
        $http.get("Composer/getAllSongsOn/"+$routeParams.var1).success(function(resSongs){
            for (var i=0; i < resSongs.length; i++) {
                // Buat di ceklis kalo udah ada di database
                $scope.songsData.id_songs.push(resSongs[i]['id_songs']);
            };
        });
    
        $http.get("Composer/getAllSongsComposer").success(function(result){
          $scope.loading = false;
          data=result;
          // console.log(data);
          $scope.data = data;

          /* jshint newcap: false */
          $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,
            sorting: {
              firstname: 'asc'     // initial sorting
            }
          }, {
            filterDelay: 50,
            total: data.length, // length of data
            getData: function ($defer, params) {
              var searchStr = params.filter().search;
              var mydata = [];

              if (searchStr) {
                searchStr = searchStr.toLowerCase();
                mydata = data.filter(function (item) {
                  return item.id_songs.toLowerCase().indexOf(searchStr) > -1 || item.title.toLowerCase().indexOf(searchStr) > -1;
                });

              } else {
                mydata = data;
              }

              mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
              $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
      });
  
      $scope.pilih = function(){
		  
        //console.log($scope.songsData.id_songs.length);
          // Remove dulu
		  localStorageService.remove('songsComposer');
          $timeout(function () {
              $scope.tampilTabSongs = false;
          });
          $("#songsTable .eusiSongs").remove();
		  
          
          no=1;
          if($scope.songsData.id_songs.length!=0){
            for(var i=0;i<$scope.songsData.id_songs.length;i++){
              $http.get("Composer/getOneSongsComposer/"+$scope.songsData.id_songs[i]).success(function(data,status,headers,config){
                console.log(data);
                // Nyimpen ke localstorage 
                // retrieve it (Or create a blank array if there isn't any info saved yet),
                var datana = JSON.parse(localStorageService.get('songsComposer')) || [];
                // add to it,
                datana.push({id: data[0].id_songs, title: data[0].title, type: data[0].type});
                // then put it back.
                localStorageService.set('songsComposer', JSON.stringify(datana));
                // append data tabel
                $("#songsTable tr:last").after("<tr class='eusiSongs'><td>"+no+"</td><td>"+data[0].title+"<input type='hidden' title='id_songs' value='"+data[0].id_songs+"'></td><td>"+data[0].type+"</td></tr>");
                no++;
              }).error(function(err){
                console.log(err);
              });
            }
          }
      }
      
  }

})();

