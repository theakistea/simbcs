(function () {
    'use strict';

    angular
        .module('material-lite')
        .controller('UpdateDataEventPackageB', ['$scope', '$http', '$location', '$routeParams', '$timeout', 'Upload', 'dataService', UpdateDataEventPackageB])
        .service('dataService', function () {
            // private variable
            var _dataObj = {};

            // public API
            this.dataObj = _dataObj;
        }).directive('numformat', ['$filter', function ($filter) {
            return {
                require: '?ngModel',
                link: function (scope, elem, attrs, ctrl) {
                    if (!ctrl) return;


                    ctrl.$formatters.unshift(function (a) {
                        return $filter(attrs.numformat)(ctrl.$modelValue)
                    });


                    ctrl.$parsers.unshift(function (viewValue) {
                        var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                        elem.val($filter(attrs.numformat)(plainNumber));
                        return plainNumber;
                    });
                }
            };
        }]);
    ;

    function UpdateDataEventPackageB($scope, $http, $location, $routeParams, $timeout, Upload, dataService) {

        $scope.total_participant;
        $scope.nom;
        $scope.conductor;
        $scope.officials;
        $scope.price_per_person;
        $scope.sub_total = 0;

        $scope.cout_total_participant = function () {
            $scope.total_participant = Number($scope.num_of_singer ? $scope.num_of_singer : 0) + Number($scope.nom ? $scope.nom : 0) + Number($scope.conductor ? $scope.conductor : 0) + Number($scope.officials ? $scope.officials : 0);
            $scope.sub_total = Number($scope.total_participant ? $scope.total_participant : 0) * Number($scope.price_per_person ? $scope.price_per_person : 0)
        };
        $http.get("EventPackageB/data_single/" + $routeParams.var1).success(function (result) {
            //  

            dataService.dataObj = {
                member: result,
                ukuran: result
            };
            //console.log(result);
            // remove heula
            
            $("#tshirt .eusiMember").remove();
            // add deui
            for (var i = 1; i <= result.length; i++) {
                console.log(result[i - 1])
                $("#tshirt tr:last").after("<tr class='eusiMember'><td>" + i + "</td><td>" + result[i - 1].member + "</td><td><input name='ukuran' ng-model='ukuran' type='text' value='" + result[i - 1].ukuran + "' id='ukuran" + i + "'></td></tr>");
            }
            $(".top").remove();
            
            //console.log(dataService.dataObj.member);
            //console.log(dataService.dataObj.ukuran);
            // Pilihan Status
            $scope.statuses = [{
                name: 'Active',
                value: 1
            }, {
                    name: 'Deactive',
                    value: 0
                }];
            console.log($scope.title);
            $scope.title = result[0]['title'];
            $scope.choir_name = result[0]['choir_name'];
            $scope.num_of_singer = result[0]['num_of_singer'];
            $scope.nom = result[0]['num_of_musician'];
            $scope.conductor = result[0]['num_of_conductor'];
            $scope.officials = result[0]['num_of_official'];
            $scope.sub_total = result[0]['sub_total'];
            $scope.name = result[0]['name'];
            $scope.check_in = result[0]['check_in'];
            $scope.check_out = result[0]['check_out'];


            $scope.time_in = new Date("October 13, 2014 " + result[0]['time_in'] + ":00");
            $scope.time_out = new Date("October 13, 2014 " + result[0]['time_out'] + ":00");


            $scope.staying_address = result[0]['staying_address'];
            $scope.single_room = result[0]['single_room'];
            $scope.single_room_person = result[0]['single_room_person'];
            $scope.two_bedded_room = result[0]['two_bedded_room'];
            $scope.two_bedded_room_person = result[0]['two_bedded_room_person'];
            $scope.three_bedded_room = result[0]['three_bedded_room'];
            $scope.three_bedded_room_person = result[0]['three_bedded_room_person'];
            $scope.four_bedded_room = result[0]['four_bedded_room'];
            $scope.four_bedded_room_person = result[0]['four_bedded_room_person'];
            $scope.los_information = result[0]['los_information'];
            $scope.vegetarian_food = result[0]['vegetarian_food'];
            $scope.hallal_food = result[0]['hallal_food'];
            $scope.first_title = result[0]['first_title'];
            $scope.last_title = result[0]['last_title'];
            $scope.address = result[0]['address'];
            $scope.contact_person = result[0]['contact_person'];
            $scope.price_per_person = result[0]['price_per_person'];
            $scope.flight_detail = result[0]['flight_detail'];
            $scope.bus_detail = result[0]['bus_detail'];
            $scope.email = result[0]['email'];
            $scope.phone = result[0]['phone'];
            $scope.notes = result[0]['notes'];

            $scope.cout_total_participant();
        });

        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.update_data = function () {
            var ukuranna = [];
            for (var i = 1; i <= (dataService.dataObj.member).length; i++) {
                //console.log($("#ukuran"+i).val());
                ukuranna.push($("#ukuran" + i).val());
            }

            console.log("Notes", $scope.notes)
            console.log(ukuranna);
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            var data = {
                'id_pd_event_package_b': $routeParams.var1,
                'num_of_musician': $scope.nom,
                'num_of_conductor': $scope.conductor,
                'num_of_official': $scope.officials,
                'flight_detail': $scope.flight_detail,
                'bus_detail': $scope.bus_detail,
                'check_in': $scope.check_in,
                'check_out': $scope.check_out,
                'time_out': moment($scope.time_out).format("HH:mm"),
                'time_in': moment($scope.time_in).format("HH:mm"),
                'single_room': $scope.single_room,
                'single_room_person': $scope.single_room_person,
                'two_bedded_room': $scope.two_bedded_room,
                'two_bedded_room_person': $scope.two_bedded_room_person,
                'three_bedded_room': $scope.three_bedded_room,
                'three_bedded_room_person': $scope.three_bedded_room_person,
                'four_bedded_room': $scope.four_bedded_room,
                'four_bedded_room_person': $scope.four_bedded_room_person,
                'hallal_food': $scope.hallal_food,
                'vegetarian_food': $scope.vegetarian_food,
                'los_information': $scope.los_information,
                'price_per_person': $scope.price_per_person,
                'sub_total': $scope.sub_total,
                'phone': $scope.phone,
                'contact_person': $scope.contact_person,
                'email': $scope.email,
                'address': $scope.address,
                'notes': $scope.notes,
                'ukuran': ukuranna,
                'member': dataService.dataObj.member,
                'staying_address': $scope.staying_address
            };
            console.log(data);

            $http.post("EventPackageB/update_data", data).success(function (data, status, headers, config) {
                //Beritahu jika data sudah berhasil di input
                alert("Update Success!");

                $location.path("EventPackageB");
            });
        }

    }

})();