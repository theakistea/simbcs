(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataLetterController', ['$scope', '$http', '$location', '$routeParams', '$timeout', 'Upload', UpdateDataLetterController]);

  function UpdateDataLetterController($scope, $http, $location, $routeParams, $timeout, Upload) {
	  
	$scope.editorOptions = {
                language: 'id'
               // uiColor: '#000000'
            };
            $scope.$on("ckeditor.ready", function( event ) {
                $scope.isReady = true;
            });
      
        
        $http.get("Letter/data_single/"+$routeParams.var1).success(function(result){
           	console.log(result[0]);
            $scope.template_name = result[0]['template_name'];
            $scope.konten = result[0]['konten'];
            $scope.type = result[0]['type'];
        }).error(function(err){
        	console.log(err);
        });

        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.update_data=function(){
            console.log($scope.photo);
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("Letter/update_data",{
    			'id_letter':$routeParams.var1,
				'template_name':$scope.template_name,
				'konten':$scope.konten,
				'type'	: $scope.type
			}).success(function(data,status,headers,config){
                //Beritahu jika data sudah berhasil di input
                alert("Update Success!");

                $location.path("Letter");
            });
        }

  }

})();