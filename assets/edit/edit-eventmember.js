(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataEventMemberController', ['$scope', '$http', '$location', '$routeParams', '$timeout', 'Upload','localStorageService', UpdateDataEventMemberController])
	.controller('PilihMemEvent', ['$scope', '$http', '$timeout','localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihMemEvent]);

  function UpdateDataEventMemberController($scope, $http, $location, $routeParams, $timeout, Upload,localStorageService) {
	// Kosongin semua localstorage pas pertama di load
	localStorage.clear();
		$scope.resetMember = function(){
          localStorageService.remove('dataMember');
          $timeout(function () {
              $scope.tampilTabMem = false;
          });
          $("#memberTable .eusiMember").remove();
        }
		$http.get("EventMember/data_single/"+$routeParams.var1+"/"+$routeParams.var2).success(function(result){
			// Member
			$http.get("EventMember/getdetailMem/"+$routeParams.var1+"/"+$routeParams.var2).success(function(res){
				$scope.mems = res;
				console.log(res);
				for(var i=0;i<res.length;i++){
					var datana = JSON.parse(localStorageService.get('dataMember')) || [];
					console.log(datana);
					// add to it,
					datana.push({id: res[i].id_member, name: res[i].name});
					// then put it back.
					localStorageService.set('dataMember', JSON.stringify(datana));
				}
			});
			$scope.id_trs_choir_event  = result[0]['id_trs_choir_event'];
			$scope.id_trs_choir  = result[0]['id_trs_choir'];
		});
        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.update_data=function(){
			var dataMember = JSON.parse(localStorageService.get('dataMember')) || [];
			$http.post("EventMember/update_data",{
				  'id_trs_choir_event':$scope.id_trs_choir_event,
				  'id_trs_choir':$scope.id_trs_choir,
				  'dataMember':dataMember
			}).success(function(data,status,headers,config){
                localStorageService.remove('dataMember');
                localStorageService.clear;
                //Beritahu jika data sudah berhasil di input
                alert("Update Success!");
                $location.path("EventMember");
            });
        }
  }
  
  function PilihMemEvent($scope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams){
    // Inisialisasi
      var dataMember = JSON.parse(localStorageService.get('dataMember')) || [];
      var no_mem=dataMember.length+1;
      var data = [];

        // inisialisasi data
        $scope.memData = {
          id_member: []
        };

        // ambil semua kategori berdasarkan choir detail
        $http.get("EventMember/getAllMemOn/"+$routeParams.var1+"/"+$routeParams.var2).success(function(resMem){
            for (var i=0; i < resMem.length; i++) {
                // Buat di ceklis kalo udah ada di database
                $scope.memData.id_member.push(resMem[i]['id_member']);
            };
        });
    
        $http.get("EventMember/getAllMemChoirSelected/"+$routeParams.var1).success(function(result){
          $scope.loading = false;
          data=result;
          // console.log(data);
          $scope.data = data;

          /* jshint newcap: false */
          $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,
            sorting: {
              firstname: 'asc'     // initial sorting
            }
          }, {
            filterDelay: 50,
            total: data.length, // length of data
            getData: function ($defer, params) {
              var searchStr = params.filter().search;
              var mydata = [];

              if (searchStr) {
                searchStr = searchStr.toLowerCase();
                mydata = data.filter(function (item) {
                  return item.id_member.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
                });

              } else {
                mydata = data;
              }

              mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
              $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
      });
  
      $scope.pilih = function(){
        console.log($scope.memData.id_member.length);
          // Remove dulu
          localStorageService.remove('dataMember');
          $timeout(function () {
              $scope.tampilTabMem = false;
          });
          $("#memberTable .eusiMember").remove();
          no_mem=1;
          if($scope.memData.id_member.length!=0){
            for(var i=0;i<$scope.memData.id_member.length;i++){
              $http.get("ChoirDetail/getOneMember/"+$scope.memData.id_member[i]).success(function(data,status,headers,config){
                console.log(data);
                // Nyimpen ke localstorage 
                // retrieve it (Or create a blank array if there isn't any info saved yet),
                var categories = JSON.parse(localStorageService.get('dataMember')) || [];
                // add to it,
                categories.push({id: data[0].id_member, name: data[0].name, type: data[0].type});
                // then put it back.
                localStorageService.set('dataMember', JSON.stringify(categories));
                // append data tabel
                $("#memberTable tr:last").after("<tr class='eusiMember'><td>"+no_mem+"</td><td>"+data[0].name+"<input type='hidden' name='id_member' value='"+data[0].id_member+"'></td></tr>");
                no_mem++;
              }).error(function(err){
                console.log(err);
              });
            }
          }
      }
  }

})();

