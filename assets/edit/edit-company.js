(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataCompanyController', ['$scope', '$http', '$location', '$routeParams', UpdateDataCompanyController]);

  function UpdateDataCompanyController($scope, $http, $location, $routeParams) {
        
        $http.get("Company/getCompanyData").success(function(result){
            $scope.id_company_data = result['id_company_data'];
            $scope.phone_number  = result['phone_number'];
            $scope.fax_number  = result['fax_number'];
            $scope.mailing_address = result['mailing_address'];
            $scope.street_address = result['street_address'];
            $scope.email_company = result['email_company'];
            $scope.contact_person = result['contact_person'];
            $scope.phone_cp = result['phone_cp'];
            $scope.email_cp = result['email_cp'];
            $scope.notes = result['notes'];
        });

        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.update_data=function(){
        
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("Company/update_data",{
			'id_company_data':$scope.id_company_data,
			'phone_number':$scope.phone_number,
			'fax_number':$scope.fax_number,
			'contact_person':$scope.contact_person,
			'email_company':$scope.email_company,
			'email_cp':$scope.email_cp,
			'phone_cp':$scope.phone_cp,
			'notes':$scope.notes,
			'mailing_address':$scope.mailing_address,
			'street_address':$scope.street_address
			}).success(function(data,status,headers,config){
                //Beritahu jika data sudah berhasil di input
                alert("Update Success!");
                $location.path("Company");
            });
        }

  }

})();
