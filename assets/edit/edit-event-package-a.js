(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataEventPackageA', ['$scope', '$http', '$location', '$routeParams', '$timeout', 'Upload', 'dataService', UpdateDataEventPackageA])
	.service('dataService', function() {
      // private variable
      var _dataObj = {};

      // public API
      this.dataObj = _dataObj;
    })
	;

  function UpdateDataEventPackageA($scope, $http, $location, $routeParams, $timeout, Upload, dataService) {      

       
        $scope.total_participant;
        $scope.nom;
        $scope.conductor;
        $scope.officials;
        $scope.price_per_person;
        $scope.sub_total = 0;

        $scope.cout_total_participant = function () {
            $scope.total_participant = Number($scope.num_of_singer ? $scope.num_of_singer : 0) + Number($scope.nom ? $scope.nom : 0) + Number($scope.conductor ? $scope.conductor : 0) + Number($scope.officials ? $scope.officials : 0);
            $scope.sub_total = Number($scope.total_participant ? $scope.total_participant :  0) * Number($scope.price_per_person ? $scope.price_per_person : 0)
        };
        $http.get("EventPackageA/data_single/"+$routeParams.var1).success(function(result){
        //  

		  dataService.dataObj = {
            member : result,
			ukuran : result
          };
		  //console.log(result);
		  // remove heula
          // add deui
          
            
        
            
            //console.log(dataService.dataObj.member);
            //console.log(dataService.dataObj.ukuran);
			// Pilihan Status
            $scope.statuses = [{
                name: 'Active',
                value: 1
            },{
                name: 'Deactive',
                value: 0
            }];

            $scope.title  = result[0]['title'];
            $scope.choir_name  = result[0]['choir_name'];
            $scope.num_of_singer  = result[0]['num_of_singer'];
            $scope.nom  = result[0]['num_of_musician'];
            $scope.conductor  = result[0]['num_of_conductor'];
            $scope.officials  = result[0]['num_of_official'];
            $scope.sub_total  = result[0]['sub_total'];
            $scope.due_date  = result[0]['due_date'];
            $scope.name  = result[0]['name'];
            $scope.first_title = result[0]['first_title'];
            $scope.last_title = result[0]['last_title'];
            $scope.staying_address = result[0]['staying_address'];
            $scope.address = result[0]['address'];
            $scope.contact_person = result[0]['contact_person'];
            $scope.price_per_person = result[0]['price_per_person'];
            $scope.flight_detail = result[0]['flight_detail'];
            $scope.email = result[0]['email'];
            $scope.phone = result[0]['phone'];
            $scope.notes = result[0]['notes'];
            $scope.cout_total_participant();

            $("#tshirt .eusiMember").remove();
            
            var data = [];
            for(var i=1;i<=result.length;i++){
                $("#tshirt tr:last").after("<tr class='eusiMember'><td>"+i+"</td><td>"+result[i-1].member+"</td><td><input name='ukuran' ng-model='ukuran' type='text' value='"+result[i-1].ukuran+"' id='ukuran"+i+"'></td></tr>");
            }
            self.tableParams = new NgTableParams({}, { dataset: data});
            $(".top").remove();
            
        });

        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.update_data=function(){
			var ukuranna = [];
			for(var i=1;i<=(dataService.dataObj.member).length;i++){
              //console.log($("#ukuran"+i).val());
			  ukuranna.push($("#ukuran"+i).val());
            }
			console.log(ukuranna);
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("EventPackageA/update_data",{
    			'id_pd_event_package_a':$routeParams.var1,
    			'num_of_musician':$scope.nom,
    			'num_of_conductor':$scope.conductor,
    			'num_of_official':$scope.officials,
    			'flight_detail':$scope.flight_detail,
    			'price_per_person':$scope.price_per_person,
    			'sub_total':$scope.sub_total,
    			'due_date':$scope.due_date,
    			'phone':$scope.phone,
    			'contact_person':$scope.contact_person,
    			'email':$scope.email,
    			'address':$scope.address,
    			'notes':$scope.notes,
    			'ukuran':ukuranna,
    			'member':dataService.dataObj.member,
    			'staying_address':$scope.staying_address
			}).success(function(data,status,headers,config){
                //Beritahu jika data sudah berhasil di input
                alert("Update Success!");

                $location.path("EventPackageA");
            });
        }

  }

})();