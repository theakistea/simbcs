(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataSongsController', ['$rootScope', '$scope', '$http', '$location', '$routeParams', '$timeout', 'Upload' , 'localStorageService', UpdateDataSongsController]);

  function UpdateDataSongsController($rootScope ,$scope, $http, $location, $routeParams, $timeout, Upload, localStorageService) {
    $scope.balok = "none";
    $scope.angka = "none";

    $scope.uploadFileBalok = function(file, errFiles){
          $scope.f = file;
          $scope.errFile = errFiles && errFiles[0];
          if (file) {
              file.upload = Upload.upload({
                  url: 'songs/uploadBalok',
                  data: {fileBalok: file} // attribut name file dengan value file
              });

              file.upload.then(function (response) {
                  $timeout(function () {
                      file.result = response.data;
                  });
                  $scope.balok = response.data;
                  console.log(response);
              }, function (response) {
                  if (response.status > 0)
                     $scope.errorMsg = response.status + ': ' + response.data;
              }, function (evt) {
                  file.progress = Math.min(100, parseInt(100.0 * 
                  evt.loaded / evt.total));
              });
          }   
      }

      $scope.uploadFileAngka = function(file, errFiles){
          $scope.f = file;
          $scope.errFile = errFiles && errFiles[0];
          if (file) {
              file.upload = Upload.upload({
                  url: 'songs/uploadAngka',
                  data: {fileAngka: file} // attribut name file dengan value file
              });

              file.upload.then(function (response) {
                  $timeout(function () {
                      file.result = response.data;
                  });
                  $scope.angka = response.data;
                  console.log(response);
              }, function (response) {
                  if (response.status > 0)
                     $scope.errorMsg = response.status + ': ' + response.data;
              }, function (evt) {
                  file.progress = Math.min(100, parseInt(100.0 * 
                  evt.loaded / evt.total));
              });
          }   
      }
        
      
    $rootScope.$on('updateSelectData', function (event, args) {
      console.log("updated")
      $scope.dataCategory = [];
      $scope.dataCategory = JSON.parse(localStorageService.get('dataCategory')) || [];

      console.log($scope.dataCategory)
     
    });
    
        $http.get("songs/data_single/"+$routeParams.var1).success(function(result){
            // file
// Song
			$http.get("songs/getCategory/"+$routeParams.var1).success(function(res){
				$scope.dataCategory = res;
				
				//console.log(res.length);
				localStorageService.set('dataCategory','[]')
				for(var i=0;i<res.length;i++){
					var datana = JSON.parse(localStorageService.get('dataCategory')) || [];
					// add to it,
					datana.push({id_songs_category: res[i].id_songs_category, title_category: res[i].title_category});
					// then put it back.
					localStorageService.set('dataCategory', JSON.stringify(datana));
				}
                console.log(JSON.parse(localStorageService.get('dataCategory')) || []);
			});


            $scope.balok = (result[0]['file_not_balok']==undefined)? 'none' : result[0]['file_not_balok'];
            $scope.angka = (result[0]['file_not_angka']==undefined)? 'none' : result[0]['file_not_angka'];

            // Pilihan accompanied type
            $scope.accompanied = [{
                name: 'Yes',
                value: 'YES'
            },{
                name: 'No',
                value: 'NO'
            }];
            $scope.accompanied_type = result[0]['accompanied_status']; // Selected appellation

            // Pilihan cat
            $http.get("songs/getCat").success(function(resDataCat){
                $scope.allCategory = resDataCat;
            }).error(function(){
                $scope.allCategory = {};
            });
            $scope.id_songs_category = result[0]['id_songs_category']; // Selected id_songs_category

            // Pilihan Status
            $scope.statuses = [{
                name: 'Active',
                value: 1
            },{
                name: 'Deactive',
                value: 0
            }];
            $scope.status = parseInt(result[0]['status']); // Selected status      

            $scope.title  = result[0]['title'];
            $scope.composer = result[0]['composer'];
            $scope.publisher = result[0]['publisher'];
            $scope.accompanied_value = result[0]['accompanied_value'];
            $scope.notes = result[0]['notes'];
            $scope.year_start = result[0]['year_start'];
            $scope.year_end = result[0]['year_end'];
        });

        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.update_data=function(){
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("songs/update_data",{
    			'id_songs':$routeParams.var1,
                'title' :$scope.title,
                'type' : $scope.type,
                'year_start' : $scope.year_start,
                'year_end' : $scope.year_end,
                'composer' : $scope.composer,
                'publisher' : $scope.publisher,
                'accompanied_status' : $scope.accompanied_status,
                'accompanied_value' : $scope.accompanied_value,
                'file_not_balok' : $scope.balok,
                'file_not_angka' : $scope.angka,
                'status' : $scope.status,
                'notes' : $scope.notes,
                'data_category' : $scope.dataCategory
			}).success(function(data,status,headers,config){
                //Beritahu jika data sudah berhasil di input
                alert("Update Success!");

                $location.path("songs");
            });
        }

  }

})();