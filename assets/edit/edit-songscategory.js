(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataSongsCategoryController', ['$scope', '$http', '$location', '$routeParams', UpdateDataSongsCategoryController]);

  function UpdateDataSongsCategoryController($scope, $http, $location, $routeParams) {
        
        $http.get("SongsCategory/data_single/"+$routeParams.var1).success(function(result){
            // Pilihan Status
            $scope.statuses = [{
                name: 'Active',
                value: 1
            },{
                name: 'Deactive',
                value: 0
            }];
            $scope.status = parseInt(result[0]['status']); // Selected status  

            $scope.id_songs_category  = result[0]['id_songs_category'];
            $scope.title_category  = result[0]['title_category'];
            $scope.notes = result[0]['notes'];
        });

        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.update_data=function(){
        
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("SongsCategory/update_data",{
			'id_songs_category':$routeParams.var1,
			'title_category':$scope.title_category,
			'notes':$scope.notes
			}).success(function(data,status,headers,config){
                //Beritahu jika data sudah berhasil di input
                alert("Update Success!");
                $location.path("SongsCategory");
            });
        }

  }

})();
