(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataEventController', ['$scope', '$http', '$location', '$routeParams', UpdateDataEventController]);

  function UpdateDataEventController($scope, $http, $location, $routeParams) {
        
        $http.get("Event/data_single/"+$routeParams.var1).success(function(result){
            // Pilihan Status
            $scope.statuses = [{
                name: 'Active',
                value: 1
            },{
                name: 'Deactive',
                value: 0
            }];
            $scope.status = parseInt(result[0]['status']); // Selected status 
            
            $scope.id_event_grade  = result[0]['id_event_grade'];
            $scope.name  = result[0]['name'];
            $scope.notes = result[0]['notes'];
        });

        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.update_data=function(){    
        
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("Event/update_data",{
    			'id_event_grade':$routeParams.var1,
    			'name':$scope.name,
    			'notes':$scope.notes,
                'status':$scope.status
			}).success(function(data,status,headers,config){
                //Beritahu jika data sudah berhasil di input
                alert("Update Success!");
                $location.path("Event");
            });
        }

  }

})();
