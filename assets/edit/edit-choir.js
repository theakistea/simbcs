(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataChoirController', ['$scope', '$http', '$location', '$routeParams', '$timeout', UpdateDataCandjController]);

  function UpdateDataCandjController($scope, $http, $location, $routeParams, $timeout) {
        
        $http.get("Choir/data_single/"+$routeParams.var1).success(function(result){
            console.log(result[0])

            // Pilihan gender
            $scope.with_amplifications = [{
                name: 'YES',
                value: 'yes'
            },{
                name: 'NO',
                value: 'no'
            }];
            $scope.with_amplification = result[0]['with_amplification']; // Selected amplification
			
			$scope.types = [{
                name: 'Championship',
                value: 'Championship'
            },{
                name: 'Competition',
                value: 'Competition'
            },{
                name: 'Evaluation Performance',
                value: 'Evaluation Performance'
            },{
                name: 'Choir Clinic',
                value: 'Choir Clinic'
            },{
                name: 'Friendship Concert',
                value: 'Friendship Concert'
            }];

            $scope.type = result[0]['type']; // Selected type
			// Pilihan Status
            $scope.statuses = [{
                name: 'Active',
                value: 1
            },{
                name: 'Deactive',
                value: 0
            }];
            $scope.status = parseInt(result[0]['status']); // Selected status  
			
            $scope.name  = result[0]['name'];
            $scope.age = result[0]['age'];
            $scope.min_singer = result[0]['min_singer'];
            $scope.max_perform_time = result[0]['max_perform_time'];
            $scope.with_amplification = result[0]['with_amplification'];
            $scope.num_of_song = result[0]['num_of_song'];
            $scope.num_of_acapela_song = result[0]['num_of_acapela_song'];
            $scope.notes = result[0]['notes'];
            $scope.code_category = result[0]['code_category'];
        });

        // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
        $scope.update_data=function(){;
            //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
            //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
            $http.post("Choir/update_data",{
    			'id_choir_category':$routeParams.var1,
    			'name':$scope.name,	
    			'type':$scope.type,
    			'age':$scope.age,
    			'min_singer':$scope.min_singer,
    			'max_perform_time':$scope.max_perform_time,
    			'num_of_song':$scope.num_of_song,
    			'num_of_acapela_song':$scope.num_of_acapela_song,
    			'with_amplification':$scope.with_amplification,
    			'code_category':$scope.code_category,
    			'notes':$scope.notes,
    			'status':$scope.status
			}).success(function(data,status,headers,config){
                //Beritahu jika data sudah berhasil di input
                alert("Update Success!");

                $location.path("Choir");
            });
        }

  }

})();