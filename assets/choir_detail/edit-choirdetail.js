(function () {
  'use strict';

  angular
    .module('material-lite')
    .controller('UpdateDataChoirDetailController', ['$scope', '$rootScope', '$http', '$location', '$filter', '$routeParams', '$timeout', 'Upload', 'localStorageService', UpdateDataChoirDetailController])

    ;

  function UpdateDataChoirDetailController($scope, $rootScope, $http, $location,$filter, $routeParams,  $timeout, Upload, localStorageService) {
    // Kosongin semua localstorage pas pertama di load
    localStorage.clear();

    $scope.codeRegistrasi = []
    $scope.isiKategori = []
    $scope.dataMember = []
    $scope.dataKonduktor = []
    $scope.dataManager = []
    $scope.kategoriChoir = [];
    $scope.dataSongs = [];

    $scope.tmpSongDetail = [];
    // reset table pake tombol
   
    $scope.resetCat = function () {
      localStorageService.remove('kategoriChoir');
      $timeout(function () {
        $scope.tampilTabCat = false;
      });
      $scope.isiKategori = []
      $("#kategoriTable .eusi").remove();
    }
    $scope.resetMember = function () {
      localStorageService.remove('dataMember');
      $timeout(function () {
        $scope.tampilTabMem = false;
      });
      $scope.dataMember = []
      $("#memberTable .eusiMember").remove();
    }
    $scope.resetKonduktor = function () {
      localStorageService.remove('dataKonduktor');
      $timeout(function () {
        $scope.tampilTabKon = false;
      });

      $scope.dataKonduktor = []
      $("#conductorTable .eusiKonduktor").remove();
    }
    $scope.resetManager = function () {
      localStorageService.remove('dataManager');
      $timeout(function () {
        $scope.tampilTabMan = false;
      });

      $scope.dataManager = []
      $("#managerTable .eusiManager").remove();
    }
    var state = {
      katInt: false,
      conInt: false,
      memInt: false,
      manInt: false
    }
    $scope.initMember = function (jsonString) {
      if (!state.memInt) {
        $scope.dataMember = []
        $scope.dataMember = JSON.parse(jsonString)
        state.memInt = true
        localStorageService.remove('dataMember');
        localStorageService.set('dataMember', (jsonString));
      }
    }

    $scope.initManager = function (jsonString) {
      if (!state.manInt) {
        $scope.dataManager = []
        $scope.dataManager = JSON.parse(jsonString)
        state.manInt = true
        localStorageService.remove('dataManager');
        localStorageService.set('dataManager', (jsonString));
      }
    }


    $rootScope.$on('updateSelectData', function (event, args) {
      console.log("updated")
      $scope.kategoriChoir = [];
      $scope.dataMember = [];
      $scope.dataKonduktor = [];
      $scope.dataManager = [];
      $scope.dataSongs = [];

      
      $scope.kategoriChoir = JSON.parse(localStorageService.get('kategoriChoir')) || [];
      $scope.dataMember = JSON.parse(localStorageService.get('dataMember')) || [];
      $scope.dataKonduktor = JSON.parse(localStorageService.get('dataKonduktor')) || [];
      $scope.dataManager = JSON.parse(localStorageService.get('dataManager')) || [];
      $scope.dataSongs = JSON.parse(localStorageService.get('dataSongs')) || [];
      
      for(var  i = 0; i < $scope.kategoriChoir.length ; i ++){
        for(var  j = 0; j < $scope.codeRegistrasi.length ; j ++){
          if($scope.kategoriChoir[i].id_choir_category == $scope.codeRegistrasi[j].id){
            console.log("Huhah")
            $scope.kategoriChoir[i].code_registrasi= $scope.codeRegistrasi[j].codeRegistrasi
          }
        }
      }

      for(var  i = 0; i < $scope.dataSongs.length ; i ++){
        for(var  j = 0; j < $scope.tmpSongDetail.length ; j ++){
          if($scope.dataSongs[i].id_songs == $scope.tmpSongDetail[j].id){
           $scope.dataSongs[i].iringan = $scope.tmpSongDetail[j].iringan
           $scope.dataSongs[i].jumlah_partitur_diterima = $scope.tmpSongDetail[j].jumlah_partitur_diterima
           $scope.dataSongs[i].surati_izin_komposer = $scope.tmpSongDetail[j].surati_izin_komposer
           $scope.dataSongs[i].durasi = $scope.tmpSongDetail[j].durasi
           $scope.dataSongs[i].persetujuan_artistic_commite = $scope.tmpSongDetail[j].persetujuan_artistic_commite
          }
        }
      }
      
    });


    $scope.getEventCode = function() {
      var found = $filter('filter')($scope.events, {id_trs_choir_event: $scope.choir_event}, true);
      $scope.code_event = found[0].code_event + ( found[0].date_start != 'null' ? found[0].date_start : '' )
      console.log($scope.code_event)
    }


    $scope.updateCodeRegistrasi =  function( ){
      $scope.codeRegistrasi = []
      for(var  i = 0; i < $scope.kategoriChoir.length; i ++){
        $scope.codeRegistrasi.push({id:$scope.kategoriChoir[i].id_choir_category , codeRegistrasi : $scope.kategoriChoir[i].code_registrasi})
      }
    };
    

    $scope.updateSongDetail =  function( ){
      $scope.tmpSongDetail = []
      console.log($scope.dataSongs)
      for(var  i = 0; i < $scope.dataSongs.length; i ++){
        $scope.tmpSongDetail.push({
          id:$scope.dataSongs[i].id_songs ,
           iringan : $scope.dataSongs[i].iringan,
           jumlah_partitur_diterima : $scope.dataSongs[i].jumlah_partitur_diterima,
           surati_izin_komposer : $scope.dataSongs[i].surati_izin_komposer,
           durasi : $scope.dataSongs[i].durasi,
           persetujuan_artistic_commite : $scope.dataSongs[i].persetujuan_artistic_commite,
          })
      }

      console.log(JSON.stringify($scope.tmpSongDetail))
    };

    $scope.uploading = false
    $scope.progress;

    $scope.uploadFiles = function (file, errFiles) {
      $scope.f = file;
      $scope.errFile = errFiles && errFiles[0];
      if (file) {
        $scope.uploading = true
        file.upload = Upload.upload({
          url: 'ChoirDetail/upload',
          data: { file: file } // attribut name file dengan value file
        });

        file.upload.then(function (response) {
         file.result = response.data;
         $scope.uploading = false
         $scope.photo = response.data;
         console.log(response);
          
        }, function (response) {
          if (response.status > 0)
            $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
          file.progress = Math.min(100, parseInt(100.0 *
            evt.loaded / evt.total));
            $scope.progress = file.progress
            console.log(file.progress)
        });
      }
    }
    $scope.delete_photo = function (filename) {
      console.log(filename);

      $http.post("ChoirDetail/deletePhoto", { filename: filename }).success(function (data, status, headers, config) {
        if (data == 1) {
          $scope.photo = "";
          $("#photo").hide();
        }
      }).error(function (err) {
        console.log(err);
      });
    }



    $http.get("ChoirDetail/data_single/" + $routeParams.var1).success(function (result) {
       $http.get("ChoirEventDetail/data_dropdown").success(function (data, status, headers, config) {
        $scope.events = data
      }).error(function (err) {
      });
      $http.get("ChoirDetail/getAllCatOn/" + $routeParams.var1).success(function (data, status, headers, config) {
          $scope.kategoriChoir = []
          $scope.kategoriChoir = data
          localStorageService.remove('kategoriChoir');
          localStorageService.set('kategoriChoir', JSON.stringify (data));
          
      }).error(function (err) {
      });
      $http.get("ChoirDetail/getAllConOn/" + $routeParams.var1).success(function (data, status, headers, config) {
            $scope.dataKonduktor = []
            $scope.dataKonduktor = data
            localStorageService.remove('dataKonduktor');
            localStorageService.set('dataKonduktor', JSON.stringify(data));
          
      }).error(function (err) {
      });

      $http.get("ChoirDetail/getAllMemOn/" + $routeParams.var1).success(function (data, status, headers, config) {
            $scope.dataMember = []
            $scope.dataMember = data
            localStorageService.remove('dataMember');
            localStorageService.set('dataMember', JSON.stringify(data));
          
      }).error(function (err) {
      });

      $http.get("ChoirDetail/getAllManOn/" + $routeParams.var1).success(function (data, status, headers, config) {
            $scope.dataManager = []
            $scope.dataManager = data
            localStorageService.remove('dataManager');
            localStorageService.set('dataManager', JSON.stringify(data));
          
      }).error(function (err) {
      });
      $http.get("ChoirDetail/getAllSongsOn/" + $routeParams.var1).success(function (data, status, headers, config) {
            $scope.dataSongs = []
            $scope.dataSongs = data
            localStorageService.remove('dataSongs');
            localStorageService.set('dataSongs', JSON.stringify(data));
          
      }).error(function (err) {
      });

      
      // Foto
      $scope.photo = (result[0]['photo'] == undefined) ? '' : result[0]['photo'];

      // Pilihan country
      $http.get("ChoirDetail/getCountry").success(function (resDataCountry) {
        $scope.allCountry = resDataCountry;
      }).error(function () {
        $scope.allCountry = {};
      });
      $scope.country = result[0]['country']; // Selected country

      // Get state
      $http.get("ChoirDetail/getState").success(function (resDataState) {
        $scope.allState = resDataState;
      }).error(function () {
        $scope.allState = {};
      });
      $scope.state = result[0]['state']; // Selected state

      // Pilihan Status
      $scope.statuses = [{
        name: 'Active',
        value: 1
      }, {
          name: 'Deactive',
          value: 0
        }];
      $scope.address_status = parseInt(result[0]['address_status']); // Selected address_status
      $scope.status = parseInt(result[0]['status']); // Selected status      

      $scope.id_trs_choir = result[0]['id_trs_choir'];
      $scope.choir_name = result[0]['choir_name'];
      $scope.institution = result[0]['institution'];
      $scope.street_address = result[0]['street_address'];
      $scope.city = result[0]['city'];
      $scope.website = result[0]['website'];
      $scope.email = result[0]['email'];
      $scope.phone = result[0]['phone'];
      $scope.social_media = result[0]['social_media'];
      $scope.place_of_birth = result[0]['place_of_birth'];
      $scope.date_of_birth = result[0]['birth'];
      $scope.status = result[0]['status'];
      $scope.date_modify = result[0]['date_modify'];
      $scope.notes = result[0]['notes'];
      $scope.choir_event = result[0]['choir_event'];
      $scope.has_surat_keterangan_usia = result[0]['has_surat_keterangan_usia'];
      $scope.has_bukti_registrasi = result[0]['has_surat_keterangan_usia'];
      $scope.has_audio_record = result[0]['has_surat_keterangan_usia'];
      $scope.has_biograpy = result[0]['has_biograpy'];
      $scope.has_surat_keterangan_usia_date = result[0]['has_surat_keterangan_usia_date'];
      $scope.has_bukti_registrasi_date = result[0]['has_bukti_registrasi_date'];
      $scope.has_audio_record_date = result[0]['has_audio_record_date'];
      $scope.has_biograpy_date = result[0]['has_biograpy_date'];
      $scope.code_event = result[0]['code_event'] +  result[0]['start_date'] ;
      
    });

    // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
    $scope.update_data = function () {
      var kategoriChoir = $scope.kategoriChoir
      var dataMember = JSON.parse(localStorageService.get('dataMember')) || [];
      var dataKonduktor = JSON.parse(localStorageService.get('dataKonduktor')) || [];
      var dataManager = JSON.parse(localStorageService.get('dataManager')) || [];
      var dataSongs = $scope.dataSongs
      
      //data akan dikirim ke controller angular/insert_data , data yang dikirim berupa json.
      //contoh => $scope.nim => nim merupakan nama ng-model pada inputan NIM di form. $scope.nim akan mengambil data nim dari inputan.
      console.log(dataMember)
      var data =  {
        'id_trs_choir': $scope.id_trs_choir,
        'choir_name': $scope.choir_name,
        'institution': $scope.institution,
        'street_address': $scope.street_address,
        'country': $scope.country,
        'state': $scope.state,
        'city': $scope.city,
        'email': $scope.email,
        'notes': $scope.notes,
        'photo' : $scope.photo,

        'has_surat_keterangan_usia': $scope.has_surat_keterangan_usia,
        'has_bukti_registrasi': $scope.has_bukti_registrasi,
        'has_audio_record': $scope.has_audio_record,
        'has_biograpy': $scope.has_biograpy,
        'katChoir': typeof kategoriChoir == 'string' ? JSON.parse(kategoriChoir) : kategoriChoir,
        'dataMem': typeof dataMember == 'string' ? JSON.parse(dataMember) : dataMember,
        'dataKon': typeof dataKonduktor == 'string' ? JSON.parse(dataKonduktor) : dataKonduktor,
        'dataMan': typeof dataManager == 'string' ? JSON.parse(dataManager) : dataManager,
        'dataSongs': typeof dataSongs == 'string' ? JSON.parse(dataSongs) : dataSongs,
        'id_trs_choir_event' :  $scope.choir_event
      }
      console.log(data)
      $http.post("ChoirDetail/update_data", data).success(function (data, status, headers, config) {
        localStorageService.remove('kategoriChoir');
        localStorageService.remove('dataMember');
        localStorageService.remove('dataKonduktor');
        localStorageService.remove('dataManager');
        localStorageService.remove('dataSongs');
        //Beritahu jika data sudah berhasil di input
        alert("Update Success!");

        $location.path("ChoirDetail");
      }).error(function (data) {
        alert(data)
      });
    }

  }

})();