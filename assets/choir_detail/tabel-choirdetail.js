(function() {
  'use strict';

  angular
    .module('material-lite')
    .controller('TablesDataChoirDetailController', ['$scope', '$http', 'ngTableParams', '$filter', TablesDataChoirDetailController]);

  function TablesDataChoirDetailController($scope, $http, ngTableParams, $filter, $location, $routeParams) {
    $scope.loading = true;

    var data = [];

    $scope.choirDetailData = {
          id_trs_choir: []
        };

    $scope.change_status = function(){
      console.log("Change status");
      console.log($scope.choirDetailData.id_trs_choir);
      // console.log($scope.candjData.id_candj.length);

      $http.post("ChoirDetail/aktif",$scope.choirDetailData.id_trs_choir).success(function(res){
        console.log(res);
         alert("Status changed!");
        datana();
      }).error(function(err){
        console.log(err);
      });
    }

    datana();
  
    function datana(){
      $http.get("ChoirDetail/data_angularnya").success(function(result){
          $scope.loading = false;
          data=result;
          console.log(data);
          $scope.data = data;

          /* jshint newcap: false */
          $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,
            sorting: {
              choir_name: 'asc'     // initial sorting
            }
          }, {
            filterDelay: 50,
            total: data.length, // length of data
            getData: function ($defer, params) {
              var searchStr = params.filter().search;
              var mydata = [];

              if (searchStr) {
                searchStr = searchStr.toLowerCase();
                mydata = data.filter(function (item) {
                  return item.id_trs_choir.toLowerCase().indexOf(searchStr) > -1 || item.choir_name.toLowerCase().indexOf(searchStr) > -1;
                });

              } else {
                mydata = data;
              }

              mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
              $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
      });
    };

  }

})();
