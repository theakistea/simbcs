(function () {
  'use strict';

  angular.module('material-lite')
    // controller utama
    .controller('InsertDataChoirDetailController', ['$scope', '$rootScope', '$http', '$location', '$timeout', 'localStorageService', '$filter' ,  'Upload', InsertDataChoirDetailController])

    // controller tambah
    .controller('InsertCatChoir', ['$scope', '$rootScope', '$http', '$timeout', 'localStorageService', InsertCatChoir])
    .controller('InsertMemberChoir', ['$scope', '$rootScope', '$http', '$timeout', 'localStorageService', InsertMemberChoir])

    // controller pilih
    .controller('PilihCatChoir', ['$scope', '$rootScope', '$http', '$timeout', 'localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihCatChoir])
    .controller('PilihMemChoir', ['$scope', '$rootScope', '$http', '$timeout', 'localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihMemChoir])
    
    ;

  function InsertDataChoirDetailController($scope, $rootScope, $http, $location, $timeout, localStorageService,$filter, Upload) {
    $scope.photo = 'default.jpg';
    $scope.codeRegistrasi=[];
    $scope.kategoriChoir = [];
    $scope.dataMember = [];
    $scope.dataKonduktor = [];
    $scope.dataManager = [];
    $scope.dataSongs = [];
    $scope.code_event;
    $scope.events = []
    
    $scope.tmpSongDetail = [];

    $http.get("ChoirEventDetail/data_dropdown").success(function (data, status, headers, config) {
      $scope.events = data
      console.log(data)
    }).error(function (err) {
      console.log(err);
    });

    $scope.uploadFiles = function (file, errFiles) {
      $scope.f = file;
      $scope.errFile = errFiles && errFiles[0];
      if (file) {
        file.upload = Upload.upload({
          url: 'ChoirDetail/upload',
          data: { file: file } // attribut name file dengan value file
        });

        file.upload.then(function (response) {
          $timeout(function () {
            file.result = response.data;
          });
          $scope.photo = response.data;
          console.log(response);
        }, function (response) {
          if (response.status > 0)
            $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
          file.progress = Math.min(100, parseInt(100.0 *
            evt.loaded / evt.total));
        });
      }
    }
    $rootScope.$on('updateSelectData', function (event, args) {
      console.log("updated")
      $scope.kategoriChoir = [];
      $scope.dataMember = [];
      $scope.dataKonduktor = [];
      $scope.dataManager = [];
      $scope.dataSongs = [];
      $scope.kategoriChoir = JSON.parse(localStorageService.get('kategoriChoir')) || [];
      $scope.dataMember = JSON.parse(localStorageService.get('dataMember')) || [];
      $scope.dataKonduktor = JSON.parse(localStorageService.get('dataKonduktor')) || [];
      $scope.dataManager = JSON.parse(localStorageService.get('dataManager')) || [];
      $scope.dataSongs = JSON.parse(localStorageService.get('dataSongs')) || [];

      $scope.trs_choir = JSON.parse(localStorageService.get('dataChoir')) || [];
      $scope.choir_name = $scope.trs_choir.choir_name
      $scope.institution = $scope.trs_choir.institution
      $scope.street_address = $scope.trs_choir.street_address
      $scope.country = $scope.trs_choir.country
      $scope.state = $scope.trs_choir.state
      $scope.city = $scope.trs_choir.city
      $scope.email = $scope.trs_choir.email
      $scope.notes = $scope.trs_choir.notes

      $('input').parent().addClass('is-dirty');

      $('textarea').parent().addClass('is-dirty');


      console.log($scope.dataSongs)
      for(var  i = 0; i < $scope.kategoriChoir.length ; i ++){
        for(var  j = 0; j < $scope.codeRegistrasi.length ; j ++){
          if($scope.kategoriChoir[i].id_choir_category == $scope.codeRegistrasi[j].id){
            console.log("Huhah")
            $scope.kategoriChoir[i].code_registrasi= $scope.codeRegistrasi[j].codeRegistrasi
          }
        }
      }
      
      for(var  i = 0; i < $scope.dataSongs.length ; i ++){
        for(var  j = 0; j < $scope.tmpSongDetail.length ; j ++){
          if($scope.dataSongs[i].id_songs == $scope.tmpSongDetail[j].id){
           $scope.dataSongs[i].iringan = $scope.tmpSongDetail[j].iringan
           $scope.dataSongs[i].jumlah_partitur_diterima = $scope.tmpSongDetail[j].jumlah_partitur_diterima
           $scope.dataSongs[i].surati_izin_komposer = $scope.tmpSongDetail[j].surati_izin_komposer
           $scope.dataSongs[i].durasi = $scope.tmpSongDetail[j].durasi
           $scope.dataSongs[i].persetujuan_artistic_commite = $scope.tmpSongDetail[j].persetujuan_artistic_commite
          }
        }
      }
      
      console.log(JSON.stringify($scope.dataSongs))
    });
    
    $scope.getEventCode = function() {
      var found = $filter('filter')($scope.events, {id_trs_choir_event: $scope.choir_event}, true);
      $scope.code_event = found[0].code_event + ( found[0].date_start != 'null' ? found[0].date_start : '' )
      console.log($scope.code_event)
    }


    $scope.updateSongDetail =  function( ){
      $scope.tmpSongDetail = []
      console.log($scope.dataSongs)
      for(var  i = 0; i < $scope.dataSongs.length; i ++){
        $scope.tmpSongDetail.push({
          id:$scope.dataSongs[i].id_songs ,
           iringan : $scope.dataSongs[i].iringan,
           jumlah_partitur_diterima : $scope.dataSongs[i].jumlah_partitur_diterima,
           surati_izin_komposer : $scope.dataSongs[i].surati_izin_komposer,
           durasi : $scope.dataSongs[i].durasi,
           persetujuan_artistic_commite : $scope.dataSongs[i].persetujuan_artistic_commite,
          })
      }

      console.log(JSON.stringify($scope.tmpSongDetail))
    };

    $scope.updateCodeRegistrasi =  function( ){
      $scope.codeRegistrasi = []
      for(var  i = 0; i < $scope.kategoriChoir.length; i ++){
        console.log($scope.kategoriChoir[i].code_registrasi )
        $scope.codeRegistrasi.push({id:$scope.kategoriChoir[i].id_choir_category , codeRegistrasi : $scope.kategoriChoir[i].code_registrasi})
      }
    };

    // $scope.insert_data => insert_data merupakan nama dari ng-click pada tombol submit
    $scope.insert_data = function () {
      // Inisialisasi
      var kategoriChoir = $scope.kategoriChoir
      var dataMember = JSON.parse(localStorageService.get('dataMember')) || [];
      var dataKonduktor = JSON.parse(localStorageService.get('dataKonduktor')) || [];
      var dataManager = JSON.parse(localStorageService.get('dataManager')) || [];
      var dataSongs = $scope.dataSongs
      $scope.kategoriChoir = kategoriChoir

      $http.post("ChoirDetail/insert_data", {
        'choir_name': $scope.choir_name,
        'institution': $scope.institution,
        'street_address': $scope.street_address? $scope.street_address: "",
        'country': $scope.country,
        'state': $scope.state,
        'city': $scope.city,
        'email': $scope.email,
        'notes': $scope.notes,
        'has_surat_keterangan_usia': $scope.has_surat_keterangan_usia,
        'has_bukti_registrasi': $scope.has_bukti_registrasi,
        'has_audio_record': $scope.has_audio_record,
        'has_biograpy': $scope.has_biograpy,
        'has_surat_keterangan_usia_date': $scope.has_surat_keterangan_usia_date,
        'has_bukti_registrasi_date': $scope.has_bukti_registrasi_date,
        'has_audio_record_date': $scope.has_audio_record_date,
        'has_biograpy_date': $scope.has_biograpy_date,
        'katChoir': kategoriChoir,
        'dataMem': dataMember,
        'dataKon': dataKonduktor,
        'dataMan': dataManager,
        'dataSongs':dataSongs ,
        'id_trs_choir_event' :  $scope.choir_event
      }).success(function (data, status, headers, config) {

        localStorageService.remove('kategoriChoir');
        localStorageService.remove('dataMember');
        localStorageService.remove('dataKonduktor');
        localStorageService.remove('dataManager');

        alert("Data Berhasil Di Input");
        $location.path("ChoirDetail");
      }).error(function (err) {
        console.log(err);
        alert(err);
      });
      //     $http.post("ChoirDetail/insert_data",{
      // //'id_candj':$scope.id_candj,
      // 'choir_name':$scope.choir_name,
      // 'street_address':$scope.street_address,
      // 'notes':$scope.notes,
      // 'photo':$scope.photo,
      // 'email':$scope.email
      // }).success(function(data,status,headers,config){
      //         //Beritahu jika data sudah berhasil di input
      //         alert("Data Berhasil Di Input");
      //         $location.path("ChoirDetail");
      //     });
    }

    /* ngpus 1
    $scope.hapus = function(){
      var students = JSON.parse(localStorageService.get('kategoriChoir'));

      students.splice(0, 1);
      localStorageService.set('kategoriChoir', JSON.stringify(students));
      console.log(students);

      if(students.length==0){
        localStorageService.remove('kategoriChoir');
      }
      // Apend
      $scope.append = function(){
        // $scope.addNewStudent('Ferdhika','xxi',18);
        // retrieve it (Or create a blank array if there isn't any info saved yet),
        var students = JSON.parse(localStorageService.get('kategoriChoir')) || [];
        // add to it,
        students.push({name: "fer", roll: "adaw", age: "19"});
        // then put it back.
        localStorageService.set('kategoriChoir', JSON.stringify(students));

        console.log(JSON.parse(localStorageService.get('kategoriChoir')));
      }
    }*/

    // Kosongin localstorage pas pertama di load
    localStorage.clear();

    // Reset tabel
    $scope.resetCat = function () {
      localStorageService.remove('kategoriChoir');
      $timeout(function () {
        $scope.tampilTabCat = false;
      });
      $scope.kategoriChoir = []
      $("#kategoriTable .eusi").remove();
    }

    $scope.resetSongs = function () {
      localStorageService.remove('dataSongs');
      $timeout(function () {
        $scope.tampilTabSong = false;
      });
      $scope.songs = []
      $("#songsTable .eusi").remove();
    }

    
    $scope.resetMember = function () {
      localStorageService.remove('dataMember');
      $timeout(function () {
        $scope.tampilTabMem = false;
      });
      $scope.dataMember = []
      $("#memberTable .eusiMember").remove();
    }
    $scope.resetKonduktor = function () {
      localStorageService.remove('dataKonduktor');
      $timeout(function () {
        $scope.tampilTabKon = false;
      });

      $scope.dataKonduktor = []
      $("#conductorTable .eusiKonduktor").remove();
    }
    $scope.resetManager = function () {
      localStorageService.remove('dataManager');
      $timeout(function () {
        $scope.tampilTabMan = false;
      });

      $scope.dataManager = []
      $("#managerTable .eusiManager").remove();
    }

  }

  // Pilih cat choir controller
  function PilihCatChoir($scope, $rootScope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams) {
    // Inisialisasi
    var data = [];

    $scope.catData = {
      id_choir_category: []
    };



    $http.get("choirDetail/getAllCatChoir").success(function (result) {
      var kategoriChoir = JSON.parse(localStorageService.get('kategoriChoir')) || [];
      var id_choir_category = []
      for(var i = 0; i < kategoriChoir.length; i ++){
        id_choir_category.push(kategoriChoir[i].id_choir_category)

      }
      $scope.catData.id_choir_category = id_choir_category


      data = result;
      // console.log(data);
      $scope.data = data;

      /* jshint newcap: false */
      $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10,
        sorting: {
          firstname: 'asc'     // initial sorting
        }
      }, {
          filterDelay: 50,
          total: data.length, // length of data
          getData: function ($defer, params) {
            var searchStr = params.filter().search;
            var mydata = [];

            if (searchStr) {
              searchStr = searchStr.toLowerCase();
              mydata = data.filter(function (item) {
                return item.id_choir_category.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
              });

            } else {
              mydata = data;
            }

            mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
            $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
    });

    $scope.pilih = function () {

      // Remove dulu
      localStorageService.remove('kategoriChoir');
      $timeout(function () {
        $scope.tampilTabCat = false;
      });
      if ($scope.catData.id_choir_category.length != 0) {
        // terus add
        for (var i = 0; i < $scope.catData.id_choir_category.length; i++) {
          $http.get("ChoirDetail/getOneCatChoir/" + $scope.catData.id_choir_category[i]).success(function (data, status, headers, config) {
            // Nyimpen ke localstorage 
            // retrieve it (Or create a blank array if there isn't any info saved yet),
            var categories = JSON.parse(localStorageService.get('kategoriChoir')) || [];
            // add to it,
            categories.push({ code_category  : data[0].code_category,  id: data[0].id_choir_category , id_choir_category: data[0].id_choir_category, name: data[0].name, type: data[0].type  , type_prefix:data[0].type_prefix});
            // then put it back.
            localStorageService.set('kategoriChoir', JSON.stringify(categories));
            // append data tabel
            $rootScope.$emit('updateSelectData', []);
          }).error(function (err) {
            console.log(err);
          });
        }
      }
    }

  }






  // Kategori
  function InsertCatChoir($scope, $rootScope, $http, $timeout, localStorageService) {
    // Inisialisasi
    $scope.kategori = {};
    var kategoriChoir = JSON.parse(localStorageService.get('kategoriChoir')) || [];
    var no = kategoriChoir.length + 1;

    $timeout(function () {
      $scope.tampilTabCat = true;
    });

    $scope.saveCat = function () {
      console.log($scope.kategori);

      // Post
      $http.post("choir/insert_data", {
        'name': $scope.kategori.nameCat,
        'type': $scope.kategori.typeCat,
        'age': $scope.kategori.ageCat,
        'min_singer': $scope.kategori.minsingCat,
        'max_perform_time': $scope.kategori.maxperfCat,
        'num_of_song': $scope.kategori.numsongCat,
        'num_of_acapela_song': $scope.kategori.numacapCat,
        'with_amplification': $scope.kategori.ampliCat,
        'notes': $scope.kategori.notesCat
      }).success(function (data, status, headers, config) {
        //console.log("last id : " +data);
        //console.log("no : " +no);

        // Nyimpen ke localstorage 
        // retrieve it (Or create a blank array if there isn't any info saved yet),
        var categories = JSON.parse(localStorageService.get('kategoriChoir')) || [];
        // add to it,
        categories.push({ code_category  : data[0].code_category,  id: data, id_choir_category: data, name: $scope.kategori.nameCat, type: $scope.kategori.typeCat  , type_prefix:data[0].type_prefix});
        // then put it back.
        localStorageService.set('kategoriChoir', JSON.stringify(categories));

        //Beritahu jika data sudah berhasil di input
        // alert("Category successfully added.");

        // Append data
        // <td><button ng-click='removeItemCat("+parseInt(no-1)+")'>Remove</button></td>
        no++;

        // Set bekas nya kosongin
        $scope.kategori = {};
        $('#modalKategori').modal('hide');
        $rootScope.$emit('updateSelectData', []);
      }).error(function (err) {
        console.log(err);
      });

    }
  }

  // Pilih member choir
  function PilihMemChoir($scope, $rootScope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams) {
    // Inisialisasi
    var dataMember = JSON.parse(localStorageService.get('dataMember')) || [];
    var no_mem = dataMember.length + 1;
    var data = [];

    $scope.memData = {
      id_member: []
    };

    $http.get("choirDetail/getAllMemChoir").success(function (result) {
      var dataMember = JSON.parse(localStorageService.get('dataMember')) || [];
      var id_member = []
      for(var i = 0; i < dataMember.length; i ++){
        id_member.push(dataMember[i].id_member)

      }
      $scope.memData.id_member = id_member

      $scope.loading = false;
      data = result;
      // console.log(data);
      $scope.data = data;

      /* jshint newcap: false */
      $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10,
        sorting: {
          firstname: 'asc'     // initial sorting
        }
      }, {
          filterDelay: 50,
          total: data.length, // length of data
          getData: function ($defer, params) {
            var searchStr = params.filter().search;
            var mydata = [];

            if (searchStr) {
              searchStr = searchStr.toLowerCase();
              mydata = data.filter(function (item) {
                return item.id_member.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
              });

            } else {
              mydata = data;
            }

            mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
            $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
    });

    $scope.pilih = function () {
      console.log($scope.memData.id_member.length);
      // Remove dulu
      localStorageService.remove('dataMember');
      $timeout(function () {
        $scope.tampilTabMem = false;
      });
      no_mem = 1;
      if ($scope.memData.id_member.length != 0) {
        for (var i = 0; i < $scope.memData.id_member.length; i++) {
          $http.get("ChoirDetail/getOneMember/" + $scope.memData.id_member[i]).success(function (data, status, headers, config) {
            console.log(data);
            // Nyimpen ke localstorage 
            // retrieve it (Or create a blank array if there isn't any info saved yet),
            var categories = JSON.parse(localStorageService.get('dataMember')) || [];
            // add to it,
            categories.push({ id: data[0].id_member, name: data[0].name, type: data[0].type });
            // then put it back.
            console.log(categories)
            localStorageService.set('dataMember', JSON.stringify(categories));
            // append data tabel
            no_mem++;

            $rootScope.$emit('updateSelectData', []);
          }).error(function (err) {
            console.log(err);
          });
        }
      }
    }
  }

  // Member
  function InsertMemberChoir($scope, $rootScope, $http, $timeout, localStorageService) {



    // Inisialisasi
    $scope.member = {};
    var memberChoir = JSON.parse(localStorageService.get('memberChoir')) || [];
    var no = memberChoir.length + 1;

    $timeout(function () {
      $scope.tampilTabMem = true;
    });

    // Pilihan country
    $http.get("ChoirDetail/getCountry").success(function (resDataCountry) {
      $scope.allCountry = resDataCountry;
    }).error(function () {
      $scope.allCountry = {};
    });
    $scope.member.country = {}; // Selected country

    // Get state
    $http.get("ChoirDetail/getState").success(function (resDataState) {
      $scope.allState = resDataState;
    }).error(function () {
      $scope.allState = {};
    });



    $scope.member.state = {}; // Selected state

    $scope.saveCat = function () {

      // Post
      $http.post("Member/insert_data", {

        'name': $scope.member.name,
        'institution': $scope.member.institution,
        'gender': $scope.member.gender,
        'appellation': $scope.member.appellation,
        'first_title': $scope.member.first_title,
        'last_title': $scope.member.last_title,
        'street_address': $scope.member.street_address,
        'country': $scope.member.country,
        'state': $scope.member.state,
        'city': $scope.member.city,
        'phone': $scope.member.phone,
        'email': $scope.member.email,
        'social_media': $scope.member.social_media,
        'website': $scope.member.website,
        'place_of_birth': $scope.member.place_of_birth,
        'date_of_birth': $scope.member.date_of_birth,
        'notes': $scope.member.notes ? $scope.member.notes : "",
        'photo': $scope.member.photo ? $scope.member.photo : "default.jpg"
      }).success(function (data, status, headers, config) {
        console.log(data)
        // Nyimpen ke localstorage 
        // retrieve it (Or create a blank array if there isn't any info saved yet),
        var categories = JSON.parse(localStorageService.get('dataMember')) || [];
        // add to it,
        categories.push({ id: data, name: $scope.member.name });
        // then put it back.
        localStorageService.set('dataMember', JSON.stringify(categories));
        console.log(categories)
        //Beritahu jika data sudah berhasil di input
        // alert("Category successfully added.");

        // Append data
        // <td><button ng-click='removeItemCat("+parseInt(no-1)+")'>Remove</button></td>

        // Set bekas nya kosongin
        $scope.member = {};
        $('#modalMember').modal('hide');
        $rootScope.$emit('updateSelectData', []);
      }).error(function (err) {
        console.log(err);
      });

    }
  }


  // Pilih konduktor choir
  function PilihKonChoir($scope, $rootScope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams) {
    // Inisialisasi
    var dataKonduktor = JSON.parse(localStorageService.get('dataKonduktor')) || [];
    var no_kon = dataKonduktor.length + 1;
    var data = [];

    $scope.konData = {
      id_conductor: []
    };

    $http.get("choirDetail/getAllKonChoir").success(function (result) {
      var dataKonduktor = JSON.parse(localStorageService.get('dataKonduktor')) || [];
      var id_conductor = []
      for(var i = 0; i < dataKonduktor.length; i ++){
        id_conductor.push(dataKonduktor[i].id_conductor)

      }
      $scope.konData.id_conductor = id_conductor

      $scope.loading = false;
      data = result;
      $scope.data = data;

      /* jshint newcap: false */
      $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10,
        sorting: {
          firstname: 'asc'     // initial sorting
        }
      }, {
          filterDelay: 50,
          total: data.length, // length of data
          getData: function ($defer, params) {
            var searchStr = params.filter().search;
            var mydata = [];

            if (searchStr) {
              searchStr = searchStr.toLowerCase();
              mydata = data.filter(function (item) {
                return item.id_conductor.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
              });

            } else {
              mydata = data;
            }

            mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
            $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
    });

    $scope.pilih = function () {
      console.log($scope.konData.id_conductor.length);
      // Remove dulu
      localStorageService.remove('dataKonduktor');
      $timeout(function () {
        $scope.tampilTabCon = false;
      });
      no_kon = 1;
      if ($scope.konData.id_conductor.length != 0) {
        for (var i = 0; i < $scope.konData.id_conductor.length; i++) {
          $http.get("ChoirDetail/getOneKonduktor/" + $scope.konData.id_conductor[i]).success(function (data, status, headers, config) {
            console.log(data);
            // Nyimpen ke localstorage 
            // retrieve it (Or create a blank array if there isn't any info saved yet),
            var datana = JSON.parse(localStorageService.get('dataKonduktor')) || [];
            // add to it,
            datana.push({ id: data[0].id_conductor, name: data[0].name, gender: data[0].gender });
            // then put it back.
            localStorageService.set('dataKonduktor', JSON.stringify(datana));
            // append data tabel
            no_kon++;
            $rootScope.$emit('updateSelectData', []);
          }).error(function (err) {
            console.log(err);
          });
        }
      }
    }
  }



})();
