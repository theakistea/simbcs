(function () {
'use strict';
angular.module('material-lite').controller('PilihManChoir', ['$scope', '$rootScope', '$http', '$timeout', 'localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihManChoir])

    

  // controller pilih konduktor choir
  function PilihManChoir($scope, $rootScope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams) {
    // Inisialisasi
    var dataManager = JSON.parse(localStorageService.get('dataManager')) || [];
    var no_man = dataManager.length + 1;
    var data = [];
    $scope.countrys  = []
    $scope.states  = []
    $scope.managerData = {photo : ''}
    $scope.show="list";
    $scope.manData = {
      id_manager: []
    };
    var getCountry = function () {
            $http.get("master/getCountry").success(function (result) {
            $scope.countrys  = result
        })
    }
    var getState = function () {
            $http.get("master/getState").success(function (result) {
            $scope.states  = result
        })
    }


    $scope.changeForm = function  (value) {
        $scope.show = value;
    }
    var dataManager = JSON.parse(localStorageService.get('dataManager')) || [];
    var id_manager = []
    for(var i = 0; i < dataManager.length; i ++){
        id_manager.push(dataManager[i].id_manager)

    }
    $scope.manData.id_manager = id_manager




    var getList = function () {
        $http.get("choirDetail/getAllManChoir").success(function (result) {
            
            $scope.loading = false;
            data = result;
            // console.log(data);
            $scope.data = data;

            /* jshint newcap: false */
            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 10,
                sorting: {
                firstname: 'asc'     // initial sorting
                }
            }, {
                filterDelay: 50,
                total: data.length, // length of data
                getData: function ($defer, params) {
                    var searchStr = params.filter().search;
                    var mydata = [];

                    if (searchStr) {
                    searchStr = searchStr.toLowerCase();
                    mydata = data.filter(function (item) {
                        return item.id_manager.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
                    });

                    } else {
                    mydata = data;
                    }

                    mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
                    $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
                });
            });
    }

    getList ();
    getCountry ();
    getState()
    
    $scope.pilih = function () {
      // console.log($scope.manData.id_manager.length);
      // Remove dulu
      localStorageService.remove('dataManager');
      $timeout(function () {
        $scope.tampilTabMan = false;
      });
      no_man = 1;
      if ($scope.manData.id_manager.length != 0) {
        for (var i = 0; i < $scope.manData.id_manager.length; i++) {
          $http.get("ChoirDetail/getOneManager/" + $scope.manData.id_manager[i]).success(function (data, status, headers, config) {
            // console.log(data);
            // Nyimpen ke localstorage 
            // retrieve it (Or create a blank array if there isn't any info saved yet),
            var datana = JSON.parse(localStorageService.get('dataManager')) || [];
            // add to it,
            datana.push({ id: data[0].id_manager, name: data[0].name, gender: data[0].gender });
            // then put it back.
            localStorageService.set('dataManager', JSON.stringify(datana));
            // append data tabel
            no_man++;

            $rootScope.$emit('updateSelectData', []);
          }).error(function (err) {
            console.log(err);
          });
        }
      }
    }

    $scope.saveData = function () {
            $scope.managerData.photo = 'default.jpg'
             $http.post("Manager/insert_data",
                $scope.managerData
             ).success(function(data,status,headers,config){
                getList();
                var datana = JSON.parse(localStorageService.get('dataManager')) || [];
                // add to it,
                datana.push({ id: data, name: $scope.managerData.name, gender: $scope.managerData.gender });
                // then put it back.
                localStorageService.set('dataManager', JSON.stringify(datana));
                // append data tabel
                $scope.manData.id_manager.push(data)
                $rootScope.$emit('updateSelectData', []);
                $scope.show = 'list'

                $scope.managerData = {}
                //Beritahu jika data sudah berhasil di input
                alert("Data Berhasil Di Input");
            }).error(function(err){
            	console.log(err);
            });
    }
  }

})()