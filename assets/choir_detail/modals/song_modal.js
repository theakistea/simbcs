(function () {
'use strict';
angular.module('material-lite').controller('PilihSongs', ['$scope', '$rootScope', '$http', '$timeout', 'localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihSongs])
  
  // controller pilih Songs choir
  function PilihSongs($scope, $rootScope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams) {
    // Inisialisasi
    var dataSongs = JSON.parse(localStorageService.get('dataSongs')) || [];
    var no_man = dataSongs.length + 1;
    var data = [];

    $scope.dataSongs = {
      id_songs: []
    };

    $http.get("choirDetail/getAllSongs").success(function (result) {
      var dataSongs = JSON.parse(localStorageService.get('dataSongs')) || [];
      var id_songs = []
      for(var i = 0; i < dataSongs.length; i ++){
        id_songs.push(dataSongs[i].id_songs)

      }
      console.log(dataSongs)
      $scope.dataSongs.id_songs = id_songs
      $scope.loading = false;
      data = result;
      // console.log(data);
      $scope.data = data;

      /* jshint newcap: false */
      $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10,
        sorting: {
          firstname: 'asc'     // initial sorting
        }
      }, {
          filterDelay: 50,
          total: data.length, // length of data
          getData: function ($defer, params) {
            var searchStr = params.filter().search;
            var mydata = [];

            if (searchStr) {
              searchStr = searchStr.toLowerCase();
              mydata = data.filter(function (item) {
                return item.id_songs.toLowerCase().indexOf(searchStr) > -1 || item.title.toLowerCase().indexOf(searchStr) > -1;
              });

            } else {
              mydata = data;
            }

            mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
            $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
    });

    $scope.pilih = function () {
      // console.log($scope.manData.id_manager.length);
      // Remove dulu
      localStorageService.remove('dataSongs');
      $timeout(function () {
        $scope.tampilTabMan = false;
      });
      no_man = 1;
      if ($scope.dataSongs.id_songs.length != 0) {
        for (var i = 0; i < $scope.dataSongs.id_songs.length; i++) {
          $http.get("ChoirDetail/getOneSong/" + $scope.dataSongs.id_songs[i]).success(function (data, status, headers, config) {
            // console.log(data);
            // Nyimpen ke localstorage 
            // retrieve it (Or create a blank array if there isn't any info saved yet),
            var datana = JSON.parse(localStorageService.get('dataSongs')) || [];
            // add to it,
            datana.push({ id_songs: data[0].id_songs, title: data[0].title });
            // then put it back.
            localStorageService.set('dataSongs', JSON.stringify(datana));
            // append data tabel
            no_man++;

            $rootScope.$emit('updateSelectData', []);
          }).error(function (err) {
            console.log(err);
          });
        }
      }
    }
  }
})()