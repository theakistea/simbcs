(function () {
    'use strict';
    angular.module('material-lite').controller('LattestChoirModal', ['$scope', '$rootScope', '$http', '$timeout', 'localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', LattestChoirModal])
    // Pilih konduktor choir
    function LattestChoirModal($scope, $rootScope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams) {
        // Inisialisasi
        
        var data = [];
        $scope.selected_id
        $scope.event = {
            name: []
        };
        
        var getList = function ()  {
            $http.get("choirDetail/getAllLattestChoir").success(function (result) {
                console.log(result)

                $scope.loading = false;
                data = result;
                $scope.data = data;

                /* jshint newcap: false */
                $scope.tableParams = new ngTableParams({
                    page: 1,            // show first page
                    count: 10,
                    sorting: {
                        firstname: 'asc'     // initial sorting
                    }
                }, {
                        filterDelay: 50,
                        total: data.length, // length of data
                        getData: function ($defer, params) {
                            var searchStr = params.filter().search;
                            var mydata = [];

                            if (searchStr) {
                                searchStr = searchStr.toLowerCase();
                                mydata = data.filter(function (item) {
                                    return item.choir_name.toLowerCase().indexOf(searchStr) > -1;
                                });

                            } else {
                                mydata = data;
                            }

                            mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
                            $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                        }
                    });
            });
        }

        getList();

        $scope.pilih = function () {
            console.log()
            if ($scope.event.name.length != 0) {
                $http.get("ChoirDetail/getOneChoir/" + $scope.event.name).success(function (data, status, headers, config) {
                    console.log(data);
                    // Nyimpen ke localstorage 
                    // retrieve it (Or create a blank array if there isn't any info saved yet),
                    localStorageService.set('dataChoir' , JSON.stringify(data.trs_choir) );
                    localStorageService.set('dataKonduktor' , '[]' );
                    localStorageService.set('dataManager' , '[]' );
                    localStorageService.set('dataMember' , '[]' );
                    //Conductor
                    for(var i = 0; i < data.conductor.length  ;i ++){
                        var datana = JSON.parse(localStorageService.get('dataKonduktor')) || [];
                        console.log(datana)
                        // add to it,
                        datana.push({ id: data.conductor[i].id_conductor, name: data.conductor[i].name, gender: data.conductor[i].gender });
                        // then put it back.
                        localStorageService.set('dataKonduktor', JSON.stringify(datana));
                        // append data tabel
                    }

                    //Manager
                    for(var i = 0 ; i < data.manager.length  ;i ++){
                        var datana = JSON.parse(localStorageService.get('dataManager')) || [];
                        console.log(datana)
                        // add to it,
                        datana.push({ id: data.manager[i].id_manager, name: data.manager[i].name, gender: data.manager[i].gender });
                        // then put it back.
                        localStorageService.set('dataManager', JSON.stringify(datana));
                        // append data tabel
                    }

                    //Manager
                    for(var i  = 0; i < data.member.length ;i ++){
                        var datana = JSON.parse(localStorageService.get('dataMember')) || [];
                        console.log(datana)
                        // add to it,
                        datana.push({ id: data.member[i].id_member, name: data.member[i].name, type: data.member[i].type });
                        // then put it back.
                        localStorageService.set('dataMember', JSON.stringify(datana));
                        // append data tabel
                    }
                    $rootScope.$emit('updateSelectData', []);
                }).error(function (err) {
                    console.log(err);
                });
            }
            
        }

    }
})()