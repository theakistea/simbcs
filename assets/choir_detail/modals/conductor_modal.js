(function () {
    'use strict';
    angular.module('material-lite').controller('PilihKonChoir', ['$scope', '$rootScope', '$http', '$timeout', 'localStorageService', 'ngTableParams', '$filter', '$location', '$routeParams', PilihKonChoir])
    // Pilih konduktor choir
    function PilihKonChoir($scope, $rootScope, $http, $timeout, localStorageService, ngTableParams, $filter, $location, $routeParams) {
        // Inisialisasi
        var dataKonduktor = JSON.parse(localStorageService.get('dataKonduktor')) || [];
        var no_kon = dataKonduktor.length + 1;
        var data = [];
        $scope.conductorData = {photo : 'default.jpg'}
        $scope.show="list";
        $scope.konData = {
            id_conductor: []
        };
        $scope.countrys  = []
        $scope.states  = []

        
        $scope.changeForm = function  (value) {
            $scope.show = value;
        }

        var getCountry = function () {
             $http.get("master/getCountry").success(function (result) {
                $scope.countrys  = result
            })
        }
        var getState = function () {
             $http.get("master/getState").success(function (result) {
                $scope.states  = result
            })
        }

        var dataKonduktor = JSON.parse(localStorageService.get('dataKonduktor')) || [];
        var id_conductor = []
        for (var i = 0; i < dataKonduktor.length; i++) {
            id_conductor.push(dataKonduktor[i].id_conductor)

        }
        $scope.konData.id_conductor = id_conductor
        
        var getList = function ()  {
            $http.get("choirDetail/getAllKonChoir").success(function (result) {
                

                $scope.loading = false;
                data = result;
                $scope.data = data;

                /* jshint newcap: false */
                $scope.tableParams = new ngTableParams({
                    page: 1,            // show first page
                    count: 10,
                    sorting: {
                        firstname: 'asc'     // initial sorting
                    }
                }, {
                        filterDelay: 50,
                        total: data.length, // length of data
                        getData: function ($defer, params) {
                            var searchStr = params.filter().search;
                            var mydata = [];

                            if (searchStr) {
                                searchStr = searchStr.toLowerCase();
                                mydata = data.filter(function (item) {
                                    return item.id_conductor.toLowerCase().indexOf(searchStr) > -1 || item.name.toLowerCase().indexOf(searchStr) > -1;
                                });

                            } else {
                                mydata = data;
                            }

                            mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
                            $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                        }
                    });
            });
        }

        getList();
        getCountry ();
        getState();

        $scope.pilih = function () {
            console.log($scope.konData.id_conductor.length);
            // Remove dulu
            localStorageService.remove('dataKonduktor');
            $timeout(function () {
                $scope.tampilTabCon = false;
            });
            no_kon = 1;
            if ($scope.konData.id_conductor.length != 0) {
                for (var i = 0; i < $scope.konData.id_conductor.length; i++) {
                    $http.get("ChoirDetail/getOneKonduktor/" + $scope.konData.id_conductor[i]).success(function (data, status, headers, config) {
                        console.log(data);
                        // Nyimpen ke localstorage 
                        // retrieve it (Or create a blank array if there isn't any info saved yet),
                        var datana = JSON.parse(localStorageService.get('dataKonduktor')) || [];
                        // add to it,
                        datana.push({ id: data[0].id_conductor, name: data[0].name, gender: data[0].gender });
                        // then put it back.
                        localStorageService.set('dataKonduktor', JSON.stringify(datana));
                        // append data tabel
                        no_kon++;
                        $rootScope.$emit('updateSelectData', []);
                    }).error(function (err) {
                        console.log(err);
                    });
                }
            }
            
        }


        $scope.saveData = function () {
            $scope.conductorData.photo = 'default.jpg'
             $http.post("Conductor/insert_data",
                $scope.conductorData
             ).success(function(data,status,headers,config){
                getList();
                var datana = JSON.parse(localStorageService.get('dataKonduktor')) || [];
                // add to it,
                datana.push({ id: data, name: $scope.conductorData.name, gender: $scope.conductorData.gender });
                // then put it back.
                localStorageService.set('dataKonduktor', JSON.stringify(datana));
                // append data tabel
                no_kon++;

                $scope.konData.id_conductor.push(data)
                $rootScope.$emit('updateSelectData', []);
                $scope.show = 'list'

                $scope.conductorData = {}
                //Beritahu jika data sudah berhasil di input
                alert("Data Berhasil Di Input");
            }).error(function(err){
            	console.log(err);
            });
        }
    }
})()