-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2016 at 04:41 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_simbcs`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'AS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua And Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas The'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CG', 'Congo'),
(50, 'CD', 'Congo The Democratic Republic Of The'),
(51, 'CK', 'Cook Islands'),
(52, 'CR', 'Costa Rica'),
(53, 'CI', 'Cote D''Ivoire (Ivory Coast)'),
(54, 'HR', 'Croatia (Hrvatska)'),
(55, 'CU', 'Cuba'),
(56, 'CY', 'Cyprus'),
(57, 'CZ', 'Czech Republic'),
(58, 'DK', 'Denmark'),
(59, 'DJ', 'Djibouti'),
(60, 'DM', 'Dominica'),
(61, 'DO', 'Dominican Republic'),
(62, 'TP', 'East Timor'),
(63, 'EC', 'Ecuador'),
(64, 'EG', 'Egypt'),
(65, 'SV', 'El Salvador'),
(66, 'GQ', 'Equatorial Guinea'),
(67, 'ER', 'Eritrea'),
(68, 'EE', 'Estonia'),
(69, 'ET', 'Ethiopia'),
(70, 'XA', 'External Territories of Australia'),
(71, 'FK', 'Falkland Islands'),
(72, 'FO', 'Faroe Islands'),
(73, 'FJ', 'Fiji Islands'),
(74, 'FI', 'Finland'),
(75, 'FR', 'France'),
(76, 'GF', 'French Guiana'),
(77, 'PF', 'French Polynesia'),
(78, 'TF', 'French Southern Territories'),
(79, 'GA', 'Gabon'),
(80, 'GM', 'Gambia The'),
(81, 'GE', 'Georgia'),
(82, 'DE', 'Germany'),
(83, 'GH', 'Ghana'),
(84, 'GI', 'Gibraltar'),
(85, 'GR', 'Greece'),
(86, 'GL', 'Greenland'),
(87, 'GD', 'Grenada'),
(88, 'GP', 'Guadeloupe'),
(89, 'GU', 'Guam'),
(90, 'GT', 'Guatemala'),
(91, 'XU', 'Guernsey and Alderney'),
(92, 'GN', 'Guinea'),
(93, 'GW', 'Guinea-Bissau'),
(94, 'GY', 'Guyana'),
(95, 'HT', 'Haiti'),
(96, 'HM', 'Heard and McDonald Islands'),
(97, 'HN', 'Honduras'),
(98, 'HK', 'Hong Kong S.A.R.'),
(99, 'HU', 'Hungary'),
(100, 'IS', 'Iceland'),
(101, 'IN', 'India'),
(102, 'ID', 'Indonesia'),
(103, 'IR', 'Iran'),
(104, 'IQ', 'Iraq'),
(105, 'IE', 'Ireland'),
(106, 'IL', 'Israel'),
(107, 'IT', 'Italy'),
(108, 'JM', 'Jamaica'),
(109, 'JP', 'Japan'),
(110, 'XJ', 'Jersey'),
(111, 'JO', 'Jordan'),
(112, 'KZ', 'Kazakhstan'),
(113, 'KE', 'Kenya'),
(114, 'KI', 'Kiribati'),
(115, 'KP', 'Korea North'),
(116, 'KR', 'Korea South'),
(117, 'KW', 'Kuwait'),
(118, 'KG', 'Kyrgyzstan'),
(119, 'LA', 'Laos'),
(120, 'LV', 'Latvia'),
(121, 'LB', 'Lebanon'),
(122, 'LS', 'Lesotho'),
(123, 'LR', 'Liberia'),
(124, 'LY', 'Libya'),
(125, 'LI', 'Liechtenstein'),
(126, 'LT', 'Lithuania'),
(127, 'LU', 'Luxembourg'),
(128, 'MO', 'Macau S.A.R.'),
(129, 'MK', 'Macedonia'),
(130, 'MG', 'Madagascar'),
(131, 'MW', 'Malawi'),
(132, 'MY', 'Malaysia'),
(133, 'MV', 'Maldives'),
(134, 'ML', 'Mali'),
(135, 'MT', 'Malta'),
(136, 'XM', 'Man (Isle of)'),
(137, 'MH', 'Marshall Islands'),
(138, 'MQ', 'Martinique'),
(139, 'MR', 'Mauritania'),
(140, 'MU', 'Mauritius'),
(141, 'YT', 'Mayotte'),
(142, 'MX', 'Mexico'),
(143, 'FM', 'Micronesia'),
(144, 'MD', 'Moldova'),
(145, 'MC', 'Monaco'),
(146, 'MN', 'Mongolia'),
(147, 'MS', 'Montserrat'),
(148, 'MA', 'Morocco'),
(149, 'MZ', 'Mozambique'),
(150, 'MM', 'Myanmar'),
(151, 'NA', 'Namibia'),
(152, 'NR', 'Nauru'),
(153, 'NP', 'Nepal'),
(154, 'AN', 'Netherlands Antilles'),
(155, 'NL', 'Netherlands The'),
(156, 'NC', 'New Caledonia'),
(157, 'NZ', 'New Zealand'),
(158, 'NI', 'Nicaragua'),
(159, 'NE', 'Niger'),
(160, 'NG', 'Nigeria'),
(161, 'NU', 'Niue'),
(162, 'NF', 'Norfolk Island'),
(163, 'MP', 'Northern Mariana Islands'),
(164, 'NO', 'Norway'),
(165, 'OM', 'Oman'),
(166, 'PK', 'Pakistan'),
(167, 'PW', 'Palau'),
(168, 'PS', 'Palestinian Territory Occupied'),
(169, 'PA', 'Panama'),
(170, 'PG', 'Papua new Guinea'),
(171, 'PY', 'Paraguay'),
(172, 'PE', 'Peru'),
(173, 'PH', 'Philippines'),
(174, 'PN', 'Pitcairn Island'),
(175, 'PL', 'Poland'),
(176, 'PT', 'Portugal'),
(177, 'PR', 'Puerto Rico'),
(178, 'QA', 'Qatar'),
(179, 'RE', 'Reunion'),
(180, 'RO', 'Romania'),
(181, 'RU', 'Russia'),
(182, 'RW', 'Rwanda'),
(183, 'SH', 'Saint Helena'),
(184, 'KN', 'Saint Kitts And Nevis'),
(185, 'LC', 'Saint Lucia'),
(186, 'PM', 'Saint Pierre and Miquelon'),
(187, 'VC', 'Saint Vincent And The Grenadines'),
(188, 'WS', 'Samoa'),
(189, 'SM', 'San Marino'),
(190, 'ST', 'Sao Tome and Principe'),
(191, 'SA', 'Saudi Arabia'),
(192, 'SN', 'Senegal'),
(193, 'RS', 'Serbia'),
(194, 'SC', 'Seychelles'),
(195, 'SL', 'Sierra Leone'),
(196, 'SG', 'Singapore'),
(197, 'SK', 'Slovakia'),
(198, 'SI', 'Slovenia'),
(199, 'XG', 'Smaller Territories of the UK'),
(200, 'SB', 'Solomon Islands'),
(201, 'SO', 'Somalia'),
(202, 'ZA', 'South Africa'),
(203, 'GS', 'South Georgia'),
(204, 'SS', 'South Sudan'),
(205, 'ES', 'Spain'),
(206, 'LK', 'Sri Lanka'),
(207, 'SD', 'Sudan'),
(208, 'SR', 'Suriname'),
(209, 'SJ', 'Svalbard And Jan Mayen Islands'),
(210, 'SZ', 'Swaziland'),
(211, 'SE', 'Sweden'),
(212, 'CH', 'Switzerland'),
(213, 'SY', 'Syria'),
(214, 'TW', 'Taiwan'),
(215, 'TJ', 'Tajikistan'),
(216, 'TZ', 'Tanzania'),
(217, 'TH', 'Thailand'),
(218, 'TG', 'Togo'),
(219, 'TK', 'Tokelau'),
(220, 'TO', 'Tonga'),
(221, 'TT', 'Trinidad And Tobago'),
(222, 'TN', 'Tunisia'),
(223, 'TR', 'Turkey'),
(224, 'TM', 'Turkmenistan'),
(225, 'TC', 'Turks And Caicos Islands'),
(226, 'TV', 'Tuvalu'),
(227, 'UG', 'Uganda'),
(228, 'UA', 'Ukraine'),
(229, 'AE', 'United Arab Emirates'),
(230, 'GB', 'United Kingdom'),
(231, 'US', 'United States'),
(232, 'UM', 'United States Minor Outlying Islands'),
(233, 'UY', 'Uruguay'),
(234, 'UZ', 'Uzbekistan'),
(235, 'VU', 'Vanuatu'),
(236, 'VA', 'Vatican City State (Holy See)'),
(237, 'VE', 'Venezuela'),
(238, 'VN', 'Vietnam'),
(239, 'VG', 'Virgin Islands (British)'),
(240, 'VI', 'Virgin Islands (US)'),
(241, 'WF', 'Wallis And Futuna Islands'),
(242, 'EH', 'Western Sahara'),
(243, 'YE', 'Yemen'),
(244, 'YU', 'Yugoslavia'),
(245, 'ZM', 'Zambia'),
(246, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `mst_candj`
--

CREATE TABLE `mst_candj` (
  `id_candj` int(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `appellation` int(1) DEFAULT NULL,
  `first_title` varchar(20) DEFAULT NULL,
  `last_title` varchar(20) DEFAULT NULL,
  `street_address` varchar(300) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `address_status` int(1) DEFAULT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `social_media` varchar(200) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `place_of_birth` varchar(30) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `notes` varchar(300) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_added` varchar(15) DEFAULT NULL,
  `date_modify` datetime DEFAULT NULL,
  `user_modify` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_candj`
--

INSERT INTO `mst_candj` (`id_candj`, `name`, `gender`, `appellation`, `first_title`, `last_title`, `street_address`, `city`, `state`, `country`, `address_status`, `phone`, `email`, `social_media`, `website`, `place_of_birth`, `date_of_birth`, `photo`, `notes`, `status`, `date_added`, `user_added`, `date_modify`, `user_modify`) VALUES
(1, 'coba member2', 'Female', 0, 'dddd', 'coba', 'cik', 'mana saja', 'Bengkulu', 'a', 1, '083434341', 'coba@dua.com', '@ilhamnfaqih', 'gg', 'gg', '0000-00-00', '', 'coba', NULL, '2016-01-18 16:35:42', 'administrator', NULL, 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `mst_choir_category`
--

CREATE TABLE `mst_choir_category` (
  `id_choir_category` int(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(15) NOT NULL,
  `age` varchar(15) NOT NULL,
  `min_singer` int(5) NOT NULL,
  `max_perform_time` int(5) NOT NULL,
  `num_of_song` int(5) NOT NULL,
  `num_of_acapela_song` int(5) NOT NULL,
  `with_amplification` varchar(5) NOT NULL,
  `notes` varchar(300) NOT NULL,
  `status` int(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_added` varchar(15) NOT NULL,
  `date_modify` datetime NOT NULL,
  `user_modify` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_choir_category`
--

INSERT INTO `mst_choir_category` (`id_choir_category`, `name`, `type`, `age`, `min_singer`, `max_perform_time`, `num_of_song`, `num_of_acapela_song`, `with_amplification`, `notes`, `status`, `date_added`, `user_added`, `date_modify`, `user_modify`) VALUES
(1, 'coba', 'coba', '12', 21, 2, 2, 2, 'YES', 'NOTHING', 1, '2016-02-08 15:57:39', 'administrator', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `mst_committee`
--

CREATE TABLE `mst_committee` (
  `id_committee` int(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `appellation` int(1) NOT NULL,
  `first_title` varchar(20) DEFAULT NULL,
  `last_title` varchar(20) DEFAULT NULL,
  `street_address` varchar(300) NOT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `address_status` int(1) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `social_media` varchar(200) NOT NULL,
  `website` varchar(50) DEFAULT NULL,
  `place_of_birth` varchar(30) NOT NULL,
  `date_of_birth` date NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `notes` varchar(300) NOT NULL,
  `status` int(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_added` varchar(15) NOT NULL,
  `date_modify` datetime NOT NULL,
  `user_modify` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_committee`
--

INSERT INTO `mst_committee` (`id_committee`, `name`, `gender`, `appellation`, `first_title`, `last_title`, `street_address`, `city`, `state`, `country`, `address_status`, `phone`, `email`, `social_media`, `website`, `place_of_birth`, `date_of_birth`, `photo`, `notes`, `status`, `date_added`, `user_added`, `date_modify`, `user_modify`) VALUES
(2, 'cobacom', 'Male', 0, 'coba', 'coba', 'coba', 'Bergamo', 'Andaman and Nicobar Islands', 'a', 1, '9887788', 'coba@coba.com', 'coba', 'coba', 'coba', '0000-00-00', '020216034338', 'coba', 1, '2016-02-02 02:43:38', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `mst_company_data`
--

CREATE TABLE `mst_company_data` (
  `id_company_data` int(11) NOT NULL,
  `phone_number` varchar(30) NOT NULL,
  `fax_number` varchar(30) NOT NULL,
  `mailing_address` varchar(100) NOT NULL,
  `street_address` varchar(300) NOT NULL,
  `email_company` varchar(50) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `phone_cp` varchar(30) NOT NULL,
  `email_cp` varchar(50) NOT NULL,
  `notes` varchar(300) NOT NULL,
  `date_modify` datetime NOT NULL,
  `user_modify` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_company_data`
--

INSERT INTO `mst_company_data` (`id_company_data`, `phone_number`, `fax_number`, `mailing_address`, `street_address`, `email_company`, `contact_person`, `phone_cp`, `email_cp`, `notes`, `date_modify`, `user_modify`) VALUES
(1, '(+6222) 5209724', '(+6222) 5209724', 'Bandung Choral Society', 'Jl.H.Aksan No.37, Mohammad Toha \r\n	Bandung- West Java\r\n', 'bcs@gmail.com', 'Dian', '+6281234567', 'dian@gmail.com', 'Everybody can sing', '2016-02-08 00:00:00', 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `mst_composer_and_arranger`
--

CREATE TABLE `mst_composer_and_arranger` (
  `id_composer_and_arranger` int(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `appellation` int(1) NOT NULL,
  `first_title` varchar(20) DEFAULT NULL,
  `last_title` varchar(20) DEFAULT NULL,
  `street_address` varchar(300) NOT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `address_status` int(1) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `social_media` varchar(200) NOT NULL,
  `website` varchar(50) DEFAULT NULL,
  `place_of_birth` varchar(30) NOT NULL,
  `date_of_birth` date NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `notes` varchar(300) NOT NULL,
  `status` int(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_added` varchar(15) NOT NULL,
  `date_modify` datetime DEFAULT NULL,
  `user_modify` varchar(15) DEFAULT NULL,
  `id_dtl_composer_and_arranger` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_composer_and_arranger`
--

INSERT INTO `mst_composer_and_arranger` (`id_composer_and_arranger`, `name`, `gender`, `appellation`, `first_title`, `last_title`, `street_address`, `city`, `state`, `country`, `address_status`, `phone`, `email`, `social_media`, `website`, `place_of_birth`, `date_of_birth`, `photo`, `notes`, `status`, `date_added`, `user_added`, `date_modify`, `user_modify`, `id_dtl_composer_and_arranger`) VALUES
(1, 'coba', 'Male', 1, NULL, NULL, 'Kamarung', 'Cimahi', 'Jawa Barat', 'Indonesia', 1, '08737373', 'coba@coba.com', 'oba', 'coba.com', 'Bandung', '2016-02-02', NULL, 'coba', 1, '2016-02-22 22:55:33', 'administrator', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_conductor`
--

CREATE TABLE `mst_conductor` (
  `id_conductor` int(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `appellation` int(1) NOT NULL,
  `first_title` varchar(20) DEFAULT NULL,
  `last_title` varchar(20) DEFAULT NULL,
  `street_address` varchar(300) NOT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `address_status` int(1) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `social_media` varchar(200) NOT NULL,
  `website` varchar(50) DEFAULT NULL,
  `place_of_birth` varchar(30) NOT NULL,
  `date_of_birth` date NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `notes` varchar(300) NOT NULL,
  `status` int(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_added` varchar(15) NOT NULL,
  `date_modify` datetime NOT NULL,
  `user_modify` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_conductor`
--

INSERT INTO `mst_conductor` (`id_conductor`, `name`, `gender`, `appellation`, `first_title`, `last_title`, `street_address`, `city`, `state`, `country`, `address_status`, `phone`, `email`, `social_media`, `website`, `place_of_birth`, `date_of_birth`, `photo`, `notes`, `status`, `date_added`, `user_added`, `date_modify`, `user_modify`) VALUES
(1, 'coba conductor', 'Male', 0, 'Dra', 'S,pd', 'Jalan jalan', 'Mana', 'Sumatra Barat', 'a', 1, '085453923843', 'siapa@gmail.com', 'coba', 'coba.com', 'Bandung', '0000-00-00', '160216120536', 'coba', 1, '2016-02-15 23:05:36', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `mst_dtl_composer_and_arranger`
--

CREATE TABLE `mst_dtl_composer_and_arranger` (
  `id_dtl_composer_and_arranger` int(15) NOT NULL,
  `id_composer_and_arranger` int(15) NOT NULL,
  `id_songs` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mst_event_grade`
--

CREATE TABLE `mst_event_grade` (
  `id_event_grade` int(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `notes` varchar(300) NOT NULL,
  `status` int(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_added` varchar(15) NOT NULL,
  `date_modify` datetime NOT NULL,
  `user_modify` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_event_grade`
--

INSERT INTO `mst_event_grade` (`id_event_grade`, `name`, `notes`, `status`, `date_added`, `user_added`, `date_modify`, `user_modify`) VALUES
(1, 'Regionals', 'Event Daerah', 1, '2016-02-09 00:18:11', 'administrator', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `mst_manager`
--

CREATE TABLE `mst_manager` (
  `id_manager` int(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `appellation` int(1) NOT NULL,
  `first_title` varchar(20) DEFAULT NULL,
  `last_title` varchar(20) DEFAULT NULL,
  `street_address` varchar(300) NOT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `address_status` int(1) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `social_media` varchar(200) NOT NULL,
  `website` varchar(50) DEFAULT NULL,
  `place_of_birth` varchar(30) NOT NULL,
  `date_of_birth` date NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `notes` varchar(300) NOT NULL,
  `status` int(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_added` varchar(15) NOT NULL,
  `date_modify` datetime NOT NULL,
  `user_modify` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_manager`
--

INSERT INTO `mst_manager` (`id_manager`, `name`, `gender`, `appellation`, `first_title`, `last_title`, `street_address`, `city`, `state`, `country`, `address_status`, `phone`, `email`, `social_media`, `website`, `place_of_birth`, `date_of_birth`, `photo`, `notes`, `status`, `date_added`, `user_added`, `date_modify`, `user_modify`) VALUES
(1, 'coba manager', 'Female', 0, 'Dr.', 'ST', 'coba aja', 'Mana', 'Riau', 'a', 1, '0889999', 'gg@gmail.com', '@ilhamnfaqih', 'gg', 'coba', '0000-00-00', '220216025943', 'coba aja', 1, '2016-02-22 01:59:43', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `mst_member`
--

CREATE TABLE `mst_member` (
  `id_member` int(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `institution` varchar(100) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `appellation` int(1) DEFAULT NULL,
  `first_title` varchar(20) DEFAULT NULL,
  `last_title` varchar(20) DEFAULT NULL,
  `street_address` varchar(300) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `address_status` int(1) DEFAULT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `social_media` varchar(200) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `place_of_birth` varchar(30) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `notes` varchar(300) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_added` varchar(15) DEFAULT NULL,
  `date_modify` datetime DEFAULT NULL,
  `user_modify` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_member`
--

INSERT INTO `mst_member` (`id_member`, `name`, `institution`, `gender`, `appellation`, `first_title`, `last_title`, `street_address`, `city`, `state`, `country`, `address_status`, `phone`, `email`, `social_media`, `website`, `place_of_birth`, `date_of_birth`, `photo`, `notes`, `status`, `date_added`, `user_added`, `date_modify`, `user_modify`) VALUES
(1, 'coba member', 'SMK 2', 'Female', 0, 'dddd', 'coba', 'cik', 'mana saja', 'Bengkulu', 'a', 1, '083434341', 'coba@dua.com', '@ilhamnfaqih', 'gg', 'gg', '0000-00-00', '220216030408', 'coba', 1, '2016-02-22 02:04:08', '', '0000-00-00 00:00:00', ''),
(3, 'lagi cek inst', 'cek', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '010316030134', NULL, 1, '2016-03-01 02:01:34', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_musician`
--

CREATE TABLE `mst_musician` (
  `id_musician` int(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `appellation` int(1) NOT NULL,
  `first_title` varchar(20) DEFAULT NULL,
  `last_title` varchar(20) DEFAULT NULL,
  `street_address` varchar(300) NOT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `address_status` int(1) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `social_media` varchar(200) NOT NULL,
  `website` varchar(50) DEFAULT NULL,
  `place_of_birth` varchar(30) NOT NULL,
  `date_of_birth` date NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `notes` varchar(300) NOT NULL,
  `status` int(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_added` varchar(15) NOT NULL,
  `date_modify` datetime NOT NULL,
  `user_modify` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_musician`
--

INSERT INTO `mst_musician` (`id_musician`, `name`, `gender`, `appellation`, `first_title`, `last_title`, `street_address`, `city`, `state`, `country`, `address_status`, `phone`, `email`, `social_media`, `website`, `place_of_birth`, `date_of_birth`, `photo`, `notes`, `status`, `date_added`, `user_added`, `date_modify`, `user_modify`) VALUES
(1, 'coba musician', 'Male', 0, 'Dr.', 'gg', 'gg', 'coba', 'Sumatra Barat', 'a', 1, '087444', 'coba@coba.com', 'apa aja', 'apa', 'Bandung', '0000-00-00', '220216030639', 'coba', 1, '2016-02-22 02:06:39', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `mst_officials`
--

CREATE TABLE `mst_officials` (
  `id_officials` int(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `appellation` int(1) NOT NULL,
  `first_title` varchar(20) DEFAULT NULL,
  `last_title` varchar(20) DEFAULT NULL,
  `street_address` varchar(300) NOT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `address_status` int(1) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `social_media` varchar(200) NOT NULL,
  `website` varchar(50) DEFAULT NULL,
  `place_of_birth` varchar(30) NOT NULL,
  `date_of_birth` date NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `notes` varchar(300) NOT NULL,
  `status` int(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_added` varchar(15) NOT NULL,
  `date_modify` datetime NOT NULL,
  `user_modify` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_officials`
--

INSERT INTO `mst_officials` (`id_officials`, `name`, `gender`, `appellation`, `first_title`, `last_title`, `street_address`, `city`, `state`, `country`, `address_status`, `phone`, `email`, `social_media`, `website`, `place_of_birth`, `date_of_birth`, `photo`, `notes`, `status`, `date_added`, `user_added`, `date_modify`, `user_modify`) VALUES
(1, 'coba off', 'Male', 0, 'Dra', 'Dra', 'coba', 'cc', 'Riau', 'a', 1, '08996840990', 'gg@gmail.com', '@cobadua, cobadua, cobadua.com', 'www.bara.co.id', 'coba', '0000-00-00', '220216030803', 'coab', 1, '2016-02-22 02:08:03', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `mst_songs`
--

CREATE TABLE `mst_songs` (
  `id_songs` int(15) NOT NULL,
  `title` varchar(300) NOT NULL,
  `composer` varchar(100) DEFAULT NULL,
  `type` varchar(15) DEFAULT NULL,
  `publisher` varchar(100) DEFAULT NULL,
  `accompanied_status` varchar(5) DEFAULT NULL,
  `accompanied_value` varchar(100) DEFAULT NULL,
  `file_not_balok` varchar(200) DEFAULT NULL,
  `file_not_angka` varchar(200) DEFAULT NULL,
  `id_songs_category` int(15) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_added` varchar(15) DEFAULT NULL,
  `date_modify` datetime DEFAULT NULL,
  `user_modify` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_songs`
--

INSERT INTO `mst_songs` (`id_songs`, `title`, `composer`, `type`, `publisher`, `accompanied_status`, `accompanied_value`, `file_not_balok`, `file_not_angka`, `id_songs_category`, `status`, `date_added`, `user_added`, `date_modify`, `user_modify`) VALUES
(1, 'PadaMu Negeri', 'Kusbini', 'POP', 'Balai Pustaka', 'NO', 'none', 'none', 'none', 1, 1, '2016-02-09 01:12:41', 'administrator', '2016-02-09 00:00:00', 'administrator'),
(2, 'coba', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2016-02-23 05:36:26', NULL, NULL, NULL),
(3, 'coba', 'coba', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-02-23 05:37:52', NULL, NULL, NULL),
(4, 'nah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2016-02-23 06:06:03', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_songs_category`
--

CREATE TABLE `mst_songs_category` (
  `id_songs_category` int(15) NOT NULL,
  `title_category` varchar(100) NOT NULL,
  `notes` varchar(300) NOT NULL,
  `status` int(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_added` varchar(15) NOT NULL,
  `date_modify` datetime NOT NULL,
  `user_modify` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_songs_category`
--

INSERT INTO `mst_songs_category` (`id_songs_category`, `title_category`, `notes`, `status`, `date_added`, `user_added`, `date_modify`, `user_modify`) VALUES
(1, 'Gospel', 'Lagu Rohani', 1, '2016-02-09 00:44:01', 'administrator', '2016-02-09 00:00:00', 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `country_id`) VALUES
(1, 'Sumatra Utara', 1),
(2, 'Sumatra Barat', 1),
(3, 'Riau', 1),
(4, 'Jambi', 1),
(5, 'Sumatra Selatan', 1),
(6, 'Bengkulu', 1),
(7, 'Lampung', 1),
(8, 'Kep. Bangka Belitung', 1),
(9, 'Kep. Riau', 1),
(10, 'DKI Jakarta', 1),
(11, 'Jawa Barat', 1),
(12, 'Jawa Tengah', 1),
(13, 'DI Yogyakarta', 1),
(14, 'Jawa Timur', 1),
(15, 'Banten', 1),
(16, 'Bali', 1),
(17, 'Nusa Tenggara Barat', 1),
(18, 'Nusa Tenggara Timur', 1),
(19, 'Kalimantan Barat', 1),
(20, 'Kalimantan Tengah', 1),
(21, 'Kalimantan Selatan', 1),
(22, 'Kalimantan Timur', 1),
(23, 'Sulawesi Utara', 1),
(24, 'Sulawesi Tengah', 1),
(25, 'Sulawesi Selatan', 1),
(26, 'Sulawesi Tenggara', 1),
(27, 'Gorontalo', 1),
(28, 'Maluku', 1),
(29, 'Maluku Utara', 1),
(30, 'Papua', 1),
(31, 'Irian Jaya Barat', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trs_choir`
--

CREATE TABLE `trs_choir` (
  `id_trs_choir` int(15) NOT NULL,
  `choir_name` varchar(100) NOT NULL,
  `institution` varchar(100) DEFAULT NULL,
  `street_address` varchar(300) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `id_trs_dtl_choir` int(15) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `notes` varchar(300) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `user_added` varchar(15) DEFAULT NULL,
  `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_modify` varchar(15) DEFAULT NULL,
  `date_modify` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trs_choir`
--

INSERT INTO `trs_choir` (`id_trs_choir`, `choir_name`, `institution`, `street_address`, `country`, `state`, `city`, `id_trs_dtl_choir`, `email`, `notes`, `photo`, `status`, `user_added`, `date_added`, `user_modify`, `date_modify`) VALUES
(1, 'coba', 'coba', 'coba', 'Indonesia', 'West Java', 'Cimahi', 1, 'coba@coba.com', 'coba', 'coba', 1, 'administrator', '2016-02-23 02:42:26', 'administrator', '2016-02-03 00:00:00');



-- CREATE TABLE "trs_choir_event" --------------------------
CREATE TABLE `trs_choir_event` ( 
	`id_trs_choir_event` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`grade` VarChar( 30 ) NULL,
	`title` VarChar( 100 ) NULL,
	`date_start` VarChar( 100 ) NULL,
	`date_finish` VarChar( 100 ) NULL,
	`country` VarChar( 30 ) NULL,
	`state` VarChar( 30 ) NULL,
	`city` VarChar( 30 ) NULL,
	`host` VarChar( 100 ) NULL,
	`PIC` VarChar( 100 ) NULL,
	`notes` VarChar( 300 ) NULL,
	`photo` VarChar( 300 ) NULL,
	`status` Int( 1 ) NULL,
	`user_added` VarChar( 30 ) NULL,
	`date_added` Timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	`user_modify` VarChar( 30 ) NULL,
	`date_modify` DateTime NULL,
	PRIMARY KEY ( `id_trs_choir_event` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;

-- --------------------------------------------------------

--
-- Table structure for table `trs_dtl_choir`
--

CREATE TABLE `trs_dtl_choir` (
  `id_trs_dtl_choir` int(15) NOT NULL,
  `id_trs_choir` int(15) DEFAULT NULL,
  `id_choir_category` int(15) DEFAULT NULL,
  `id_member` int(15) DEFAULT NULL,
  `id_manager` int(15) DEFAULT NULL,
  `id_conductor` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Dumping data for table `trs_dtl_choir`
--

INSERT INTO `trs_dtl_choir` (`id_trs_dtl_choir`, `id_trs_choir`, `id_choir_category`, `id_member`, `id_manager`, `id_conductor`) VALUES
(1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `idUser` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `firstName` varchar(20) NOT NULL,
  `lastName` varchar(20) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUserGroup` int(11) NOT NULL,
  `active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`idUser`, `username`, `firstName`, `lastName`, `email`, `password`, `created`, `idUserGroup`, `active`) VALUES
(1, 'admin', 'Ilham', 'Faqih', 'ilhamn.faqih@gmail.com', 'admin123', '2016-01-09 01:24:26', 1, 1),
(2, 'coba', 'coba', 'lagi', 'coba@coba.com', 'coba', '2016-01-10 06:48:05', 2, 1),
(3, 'kangbahar', 'bahar', 'tea', 'baharudin@bahar.com', 'bahar', '2016-01-10 07:08:33', 2, 1),
(4, 'a', 'a', 'a', 'a@a.com', 'a', '2016-01-10 07:09:07', 1, 1),
(5, 'a', 'a', 'a', 'a@a.com', 'a', '2016-01-10 07:24:37', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `idUserGroup` int(11) NOT NULL,
  `permissionName` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`idUserGroup`, `permissionName`) VALUES
(1, 'Administrator'),
(2, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `user_group_permission`
--

CREATE TABLE `user_group_permission` (
  `idGroupPermission` int(11) NOT NULL,
  `idUserGroup` int(11) NOT NULL,
  `idUserPermission` int(11) NOT NULL,
  `create` int(1) NOT NULL,
  `update` int(1) NOT NULL,
  `delete` int(1) NOT NULL,
  `show` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_permission`
--

CREATE TABLE `user_permission` (
  `idUserPermission` int(11) NOT NULL,
  `pageUserPermission` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_candj`
--
ALTER TABLE `mst_candj`
  ADD PRIMARY KEY (`id_candj`);

--
-- Indexes for table `mst_choir_category`
--
ALTER TABLE `mst_choir_category`
  ADD PRIMARY KEY (`id_choir_category`);

--
-- Indexes for table `mst_committee`
--
ALTER TABLE `mst_committee`
  ADD PRIMARY KEY (`id_committee`);

--
-- Indexes for table `mst_company_data`
--
ALTER TABLE `mst_company_data`
  ADD PRIMARY KEY (`id_company_data`);

--
-- Indexes for table `mst_composer_and_arranger`
--
ALTER TABLE `mst_composer_and_arranger`
  ADD PRIMARY KEY (`id_composer_and_arranger`);

--
-- Indexes for table `mst_conductor`
--
ALTER TABLE `mst_conductor`
  ADD PRIMARY KEY (`id_conductor`);

--
-- Indexes for table `mst_dtl_composer_and_arranger`
--
ALTER TABLE `mst_dtl_composer_and_arranger`
  ADD PRIMARY KEY (`id_dtl_composer_and_arranger`);

--
-- Indexes for table `mst_event_grade`
--
ALTER TABLE `mst_event_grade`
  ADD PRIMARY KEY (`id_event_grade`);

--
-- Indexes for table `mst_manager`
--
ALTER TABLE `mst_manager`
  ADD PRIMARY KEY (`id_manager`);

--
-- Indexes for table `mst_member`
--
ALTER TABLE `mst_member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `mst_musician`
--
ALTER TABLE `mst_musician`
  ADD PRIMARY KEY (`id_musician`);

--
-- Indexes for table `mst_officials`
--
ALTER TABLE `mst_officials`
  ADD PRIMARY KEY (`id_officials`);

--
-- Indexes for table `mst_songs`
--
ALTER TABLE `mst_songs`
  ADD PRIMARY KEY (`id_songs`);

--
-- Indexes for table `mst_songs_category`
--
ALTER TABLE `mst_songs_category`
  ADD PRIMARY KEY (`id_songs_category`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trs_choir`
--
ALTER TABLE `trs_choir`
  ADD PRIMARY KEY (`id_trs_choir`);

--
-- Indexes for table `trs_dtl_choir`
--
ALTER TABLE `trs_dtl_choir`
  ADD PRIMARY KEY (`id_trs_dtl_choir`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`idUserGroup`);

--
-- Indexes for table `user_group_permission`
--
ALTER TABLE `user_group_permission`
  ADD PRIMARY KEY (`idGroupPermission`);

--
-- Indexes for table `user_permission`
--
ALTER TABLE `user_permission`
  ADD PRIMARY KEY (`idUserPermission`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_candj`
--
ALTER TABLE `mst_candj`
  MODIFY `id_candj` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_choir_category`
--
ALTER TABLE `mst_choir_category`
  MODIFY `id_choir_category` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_committee`
--
ALTER TABLE `mst_committee`
  MODIFY `id_committee` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mst_company_data`
--
ALTER TABLE `mst_company_data`
  MODIFY `id_company_data` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_composer_and_arranger`
--
ALTER TABLE `mst_composer_and_arranger`
  MODIFY `id_composer_and_arranger` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_conductor`
--
ALTER TABLE `mst_conductor`
  MODIFY `id_conductor` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_dtl_composer_and_arranger`
--
ALTER TABLE `mst_dtl_composer_and_arranger`
  MODIFY `id_dtl_composer_and_arranger` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_event_grade`
--
ALTER TABLE `mst_event_grade`
  MODIFY `id_event_grade` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_manager`
--
ALTER TABLE `mst_manager`
  MODIFY `id_manager` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_member`
--
ALTER TABLE `mst_member`
  MODIFY `id_member` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mst_musician`
--
ALTER TABLE `mst_musician`
  MODIFY `id_musician` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_officials`
--
ALTER TABLE `mst_officials`
  MODIFY `id_officials` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_songs`
--
ALTER TABLE `mst_songs`
  MODIFY `id_songs` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mst_songs_category`
--
ALTER TABLE `mst_songs_category`
  MODIFY `id_songs_category` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `trs_choir`
--
ALTER TABLE `trs_choir`
  MODIFY `id_trs_choir` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `trs_dtl_choir`
--
ALTER TABLE `trs_dtl_choir`
  MODIFY `id_trs_dtl_choir` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `idUserGroup` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_group_permission`
--
ALTER TABLE `user_group_permission`
  MODIFY `idGroupPermission` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_permission`
--
ALTER TABLE `user_permission`
  MODIFY `idUserPermission` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
