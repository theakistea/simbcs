SET SQL_SAFE_UPDATES = 0;
alter table trs_dtl_choir_cat add column code_registrasi varchar(12);
alter table mst_choir_category add column type_prefix varchar(12);

UPDATE mst_choir_category set type_prefix= 'CO' where type in ('competition' , 'Competition');
UPDATE mst_choir_category set type_prefix= 'CH' where type in ('championship' , 'Championship');

alter table trs_choir_event add column code_event varchar(30);

alter table trs_choir add column has_surat_keterangan_usia int;
alter table trs_choir add column has_bukti_registrasi int;
alter table trs_choir add column has_audio_record int;
alter table trs_choir add column has_biograpy int;

update trs_choir set has_surat_keterangan_usia = 0 , has_bukti_registrasi = 0 , has_audio_record = 0 , has_biograpy=0 where 1=1;



alter table mst_choir_category add column code_category varchar(12);

SET SQL_SAFE_UPDATES = 1;